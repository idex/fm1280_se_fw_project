#include "hw.h"
#include "FM_libproto.h"

u16 hw_get_info (u8 *buf)
{
    u16 i = 0;
    
    buf[i++] = 0xF0;
    buf[i++] = 44;
	nvr_data_read (0x00DE0180, 44, &buf[i]);
	i += 44;

    buf[i++] = 0xF1;
    buf[i++] = 0x04;
    //uwrite4 (&buf[i], ());
    i += 4;

    buf[i++] = 0xF2;
    buf[i++] = 0x04;
	//uwrite4 (&buf[i], ());
	i += 4;

    buf[i++] = 0xF3;
    buf[i++] = 0x04;
	//uwrite4 (&buf[i], ());
	i += 4;
    
    buf[i++] = 0xF4;
    buf[i++] = 32;
	//();
	i += 32;

    return i;
}

u32 hw_iso14443_crc (u8 *buf, u32 len, u8 crc[2])
{
    u32 val = 0x00006363;
    u32 ret;

    ret = crc_cal(0, (u32)buf, len, &val);
    
    crc[0] = HIBYTE(val);
    crc[1] = LOBYTE(val);

    return (ret & FIRMWARE_ERR_MASK);
}


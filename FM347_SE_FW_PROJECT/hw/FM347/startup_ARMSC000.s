;/**************************************************************************//**
; * @file     startup_ARMSC000.s
; * @brief    CMSIS Core Device Startup File for
; *           ARMSC000 Device Series
; * @version  V1.08
; * @date     23. November 2012
; *
; * @note
; *
; ******************************************************************************/
;/* Copyright (c) 2011 - 2012 ARM LIMITED
;
;   All rights reserved.
;   Redistribution and use in source and binary forms, with or without
;   modification, are permitted provided that the following conditions are met:
;   - Redistributions of source code must retain the above copyright
;     notice, this list of conditions and the following disclaimer.
;   - Redistributions in binary form must reproduce the above copyright
;     notice, this list of conditions and the following disclaimer in the
;     documentation and/or other materials provided with the distribution.
;   - Neither the name of ARM nor the names of its contributors may be used
;     to endorse or promote products derived from this software without
;     specific prior written permission.
;   *
;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;   ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
;   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;   POSSIBILITY OF SUCH DAMAGE.
;   ---------------------------------------------------------------------------*/
;/*
;//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------
;*/


; <h> Stack Configuration
;   <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Stack_Size      EQU     0x00000C00

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp


; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Heap_Size       EQU     0x00000200

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit


                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset

                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       DCD     __initial_sp              ; Top of Stack
                DCD     Reset_Handler             ; Reset Handler
                DCD     NMI_Handler               ; NMI Handler
                DCD     HardFault_Handler         ; Hard Fault Handler
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     SVC_Handler               ; SVCall Handler
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     PendSV_Handler            ; PendSV Handler
                DCD     SysTick_Handler           ; SysTick Handler

                ; External Interrupts
                DCD     ct_clk_on_off_IRQHandler  ;  0: CT_CLK_ON_OFF
                DCD     ct_rst_n_IRQHandler       ;  1: CT_RST_N
                DCD     cl_on_off_IRQHandler      ;  2: CL_ON_OFF
                DCD     ct_vcc_on_off_IRQHandler     ;  3: VCC_ON_OFF
                DCD     ct_IRQHandler             ;  4: CT
                DCD     cla_IRQHandler            ;  5: CLA
                DCD     gpio_IRQHandler           ;  6: GPIO
                DCD     uart_IRQHandler           ;  7: UART
                DCD     spi_IRQHandler            ;  8: SPI
                DCD     pae_IRQHandler            ;  9: PAE
                DCD     hash_IRQHandler           ; 10: HASH
                DCD     bca_IRQHandler            ; 11: BCA
                DCD     timer_a_IRQHandler        ; 12: Timer_A
                DCD     timer_b_IRQHandler        ; 13: Timer_B
                DCD     random_IRQHandler         ; 14: Random
                DCD     nvm_ew_IRQHandler         ; 15: NVM_EW
                DCD     cl_lvolt_IRQHandler       ; 16: CL_LVOLT
                DCD     ct_lvolt_IRQHandler       ; 17: CT_LVOLT
                DCD     nvm_lvolt_IRQHandler      ; 18: NVM_LVOLT
                DCD     ee_check_fail_IRQHandler  ; 19: EE_Check_Fail
                DCD     i2c_slave_IRQHandler      ; 20: I2C_Slave
                DCD     i2c_master_IRQHandler     ; 21: I2C_Master
                DCD     crc_IRQHandler            ; 22: CRC
                DCD     Default_Handler           ; 23: Reserved
                DCD     Default_Handler           ; 24: Reserved
                DCD     Default_Handler           ; 25: Reserved
                DCD     Default_Handler           ; 26: Reserved
                DCD     Default_Handler           ; 27: Reserved
                DCD     Default_Handler           ; 28: Reserved
                DCD     Default_Handler           ; 29: Reserved
                DCD     Default_Handler           ; 30: Reserved
                DCD     Default_Handler           ; 31: Reserved
__Vectors_End

__Vectors_Size  EQU     __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY


; Reset Handler

Reset_Handler   PROC
                EXPORT  Reset_Handler              [WEAK]
                ;IMPORT  SystemInit 
                IMPORT  main
                ;LDR     R0, =SystemInit
                ;BLX     R0
                LDR     R0, =main
                BX      R0
                ENDP


; Dummy Exception Handlers (infinite loops which can be modified)

NMI_Handler     PROC
                EXPORT  NMI_Handler                [WEAK]
                B       .
                ENDP
HardFault_Handler\
                PROC
                EXPORT  HardFault_Handler          [WEAK]
                B       .
                ENDP
SVC_Handler     PROC
                EXPORT  SVC_Handler                [WEAK]
                B       .
                ENDP
PendSV_Handler  PROC
                EXPORT  PendSV_Handler             [WEAK]
                B       .
                ENDP
SysTick_Handler PROC
                EXPORT  SysTick_Handler            [WEAK]
                B       .
                ENDP

Default_Handler PROC

                EXPORT  ct_clk_on_off_IRQHandler   [WEAK]
                EXPORT  ct_rst_n_IRQHandler        [WEAK]
                EXPORT  cl_on_off_IRQHandler       [WEAK]
                EXPORT  ct_vcc_on_off_IRQHandler   [WEAK]
                EXPORT  ct_IRQHandler              [WEAK]
                EXPORT  cla_IRQHandler             [WEAK]
                EXPORT  gpio_IRQHandler            [WEAK]
                EXPORT  uart_IRQHandler            [WEAK]
                EXPORT  spi_IRQHandler             [WEAK]
                EXPORT  pae_IRQHandler             [WEAK]
                EXPORT  hash_IRQHandler            [WEAK]
                EXPORT  bca_IRQHandler             [WEAK]
                EXPORT  timer_a_IRQHandler         [WEAK]
                EXPORT  timer_b_IRQHandler         [WEAK]
                EXPORT  random_IRQHandler          [WEAK]
                EXPORT  nvm_ew_IRQHandler          [WEAK]
                EXPORT  cl_lvolt_IRQHandler        [WEAK]
                EXPORT  ct_lvolt_IRQHandler        [WEAK]
                EXPORT  nvm_lvolt_IRQHandler       [WEAK]
                EXPORT  ee_check_fail_IRQHandler   [WEAK]
                EXPORT  i2c_slave_IRQHandler       [WEAK]
                EXPORT  i2c_master_IRQHandler      [WEAK]
                EXPORT  crc_IRQHandler             [WEAK]
                EXPORT  rambist_IRQHandler         [WEAK]
                EXPORT  Default_Handler            [WEAK]                    

ct_clk_on_off_IRQHandler
ct_rst_n_IRQHandler
cl_on_off_IRQHandler
ct_vcc_on_off_IRQHandler
ct_IRQHandler
cla_IRQHandler
gpio_IRQHandler
uart_IRQHandler
spi_IRQHandler
pae_IRQHandler
hash_IRQHandler
bca_IRQHandler
timer_a_IRQHandler
timer_b_IRQHandler
random_IRQHandler
nvm_ew_IRQHandler
cl_lvolt_IRQHandler
ct_lvolt_IRQHandler
nvm_lvolt_IRQHandler
ee_check_fail_IRQHandler
i2c_slave_IRQHandler 
i2c_master_IRQHandler
crc_IRQHandler       
rambist_IRQHandler   
Default_IRQHandler   
                B       .

                ENDP


                ALIGN


; User Initial Stack & Heap

                IF      :DEF:__MICROLIB

                EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit

                ELSE

                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap

__user_initial_stackheap PROC
                LDR     R0, = Heap_Mem
                LDR     R1, = (Stack_Mem + Stack_Size)
                LDR     R2, = (Heap_Mem +  Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR
                ENDP

                ALIGN

                ENDIF


                END

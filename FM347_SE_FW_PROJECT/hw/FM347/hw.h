#ifndef _HW_H
#define _HW_H

#include "types.h"
#include "hw_config.h"
#include "FM_CryptoLib.h"
#include "FM_DriverLib.h"
#include "FM_DriverDef.h"

#include "hw_ct.h"
#include "hw_cl.h"
#include "hw_fm347.h"
//#include "hw_nvm.h"

#define RAM_BUF_ATTR  __attribute__((section("ram_buffer_section")))

#include "setjmp.h"
typedef jmp_buf       int_jmp_buf;
#define int_setjmp    setjmp


#define FIRMWARE_ERR_MASK   (0x0000FFFF)
#define FIRMWARE_ERR_NONE   (0xFFFF6996)
#define FIRMWARE_ERR_OK     (0x00000000) //=FIRMWARE_OK_FLAG
#define FIRMWARE_ERR_INVAL  (0x00000003) //address is invalid
#define FIRMWARE_ERR_FAIL   (0x00000081)
#define FIRMWARE_ERR_RETRY  (0x00FFFFFF)

#define FIRMWARE_ASSERT(a)  do { \
    if ((a) & FIRMWARE_ERR_MASK) \
        return MAKEWORD(0x65, LOBYTE((a) & FIRMWARE_ERR_MASK)); \
} while(0);


enum cos_flag_e
{
    COS_FLG_CL_FIELD_ON  = 0x01,
    COS_FLG_CT_VCC_ON    = 0x10,
    COS_FLG_CT_ACTIVATED = 0x08,
    COS_FLG_CL_ACTIVATED = 0x80,
    //COS_FLG_CL_RESET     = 0x40,
};

#define hw_cos_flag                (cos_flag)


u32 hw_iso14443_crc (u8 *buf, u32 len, u8 crc[2]);

u16 hw_get_info (u8 *buf);

#endif//_HW_H

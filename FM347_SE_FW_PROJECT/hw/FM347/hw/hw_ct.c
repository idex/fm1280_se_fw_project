#include "hw_ct.h"
#include "hw.h"
#include "main.h"


u8 RAM_BUF_ATTR g_ct_tx_buf[MAX_CT_RX_BUF_SIZE] __attribute__((aligned(4)));  /* Tx Buffer */
u8 RAM_BUF_ATTR g_ct_rx_buf[MAX_CT_RX_BUF_SIZE] __attribute__((aligned(4)));  /* Rx Buffer */
u32 g_ct_rx_count;                    /* size of Rx buffer */   

const u16 atr_f_table[16] = 
{ 
    0, 372, 558, 744, 1116, 1488, 1860, 0, 0, 512, 768, 1024, 1536, 2048, 0, 0 
};

extern void ct_clk_set(void) ;
extern program_tracker(u8 * data_buff,u8 data_len);
void hw_ct_wtx (u8 cmd, u8 wi, u8 fi)
{
    reg_timer_stop |= TIMEB_STOP0;
    
    if (CMD_DIRECT_PROCESS == cmd)
    {
        reg_ct_etu_cnt = 5;
        NVIC_ISPR |= 0x2000; //timerb
        
        return;
    }
    
    if (CMD_WTX_START == cmd)
    {
        reg_timerb_config = 0x00050000; //CT CLK
        
        if (CT_PROTOCOL_T0 == g_ct_ctx.type)
        {
            /* timerb_preset0 的值是实际值的 2/3，考虑到有个处理余量 */
            reg_timerb_preset0 = (wi * 960 * atr_f_table[fi]) * 2 / 3; 
        }
        else
        {
            //TODO: howt to set T=1 timerb_preset0   BWT = 11etu+2^BWI*960*Fd/f
        }
        
        reg_timer_int_config |= TIMEB_INT_EN0; 
            
        reg_timer_start |= TIMEB_START0;
       
    }
    else
    {
        reg_timer_int_config &= ~TIMEB_INT_EN0;        
    }
}

void hw_ct_set_baud (u8 fi, u8 di)
{
    interface_baud_cfg (0x01, (fi << 4) | di); 
}

/*
 * NOTE: tx_len must be less than FM336_CT_BUF_SIZE
 */
u8 hw_ct_transceive (u32 cmd, const u8 *tx_buf, u16 tx_len, u8 *rx_buf, u32 *rx_len)
{
    cmd &= ~CMD_DIRECT_PROCESS;
  
    if (g_hw_cfg.io.ct_cfg.ct_tx_egt_num & CT_TX_EGT)
    {       
        cmd |= (g_hw_cfg.io.ct_cfg.ct_tx_egt_num & CT_TX_EGT) << 3;
        cmd |= CT_TX_EGT_EN; 
    }
    
    hw_ct_wtx (CMD_WTX_STOP, 0, 0);
    
    // wait for the end of the previous frame
    while (reg_chip_common_ctrl & 0x0300) ;
    
    while (reg_ct_etu_cnt > 0) ;
    
    ct_data_transceive (cmd, (u8 *)tx_buf, tx_len, rx_buf, (u32 *)rx_len, (u32)(tx_buf + tx_len));
     
    return 1;
}





void hw_ct_ioctrl (u8 param)
{   	
    u32 init_cfg = uread4 (g_hw_cfg.io.ct_cfg.ct_t0_init_cfg);
    hw_ct_wtx (CMD_WTX_STOP, 0, 0);


    g_ct_rx_count = 0;

    switch (param)
    {    
        case IO_CTRL_CLOSE:
			
            ct_interface_init (CT_INTERFACE_OFF, init_cfg);
            break;
        
        case IO_CTRL_RESET:

            ct_interface_init (CT_INTERFACE_OFF, init_cfg);
            //break; //continue to run the following lines
        
        case IO_CTRL_OPEN:    
		
            ct_interface_init (CT_INTERFACE_ON, init_cfg);
            reg_ct_baud_cfg = g_hw_cfg.io.ct_cfg.ct_atr_baud; //init atr baud  


            break;
    }
}

const io_ops_t ct_ops = {
  /*.ioctrl       = */ hw_ct_ioctrl,
  /*.transceive   = */ hw_ct_transceive,
  /*.wtx          = */ hw_ct_wtx,
  /*.setbaud      = */ hw_ct_set_baud,
};


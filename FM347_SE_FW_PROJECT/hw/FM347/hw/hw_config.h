#ifndef __HW_CONFIG_H__
#define __HW_CONFIG_H__

#include "types.h"
#include "options.h"
#include "tools.h"
#include "io.h"

#define HW_IO_ATR_DEFAULT  "\x3B\x74\x18\x00\x00\x90\x03\x47\x00"
#define HW_IO_ATS_DEFAULT  "\x09\x78\x80\x70\x02\x90\x03\x47\x00"

typedef struct _ee_hw_rte_block_s
{
/**   NOTE: 必须4字节对齐
 *
 *    寄存器配置开关
 *
 *    config_en[0]必须为0x64，表示配置区有效，不然直接跳过，防止乱配情况。
 *    后面24 bit表示24个配置项，哪一位为1就表示哪一个配置可以配置
 *
 *    config_en[1]
 *
 *    config_en[2]
 *
 *    config_en[3]
 *      +----------------------------------------------
 *      |  b0  |
 *      |----------------------------------------------
 *      |  b1  |
 *      |----------------------------------------------
 *      |  b2  |
 *      |----------------------------------------------
 *      |  b3  |
 *      |----------------------------------------------
 *      |  b4  |
 *      |----------------------------------------------
 *      |  b5  |
 *      |----------------------------------------------
 *      |  b6  |
 *      |----------------------------------------------
 *      |  b7  |
 *      +----------------------------------------------
 *
 */
    u8 config_en[4];

    //u8 dfs_cpu_cfg;
    //u8 dfs_func_cfg;


/* Total: 6B */
} hw_rte_block_t;

#define HW_RTE_BLOCK_SIZE   (sizeof(hw_rte_block_t))

extern const u8 hw_rte_default[];

void hw_rte_get (void);

////////////////////////////////////////////////////////////////////////////////

typedef struct _ee_hw_config_block_s
{
    io_config_block_t   io;
    hw_rte_block_t      hw;
} hw_config_t;

extern hw_config_t g_hw_cfg;

void hw_config_set (void);
void hw_config_get (u8 st);

#endif//__HW_CONFIG_H__


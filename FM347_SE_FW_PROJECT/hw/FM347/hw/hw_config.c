#include "hw_config.h"
#include "main.h"

hw_config_t g_hw_cfg;

/*
const u8 hw_rte_default[] = {
    0x00, 0x00, 0x00, 0x00, //regs_config
    0x03, //config_en0
    0x00, //config_en1
    0x0B, 0x0B, //dfs_xxx_cfg
    0x01, 0x01, //cl_low_xxx
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x0A, 0x0A, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, //fs_table
};
*/

void hw_rte_get (void)
{

}

u8 io_config_get (io_config_block_t *io)
{   
    /* If the parameters of cl and ct art not configured,
       initialize them to default values */
    io_config_init (io, (u8 *)HW_IO_ATR_DEFAULT, (u8 *)HW_IO_ATS_DEFAULT);

    return 1;//TRUE
}

void hw_config_get (u8 st)
{
    io_config_get (&g_hw_cfg.io);
    io_protocol_init (&g_hw_cfg.io);
    
    if (IO_DEFAULT == g_hw_cfg.io.ct_en)
    {
        g_hw_cfg.io.ct_cfg.ct_atr[g_hw_cfg.io.ct_cfg.ct_atr_len-1] = st;
    }
    
    if (IO_DEFAULT == g_hw_cfg.io.cl_en)
    {
		g_hw_cfg.io.cl_cfg.cl_ats[g_hw_cfg.io.cl_cfg.cl_ats_len-1] = st;
	}

}

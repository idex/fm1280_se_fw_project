#include "hw.h"
#include "main.h"
#include "serialinterface.h"
/* Don't use function mode
 * Because interrupt_exit_before_longjump will change registry r4/r5
void int_longjmp (jmp_buf env, int val)
{
    interrupt_exit_before_longjump ();
    longjmp (env, val);
}*/

extern vu8 HSI_change;
extern vu8 HSI_status;
extern vu8 idex_wtx_flag;
extern vu32 system_frequency;
extern vu8 Idex_free_timer_flag;
extern vu32 Idex_free_timer_counter;
extern vu8 reset_dedicated_flag;

#ifdef sleeve_enrollment_support
extern vu8 sleeve_enable_flag;
extern vu8 process_in_waiting;
extern void blinkControl(void);
extern void wtxFsm(WtxEvent_t event);
#endif
extern void program_tracker(u8 * data_buff,u8 data_len);
extern void hw_ct_ioctrl_open(void);
extern void idex_sleeve_timer_start(void);
void NMI_Handler (void)
{


 	while (1) ;
}

void HardFault_Handler(void)
{
}


void ct_clk_on_off_IRQHandler (void) 
{

}

void ct_rst_n_IRQHandler (void)      
{

    if (reg_clk_status1 & CT_RST_N_FLAG)
    {
#ifdef sleeve_enrollment_support
			sleeve_enable_flag=0;
#endif

		
	if(CT_VCC_CLASS_C_FLAG&reg_clk_status1)
		{
		 ct_deactivate (&g_ct_ctx,1);
		 int_longjmp (jmp_init, IO_UNKNOWN);
		}
		

        if ((card_rte.curr_io != IO_CT) || (! g_ct_ctx.activated))
        {
		ct_activate (&g_ct_ctx);
        }
        else
        {
            hw_cos_flag |= COS_FLG_CT_ACTIVATED; 
            int_longjmp (jmp_init, IO_CT);
        }
    } 

}




void ct_vcc_on_off_IRQHandler (void)    
{
    	 reg_int_status &= ~VCC_ON_OFF_INT_FLAG;	

    if (reg_clk_status1 & CT_VCC_ON_FLAG)
    {   

        hw_cos_flag |= COS_FLG_CT_VCC_ON;    
        hw_ct_ioctrl (IO_CTRL_OPEN);

    }
    else
    {

        hw_cos_flag &= ~COS_FLG_CT_VCC_ON;
        ct_deactivate (&g_ct_ctx, 1);
        if (card_rte.curr_io == IO_CT)
		{
			int_longjmp (jmp_init, IO_SHUTDOWN);
		}
    }
    
    //int_status &= ~VCC_ON_OFF_INT_FLAG;  //Firmware clear
}

void ct_IRQHandler (void)
{
    u32 intflg = reg_ct_int_flag;
	
	reg_ct_int_flag = 0x00;    
#ifdef sleeve_enrollment_support
	sleeve_enable_flag=0;
#endif

    do 
    {
        if (intflg & CT_ERROR_FLAG)  
        {
            break;
        }
        
        if (intflg & CT_TX_FLAG1) //the last character is sent.
        {
            reg_ct_etu_cnt = g_ct_ctx.ct_cfg->ct_tx_egt_num;
        }
        
        if (intflg & CT_TX_FLAG0)
        {

        }
                   
        if (intflg & CT_RX_FLAG1)   
        {           
            if (CT_PROTOCOL_T0 == g_ct_ctx.type) //T = 0
            {
                reg_ct_etu_cnt = g_ct_ctx.ct_cfg->ct_tx_etu_cnt;
            }
            
            if (false == ct_receive (&g_ct_ctx, g_ct_rx_buf, g_ct_rx_count))
                break;
        }
        
        return;
        
    } while (0);
    
    ct_deactivate (&g_ct_ctx, false);                
}

void cl_on_off_IRQHandler (void)
{
 
    if (reg_clk_status1 & CL_CLK_ON_FLAG)
    {
#ifdef sleeve_enrollment_support
sleeve_enable_flag=0;

#endif
        hw_cos_flag |= COS_FLG_CL_FIELD_ON;

        hw_cl_ioctrl (IO_CTRL_OPEN);
    }
    else
    {
        hw_cos_flag &= ~COS_FLG_CL_FIELD_ON;
        
        cl_deactivate (&g_cl_ctx, 1);
        
        if (card_rte.curr_io == IO_CL)
		{
            int_longjmp (jmp_init, IO_SHUTDOWN);
		}
    }
    
}

void cla_IRQHandler (void) 
{

    u32 intflg = reg_cla_int_flag;
    reg_cla_int_flag = 0x00;

    do 
    {
        if (intflg & CLA_RX_TOTAL_ERR_FLAG)
        {
            break;
        }

        if ((intflg & CLA_RX_FLAG) || (intflg & CLA_TR_FLAG))
        {
            if (reg_chip_common_ctrl & BIT5) //the first correct RATS command
            {
                //CPU
                if (TCL_CMD_RATS != g_cl_rx_buf[0])
                    break;

                g_cl_rx_buf[0] = 0;

                if ((card_rte.curr_io == IO_CT) || (! g_cl_ctx.activated))
                {
                    cl_activate (&g_cl_ctx, false);
                }
                else
                {
                    hw_cos_flag |= COS_FLG_CL_ACTIVATED; 
                    
                    int_longjmp (jmp_init, IO_CL);
                }
            }
            else
            {
                if (false == cl_receive (&g_cl_ctx, g_cl_rx_buf, g_cl_rx_count))
                {  
                    break;
                }

            }
        }
        
        return;
        
    } while (0);
    
    cl_reset (&g_cl_ctx);
}

void timer_a_IRQHandler (void)
{
	if(2==Idex_free_timer_flag)
		{
		Idex_free_timer_counter++;
		reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG0;
		return;
		}
		
#ifdef sleeve_enrollment_support
	if(1==sleeve_enable_flag)
		{
		Idex_free_timer_counter++;
		reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG0;//IO_SLEEVE_ENROLLMENT
		if(2000<Idex_free_timer_counter)
			{
			reg_timer_stop |= TIMEA_STOP0;
			NVIC_ICER = VCC_ON_OFF_INT_BIT|CL_ON_OFF_INT_BIT;
			int_longjmp (jmp_init, IO_SLEEVE_ENROLLMENT);
			}

		
		}
	if(sleeve_enable_flag==0x55)//sleeve process
		{
		reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG0;
		blinkControl();
		}
#endif




    if ( g_cl_ctx.wtx_trigger ||   (TIMEA_INT_FLAG0 == (reg_timer_ovflow_flag & TIMEA_INT_FLAG0)) )
    {
        reg_timer_stop |= TIMEA_STOP0;
        reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG0;
	if(0==idex_wtx_flag)
		{
		if (! cl_wtx (&g_cl_ctx, false))
			{
			int_longjmp (jmp_init, IO_CL_DESELECT);
			}
		}
	else
		{
		wtxFsm(EV_WTX_START);	
		}
    }


}

void timer_b_IRQHandler (void)
{

	if(1==Idex_free_timer_flag)
		{
		Idex_free_timer_counter++;
		 reg_timer_ovflow_flag &= ~TIMEB_INT_FLAG0;
		 return;
		}
	
    if ( g_ct_ctx.wtx_trigger || 
        (TIMEB_INT_FLAG0 == (reg_timer_ovflow_flag & TIMEB_INT_FLAG0)) )
    {
        reg_timer_stop |= TIMEB_STOP0;
        reg_timer_ovflow_flag &= ~TIMEB_INT_FLAG0;

        ct_wtx (&g_ct_ctx, false);
    }
    
    reg_timer_ovflow_flag &= ~TIMEB_INT_FLAG1;
}


void gpio_IRQHandler()
{
	NVIC_ICER = GPIO_INT_BIT;
	reg_pad_interrupt_flag = 0;
	NVIC_ICPR = GPIO_INT_BIT;
	HSI_change=1;
	HSI_status=((reg_gpio_din&GPIO1_DIN)==GPIO1_DIN)? 1:0;

	NVIC_ISER = GPIO_INT_BIT;//enable gpio interrupt
}



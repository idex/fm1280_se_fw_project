#include "hw.h"
#include "main.h"

/* Don't use function mode
 * Because interrupt_exit_before_longjump will change registry r4/r5
void int_longjmp (jmp_buf env, int val)
{
    interrupt_exit_before_longjump ();
    longjmp (env, val);
}*/

#define int_longjmp(env, val) do {\
    interrupt_exit_before_longjump ();\
    longjmp ((env), (val));\
} while(0);

void NMI_Handler (void)
{
 	while (1) ;
}

void ct_clk_on_off_IRQHandler (void) 
{
#if _configINT_UART_DEBUG_
    //uart_send_byte (0xC2);
#endif
    //int_status &= ~CT_CLK_ON_OFF_INT_FLAG;  //Firmware clear
}

void ct_rst_n_IRQHandler (void)      
{
#if _configINT_UART_DEBUG_    
    //uart_send_byte (0xC3);
#endif
		//reg_clk_ctrl0 |= VCC_CLASS_DET_EN;            
		if ((reg_clk_status1 & CT_VCC_CLASS_C_FLAG) == CT_VCC_CLASS_C_FLAG)
		{            
            //�Ƿ�֧��class C
			return;
		}	
	
    if (reg_clk_status1 & CT_RST_N_FLAG)
    {
        if ((card_rte.curr_io != IO_CT) || (! g_ct_ctx.activated))
        {
            ct_activate (&g_ct_ctx);
        }
        else
        {
            hw_cos_flag |= COS_FLG_CT_ACTIVATED; 

            //int_status &= ~CT_RST_N_INT_FLAG;  //Firmware clear
            
            int_longjmp (jmp_init, IO_CT);
        }
    } 
    
    //int_status &= ~CT_RST_N_INT_FLAG;  //Firmware clear
}

void ct_vcc_on_off_IRQHandler (void)    
{
#if _configINT_UART_DEBUG_    
    //uart_send_byte (0xC1);
#endif

    if (reg_clk_status1 & CT_VCC_ON_FLAG)
    {   
        //close CL adaptive frequency in Firmware
        //clk_ctrl0 &= ~(VDD_FOR_CLKFREQ_DET_EN | DFS_ADAPT_EN);
				
				reg_clk_ctrl0 |= VCC_CLASS_DET_EN;
			
        hw_cos_flag |= COS_FLG_CT_VCC_ON;    

        hw_ct_ioctrl (IO_CTRL_OPEN);
    }
    else
    {
        hw_cos_flag &= ~COS_FLG_CT_VCC_ON;
        ct_deactivate (&g_ct_ctx, 1);
        
        if (card_rte.curr_io == IO_CT)
		{
			int_longjmp (jmp_init, IO_SHUTDOWN);
		}
    }
    
    //int_status &= ~VCC_ON_OFF_INT_FLAG;  //Firmware clear
}

void ct_IRQHandler (void)
{
    u32 intflg = reg_ct_int_flag;
	reg_ct_int_flag = 0x00;     
#if _configINT_UART_DEBUG_    
    //uart_send_byte (0xC4);
#endif    
    do 
    {
        if (intflg & CT_ERROR_FLAG)  
        {
            break;
        }
        
        if (intflg & CT_TX_FLAG1) //the last character is sent.
        {
            reg_ct_etu_cnt = g_ct_ctx.ct_cfg->ct_tx_egt_num;
        }
        
        if (intflg & CT_TX_FLAG0)
        {

        }
                   
        if (intflg & CT_RX_FLAG1)   
        {           
            if (CT_PROTOCOL_T0 == g_ct_ctx.type) //T = 0
            {
                reg_ct_etu_cnt = g_ct_ctx.ct_cfg->ct_tx_etu_cnt;
            }
            
            if (false == ct_receive (&g_ct_ctx, g_ct_rx_buf, g_ct_rx_count))
                break;
        }
        
        return;
        
    } while (0);
    
    ct_deactivate (&g_ct_ctx, false);                
}

void cl_on_off_IRQHandler (void)
{

#if _configINT_UART_DEBUG_    
   // uart_send_byte (0xA1);
#endif    
    if (reg_clk_status1 & CL_CLK_ON_FLAG)
    {
        //open CL adaptive frequency in Firmware
        //if (! IS_DUAL_INTF())
        //    clk_ctrl0 |= VDD_FOR_CLKFREQ_DET_EN | DFS_ADAPT_EN;
        
        hw_cos_flag |= COS_FLG_CL_FIELD_ON;

        hw_cl_ioctrl (IO_CTRL_OPEN);
    }
    else
    {
        hw_cos_flag &= ~COS_FLG_CL_FIELD_ON;
        
        cl_deactivate (&g_cl_ctx, 1);
        
        if (card_rte.curr_io == IO_CL)
		{
            int_longjmp (jmp_init, IO_SHUTDOWN);
		}
    }
    
    //int_status &= ~CL_ON_OFF_INT_FLAG; //Firmware clear
}

void cla_IRQHandler (void) 
{

    u32 intflg = reg_cla_int_flag;
    reg_cla_int_flag = 0x00;

#if _configINT_UART_DEBUG_    
    //uart_send_byte(0xA2);
#endif    
    do 
    {
        if (intflg & CLA_RX_TOTAL_ERR_FLAG)
        {
            break;
        }
        
        if (intflg & CLA_TX_FLAG)
        {
            //uart_send_byte(0xA3);
        } 
        
        if ((intflg & CLA_RX_FLAG) || (intflg & CLA_TR_FLAG))
        {
            if (reg_chip_common_ctrl & BIT5) //the first correct RATS command
            {
                //CPU
                if (TCL_CMD_RATS != g_cl_rx_buf[0])
                    break;

                g_cl_rx_buf[0] = 0;

                if ((card_rte.curr_io == IO_CT) || (! g_cl_ctx.activated))
                {
                    cl_activate (&g_cl_ctx, false);
                }
                else
                {
                    hw_cos_flag |= COS_FLG_CL_ACTIVATED; 
                    
                    int_longjmp (jmp_init, IO_CL);
                }
            }
            else
            {
                if (false == cl_receive (&g_cl_ctx, g_cl_rx_buf, g_cl_rx_count))
                {  
                    break;
                    //hw_cos_flag |= COS_FLG_CL_RESET; 
                        
                    //int_longjmp (jmp_init, IO_CL);
                }

            }
        }
        
        return;
        
    } while (0);
    
    cl_reset (&g_cl_ctx);
}

void timer_a_IRQHandler (void)
{
#if _configINT_UART_DEBUG_
    //uart_send_byte(0xAF);
#endif
    
    //if (TIMEA_INT_FLAG0 == (timer_ovflow_flag & TIMEA_INT_FLAG0))
    {
        reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG0;
    } 
    
    if ( g_cl_ctx.wtx_trigger || 
        (TIMEA_INT_FLAG1 == (reg_timer_ovflow_flag & TIMEA_INT_FLAG1)) )
    {
        reg_timer_stop |= TIMEA_STOP0 | TIMEA_STOP1;
        reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG1;

        if (! cl_wtx (&g_cl_ctx, false))
        {
            int_longjmp (jmp_init, IO_CL_DESELECT);
        }
    }

}

void timer_b_IRQHandler (void)
{
#if _configINT_UART_DEBUG_
    //uart_send_byte(0xCF);
#endif    
    if ( g_ct_ctx.wtx_trigger || 
        (TIMEB_INT_FLAG0 == (reg_timer_ovflow_flag & TIMEB_INT_FLAG0)) )
    {
        reg_timer_stop |= TIMEB_STOP0;
        reg_timer_ovflow_flag &= ~TIMEB_INT_FLAG0;

        ct_wtx (&g_ct_ctx, false);
    }
    
    reg_timer_ovflow_flag &= ~TIMEB_INT_FLAG1;
}

void gpio_IRQHandler()
{

}


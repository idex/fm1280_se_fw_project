#ifndef __FM_347_H__
#define __FM_347_H__
// feedback to £ºchennannan@fmsh.com.cn

// update history
// 2016-06-01 1.0
// 2016-06-12 1.1  
// 2016-06-24 1.2  
// 2016-08-18 1.3  
// 2016-09-13 1.4  
// 2016-09-14 1.5 
// 2016-10-24 1.6  add hspi Module
// 2017-09-01 1.7  v02 modify,add NVM_ERASE_MODE_MASK and other 3 regs
//                            add cla_ldo12_trim_mask reg but define                  
//                            add HSPI_CLK_EN define                  
//                            add RAM_INIT_EN define                  

#ifndef u8
	#define u8  unsigned char	
#endif

#ifndef u16
	#define  u16 unsigned short 	
#endif

#ifndef u32
	#define  u32 unsigned long 	
#endif 

/*------------------------------register overview--------------------------
No      Module              Addr
1       mmu                 
2       nvm_ctrl            
3       jca                 
4       crc_ctrl            
5       interrupt       
6       hash                
7       bca                 
8       pae                 
9       mode_ctrl           
10      clk_gen             
11      rst_gen             
12      security_ctrl       
13      random_ctrl         
14      ram_init              
15      timer               
16      WDT                 
17      test          
18      pad_ctrl
19      ct                  
20      cla                 
21      spi                 
22      uart                 
23      i2c  
24      dummy             
25      sc000            
-------------------------------------------------------------------------------*/


//memory start address define
#define SYS_RAM_START         ((volatile unsigned char*)(0x00800000))
#define FIRMWARE_RAM_START    ((volatile unsigned char*)(0x009F0000))
#define PAE_RAM_START         ((volatile unsigned int*)(0x00A00000))
#define PAE_RF_START          ((volatile unsigned int*)(0x00A80000))
#define CLA_RAM_BUF_START     ((volatile unsigned char*)(0x00AF0000))
#define USER_DATA_NVM_START   ((volatile unsigned char*)(0x00B00000))
#define KEY_DATA_NVM_START    ((volatile unsigned char*)(0x00D00000))
#define M1_DATA_NVM_START     ((volatile unsigned char*)(0x00DA0000))
#define CFG_NVM_START         ((volatile unsigned char*)(0x00DE0000))


#define CLA_RAM_BUF_HWORD_START   ((volatile unsigned short*)(0x00AF0000))
#define CLA_RAM_BUF_WORD_START    ((volatile unsigned int*)(0x00AF0000))

#define CFG_NVM_HWORD_START       ((volatile unsigned short*)(0x00DE0000)) 
#define CFG_NVM_WORD_START        ((volatile unsigned int*)(0x00DE0000)) 

#define M1_DATA_NVM_WORD_START    ((volatile unsigned int*)(0x00DA0000))
#define M1_DATA_NVM_HWORD_START   ((volatile unsigned short*)(0x00DA0000))

// common define <begin>--------------------------------------------------------
#ifndef BIT0

	#define BIT0            (((u32)1) << 0x00)
	#define BIT1            (((u32)1) << 0x01)
	#define BIT2            (((u32)1) << 0x02)
	#define BIT3            (((u32)1) << 0x03)
	#define BIT4            (((u32)1) << 0x04)
	#define BIT5            (((u32)1) << 0x05)
	#define BIT6            (((u32)1) << 0x06)
	#define BIT7            (((u32)1) << 0x07)
	#define BIT8            (((u32)1) << 0x08)
	#define BIT9            (((u32)1) << 0x09)
	#define BIT10           (((u32)1) << 0x0A)
	#define BIT11           (((u32)1) << 0x0B)
	#define BIT12           (((u32)1) << 0x0C)
	#define BIT13           (((u32)1) << 0x0D)
	#define BIT14           (((u32)1) << 0x0E)
	#define BIT15           (((u32)1) << 0x0F)
	#define BIT16           (((u32)1) << 0x10)
	#define BIT17           (((u32)1) << 0x11)
	#define BIT18           (((u32)1) << 0x12)
	#define BIT19           (((u32)1) << 0x13)
	#define BIT20           (((u32)1) << 0x14)
	#define BIT21           (((u32)1) << 0x15)
	#define BIT22           (((u32)1) << 0x16)
	#define BIT23           (((u32)1) << 0x17)
	#define BIT24           (((u32)1) << 0x18)
	#define BIT25           (((u32)1) << 0x19)
	#define BIT26           (((u32)1) << 0x1A)
	#define BIT27           (((u32)1) << 0x1B)
	#define BIT28           (((u32)1) << 0x1C)
	#define BIT29           (((u32)1) << 0x1D)
	#define BIT30           (((u32)1) << 0x1E)
	#define BIT31           (((u32)1) << 0x1F)
#endif


#define REG_T(X)        (*(volatile u32 *)(X))   // define u32 register type
#define REGU8_T(X)      (*(volatile u8 *)(X))    // define u8 register type
#define REG_ADDR_T(X)   ((volatile u32 *)(X))    // define u32 register array type
#define REGU8_ADDR_T(X) ((volatile u8 *)(X))     // define u8 register array type
// common define <end>----------------------------------------------------------


// --------------------------------------------------------------------------------------
// ----------               MMU Module Register Define                         ----------
// ----------                                                                  ----------
// ----------NO      : 1                                                       ----------
// ----------Addr    : 0x00E0_0000~0x00E0_0180                                 ----------
// ----------Reg Num : 26 word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------

#define MMU_BASE                              0x00E00000
// #define register_name                     register_addr                    reset value
#define nvm_op_src_start_addr                (REG_T(MMU_BASE+0x00))  

#define nvm_op_dest_start_addr               (REG_T(MMU_BASE+0x04))  

#define nvm_op_length                        (REG_T(MMU_BASE+0x08))  

#define nvm_op_mode                          (REG_T(MMU_BASE+0x0C))  
  #define NVM_CELL_1_OK_FLAG                   BIT29
  #define NVM_CELL_0_OK_FLAG                   BIT28
  #define NVM_DEST_SET_OK_FLAG                 BIT27
  #define NVM_OUT_SECTOR_ERR_N_FLAG            BIT26
  #define NVM_AUTH_OK_FLAG                     BIT24
  #define NVM_DEST_SECTOR_TYPE                 BIT23
  #define NVM_OP_MODE                          0x000FFFFF
                                               
  #define NVM_VERIFY_RD_MODE                   0x00040000
  #define NVM_NORMAL_RD_MODE                   0x00000000
                                               
  #define NVM_CELL_CHECK_EN                    0x00010000
                                               
  #define NVM_RETRY_ERASE_ORDER_1              0x00000000
  #define NVM_RETRY_ERASE_ORDER_2              0x00004000
  #define NVM_RETRY_ERASE_ORDER_3              0x00008000
  #define NVM_RETRY_ERASE_ORDER_4              0x0000C000
                                                 
  #define NVM_RETRY_ERASE_EN                   0x00001000
                                               
  #define NVM_PRE_PROGRAM_EN                   0x00000400
                                               
  #define NVM_CELL_0_PROGRAM_EN                0x00000100
                                               
  #define NVM_DATA_CHECK_EN                    0x00000040
                                               
  #define NVM_CHIP_ERASE_EN                    0x00000010
                                               
  #define NVM_SECTOR_ERASE_EN                  0x00000004
                                               
  #define NVM_PROGRAM_EN                       0x00000001
  
  #define NVM_ERASE_MODE_MASK                  0x0000F03C 
  #define NVM_PROGRAM_MODE_MASK                0x00000F03
  #define NVM_RD_CHECK_MODE_MASK               0x000F00C0
  
#define nvm_op_start                         (REG_T(MMU_BASE+0x10))  
  #define NVM_CFG_REGISTER_WRITE_EN            0x55AAAA55
  #define NVM_OP_START_EN                      0x5AA5A55A

#define debug_pc_value                       (REG_T(MMU_BASE+0x14))  
#define cos_flag                             (REG_T(MMU_BASE+0x18))  

#define secu_transfer_cfg                    (REG_T(MMU_BASE+0x1C))  
  #define SECU_MASK_LOAD_EN                    BIT6
  #define WORD_SECU_TRANSFER_MODE              0x00000000
  #define BYTE_SECU_TRANSFER_MODE              BIT1
  #define SECU_TRANSFER_EN                     BIT0
  

#define reg_mmu_err_flag                         (REG_T(MMU_BASE+0x20)) 
  #define CPU_ACCESS_ERR_FLAG                  BIT15
  #define JCA_ACCESS_ERR_FLAG                  BIT14
  #define CRC_ACCESS_ERR_FLAG                  BIT13
  #define WRITE_ERR_FLAG                       BIT10
  #define READ_ERR_FLAG                        BIT9
  #define DATA_ACCESS_BLACK_FLAG               BIT8
  #define ROM_CHECK_ERR_FLAG                   BIT5
  #define PROG_NVM_CHECK_ERR_FLAG              BIT4
  #define DATA_NVM_CHECK_ERR_FLAG              BIT3
  #define DATA_NVM_CORRECT_ERR_FLAG            BIT2
  #define RAM_CHECK_ERR_FLAG                   BIT1
  #define DPRAM_CHECK_ERR_FLAG                 BIT0
  

#define reg_mmu_err_bus_addr                     (REG_T(MMU_BASE+0x24))  

#define reg_bus_signature_cfg                    (REG_T(MMU_BASE+0x50))  
  #define BUS_SIGNATURE_EN                     BIT15
  #define BUS_SIGNATURE_MODE                   BIT14
  #define BUS_SIGNATURE_BUS_TYPE               BIT13
  #define BUS_ROM_SIGNATURE_EN                 BIT10
  #define BUS_NVM_SIGNATURE_EN                 BIT9
  #define BUS_RAM_SIGNATURE_EN                 BIT8
  #define BUS_SIGNATURE_PC_BUS_BUSY            BIT2
  #define BUS_SIGNATURE_HANGUP                 BIT1
  #define BUS_SIGNATURE_DATA_BUS_BUSY          BIT0


#define reg_mmu_cfg                              (REG_T(MMU_BASE+0x160)) 
  #define NVM_DOUBLE_READ_EN                   0x00000000
  #define NVM_NORMAL_READ_EN                   0x00000001

// --------------------------------------------------------------------------------------
// ----------               NVM_CTRL Module Register Define                    ----------
// ----------                                                                  ----------
// ----------NO      : 2                                                       ----------
// ----------Addr    : 0x00E0_0200~0x00E0_0208                                 ----------
// ----------Reg Num : 3 word                                                  ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define NVM_BASE                             0x00E00200
// #define register_name                     register_addr                    reset value
#define reg_nvm_op_status                        (REG_T(NVM_BASE+0x00))
  #define NVM_OP_INT_STOP_FLAG                 BIT7
  #define NVM_OP_LV_ALARM_STOP_FLAG            BIT6
  #define NVM_OP_DATA_CHECK_ERR_FLAG           BIT3
  #define NVM_OP_END_INT_FLAG                  BIT0
  
#define reg_nvm_prog_cfg                         (REG_T(NVM_BASE+0x04)) 
  #define NVM_PRE_PROG_TIME_CFG                (BIT12|BIT11|BIT10|BIT9|BIT8)
  #define NVM_PROG_TIME_CFG                    (BIT6|BIT5|BIT4|BIT3|BIT2|BIT1|BIT0)


#define reg_nvm_erase_cfg                        (REG_T(NVM_BASE+0x08)) 
  #define NVM_CHIP_ERASE_TIME_CFG              0x00FF0000
  #define NVM_RETRY_ERASE_TIME_CFG             (BIT12|BIT11|BIT10|BIT9|BIT8)
  #define NVM_SECTOR_ERASE_TIME_CFG            (BIT6|BIT5|BIT4|BIT3|BIT2|BIT1|BIT0)

#define reg_nvm_wup_cfg                          (REG_T(NVM_BASE+0x0C)) 
  #define LDO_WAKEUP_TIME_CFG                  0x0000FF00
  #define NVM_DPD_WAKEUP_TIME_CFG              0x000000FF
  

// --------------------------------------------------------------------------------------
// ----------               INTERRUPT Module Register Define                   ----------
// ----------                                                                  ----------
// ----------NO      : 5                                                       ----------
// ----------Addr    : 0x00E0_0500~0x00E0_0508                                 ----------
// ----------Reg Num : 3 word                                                  ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define INTERRUPT_BASE                        0x00E00500
// #define register_name                     register_addr                    reset value
#define reg_int_hangup_cfg                       (REG_T(INTERRUPT_BASE+0x00))
  #define NMI_INT_HANGUP_EN                    BIT31
  #define CT_WTX_INT_HANGUP_EN                 BIT25
  #define CLA_WTX_INT_HANGUP_EN                BIT24
  #define I2C_INT_HANGUP_EN                    BIT20
  #define DATANVM_CHECK_ERR_INT_HANGUP_EN      BIT19
  #define LDO16_LVOLT_INT_HANGUP_EN            BIT18
  #define CT_VCC_LVOLT_INT_HANGUP_EN           BIT17
  #define CL_VDD_LVOLT_INT_HANGUP_EN           BIT16
  #define RANDOM_INT_HANGUP_EN                 BIT14
  #define TIMER_B_INT_HANGUP_EN                BIT13
  #define TIMER_A_INT_HANGUP_EN                BIT12
  #define BCA_STOP_INT_HANGUP_EN               BIT11
  #define HASH_STOP_INT_HANGUP_EN              BIT10
  #define PAE_STOP_INT_HANGUP_EN               BIT9
  #define SPI_INT_HANGUP_EN                    BIT8
  #define UART_INT_HANGUP_EN                   BIT7
  #define GPIO_INT_HANGUP_EN                   BIT6
  #define CLA_INT_HANGUP_EN                    BIT5
  #define CT_INT_HANGUP_EN                     BIT4
  #define VCC_ON_OFF_INT_HANGUP_EN             BIT3
  #define CL_ON_OFF_INT_HANGUP_EN              BIT2
  #define CT_RST_N_INT_HANGUP_EN               BIT1
  #define CT_CLK_ON_OFF_INT_HANGUP_EN          BIT0


#define reg_int_vector_table_offset              (REG_T(INTERRUPT_BASE+0x04))

#define reg_int_status                           (REG_T(INTERRUPT_BASE+0x08))
  #define VCC_ON_OFF_INT_FLAG                  BIT3
  #define CL_ON_OFF_INT_FLAG                   BIT2
  #define CT_RST_N_INT_FLAG                    BIT1
  #define CT_CLK_ON_OFF_INT_FLAG               BIT0

#define NVIC_ISER                            (REG_T(0xE000E100))
#define NVIC_ICER                            (REG_T(0xE000E180))
#define NVIC_ISPR                            (REG_T(0xE000E200))
#define NVIC_ICPR                            (REG_T(0xE000E280))

  #define CLA_M1_NVM_BUS_RELEASE_INT_BIT       BIT25
  #define JCA_STOP_INT_BIT                     BIT24
  #define SOFT_INT_BIT                   	     BIT23
  #define RAMINIT_END_INT_BIT            	     BIT22
  #define CRC_END_INT_BIT                	     BIT21
  #define I2C_INT_BIT                    	     BIT20
  #define DATANVM_CHECK_ERR_INT_BIT      	     BIT19
  #define LDO16_LVOLT_INT_BIT            	     BIT18
  #define CT_VCC_LVOLT_INT_BIT           	     BIT17
  #define CL_VDD_LVOLT_INT_BIT           	     BIT16
  #define NVM_EW_END_INT_BIT             	     BIT15
  #define RANDOM_INT_BIT                 	     BIT14
  #define TIMER_B_INT_BIT                	     BIT13
  #define TIMER_A_INT_BIT                	     BIT12
  #define BCA_STOP_INT_BIT               	     BIT11
  #define HASH_STOP_INT_BIT              	     BIT10
  #define PAE_STOP_INT_BIT               	     BIT9
  #define SPI_INT_BIT                    	     BIT8
  #define UART_INT_BIT                   	     BIT7
  #define GPIO_INT_BIT                   	     BIT6
  #define CLA_INT_BIT                    	     BIT5
  #define CT_INT_BIT                     	     BIT4
  #define VCC_ON_OFF_INT_BIT             	     BIT3
  #define CL_ON_OFF_INT_BIT              	     BIT2
  #define CT_RST_N_INT_BIT               	     BIT1
  #define CT_CLK_ON_OFF_INT_BIT          	     BIT0


// --------------------------------------------------------------------------------------
// ----------               HSPI Module Register Define                         ----------
// ----------                                                                  ----------
// ----------NO      : 5                                                       ----------
// ----------Addr    : 0x00E0_0600~0x00E0_0610                                 ----------
// ----------Reg Num : 5 word                                                  ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define HSPI_BASE                             0x00E00600
// #define register_name                     register_addr                    reset value
#define reg_hspcr1                               (REG_T(HSPI_BASE+0x00)) 
  #define HSPI_ACTIVE                          BIT15
  #define HSPI_SSN_EDGE_INT_EN                 BIT14
  #define HSPI_ERR_INT_EN                      BIT13
  #define HSPI_TXFIFO_HALF_INT_EN              BIT12
  #define HSPI_RXFIFO_HALF_INT_EN              BIT11
  #define HSPI_TXFIFO_EMPTY_INT_EN             BIT10
  #define HSPI_RXFIFO_NOT_EMPTY_INT_EN         BIT9
  #define HSPI_TX_ONLY_EN                      BIT8
  #define HSPI_TX_ONLY_AUTO_STOP_EN            BIT7
  #define HSPI_MOSI_INPUT_FILTER_EN            BIT6
  #define HSPI_SCK_INPUT_FILTER_EN             BIT5
  #define HSPI_SSN_INPUT_FILTER_EN             BIT4
  #define HSPI_SLAVE_SEND_MODE_CFG             BIT3
  #define HSPI_DATA_FRAME_CFG                  BIT2
  #define HSPI_CLK_POLARITY_CFG                BIT1
  #define HSPI_CLK_PHASE_CFG                   BIT0

#define reg_hspcr2                               (REG_T(HSPI_BASE+0x04)) 
  #define HSPI_CLR_SSN_EDGE_INT                BIT5
  #define HSPI_CLR_TXFIFO                      BIT4
  #define HSPI_CLR_RXFIFO                      BIT3
  #define HSPI_CLR_TRAN_ERR                    BIT2
  #define HSPI_CLR_RXFIFO_COLL_ERR             BIT1
  #define HSPI_CLR_TXFIFO_COLL_ERR             BIT0


#define reg_hspsr                                 (REG_T(HSPI_BASE+0x08)) 
  #define HSPI_BYTE_CYCLE_SYS_CLK_CNT          (BIT31|BIT30|BIT29|BIT28)
  #define HSPI_SSN_FLAG                        BIT24
  #define HSPI_SLAVE_ERR_FLAG                  BIT19
  #define HSPI_RXFIFO_OVERFLOW_FLAG            BIT18
  #define HSPI_TXFIFO_OVERFLOW_FLAG            BIT17
  #define HSPI_BUSY_FLAG                       BIT16
  #define HSPI_TXFIFO_HALF_FLAG                BIT13
  #define HSPI_TXFIFO_EMPTY_FLAG               BIT12
  #define HSPI_TXFIFO_LENGTH                   (BIT11|BIT10|BIT9|BIT8)
  #define HSPI_SSN_EDGE_INT_FLAG               BIT6
  #define HSPI_RXFIFO_HALF_FLAG                BIT5
  #define HSPI_RXFIFO_NOT_EMPTY_FLAG           BIT4
  #define HSPI_RXFIFO_LENGTH                   (BIT3|BIT2|BIT1|BIT0)


#define hspi_tx_fifo                          (REG_T(HSPI_BASE+0x0C)) 
                                            
                                            
#define hspi_rx_fifo                          (REG_T(HSPI_BASE+0x10)) 

// --------------------------------------------------------------------------------------
// ----------               MODE_CTRL Module Register Define                   ----------
// ----------                                                                  ----------
// ----------NO      : 9                                                       ----------
// ----------Addr    : 0x00E8_0000~0x00E8_003C                                 ----------
// ----------Reg Num : 16 word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define MODE_CTRL_BASE                       0x00E80000
// #define register_name                     register_addr                    reset value

#define reg_chip_status                          (REG_T(MODE_CTRL_BASE+0x28))
  #define CHIP_INTERFACE_MODE                  (BIT2|BIT1|BIT0)
  #define CONTACT_INTERFACE                    BIT2
  #define CT_INTERFACE                         BIT1
  #define CLA_INTERFACE                        BIT0


#define reg_chip_common_ctrl                     (REG_T(MODE_CTRL_BASE+0x2C))

// --------------------------------------------------------------------------------------
// ----------               CLK_GEN Module Register Define                     ----------
// ----------                                                                  ----------
// ----------NO      : 10                                                      ----------
// ----------Addr    : 0x00E8_0100~0x00E8_0130                                 ----------
// ----------Reg Num : 13 word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define CLK_GEN_BASE                         0x00E80100
// #define register_name                     register_addr                    reset value
#define reg_clk_ctrl0                            (REG_T(CLK_GEN_BASE+0x00)) 
  #define CPU_SCLK_GATE_EN                     BIT31
  #define NVM_WAKE_UP_SOFT_EN                  BIT30
  #define M1_LPMODE_AUTO_CTRL_EN               BIT29
  #define VDD_FOR_CLKFREQ_DET_EN               BIT28
  #define VDD_FOR_CURR_SOURCE_DET_EN           BIT27
  #define RF_FIELD_DET_EN                      BIT26
  #define VCC_CLASS_DET_EN                     BIT25
  #define DFS_ADAPT_EN                         BIT24
  #define DFS_RANDOM_GATE_EN                   BIT23
  #define DFS_RANDOM_CFG_EN                    BIT22
  #define RAMINIT_CLK_EN                       BIT21
  #define JCA_CLK_EN                           BIT20
  #define CRC_CLK_EN                           BIT19
  //#define MODE_CTRL_CLK_EN                     BIT18       //--------mode_ctrl clk don't have gate ctrl
  #define RANDOM_CLK_EN                        BIT17
  #define TIMER_CLK_EN                         BIT16
  #define HASH_CLK_EN                          BIT11
  #define DPRAM_CLK_EN                         BIT10
  #define PAE_CLK_EN                           BIT9
  #define BCA_CLK_EN                           BIT8
  #define HSPI_CLK_EN                          BIT6          //-------update for add hspi clk gate ctrl
  #define I2C_SLAVE_CLK_EN                     BIT5
  #define I2C_MASTER_CLK_EN                    BIT4
  #define SPI_CLK_EN                           BIT3
  #define UART_CLK_EN                          BIT2
  #define CLA_CLK_EN                           BIT1
  #define CT_CLK_EN                            BIT0


#define reg_clk_ctrl2                            (REG_T(CLK_GEN_BASE+0x08)) // 3F3F_0404h
  #define CPU_FIX_FREQ_CFG                     (BIT13|BIT12|BIT11|BIT10|BIT9|BIT8)
  #define FUNC_FIX_FREQ_CFG                    (BIT5|BIT4|BIT3|BIT2|BIT1|BIT0)  
  
#define clk_status0                           (REG_T(CLK_GEN_BASE+0x0C)) // 0000_0000h
  #define NVM_ACTIVE_FLAG                       BIT29
  #define VDD16_OK_FLAG                         BIT28
  #define VDD_CLAMP_CURRENT_FLAG                (BIT27|BIT26)
  #define VDD_VOLT_FLAG                         (BIT25|BIT24)
  #define SYS_DFS_CFG                           (BIT21|BIT20|BIT19|BIT18|BIT17|BIT16)
  #define RF_FIELD_8MA_OK_FLAG                  BIT11
  #define RF_FIELD_2MA_OK_FLAG                  BIT10
  #define RF_FIELD_1MA_OK_FLAG                  BIT9
  #define VDD_RECT_VOLT_FLAG                    BIT8
  #define CURRENT_SOURCE_CFG                    (BIT3|BIT2|BIT1|BIT0)
  

#define reg_clk_status1                           (REG_T(CLK_GEN_BASE+0x10)) // 0000_0000h
  #define CL_VDD_LV_ALARM_FLAG                  BIT9
  #define CT_VCC_LV_ALARM_FLAG                  BIT8
  #define CT_VCC_CLASS_C_FLAG                   BIT6
  #define CT_VCC_CLASS_B_FLAG                   BIT5
  #define CT_VCC_CLASS_A_FLAG                   BIT4
  #define CL_CLK_ON_FLAG                        BIT3
  #define CT_CLK_ON_FLAG                        BIT2
  #define CT_RST_N_FLAG                         BIT1
  #define CT_VCC_ON_FLAG                        BIT0
  

#define reg_sleep_mode_cfg0                        (REG_T(CLK_GEN_BASE+0x1C)) // 0000_00d2h
#define reg_sleep_mode_cfg1                        (REG_T(CLK_GEN_BASE+0x20)) // 0000_00d2h
#define reg_sleep_mode_wake_up_cfg                 (REG_T(CLK_GEN_BASE+0x24)) // 0000_00d2h

#define current_source_ctrl1                   (REG_T(CLK_GEN_BASE+0x2C)) // 0000_00d2h


#define reg_power_ctrl                             (REG_T(CLK_GEN_BASE+0x34))//
  #define POWER_SOFT_SEL                         BIT1
  #define POWER_SOFT_CTRL_EN                     BIT0   




// --------------------------------------------------------------------------------------
// ----------               RESET_GEN Module Register Define                   ----------
// ----------                                                                  ----------
// ----------NO      : 11                                                       ----------
// ----------Addr    : 0x00E8_0200~0x00E8_0204                                 ----------
// ----------Reg Num : 2  word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define RST_GEN_BASE                         0x00E80200
// #define register_name                     register_addr                    reset value
#define reg_reset_ctrl                           (REG_T(RST_GEN_BASE+0x00)) 
  #define I2C_SOFT_RST                         BIT7
  #define UART_SOFT_RST                        BIT6
  #define CL_SOFT_RST                          BIT5
  #define CT_SOFT_RST                          BIT4
  #define SPI_SOFT_RST                         BIT3
  #define SECURITY_SOFT_RST                    BIT2
  #define CPU_SOFT_RST                         BIT1
  #define CHIP_SOFT_RST                        BIT0


#define reg_reset_status                         (REG_T(RST_GEN_BASE+0x04)) 
  #define CPU_RST_FLAG                         BIT4
  #define CHIP_SOFT_RST_FLAG                   BIT3
  #define SECURITY_RST_FLAG                    BIT2
  #define WDT_RST_FLAG                         BIT1
  #define POWERON_RST_FLAG                     BIT0



// --------------------------------------------------------------------------------------
// ----------               Time Module Register Define                        ----------
// ----------                                                                  ----------
// ----------NO      : 15                                                      ----------
// ----------Addr    : 0x00E8_0700~0x00E8_0734                                 ----------
// ----------Reg Num : 14 word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define TIMER_BASE                           0x00E80700
// #define register_name                     register_addr                    reset value
#define reg_timera_config                        (REG_T(TIMER_BASE+0x00)) 
  #define TIMEA_CLK_SEL1                       (BIT22|BIT21|BIT20)
  #define TIMEA_CLK_SEL0                       (BIT18|BIT17|BIT16)
  #define TIMEA_REPEAT_EN0                     BIT12
  #define TIMEA_REPEAT_EN1                     BIT8
  #define TIMEA_SYN_STOP_EN                    BIT4
  #define TIMEA_MODE                           (BIT1|BIT0)


#define reg_timerb_config                        (REG_T(TIMER_BASE+0x04)) 
  #define TIMEB_CLK_SEL1                       (BIT22|BIT21|BIT20)
  #define TIMEB_CLK_SEL0                       (BIT18|BIT17|BIT16)
  #define TIMEB_REPEAT_EN0                     BIT12
  #define TIMEB_REPEAT_EN1                     BIT8
  #define TIMEB_SYN_STOP_EN                    BIT4
  #define TIMEB_MODE                           (BIT1|BIT0)

#define reg_timer_start                          (REG_T(TIMER_BASE+0x08)) 
  #define TIMEA_AUTO_START_EN                  BIT16
  #define TIMEB_START1                         BIT12
  #define TIMEB_START0                         BIT8
  #define TIMEA_START1                         BIT4
  #define TIMEA_START0                         BIT0


#define reg_timer_stop                           (REG_T(TIMER_BASE+0x0C)) 
  #define TIMEB_STOP1                          BIT12
  #define TIMEB_STOP0                          BIT8
  #define TIMEA_STOP1                          BIT4
  #define TIMEA_STOP0                          BIT0


#define reg_timer_int_config                     (REG_T(TIMER_BASE+0x10)) 
  #define TIMEB_INT_EN1                        BIT12
  #define TIMEB_INT_EN0                        BIT8
  #define TIMEA_INT_EN1                        BIT4
  #define TIMEA_INT_EN0                        BIT0



#define reg_timer_ovflow_flag                    (REG_T(TIMER_BASE+0x14)) 
  #define TIMEB_INT_FLAG1                      BIT12
  #define TIMEB_INT_FLAG0                      BIT8
  #define TIMEA_INT_FLAG1                      BIT4
  #define TIMEA_INT_FLAG0                      BIT0


#define reg_timera_preset0                       (REG_T(TIMER_BASE+0x18)) 
#define reg_timera_preset1                       (REG_T(TIMER_BASE+0x1C)) 
#define reg_timerb_preset0                       (REG_T(TIMER_BASE+0x20)) 
#define reg_timerb_preset1                       (REG_T(TIMER_BASE+0x24)) 
#define reg_timera_counter0                      (REG_T(TIMER_BASE+0x28)) 
#define reg_timera_counter1                      (REG_T(TIMER_BASE+0x2C)) 
#define reg_timerb_counter0                      (REG_T(TIMER_BASE+0x30)) 
#define reg_timerb_counter1                      (REG_T(TIMER_BASE+0x34)) 


// --------------------------------------------------------------------------------------
// ----------               WDT Module Register Define                         ----------
// ----------                                                                  ----------
// ----------NO      : 16                                                      ----------
// ----------Addr    : 0x00E8_0800~0x00E8_080C                                 ----------
// ----------Reg Num : 4  word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define WDT_CTRL_BASE                        0x00E80800
// #define register_name                     register_addr                    reset value
#define reg_wdt_timer_config                     (REG_T(WDT_CTRL_BASE+0x00)) 

#define reg_wdt_clr                              (REG_T(WDT_CTRL_BASE+0x04)) 
  #define WDT_CLEAR_EN                         0x5A

#define reg_wdt_en                               (REG_T(WDT_CTRL_BASE+0x08)) 
#define reg_wdt_cnt                              (REG_T(WDT_CTRL_BASE+0x0C))

// --------------------------------------------------------------------------------------
// ----------               PAD_CTRL Module Register Define                    ----------
// ----------                                                                  ----------
// ----------NO      : 18                                                      ----------
// ----------Addr    : 0x00E9_0000~0x00E9_0020                                 ----------
// ----------Reg Num : 9  word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define PAD_BASE                             0x00E90000
// #define register_name                     register_addr                    reset value
#define reg_pad_function_config                  (REG_T(PAD_BASE+0x00)) 
  #define PAD10_SEL                            (BIT26|BIT25|BIT24)
  #define PAD09_SEL                            (BIT22|BIT21|BIT20)
  #define PAD08_SEL                            (BIT18|BIT17|BIT16)
  #define PAD07_SEL                            (BIT14|BIT13|BIT12)
  #define PAD06_SEL                            (BIT10|BIT9|BIT8)
  #define PAD05_SEL                            (BIT6|BIT5|BIT4)
  #define PAD04_SEL                            (BIT2|BIT1|BIT0)

#define PAD10_MASK							 (BIT27|BIT26|BIT25|BIT24)
#define PAD09_MASK							 (BIT23|BIT22|BIT21|BIT20)
#define PAD08_MASK							 (BIT19|BIT18|BIT17|BIT16)
#define PAD07_MASK							 (BIT15|BIT14|BIT13|BIT12)
#define PAD06_MASK							 (BIT11|BIT10|BIT9|BIT8)
#define PAD05_MASK							 (BIT7|BIT6|BIT5|BIT4)
#define PAD04_MASK							 (BIT3|BIT2|BIT1|BIT0)

  
                                               
  #define CLOSE_PAD                            0x00
  #define GPIO_PAD_CONFIG                      0x01
  #define CT_PAD_CONFIG                        0x02
  #define SPI_PAD_CONFIG                       0x03
  #define UART_PAD_CONFIG                      0x04
  #define I2C_PAD_CONFIG                       0x05
  

#define reg_pad_mode_config                      (REG_T(PAD_BASE+0x04)) 
  #define PAD10_PUPD_EN                        (BIT27|BIT26)
  #define PAD10_OS_EN                          BIT25
  #define PAD10_OEB                            BIT24
  #define PAD09_PUPD_EN                        (BIT23|BIT22)
  #define PAD09_OS_EN                          BIT21
  #define PAD09_OEB                            BIT20
  #define PAD08_PUPD_EN                        (BIT19|BIT18)
  #define PAD08_OS_EN                          BIT17
  #define PAD08_OEB                            BIT16
  #define PAD07_PUPD_EN                        (BIT15|BIT14)
  #define PAD07_OS_EN                          BIT13
  #define PAD07_OEB                            BIT12
  #define PAD06_PUPD_EN                        (BIT11|BIT10)
  #define PAD06_OS_EN                          BIT9
  #define PAD06_OEB                            BIT8
  #define PAD05_PUPD_EN                        (BIT7|BIT6)
  #define PAD05_OS_EN                          BIT5
  #define PAD05_OEB                            BIT4
  #define PAD04_PUPD_EN                        (BIT3|BIT2)
  #define PAD04_OS_EN                          BIT1
  #define PAD04_OEB                            BIT0


#define reg_gpio_dout                            (REG_T(PAD_BASE+0x08)) 
  #define GPIO7_DOUT                           BIT6
  #define GPIO6_DOUT                           BIT5
  #define GPIO5_DOUT                           BIT4
  #define GPIO4_DOUT                           BIT3
  #define GPIO3_DOUT                           BIT2
  #define GPIO2_DOUT                           BIT1
  #define GPIO1_DOUT                           BIT0


#define reg_gpio_din                             (REG_T(PAD_BASE+0x0C)) 
  #define GPIO7_DIN                            BIT6
  #define GPIO6_DIN                            BIT5
  #define GPIO5_DIN                            BIT4
  #define GPIO4_DIN                            BIT3
  #define GPIO3_DIN                            BIT2
  #define GPIO2_DIN                            BIT1
  #define GPIO1_DIN                            BIT0


#define reg_pad_interrupt_config                 (REG_T(PAD_BASE+0x10)) 
  #define PAD10_INT_SEL                        (BIT26|BIT25|BIT24)
  #define PAD09_INT_SEL                        (BIT22|BIT21|BIT20)
  #define PAD08_INT_SEL                        (BIT18|BIT17|BIT16)
  #define PAD07_INT_SEL                        (BIT14|BIT13|BIT12)
  #define PAD06_INT_SEL                        (BIT10|BIT9|BIT8)
  #define PAD05_INT_SEL                        (BIT6|BIT5|BIT4)
  #define PAD04_INT_SEL                        (BIT2|BIT1|BIT0)

  #define HIGH_LEVEL_INT                       0x01
  #define LOW_LEVEL_INT                        0x02
  #define POSEDGE_INT                          0x03
  #define NEGEDGE_INT                          0x04
  #define EDGE_INT                             0x07
  

#define reg_pad_interrupt_flag                   (REG_T(PAD_BASE+0x14)) 
  #define PAD_RESUME_INT_FLAG                  BIT7
  #define PAD10_INT_FLAG                       BIT6
  #define PAD09_INT_FLAG                       BIT5
  #define PAD08_INT_FLAG                       BIT4
  #define PAD07_INT_FLAG                       BIT3
  #define PAD06_INT_FLAG                       BIT2
  #define PAD05_INT_FLAG                       BIT1
  #define PAD04_INT_FLAG                       BIT0


#define reg_pad_resume_config                    (REG_T(PAD_BASE+0x18)) 
  #define PAD10_RESUME_SEL                     BIT25
  #define PAD10_RESUME_EN                      BIT24
  #define PAD09_RESUME_SEL                     BIT21
  #define PAD09_RESUME_EN                      BIT20
  #define PAD08_RESUME_SEL                     BIT17
  #define PAD08_RESUME_EN                      BIT16
  #define PAD07_RESUME_SEL                     BIT13
  #define PAD07_RESUME_EN                      BIT12
  #define PAD06_RESUME_SEL                     BIT9
  #define PAD06_RESUME_EN                      BIT8
  #define PAD05_RESUME_SEL                     BIT5
  #define PAD05_RESUME_EN                      BIT4
  #define PAD04_RESUME_SEL                     BIT1
  #define PAD04_RESUME_EN                      BIT0



#define reg_pad_data                             (REG_T(PAD_BASE+0x1C)) 





  #define PAD10_DIN                            BIT25
  #define PAD10_DOUT                           BIT24
  #define PAD09_DIN                            BIT21
  #define PAD09_DOUT                           BIT20
  #define PAD08_PU_EN 						 BIT19
  #define PAD08_DIN                            BIT17
  #define PAD08_DOUT                           BIT16
  #define PAD07_PU_EN 						 BIT15
  #define PAD07_DIN                            BIT13
  #define PAD07_DOUT                           BIT12
  #define PAD06_PU_EN 						 BIT11
  #define PAD06_DIN                            BIT9
  #define PAD06_DOUT                           BIT8
  #define PAD05_PU_EN 						 BIT7
  #define PAD05_DIN                            BIT5
  #define PAD05_DOUT                           BIT4
  #define PAD04_PU_EN 						 BIT3
  #define PAD04_DIN                            BIT1
  #define PAD04_DOUT                           BIT0




#define reg_gpio_test_en                         (REG_T(PAD_BASE+0x20)) 
  #define GPIO7_TEST_EN                        BIT6
  #define GPIO6_TEST_EN                        BIT5
  #define GPIO5_TEST_EN                        BIT4
  #define GPIO4_TEST_EN                        BIT3
  #define GPIO3_TEST_EN                        BIT2
  #define GPIO2_TEST_EN                        BIT1
  #define GPIO1_TEST_EN                        BIT0


// --------------------------------------------------------------------------------------
// ----------               CT_CTRL Module Register Define                     ----------
// ----------                                                                  ----------
// ----------NO      : 19                                                      ----------
// ----------Addr    : 0x00E9_0100~0x00E9_0120                                 ----------
// ----------Reg Num : 9 word                                                  ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define CT_CTRL_BASE                         0x00E90100
// #define register_name                     register_addr                    reset value
// ---- ct7816 transmit/receive mode config register---------- 
#define reg_ct_rt_ctrl                          (REG_T(CT_CTRL_BASE+0x00))  
  #define CT_TX_RX_EN                         BIT12
  #define CT_CLR_FIFO                         BIT8
  #define CT_RX_EN                            BIT4
  #define CT_TX_EN                            BIT0
  // ---- ct7816 data frame type config register---------- 
#define ct_frame_cfg                        (REG_T(CT_CTRL_BASE+0x04))  
  #define CT_PARITY_MODE                      (BIT25|BIT24)
  #define CT_BGT_CHECK_EN                     BIT23
  #define CT_RX_REPEAT_EN                     BIT22
  #define CT_TX_REPEAT_EN                     BIT21
  #define CT_CONVENTION_CFG                   BIT20
  #define CT_TR_REPEAT_TIME                   (BIT18|BIT17|BIT16)
  #define CT_RX_GUARD_TIME_CFG                BIT12
  #define CT_TX_GUARD_TIME_CFG                BIT8
  #define CT_TX_EGT                           0xFF

// ---- ct7816 baudrate config register---------- 
#define reg_ct_baud_cfg                         (REG_T(CT_CTRL_BASE+0x08))  
  #define BAUD_ACTIVE_MODE                    BIT8
  #define CT_FI                               0xF0
  #define CT_DI                               0x0F
											  
  #define CT_BAUD_9600                        0x11
  #define CT_BAUD_19200                       0x12
  #define CT_BAUD_38400                       0x13
  #define CT_BAUD_76800                       0x14
  #define CT_BAUD_57600                       0x38
  #define CT_BAUD_115200                      0x18
  #define CT_BAUD_116000                      0x95
  #define CT_BAUD_232000                      0x96

// ---- ct7816 interrupt enable register---------- 
#define reg_ct_int_cfg                          (REG_T(CT_CTRL_BASE+0x0C))  
  #define CT_FIFO_LENGTH_MATCH_CFG            (BIT26|BIT25|BIT24)
  #define CT_FIFO_FULL_INT_EN                 BIT22
  #define CT_FIFO_LENGTH_MATCH_INT_EN         BIT21
  #define CT_FIFO_EMPTY_INT_EN                BIT20
  #define CT_ERROR_INT_EN                     BIT16
  #define CT_RX_INT_EN1                       BIT12
  #define CT_RX_INT_EN0                       BIT8
  #define CT_TX_INT_EN1                       BIT4
  #define CT_TX_INT_EN0                       BIT0

// ---- ct7816 interrupt flag register---------- 
#define reg_ct_int_flag                         (REG_T(CT_CTRL_BASE+0x10)) 
  #define CT_FIFO_FULL_FLAG                   BIT22
  #define CT_FIFO_LENGTH_MATCH_FLAG           BIT21
  #define CT_FIFO_EMPTY_FLAG                  BIT20
  #define CT_ERROR_FLAG                       BIT16
  #define CT_RX_FLAG1                         BIT12
  #define CT_RX_FLAG0                         BIT8
  #define CT_TX_FLAG1                         BIT4
  #define CT_TX_FLAG0                         BIT0

// ---- ct7816 status and flag register---------- 
#define reg_ct_status                           (REG_T(CT_CTRL_BASE+0x14))  
  #define CT_WRITE_FIFO_STATE                 BIT28
  #define CT_RX_BUSY                          BIT25
  #define CT_TX_BUSY                          BIT24
  #define CT_FIFO_FULL                        BIT22
  #define CT_FIFO_HALF_EMPTY                  BIT21
  #define CT_FIFO_EMPTY                       BIT20
  #define CT_FIFO_STATUS                      (BIT19|BIT18|BIT17|BIT16)
  #define CT_EXTRA_ETU_CNT                    (BIT13|BIT12)
  #define CT_COLL_ERR                         BIT4
  #define CT_TX_PARITY_ERR                    BIT3
  #define CT_RX_PARITY_ERR                    BIT2
  #define CT_FRAME_ERR                        BIT1
  #define CT_FIFO_ERR                         BIT0

#define reg_ct_etu_cnt                          (REG_T(CT_CTRL_BASE+0x38)) 


// --------------------------------------------------------------------------------------
// ----------               CLA_CTRL Module Register Define                    ----------
// ----------                                                                  ----------
// ----------NO      : 20                                                      ----------
// ----------Addr    : 0x00E9_0200~0x00E9_0290                                 ----------
// ----------Reg Num : 26 word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define CLA_CTRL_BASE                       0x00E90200
// #define register_name                     register_addr                    reset value

#define reg_cla_int_flag                        (REG_T(CLA_CTRL_BASE+0x10)) 
//------------only valid with CLA_ORG module is active---------- 
  #define CLA_TX_M1_CMD_FLAG                  BIT29
  #define CLA_RX_M1_CMD_FLAG                  BIT28
//------------only valid with CLA_NEW module is active---------- 
  #define CLA_ANTI_CMD_ERR_FLAG               BIT21
  #define CLA_ANTI_UID_UNMATCH_FLAG           BIT20
//------------valid with both CLA_ORG and CLA_NEW module--------
  #define CLA_TX_ERR_FLAG                     BIT19
  #define CLA_RX_ERR_FLAG                     BIT18
  #define CLA_RX_PARITY_ERR_FLAG              BIT17
  #define CLA_RX_CRC_ERR_FLAG                 BIT16
  #define CLA_TR_FLAG                         BIT8
  #define CLA_TX_FLAG                         BIT4
  #define CLA_RX_FLAG                         BIT0

  #define CLA_RX_TOTAL_ERR_FLAG               (BIT18|BIT17|BIT16)


#define reg_cla_rt_common_cfg                   (REG_T(CLA_CTRL_BASE+0x88))
  #define CLA_CPU_SLEEP_MODE_SEL              BIT8
  #define CLA_TR_ACTIVE_MODE                  BIT4
  #define CLA_TX_DATA_MEM_ADDR_MODE           BIT0

// --------------------------------------------------------------------------------------
// ----------               SPI_CTRL Module Register Define                    ----------
// ----------                                                                  ----------
// ----------NO      : 21                                                      ----------
// ----------Addr    : 0x00E9_0400~0x00E9_040C                                 ----------
// ----------Reg Num : 4 word                                                  ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define SPI_BASE                             0x00E90400
// #define register_name                     register_addr                    reset value
#define spcr                                (REG_T(SPI_BASE+0x00)) 
  #define SPI_ERR_INT_EN                      BIT26
  #define SPI_TX_INT_EN                       BIT25
  #define SPI_RX_INT_EN                       BIT24
  #define SPI_CLR_TXBUF                       BIT23
  #define SPI_CLR_RXBUF                       BIT22
  #define SPI_CLR_MASTER_ERR                  BIT21
  #define SPI_CLR_SLAVE_ERR                   BIT20
  #define SPI_TX_ONLY_EN                      BIT19
  #define SPI_MASTER_SSN_SOFT_CFG             BIT18
  #define SPI_SLAVE_INPUT_FILTER_EN           BIT17
  #define SPI_SLAVE_SEND_MODE_CFG             BIT16
  #define SPI_MASTER_SAMPLE_MODE_CFG          BIT15
  #define SPI_TX_ONLY_AUTO_STOP_EN            BIT14
  #define SPI_ACTIVE                          BIT13
  #define SPI_MASTER_SSN_SOFT_CFG_EN          BIT12
  #define SPI_MASTER_WAIT_TIME_CFG            (BIT11|BIT10)
  #define SPI_MASTER_BAUD_CFG                 (BIT7|BIT6|BIT5)
  #define SPI_MASTER_SSN_MODE_CFG             BIT4
  #define SPI_DATA_FRAME_CFG                  BIT3
  #define SPI_WORKING_MODE_CFG                BIT2
  #define SPI_CLK_POLARITY_CFG                BIT1
  #define SPI_CLK_PHASE_CFG                   BIT0


#define spsr                                (REG_T(SPI_BASE+0x04)) 
  #define SPI_MASTER_ERR_FLAG                 BIT6
  #define SPI_SLAVE_ERR_FLAG                  BIT5
  #define SPI_RXBUF_OVERFLOW_FLAG             BIT4
  #define SPI_TXBUF_OVERFLOW_FLAG             BIT3
  #define SPI_BUSY_FLAG                       BIT2
  #define SPI_TXBUF_EMPTY_FLAG                BIT1
  #define SPI_RXBUF_FULL_FLAG                 BIT0  


#define spi_txbuf                           (REG_T(SPI_BASE+0x08)) 
                                            
                                            
#define spi_rxbuf                           (REG_T(SPI_BASE+0x0C)) 



// --------------------------------------------------------------------------------------
// ----------               UART_CTRL Module Register Define                   ----------
// ----------                                                                  ----------
// ----------NO      : 22                                                      ----------
// ----------Addr    : 0x00E9_0500~0x00E9_0510                                 ----------
// ----------Reg Num : 5  word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
#define UART_BASE                            0x00E90500
// #define register_name                     register_addr                    reset value
#define reg_uart_rt_ctrl                         (REG_T(UART_BASE+0x00)) 
  #define UART_RX_EN                           BIT17
  #define UART_TX_EN                           BIT16
  #define UART_ERR_FLAG_INT_EN                 BIT10
  #define UART_RX_FLAG_INT_EN                  BIT9
  #define UART_TX_FLAG_INT_EN                  BIT8
  #define UART_BAUD_MODE_CFG                   BIT3
  #define UART_DATA_MODE_CFG                   (BIT2|BIT1)
  #define UART_STOP_BIT_CFG                    BIT0


#define reg_uart_rt_status                       (REG_T(UART_BASE+0x04)) 
  #define UART_RX_PARITY_ERR_FLAG              BIT4
  #define UART_RX_FRAME_ERR_FLAG               BIT3
  #define UART_DATABUF_OVERFLOW_FLAG           BIT2
  #define UART_RX_FLAG                         BIT1
  #define UART_TX_FLAG                         BIT0


#define reg_uart_rxreg                           (REG_T(UART_BASE+0x08)) 
  #define UART_RX_9_DATA                       BIT8
  #define UART_RX_BYTE_DATA                    0xFF
  

#define reg_uart_txreg                           (REG_T(UART_BASE+0x0C)) 
  #define UART_TX_9_DATA                       BIT8
  #define UART_TX_BYTE_DATA                    0xFF

#define reg_uart_spbrg                           (REG_T(UART_BASE+0x10)) 
  #define UART_BAUD_CFG2                       0x03FF0000
  #define UART_BAUD_CFG1                       0x000003FF

// --------------------------------------------------------------------------------------
// ----------               SC000 Module Register Define                       ----------
// ----------                                                                  ----------
// ----------NO      : 25                                                      ----------
// ----------Addr    : 0xE000_1000~0xE000_EF90                                 ----------
// ----------Reg Num : 38 word                                                 ----------
// ----------RW Mode : word write/read                                         ----------
// --------------------------------------------------------------------------------------
                                          
#define SYST_CSR                             (REG_T(0xE000E010))
#define SYST_RVR                             (REG_T(0xE000E014))
#define SYST_CVR                             (REG_T(0xE000E018))
#define SYST_CALIB                           (REG_T(0xE000E01C))

#define SCR                                  (REG_T(0xE000ED10))
//====================================================================
// cla interface data buf
#define cl_ram    ((volatile unsigned char*)(0x00AF0000))
#define CL_RAM_BUF_SIZE 288

// 7816 ct interface baudrate config
#define BAUD_9600               0x11
#define BAUD_19200              0x12
#define BAUD_38400              0x13
#define BAUD_76800              0x14
#define BAUD_57600              0x38
#define BAUD_115200             0x18
#define BAUD_116000             0x95
#define BAUD_07000              0x91
#define BAUD_14000              0x92
#define BAUD_28000              0x93
#define BAUD_56000              0x94
#define BAUD_232000             0x96

// cla interface baudrate config
#define Transmit_Flag_mask      (~0x07)
#define Transmit_Baud_106K      0x00
#define Transmit_Baud_212K      0x04
#define Transmit_Baud_424K      0x08
#define Transmit_Baud_848K      0x0C
#define Receive_Baud_106K       0x00
#define Receive_Baud_212K       0x01
#define Receive_Baud_424K       0x02
#define Receive_Baud_848K       0x03
#define DSI_Mask                0x0C
#define DRI_Mask                0x03

#define DSI_Max                 Transmit_Baud_848K
#define DRI_Max                 Receive_Baud_848K

#define CL_ST_INT           BIT0   //interrupt occur
#define CL_ACTIVATED        ((reg_chip_status & CLA_INTERFACE) == CLA_INTERFACE)

#define INTERFACE_TYPE_SEL_CLA  0
#define INTERFACE_TYPE_SEL_CT   1
#define INTERFACE_TYPE_SEL_UART 2

#define EE_RAM_BUF_SIZE         128
#define NRAM_SIZE               0x4000
#define NRAM_AREA_BEGIN_ADR     0x00800000
#define EEPROM_AREA_BEGIN_ADR   0x00B00000

#endif

#ifndef __HW_FM347_H__
#define __HW_FM347_H__

#include "FM347.h"
#include "types.h"
#include "options.h"

#define __NOP           __nop

#define SLEEP()          do { SCR = 0x00;  __wfe(); } while (0);

//register: chip status
//#define CHIP_INTERFACE_MODE    (CT_INTERFACE | CLA_INTERFACE | CONTACT_INTERFACE)

#define MAX_TMP_BUF_SIZE  2048
extern u8 g_tmp_buf[MAX_TMP_BUF_SIZE];

typedef enum IRQn {
/******  SC000 Processor Exceptions Numbers ***************************************************/
  NonMaskableInt_IRQn         = -14,    /*!< 2 Non Maskable Interrupt                         */
  HardFault_IRQn	          = -13,    /*!< 3 SC000 Hard Fault Interrupt                     */
  SVCall_IRQn                 = -5,     /*!< 11 SC000 SV Call Interrupt                       */
  PendSV_IRQn                 = -2,     /*!< 14 SC000 Pend SV Interrupt                       */
  SysTick_IRQn                = -1,     /*!< 15 SC000 System Tick Interrupt                   */

/******  SC000IKMCU SC000 specific Interrupt Numbers ********************************************/
                                        /*!< maximum of 32 Interrupts are possible            */
  ct_clk_on_off_IRQn          = 0,
  ct_rst_n_IRQn               = 1,
  cl_on_off_IRQn              = 2,
  vcc_on_off_IRQn             = 3,
  ct_IRQn                     = 4,
  cla_IRQn                    = 5,
  gpio_IRQn                   = 6,
  uart_IRQn                   = 7,
  spi_IRQn                    = 8,
  pae_IRQn                    = 9,
  hash_IRQn                   = 10,
  bca_IRQn                    = 11,
  timer_a_IRQn                = 12,
  timer_b_IRQn                = 13,
  random_IRQn                 = 14,
  nvm_ew_IRQn                 = 15,
  cl_lvolt_IRQn               = 16,
  ct_lvolt_IRQn               = 17,
  nvm_lvolt_IRQn              = 18,
  ee_check_fail_IRQn          = 19,
  i2c_slave_IRQn              = 20,
  i2c_master_IRQn             = 21,
  crc_IRQn                    = 22,
} IRQn_Type;

/*
 * 各界面激活硬件条件:
 * [2]OTHER_ACTIVE = vddct_on_syn2 & ~ct_rst_n_syn2 & ~ct_clk_on_syn2;
 * [1]CT_ACTIVE = vddct_on_syn2 & (ct_rst_n_syn2 | ct_clk_on_syn2);
 * [0]CL_ACTIVE = cl_clk_on;
 */
#define CL_ACTIVATED     ((reg_chip_status & CLA_INTERFACE) == CLA_INTERFACE)
#define CT_ACTIVATED     ((reg_chip_status & CT_INTERFACE) == CT_INTERFACE)
#define SPI_ACTIVATED    ((reg_chip_status & CONTACT_INTERFACE) == CONTACT_INTERFACE)
#define I2C_ACTIVATED    ((reg_chip_status & CONTACT_INTERFACE) == CONTACT_INTERFACE)
#define UART_ACTIVATED   ((reg_chip_status & CONTACT_INTERFACE) == CONTACT_INTERFACE)

#define IS_DUAL_INTF()   (0 == (reg_cla_rt_common_cfg & CLA_TR_ACTIVE_MODE))

#if _configUART_DEBUG_

extern void uart_init(void);
extern void uart_send_byte (u8 bt);
extern void uart_send_word (u32 value);
extern void uart_send_buf(u8 *buf, u32 len);

#else

#define uart_init()
#define uart_send_byte(a)
#define uart_send_word(a)
#define uart_send_buf(a, b)

#endif//#if _configUART_DEBUG_


//----------------------------------------------------------------------------
void pad8_open (void);
void pad8_close (void);

#define HW_CLK_CT        (0x69)
#define HW_CLK_CL        (0x96)
void hw_clock_init (u8 mode);

void hw_startup (u8 jmp);
void hw_init (void);

#endif//__HW_FM347_H__



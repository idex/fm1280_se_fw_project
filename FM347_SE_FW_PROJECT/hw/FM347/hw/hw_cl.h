#ifndef __HW_CL_H__
#define __HW_CL_H__

#include "types.h"

#include "fm_hw_io.h"
#include "options.h"

// RAM
#define MAX_CLA_RX_BUF_SIZE     (288) 
#define MAX_CLA_TX_BUF_SIZE     (_configMAX_APDU_SIZE_)

extern u32 g_cl_rx_count;
extern u8  g_cl_rx_buf[MAX_CLA_RX_BUF_SIZE];
extern u8  g_cl_tx_buf[MAX_CLA_RX_BUF_SIZE];


#define CL_CMD_DESELECT     (CLA_TX_CRC_EN | CLA_TX_EN)
#define CL_CMD_TRANSCEIVE   (CLA_TX_CRC_EN | CLA_RX_CRC_EN | CLA_TRANSMIT_CONTROL)   
#define CL_CMD_SEND         (CLA_TX_CRC_EN | CLA_RX_CRC_EN | CLA_TX_EN)
#define CL_CMD_RECEIVE      (CLA_TX_CRC_EN | CLA_RX_CRC_EN | CLA_RX_EN)


void hw_cl_wtx (u8 cmd, u8 fwi, u8 wtxm);
void hw_cl_ioctrl (u8 param);

extern const io_ops_t cl_ops;

#endif//__HW_CL_H__


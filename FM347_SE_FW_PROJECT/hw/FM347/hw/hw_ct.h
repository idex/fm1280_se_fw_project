#ifndef _HW_CT_H
#define _HW_CT_H

#include "types.h"

#include "fm_hw_io.h"
#include "fm_proto_ct.h"

#include "options.h"

#define MAX_CT_FIFO_SIZE        (8)

#define MAX_CT_BUF_EXT          (10)
#define MAX_CT_RX_BUF_SIZE 		(278 + MAX_CT_BUF_EXT)//(262 + MAX_CT_BUF_EXT)
#define MAX_CT_TX_BUF_SIZE      (_configMAX_APDU_SIZE_)

extern u8  g_ct_tx_buf[MAX_CT_RX_BUF_SIZE];  /* Tx Buffer */
extern u8  g_ct_rx_buf[MAX_CT_RX_BUF_SIZE];  /* Rx Buffer */
extern u32 g_ct_rx_count;                    /* size of Rx buffer */   

void hw_ct_wtx (u8 cmd, u8 wi, u8 fi);
void hw_ct_ioctrl (u8 param);

extern const io_ops_t ct_ops;

#endif//_HW_CT_H

#include "hw_fm347.h"
#include "tools.h"
#include "main.h"

//#include "core_sc000.h"
//#include "cmsis_armcc.h"



u8 RAM_BUF_ATTR g_tmp_buf[MAX_TMP_BUF_SIZE] __attribute__((aligned(4)));
extern vu8 blinkCtrl;
extern vu8 sleeve_enable_flag;
extern vu32 Idex_free_timer_counter;

extern void program_tracker(u8 *data_buff, u8 data_len);
extern void idex_sleeve_timer_stop(void);
extern void fm1280_sleeve_enrollment(void);
void pad8_open (void)
{
    reg_pad_function_config |= 0x00010000; //set pad8 to use gpio5
	reg_pad_mode_config |= 0x00080000;  //打开上拉电阻，pad默认为高电平
    reg_gpio_dout = 0x0000007F;
    //pad_mode_config |= 0x00040000; //打开下拉电阻，pad默认为低电平
}

void pad8_close (void)
{
    reg_pad_function_config &= ~0x00010000; //set pad8 to use gpio5
	reg_pad_mode_config &= ~0x00080000;  //打开上拉电阻，pad默认为高电平
}


void hw_startup (u8 jmp)
{

    //hw_clock_init ();
    reg_clk_ctrl0 |= TIMER_CLK_EN|VCC_CLASS_DET_EN;

    NVIC_ISER |= TIMER_B_INT_BIT | TIMER_A_INT_BIT;
	   
    switch (jmp)
    {

        case IO_CT:
            while (! (hw_cos_flag & COS_FLG_CT_ACTIVATED)) ;
            hw_cos_flag &= ~COS_FLG_CT_ACTIVATED;
            ct_activate (&g_ct_ctx);
            break;
        case IO_CL:
            while (! (hw_cos_flag & COS_FLG_CL_ACTIVATED)) ;
            hw_cos_flag &= ~COS_FLG_CL_ACTIVATED;
            cl_activate (&g_cl_ctx, 1);
            break;

        case IO_SHUTDOWN:
        	break;            

        case IO_CL_DESELECT:
            cl_deactivate (&g_cl_ctx, false);
            break;
	case IO_SLEEVE_ENROLLMENT:
		{
		sleeve_enable_flag=0;
		Idex_free_timer_counter=0;
		idex_sleeve_timer_stop();
		blinkCtrl=0;
		sleeve_enable_flag=0x55;//sleeve enrollment in process, used for timer a;

		fm1280_sleeve_enrollment();
		break;
		}

        default:

            NVIC_ISER = VCC_ON_OFF_INT_BIT | CL_ON_OFF_INT_BIT;

            while ((reg_chip_status & (CT_INTERFACE | CLA_INTERFACE)) == 0) ;
            while (! (hw_cos_flag & (COS_FLG_CL_FIELD_ON | COS_FLG_CT_VCC_ON)));

            hw_cos_flag &= ~(COS_FLG_CL_FIELD_ON | COS_FLG_CT_VCC_ON);

            break;
    }
}

void hw_init (void)
{    
    //solve unstable bug (6502) when operating EE on CL interface
    reg_clk_ctrl0 |= BIT27; 
    
    //pad8_open ();
}

#include "hw.h"
#include "main.h"


u32 g_cl_rx_count;
u8 RAM_BUF_ATTR g_cl_rx_buf[MAX_CLA_RX_BUF_SIZE] __attribute__((aligned(4)));
u8 RAM_BUF_ATTR g_cl_tx_buf[MAX_CLA_RX_BUF_SIZE] __attribute__((aligned(4)));

void hw_cl_set_baud (u8 dsi, u8 dri)
{
    //u8 tx = baud & (BIT2 | BIT3 | BIT4);

    //interface_baud_cfg (0x00, (tx << 2) | (baud & (BIT0 | BIT1)));

    interface_baud_cfg (0x00, (dsi << 4) | dri);
}

void hw_cl_wtx (u8 cmd, u8 fwi, u8 wtxm)
{
    reg_timer_stop  |= TIMEA_STOP0 | TIMEA_STOP1;

	switch (cmd)
	{
		case CMD_DIRECT_PROCESS:
	        NVIC_ISPR |= 0x1000; //timera
			break;
		
		case CMD_WTX_START://wtx init
			reg_timera_config = 0x00060102; //8mhz clk, timer0 en, mode: 24 bit timer0+8bit timer 1;
			reg_timera_preset0=800 * (2 << fwi);
			reg_timer_int_config |= TIMEA_INT_EN0;
			reg_timer_start |= TIMEA_START0 ;
/*
	        reg_timera_config = 0x00770102; //CL CLK/4
	        reg_timera_preset0 = 400 * (2 << fwi);
	        reg_timera_preset1 = wtxm;
	        reg_timer_int_config |= TIMEA_INT_EN1;

	        reg_timer_start |= TIMEA_START0 | TIMEA_START1;
*/
			break;

		case CMD_WTX_STOP:
		case CMD_DESELECT:

        	reg_timer_int_config &= ~TIMEA_INT_EN0;
			
		default:
			break;
   	}
}

u8 hw_cl_transceive (u32 cmd, const u8 *tx_buf, u16 tx_len, u8 *rx_buf, u32 *rx_len)
{
    if (IS_DUAL_INTF() || (CMD_DIRECT_PROCESS == (cmd & CMD_DIRECT_PROCESS)) )
    {
        cmd &= ~CMD_DIRECT_PROCESS;

        // wait for the end of the previous frame
        while (reg_chip_common_ctrl & 0x0003) ;

        cla_data_transceive (cmd, (u8 *)tx_buf, tx_len, rx_buf, rx_len, (u32)(tx_buf + tx_len));
        //uart_send_word(ret);
        /* NOTE: 由于cla_data_transceive 会进入休眠，因此此处无法正常打印信息 */

        return 1;
    }

    return 0;
}

void hw_cl_ioctrl (u8 param)
{
    hw_cl_wtx (CMD_WTX_STOP, 0, 0);
    g_cl_rx_count = 0;

    switch (param)
    {
        case IO_CTRL_OPEN:
            cla_interface_init (CLA_INTERFACE_ON, g_cl_rx_buf, &g_cl_rx_count);
            break;

        case IO_CTRL_CLOSE:
            cla_interface_init (CLA_INTERFACE_OFF, NULL, NULL);
            break;

        case IO_CTRL_RESET:
            //The PICC return to receive mode.
            cla_data_transceive (CMD_RECEIVE, NULL, 0, g_cl_rx_buf, &g_cl_rx_count, 0);
            break;
    }
}

const io_ops_t cl_ops = {
  /*.ioctrl       = */ hw_cl_ioctrl,
  /*.transceive   = */ hw_cl_transceive,
  /*.wtx          = */ hw_cl_wtx,
  /*.setbaud      = */ hw_cl_set_baud,
};


#ifndef _FM_PROTO_DRV_H
#define _FM_PROTO_DRV_H

/*
 * rodata: 0x5A
 * text  : 0xf2a
 */

#include "fm_proto_types.h"
#include "fm_proto_ct.h"
#include "fm_proto_cl.h"
#include "fm_proto_apdu.h"
#include "iso7816.h"

/*
0x00    GetProtoLibVersion
0x01    apdu_get_response
0x02    apdu_expect_body        
0x03    apdu_envelope    
        
0x04    cl_activate             
0x05    cl_deactivate       
0x06    cl_process          
0x07    cl_receive          
0x08    cl_respond          
0x09    cl_wtx              
0x0A    cl_reset            
0x0B    cl_init             
        
0x0C    ct_activate             
0x0D    ct_deactivate       
0x0E    ct_process          
0x0F    ct_receive          
0x00    ct_respond          
0x01    ct_wtx              
0x12    ct_init 

0x13    
0x14    
0x15    
0x16    
0x17    
0x18    

*/

extern u32 GetProtoLibVersion (void);

extern u16  apdu_envelope (apdu_t *apdu, u8 ishead);
extern u16  apdu_get_response (apdu_t *apdu);
extern u16  apdu_expect_body (ct_ctx_t *ct_ctx, apdu_t *apdu);

extern u8   cl_activate (cl_ctx_t *cl_ctx, u8 send_en);
extern void cl_deactivate (cl_ctx_t *cl_ctx, u8 shutdown);
extern void cl_process (cl_ctx_t *cl_ctx);
extern u8   cl_receive (cl_ctx_t *cl_ctx, u8 *rx_buf, u16 rx_count);
extern void cl_respond (cl_ctx_t *cl_ctx);
extern u8   cl_wtx (cl_ctx_t *cl_ctx, u8 trigger);
extern void cl_reset (cl_ctx_t *cl_ctx);
extern void cl_init (cl_ctx_t *cl_ctx);

extern void ct_activate (ct_ctx_t *ct_ctx);
extern void ct_deactivate (ct_ctx_t *ct_ctx, u8 shutdown);
extern void ct_process (ct_ctx_t *ct_ctx);
extern u8   ct_receive (ct_ctx_t *ct_ctx, u8 *rx_buf, u16 rx_count);
extern void ct_respond (ct_ctx_t *ct_ctx);
extern void ct_wtx (ct_ctx_t *ct_ctx, u8 trigger);
extern void ct_init (ct_ctx_t *ct_ctx); 


#endif//_FM_PROTO_DRV_H

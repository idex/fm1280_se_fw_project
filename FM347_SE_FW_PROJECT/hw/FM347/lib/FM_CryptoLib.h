#ifndef _FM_CRYPTO_LIB_H_
	#define  _FM_CRYPTO_LIB_H_

	#include "FM_CryptoLib_Struct.h"

	//Get FirmwareVersion
	extern u32   GetFirmwareLibVersion (void);
	extern void GetFirmwareLibVersion_msg(u8 *buf);

	//RSA
	extern void RSA_Init (void);
	extern u8   RSA_Public (RSA_KEY *key, LPBYTE In, LPBYTE Out);
	extern u8   RSA_Private (RSA_KEY *key, LPBYTE In, LPBYTE Out);
	extern u8   RSA_GenKeyPair (RSA_KEY *key);
	extern u8   RSA_CalculateE (RSA_KEY *key, LPBYTE EList, LPBYTE E);
	
	//ECC
	extern u8   ECC_Init (ECC_KEY *key, ECC_PARAM *param);
	extern u8   ECC_GenKeyPair (ECC_KEY *key);
	extern u8   ECC_GetPublicKey (ECC_KEY *key);
	extern u8   ECC_CheckPublicKey (ECC_KEY *key);
	extern void SM2_ComputeZ (ECC_PARAM* param, AFFPOINT* pubkey, LPBYTE IDA, u32 ENTLA, LPBYTE ZBuf);
	extern u8   SM2_SignHash   (ECC_KEY *key, LPBYTE HashBuf, LPBYTE Sig);
	extern u8   SM2_VerifyHash (ECC_KEY *key, LPBYTE HashBuf, LPBYTE Sig);
	extern u8   SM2_Enc (ECC_KEY *key, LPBYTE In, u32 InLen, SM2_CIPHER *c);
	extern u8   SM2_Dec (ECC_KEY *key, SM2_CIPHER *c, LPBYTE Out, u32 *pOutLen);
	extern u8   SM2_KeyExch (ECC_KEY *key, AFFPOINT *PB, AFFPOINT *RA, AFFPOINT *RB, 
                LPBYTE rA, LPBYTE IDA, u32 ENTLA, LPBYTE IDB, u32 ENTLB, 
                LPBYTE KeyBuf, u32 KeyLen);	
	extern u8   ECDSA_SignHash (ECC_KEY *key, LPBYTE HashBuf, LPBYTE Sig);	
	extern u8   ECDSA_VerifyHash (ECC_KEY *key, LPBYTE HashBuf, LPBYTE Sig);
	extern u8   ECDH_KeyExch (ECC_KEY *key, AFFPOINT *Q, LPBYTE z);
	extern u8   ECC_PointMult (ECC_KEY *key, AFFPOINT *In,
				  		       PBIGNUM k, AFFPOINT *Out);
	extern u8   ECC_PointAdd (ECC_KEY *key, 
							  AFFPOINT *In1, AFFPOINT *In2, 
							  AFFPOINT *Out);
	extern u8   ECC_VerifyECParam (ECC_KEY *key);
	extern u8   ECC_PointCompress (ECC_KEY *key, AFFPOINT *In, u8 *PCC, PBIGNUM X);
	extern u8   ECC_PointDeCompress (ECC_KEY *key, u8 PCC, PBIGNUM X, PBIGNUM Y);
	extern u8   ECC_AffinePointCheck (ECC_KEY *key, AFFPOINT *In);

	//BCA	
	extern u8   BCA_SetKey (u8 Alg, LPBYTE Key, u8 KeyLen, u8 Mode, u8 EncDec);
	extern u8   BCA_EncDec (u8 Alg, LPBYTE Iv, LPBYTE In, LPBYTE Out, u32 Len);
	extern u8   BCA_GeneralMac (u8 Alg, LPBYTE Iv, LPBYTE In, u32 Len);
	extern void BCA_DestroyKey (void);

	//HASH	
	extern u8   HASH_Init     (u8 HashType);
	extern u8   HASH_Update   (LPBYTE In, u32 Len);
	extern u8   HASH_Final    (LPBYTE Out);
	extern u8   HASH_InitEx   (HASH_CTX *ctx, u8 HashType);
	extern u8   HASH_UpdateEx (HASH_CTX *ctx, LPBYTE In, u32 Len);
	extern u8   HASH_FinalEx  (HASH_CTX *ctx, LPBYTE Out);

	//RNG
	extern u8   RNG_GetRandNum (LPBYTE pRand, u32 Len);
	extern u8   RNG_StartUpTest (void);
	
	//OTHER	
	extern void BigNum_ModAdd(PBIGNUM X, PBIGNUM Y, PBIGNUM M, u32 LenByWord, PBIGNUM Out);
	extern void BigNum_ModSub(PBIGNUM X, PBIGNUM Y, PBIGNUM M, u32 LenByWord, PBIGNUM Out);	
	extern u8 	SecureMemCopy (PVOID MemDestA, PVOID MemDestB, 
					PVOID MemSrcA, PVOID MemSrcB, u32 LenA, u32 LenB);
	extern u8 	BigNum_ModMult (PBIGNUM X, PBIGNUM Y, 
					PBIGNUM M, u32 LenByWord, PBIGNUM Out);
	extern u8 	BigNum_ModInv (PBIGNUM X, PBIGNUM M, u32 LenByWord, PBIGNUM Out);
	extern u32  GetCryptoLibVersion (void);	//Get CryptoLibVersion


// ============算法类驱动函数列表 begin ============
	extern void PAE_ClkOn (void);		
	extern void PAE_ClkOff (void);		
	extern void Random_ClkOn (void);		
	extern void Random_ClkOff (void);		
	extern void HASH_ClockOn (void);
	extern void HASH_ClockOff (void);
	extern u8   BCA_ClockOn (void);
	extern u8   BCA_ClockOff (void);
	extern u8   BCA_BlockRun (LPBYTE pIn, LPBYTE pOut, u8 Len, u8 bMac);
	extern void BCA_SetIV (LPBYTE Iv, u8 BlockLen);
	extern void BCA_GetIV (LPBYTE Iv, u8 BlockLen);
	extern void SM2_GetParam (u8 type, LPBYTE buf);

// ============内部函数 列表 begin ============
	extern u8   RSA_Private_T (RSA_KEY *key, LPBYTE In, LPBYTE Out);
	extern void DMAXdata2XdataByte(PBIGNUM ptrXramSrc, PBIGNUM ptrXramDest, u32 len);
	extern u16  NN_Bytes(PBIGNUM a, u16 bytes);
	extern u16  FMSH_NN_Cmp (PBIGNUM a, u16 abytes, PBIGNUM b, u16 bbytes);
	extern void SM2_ECDH_KDF_SM3(AFFPOINT *U, u32 ECCP_Len, 
					LPBYTE ZA, LPBYTE ZB, LPBYTE KBuf, u32 kLen);

	extern u8   ECC_PointMult_Comb (ECC_KEY *key, PBIGNUM k, AFFPOINT *Out);
	extern void ECC_PreCombPoint (ECC_KEY *key);
	extern u8   SM2_ECC_ECMQV (ECC_KEY *key, AFFPOINT *PB,
			   	 	LPBYTE x1_Buf, AFFPOINT *RB, LPBYTE r_Buf, 
			   	 	AFFPOINT *PU, u8 nType);
	
	extern void BCA_Drng_On (void);
	extern void BCA_CloseDFA (void);
	extern void BCA_StartTB(void);
	extern void BCA_UpdateCSR (u8 value);
	extern u8   RNG_SelfTest (void);
	extern u8   RNG_SingleTest (u8 *pRandom);
	extern void RNG_GetPRNGRandNum (LPBYTE pRand, u32 Len);

	extern void FMSH_NN_HardAlg (PBIGNUM b_buf, PBIGNUM c_buf,u32 len, u8 Alg);
	extern void FMSH_NN_LongDivLongPro (PBIGNUM ptrA, PBIGNUM ptrB, u32 btLenByWord, 
					PBIGNUM ptrSh, PBIGNUM ptrR);
	extern u32  Fm_GetRandom(void);
//java中间层,大数运算的函数声明列表 End ============
#endif 

#ifndef __DRIVER_DEF_H__
#define __DRIVER_DEF_H__


//firmware drive function return value define
//high half byte flag define

  #define NVM_EW_RECOVERY_STATE_FLAG                     BIT24    //擦写操作的状态标志
                                                                 //0---正常数据擦写状态        1---防拔擦写的恢复状态
                                                                 
  #define ANTI_TEARING_BAK_DATA_VALID_FLAG               BIT21    //NVR的防拔数据备份区有效标志
                                                                 //0---数据备份区无效          1---数据备份区有效，防拔标志是全1
                                                                 
  #define ANTI_TEARING_FLAG_VALID_FLAG                   BIT20    //NVR的防拔标志状态位
                                                                 //0---防拔标志无效            1---防拔标志有效，防拔标志不是全0，也不是全1

  #define NVM_ALLONE_WORD_ADDR_FIND_FLAG                 BIT19    //第一个物理cell全1的word地址找到标志，高有效
  #define NVM_CELL_CHECK_ALL_ONE_OK_FLAG                 BIT18    //全1检查标志，物理cell检查时会刷新该标志
  #define NVM_CELL_CHECK_ALL_ZERO_OK_FLAG                BIT17    //全0检查标志，物理cell检查时会刷新该标志
                                                                 
  #define NVM_BIG_SECTOR_ACTIVE_FLAG                     BIT16    //NVM的sector有效标志
                                                                  //0---256/Sector单元被选中    1---2KB/Secotr单元被选中
  
//low half byte error code define
  
  #define FIRMWARE_OK_FLAG                               0x00    
  
  #define NVM_OP_INTERRUPT_STOP_FLAG                     0x01    //中断打断了NVM的操作流程   
  
  #define NVM_OP_LVOLT_STOP_FLAG                         0x02    //低电压报警打断了NVM的操作流程   
                         
  #define FIRMWARE_ACCESS_ADDR_ERROR_FLAG                0x03    //目标地址不是NVM有效地址   
                         
  #define FIRMWARE_ACCESS_ADDR_OVERFLOW_FLAG             0x04    //NVM的擦写操作必须以sector为单位，目前的配置有溢出
  
  #define NVM_DATA_CMP_ERROR_FLAG                        0x05    //擦写后的数据比较操作失败
  
  #define FIRMWARE_ACCESS_AUTH_ERROR_FLAG                0x06    //NVM的读写操作产生权限错误 
  
  #define NVM_RETRY_ERASE_TIME_OVER_FLAG                 0x07    //Retry erase的次数已经结束，还无法正常将sector擦成全FF
  
  #define FIRMWARE_OP_LENGTH_ERROR_FLAG                  0x08    //Nvm操作的长度配置参数错误，包括以下错误：
                                                                 //(1)	起始地址加长度超过sector边界                       
                                                                 //(2)	必须以word为基本单位进行擦写，byte长度的低2位不为0 
                                                                 //(3)	长度参数定义为全0                                  
                                                                   
  #define FIRMWARE_PARAMETER_ERROR_FLAG                  0x09    //命令参数错误
                                                                 //(1) api_03----输入的UID级数与芯片支持的级数冲突   
                                                                 
  #define FIRMWARE_EXCUTE_COLLISION_FLAG                 0x0A    
  
  #define FIRMWARE_EXCUTE_ERROR_FLAG                     0x0F    



//函数参数常量定义
//----------------------------api_02 cfg_word_write
//param cfg_word_write_mode
#define CFG_WORD_REG_WRITE_EN			BIT1		        //配置字寄存器更改使能位，高有效
#define CFG_WORD_NVM_WRITE_EN			BIT0		        //NVM中的配置字更改使能位，高有效


//----------------------------api_03 cla_infor_data_init
//param init_mode
#define INFOR_DATA_RAM_WRITE_EN			BIT1		      //缓存中的信息数据更改使能位，高有效
#define INFOR_DATA_NVM_WRITE_EN			BIT0		      //NVM中的信息数据更改使能位，高有效
//param Infor_data_sel
//uid_length_cfgBIT[7~6]
#define UID_LENGTH_CFG_4B				    0x00		      //00/01：4Byte UID，1Byte SAK 
#define UID_LENGTH_CFG_7B				    BIT7		      //10：7Byte UID, 2Byte SAK
#define UID_LENGTH_CFG_10B				 (BIT7|BIT6)	  //11：10Byte UID, 3Byte SAK
#define UID_SEL							        BIT2		      //1：初始化UID数据
#define SAK_SEL							        BIT1		      //1：初始化SAK数据
#define ATQA_SEL						        BIT0		      //1：初始化ATQA数据


//API_06_get_dataee_size 
// 返回错误码定义参见NVM操作返回常用错误码

//API_07_nvm_sector_erase
//API_08_nvm_sector_program
//API_09_nvm_wordcell_0_program
//API_0A_nvm_sector_erase_program
//API_0B_nvm_wordcell_check
//param nvm_type
//目标写入地址的sector类型。
//0x00：选择NVM中256/Sector的单元
//其他值：选择NVM中2KB/Sector的单元
#define NVM_EARSE_TYPE_256B_SECTOR		0x00
#define NVM_EARSE_TYPE_2KB_SECTOR		0xFF
//返回数据定义,返回1yte数据，高4bit标志，低4bit结果，他们的定义分别描述如下
//bit[7]	nvm_cell_1_ok_flag	全1检查标志，物理cell检查时会刷新该标志
//bit[6]	nvm_cell_0_ok_flag	全0检查标志，物理cell检查时会刷新该标志
//bit[5]	allone_word_addr_find_flag	第一个物理cell全1的word地址找到标志，高有效
//bit[4]	sector_type_flag	NVM操作物理sector的标志信号，代表当前擦写的目标NVM的具体sector定义。
//								0：256B/Sector的NVM区被选中
//								1：2KB/Sector的NVM区被选中
// 低4bit结果返回错误码定义参见NVM操作返回常用错误码

//API_0C_ct_interface_init
//param on_off_switch, 7816接口开关定义
#define CT_INTERFACE_ON		0xFF		//开
#define CT_INTERFACE_OFF	0x00		//关

//param ct_interface_cfg, CT参数配置,
//BIT[25:24]
#define CT_PARITY_MODE_EVEN	0x00			//00：Even
#define CT_PARITY_MODE_ODD	BIT24			//01：Odd
#define CT_PARITY_MODE_ONE	BIT25			//10：Always '1'
#define CT_PARITY_MODE_NONE	(BIT25|BIT24)	//11：RFU(目前的逻辑，相当于不检查parity)
//BIT[22:21]
#define CT_RX_REPEAT_EN		BIT22			//奇偶校验错，根据T=0协议自动回发error signal
#define CT_TX_REPEAT_EN		BIT21			//收到奇偶校验出错标志（error signal），根据T＝0协议自动进行回发
//BIT[20]
#define CT_CONVENTION_CFG	BIT20			//1：先收发MSB (反转电平)
//BIT[18~16]ct_tr_repeat_time
#define CT_TR_REPEAT_TIME_0		 0x00
#define CT_TR_REPEAT_TIME_1	 	(((u32)1) << 0x10)
#define CT_TR_REPEAT_TIME_2	 	(((u32)2) << 0x10)
#define CT_TR_REPEAT_TIME_3	 	(((u32)3) << 0x10)
#define CT_TR_REPEAT_TIME_4	 	(((u32)4) << 0x10)
#define CT_TR_REPEAT_TIME_5	 	(((u32)5) << 0x10)
#define CT_TR_REPEAT_TIME_6	 	(((u32)6) << 0x10)
#define CT_TR_REPEAT_TIME_7	 	(((u32)7) << 0x10)
//BIT[12]ct_tr_repeat_time
//接收过程Guard Time长度控制位
// 	0：Guard time为2 etu
// 	1：Guard time为1 etu
#define CT_RX_GUARD_TIME_CFG_1ETU	 BIT12	//1：Guard time为1 etu
//返回值定义
//BIT[0] ct_not_support	该卡片不支持ct接口
#define ERR_CT_NOT_SUPPORT		BIT0

//API_0D_ct_data_transceive
// param ct_transceive_mode
// BIT[7~3]	Tx_egt_num[4:0]	发送时插入的EGT的数目
// 当tx_egt_en为高电平时，该配置才起作用。在发送数据时会插入指定数目的EGT，最后一个byte发送时不插入EGT
// 当tx_egt_en为低电平时，所有发送的数据字节都不插入EGT
//  BIT[2]	tx_egt_en	发送时插入EGT的使能位，该位置1后，在发送流程中除了最后一个字节为了满足反向间隔时间，不插入EGT，其他byte都插入1个EGU，保证高波特率的卡机兼容性。
// BIT[1:0]	ct_transceive_mode	ISO7816接触接口的收发模式配置位
//	00：无操作 
//	01：启动接收
//	10：启动发送
//	11：启动发送并接收，硬件发送完数据后自动启动接收
#define CT_TX_EGT_NUM_0x00	0x00
#define CT_TX_EGT_NUM_0x01	(((u32)1)  << 3)
#define CT_TX_EGT_NUM_0x02	(((u32)2)  << 3)
#define CT_TX_EGT_NUM_0x03	(((u32)3)  << 3)
#define CT_TX_EGT_NUM_0x04	(((u32)4)  << 3)
#define CT_TX_EGT_NUM_0x05	(((u32)5)  << 3)
#define CT_TX_EGT_NUM_0x06	(((u32)6)  << 3)
#define CT_TX_EGT_NUM_0x07	(((u32)7)  << 3)
#define CT_TX_EGT_NUM_0x08	(((u32)8)  << 3)
#define CT_TX_EGT_NUM_0x09	(((u32)9)  << 3)
#define CT_TX_EGT_NUM_0x0A	(((u32)10) << 3)
#define CT_TX_EGT_NUM_0x0B	(((u32)11) << 3)
#define CT_TX_EGT_NUM_0x0C	(((u32)12) << 3)
#define CT_TX_EGT_NUM_0x0D	(((u32)13) << 3)
#define CT_TX_EGT_NUM_0x0E	(((u32)14) << 3)
#define CT_TX_EGT_NUM_0x0F	(((u32)15) << 3)
#define CT_TX_EGT_EN					BIT2
#define CT_TRMODE_RX_EN					BIT0
#define CT_TRMODE_TX_EN					BIT1
#define CT_TRMODE_TXRX_EN				(BIT1|BIT0)
//返回值定义 非0代表有错误产生，数据bit对应的错误代码定义如下表所示
//BIT[2]	Tx_mode_error	参数配置错误，包括
//			(1)	收发模式配置成全0，不是有效的收发模式
//			(2)	启动发送同时发送数据的长度配置成0
//			(3)	启动接收，同时接收数据的长度配置成0
//BIT[1]	Tr_collision_flag		前一次的发送或接收状态还未结束
//BIT[0]	Ct_interface_not_open	Ct当前的状态未处于有效激活状态
#define ERR_CT_TX_MODE_ERROR			BIT2
#define ERR_CT_TR_COLLISION_FLAG		BIT0
#define ERR_CT_CT_INTERFACE_NOT_OPEN	BIT1

//API_0E_cla_interface_init
//param on_off_switch, CLA接口开关定义
#define CLA_INTERFACE_ON	0xFF		//开
#define CLA_INTERFACE_OFF	0x00		//关
//param cla_anti_level_cfg	UID防冲突级数配置
//BIT[1:0]
//00：自适应
//01：1重UID
//10：2重UID
//11：3重UID
#define CLA_ANTI_LEVEL_CFG_AUTO			0x00
#define CLA_ANTI_LEVEL_CFG_1			BIT0
#define CLA_ANTI_LEVEL_CFG_2			BIT1
#define CLA_ANTI_LEVEL_CFG_3			(BIT1|BIT0)
//返回值定义 非0代表有错误产生，数据bit对应的错误代码定义如下表所示
//BIT[1]	anti_level_not_support	该卡片不支持参数对应的防冲突级别，或者防冲突级别与之前cla_infor_data_init函数初始化的UID长度不匹配
//BIT[0]	cla_not_support	该卡片不支持cla接口
#define ERR_CLA_ANTI_LEVEL_NOT_SUPPORT	BIT1
#define ERR_CLA_CLA_NOT_SUPPORT			BIT0

//API_0F_cla_data_transceive
// param cla_transceive_mode
// BIT[2:0]
// 非接触接口的收发模式配置位
// 	其他：空操作 
// 	001：启动接收
// 	010：启动普通数据发送
// 	011：启动发送并接收，硬件发送完数据后自动启动接收
// 	110：启动deselect命令发送
#define CLA_START_RX			BIT0
#define CLA_START_TX			BIT1
#define CLA_START_TXRX			(BIT1|BIT0)
#define CLA_START_TX_DESELECT	(BIT2|BIT1)
// param cla_transceive_config
//BIT[5]	cla_tx_crc_en	发送CRC使能位，高有效
//	0：发送数据不发送CRC数据
//	1：发送数据硬件自动计算CRC并发送
//BIT[4]	cla_rx_crc_en	接收CRC使能位，高有效
//	0：接收数据不校验CRC
//	1：接收数据硬件自动校验CRC，无论校验成功与否，CRC数据都进入接口缓存中
//BIT[3]	cla_tx_parity_en	发送数据带parity位，高有效
//	0：发送过程每字节末尾不带parity位
//	1:发送数据每字节末尾自动添加parity位
//BIT[2]	cla_rx_parity_en	接收数据带parity位，高有效
//	0:接收过程bit计数不计算parity位
//	1：接收过程bit计数计算parity位
//BIT[1]	cla_tx_parity_mode	选择发送数据的parity类型
//	0：偶校验
//	1：奇校验
//BIT[0]	cla_rx_parity_mode	选择接收数据的parity类型
//	0：偶校验
//	1：奇校验
#define CLA_TRMODE_TX_CRC_EN		BIT5
#define CLA_TRMODE_RX_CRC_EN		BIT4
#define CLA_TRMODE_TX_PARITY_EN		BIT2
#define CLA_TRMODE_RX_PARITY_EN		BIT1
#define CLA_TRMODE_TX_PARITY_ODD	
#define CLA_TRMODE_TX_PARITY_EVEN	BIT0


// NVR COS config data define <end>---------------------
  
  #define NVR_COS_CONFIG_DATA_VALID_FLAG                0x55AAAA55             //0xDE_0000h
  
  #define NVM_COS_CFG_MASK                              0x000000FF
  #define NVM_COS_CFG_VALID_FLAG                        0x0000005A
  
  #define CT_USER_COS_JUMP_CFG_MASK                     0xF0000000
  #define CT_USER_COS_JUMP_EN                           0x50000000
  #define CL_USER_COS_JUMP_CFG_MASK                     0x0F000000
  #define CL_USER_COS_JUMP_EN                           0x05000000
  #define USER_CODE_ENTRANCE_MASK                       0x00FFFFFF
  
  #define RAM_INIT_ENABLE_MASK                          0x0F000000
  #define RAM_INIT_ENABLE                               0x05000000
  #define RAM_INIT_START_ADDR_MASK                      0x00FF0000
  #define RAM_INIT_END_ADDR_MASK                        0x0000FF00
  #define RAM_INIT_FREQUENCY_MASK                       0x000000FF
  
  
  #define DISABLE_BOOT_FLAG_ADDR                        0x00DE01F0
  

//  NVR COS config data  define <end>---------------------

// chip_common_ctrl register(0x00E8_002C) bit define <begin>---------------------
  #define BOOT_MODE_CFG_MASK                           (BIT31|BIT30)
  #define BOOT_MODE_EN                                  BIT30

  #define FIRMWARE_COS_BIT                              0x007FFFFF
  #define FIRMWARE_COS_CFG_VALID                        BIT22
  
  #define CLA_LDO12_TRIM_CHANGE_EN                      BIT21
  #define CHIP_DFS_ADAPT_EN                             BIT20             
  #define UID_CHANGE_EN                                 BIT19
  #define SAK_CHANGE_EN                                 BIT18
  #define ATQA_CHANGE_EN                                BIT17
  
  #define NVR_SEC_ANTI_TEARING_FLAG_MASK                (BIT16|BIT15|BIT14|BIT13)
  #define NVR_SEC_BAK_AREA_VALID_FLAG_MASK              (BIT16|BIT15)
  #define NVR_SEC_ANTI_TEARING_FLAG_VALID_FLAG_MASK     (BIT14|BIT13)
  
  #define NVR_SEC3_BAK_AREA_VALID_FLAG                  (BIT16|BIT15)
  #define NVR_SEC3_ANTI_TEARING_FLAG_VALID_FLAG         (BIT14|BIT13)
  
  #define NVR_SEC2_BAK_AREA_VALID_FLAG                  BIT16
  #define NVR_SEC2_ANTI_TEARING_FLAG_VALID_FLAG         BIT14

  #define NVR_SEC0_BAK_AREA_VALID_FLAG                  BIT15
  #define NVR_SEC0_ANTI_TEARING_FLAG_VALID_FLAG         BIT13
  
  #define CT_INTERFACE_FLAG_MASK                        0x1F00  
  #define CT_BAUD_CHANGE_EN                             BIT12
  #define CT_INTERFACE_ACTIVE_FLAG                      BIT11
  #define CT_FIRST_RECEIVE_FLAG_N                       BIT10
  #define CT_INTERFACE_TRANSMIT_FLAG                    BIT9
  #define CT_INTERFACE_RECEIVE_FLAG                     BIT8
  
  #define CLA_CASCADE_LEVEL_SUPPORT                     (BIT7|BIT6)
    #define CLA_CASCADE_LEVEL_1                          0x00
    #define CLA_CASCADE_LEVEL_2                          0x40
    #define CLA_CASCADE_LEVEL_3                          0x80
    #define CLA_CASCADE_LEVEL_FLEXIBLE                   0xC0
     
  #define CLA_INTERFACE_FLAG_MASK                        0x003F  
  #define CLA_INTERFACE_OP_MASK                          0x0007  
  #define CLA_MIFARE_CMD_ACTIVE_FLAG                     BIT5   
  #define CLA_BAUD_CHANGE_EN                             BIT4
  #define CLA_INTERFACE_ACTIVE_FLAG                      BIT3
  #define CLA_INTERFACE_DESELECT_CMD_FLAG                BIT2
  #define CLA_INTERFACE_TRANSMIT_FLAG                    BIT1
  #define CLA_INTERFACE_RECEIVE_FLAG                     BIT0
  
// chip_common_ctrl register bit define <end>---------------------


// mmu_dummy register(0x00E0_0180) bit define <begin>---------------------
  #define CLA_CASCADE_LEVEL3_ACTIVE                      BIT23
  #define CLA_CASCADE_LEVEL2_ACTIVE                      BIT22
  #define CLA_HALT_STATE_FLAG                            BIT21
  #define CLA_STATE_RESET_REQ                            BIT20
  #define CLA_CASCADE_LEVEL_FINISH_FLAG                  BIT19
  #define CLA_CPU_STATE_FLAG                             BIT18
  #define CLA_MIFARE_STATE_FLAG                          BIT17
  #define CLA_READY_STATE_FLAG                           BIT16 
  #define CLA_LDO12_TRIM_CHANGE_FLAG                     BIT8

//-------------cla polling_card(-3) state--------------------

  #define CLA_CASCADE_LEVEL_3_FINISH_FLAG                BIT15
  #define CLA_CASCADE_LEVEL_2_FINISH_FLAG                BIT14
  #define CLA_CASCADE_LEVEL_1_FINISH_FLAG                BIT13
  #define CLA_SELECT_CMD_FLAG                            BIT12
  
  #define CLA_CURRENT_CASCADE_LEVEL                      (BIT11|BIT10|BIT9)
    #define CLA_CURRENT_CASCADE_LEVEL_1                   BIT9
    #define CLA_CURRENT_CASCADE_LEVEL_2                   BIT10
    #define CLA_CURRENT_CASCADE_LEVEL_3                   BIT11

//-------------cla mifare_card state-------------------------

  #define CLA_MIFARE_AUTH_PASS_FLAG                      BIT15
  #define CLA_MIFARE_AUTH_KEY_TYPE                       BIT14     //0: KEYA        1: KEYB
  #define CLA_MIFARE_TRAILER_BLOCK_WR_FLAG               BIT13
  #define CLA_MIFARE_CMD_CASCADE_FLAG                    BIT12     //0: 1st command state    1: 2nd command state    
  #define CLA_MIFARE_PROCESS_STATE_FLAG                  BIT6      //0: wait for next cmd receive    1: need access nvm
  #define CLA_MIFARE_CMD_DATA                            (BIT11|BIT10|BIT9)
    #define CLA_MIFARTE_DECREMENT_CMD                     0x0200
    #define CLA_MIFARTE_INCREMENT_CMD                     0x0600
    #define CLA_MIFARTE_RESTORE_CMD                       0x0A00
    #define CLA_MIFARTE_AUTH_CMD                          0x0400
    #define CLA_MIFARTE_WRITE_CMD                         0x0800
  
  #define CLA_MIFARE_AUTH_SECTOR_NUM                     (BIT5|BIT4|BIT3|BIT2|BIT1|BIT0)    

//-------------cla cpu_card(-4) state------------------------

  #define CLA_TX_BAUD_CFG                               (BIT7|BIT6|BIT5|BIT4)
    #define CLA_106K_TX_BAUD                              0x00
    #define CLA_212K_TX_BAUD                              0x10
    #define CLA_424K_TX_BAUD                              0x20
    #define CLA_848K_TX_BAUD                              0x30
    #define CLA_1690K_TX_BAUD                             0x40
    #define CLA_3390K_TX_BAUD                             0x50
    #define CLA_6780K_TX_BAUD                             0x60

  #define CLA_RX_BAUD_CFG                               (BIT1|BIT0)
    #define CLA_106K_RX_BAUD                              0x00
    #define CLA_212K_RX_BAUD                              0x01
    #define CLA_424K_RX_BAUD                              0x02
    #define CLA_848K_RX_BAUD                              0x03

// mmu_dummy register bit define <end>---------------------


// dummy_reg1 register(0x00E9_0800) bit define <begin>---------------------

  #define CT_SEND_INCLUDE_SW_BYTE_FLAG                  (BIT28|BIT27)
  #define CT_SEND_INCLUDE_SW_HBYTE_FLAG                 BIT28
  #define CT_SEND_INCLUDE_SW_LBYTE_FLAG                 BIT27
  #define CURRENT_SOURCE_DEC_EN                         BIT20
  #define SYSTICK_FIRM_USE_FLAG                         BIT19 
  
  #define POWER_SOFT_SEL_EN                             BIT9
  #define POWER_SOFT_PRIORITY_SEL                       BIT8
  #define PRETEST_WITH_NVM_PROG_EN                      BIT7
  #define NVM_CHECK_INDEPENDENT_MODE_EN                 BIT6
  #define NVM_PRE_PROG_MODE_EN                          BIT5
  #define NVM_RETRY_ERASE_MODE_EN                       BIT4
  #define VDD16_LVOLT_RESET_CHIP_EN                     BIT3
  #define MIFARE_KEYB_MODE                              BIT2
  #define MIFARE_BAK_AREA_CFG                           (BIT1|BIT0)

// dummy_reg1 register(0x00E8_0018) bit define <begin>---------------------


// dummy_reg2 register(0x00E9_0804) bit define <begin>---------------------
  

// dummy_reg2 register(0x00E8_0018) bit define <begin>---------------------


// nvm_operation parameter define <begin>---------------------
  
  #define DATA_NVM_SECTOR_SEL                           0x00
  #define CODE_NVM_SECTOR_SEL                           0x01
  
  #define NVM_PHYSICAL_CELL_CHECK_MODE                  0x48  
  #define NVM_PHYSICAL_CELL_PROGRAM_MODE                0x81   
  #define NVM_PROGRAM_CHECK_MODE                        0x09  
  #define NVM_ERASE_CHECK_MODE                          0x4A   
  #define NVM_ERASE_PROGRAM_CHECK_MODE                  0x0B  
  #define NVM_PHYSICAL_CELL_PROGRAM_CHECK_MODE          0xC9              
  
  
//  nvm_operation parameter define <end>---------------------


#endif

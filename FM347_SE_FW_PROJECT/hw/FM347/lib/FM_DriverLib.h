#ifndef __DRIVER_LIB_H__
#define __DRIVER_LIB_H__

#include "FM_DriverDef.h"

/*
���	����s
0x00	chip_start_up_process
0x01	bootload_code_exit
0x02	cfg_word_write
0x03	cla_infor_data_init
0x04	nvr_data_write
0x05	nvm_type_check
0x06	get_dataee_size
0x07	nvm_sector_erase
0x08	nvm_sector_program
0x09	nvm_wordcell_0_program
0x0A	nvm_sector_erase_program
0x0B	nvm_wordcell_check
0x0C	ct_interface_init
0x0D	ct_data_transceive
0x0E	cla_interface_init
0x0F	cla_data_transceive
0x10	interface_baud_cfg
0x11	disable_boot
0x12	return_boot
0x13    nvr_data_read
0x14    nvr_antitear_data_recovery
0x15    crc_cal
0x16    interrupt_exit_before_longjump
0x17    nvm_chip_erase
0x18 	m1_card_generate 
0x19    m1_block_read 
0x1A    m1_block_write
0x1B    RFU
0x1C    RFU
0x1D    RFU
0x1E    RFU
0x1F    GetDriverLibVersion
*/

/* �������� */
extern void chip_start_up_process(void) ;
extern void bootload_code_exit(u32 user_code_entrance_addr);
extern u32 cfg_word_write(u8 cfg_word_write_mode, u8 cfg_word_num, u32 cfg_word_wr_data) ;
extern u32 cla_infor_data_init(u8 init_mode, u8 infor_data_cfg, u8 *infor_data_buf) ;
extern u32 nvr_data_write(u32 write_nvr_addr, u8* write_data_buf, u32 write_data_byte_length, u32 write_mode);
extern u32 nvm_type_check(u32 nvm_op_dest_addr);
extern u32 get_dataee_size(void);
extern u32 nvm_sector_erase(u8 nvm_type,u32 nvm_op_dest_addr) ;
extern u32 nvm_sector_program(u8 nvm_type,u32 nvm_op_sr_buf_addr, u32 nvm_op_dest_addr, u32 nvm_op_byte_length) ;
extern u32 nvm_wordcell_0_program(u8 nvm_type,u32 nvm_op_dest_addr,u32 nvm_op_byte_length) ;
extern u32 nvm_sector_erase_program(u8 nvm_type, u32 nvm_op_sr_buf_addr, u32 nvm_op_dest_addr) ;
extern u32 nvm_wordcell_check(u8 nvm_type, u32 nvm_op_dest_addr, u32 nvm_op_byte_length, u32* first_allone_word_addr_in_sector) ;
extern u32 ct_interface_init(u8 on_off_switch, u32 ct_interface_cfg) ;
extern u32 ct_data_transceive(u32 ct_transceive_mode, u8* ct_tx_data_buf, u32  ct_tx_data_length, u8* ct_rx_data_buf, u32*  ct_rx_data_length, u32 ct_tx_data_buf_end_addr ) ;
extern u32 cla_interface_init(u8 on_off_switch, u8* cla_rx_data_buf, u32* cla_rx_data_length) ;
extern u32 cla_data_transceive(u32 cla_transceive_mode, u8* cla_tx_data_buf, u32  cla_tx_data_length, u8* cla_rx_data_buf, u32* cla_rx_data_length, u32 cla_tx_data_buf_end_addr ) ;
extern u32 interface_baud_cfg(u8 interface_type_sel, u32 baud_cfg) ;
extern u32 disable_boot(u32 app_data_start_sector_addr, u32 app_data_end_sector_addr);
extern u32 return_boot(void);
extern u32 nvr_data_read(u32 nvr_data_rd_addr, u32 nvr_data_rd_byte_length, u8* nvr_data_rd_buf_addr);
extern u32 nvr_antitear_data_recovery(u32 recovery_mode);
extern u32 crc_cal(u8 crc_mode, u32 crc_cal_start_addr, u32 crc_cal_length, u32* crc_data_value);
extern void interrupt_exit_before_longjump(void) ;
extern u32 nvm_chip_erase(void);
extern u32 m1_card_generate(void);
extern u32 m1_block_read (u32 block_num, u32 *m1_read_data_buf);
extern u32 m1_block_write(u32 block_num, u32 *ptrRamAddr);

extern u32 GetDriverLibVersion(void);
#endif

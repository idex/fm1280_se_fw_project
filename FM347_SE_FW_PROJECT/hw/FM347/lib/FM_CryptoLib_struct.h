#ifndef _FL_CRYPTO_STRUCT_H_ 
	#define _FL_CRYPTO_STRUCT_H_

	#ifndef s8
		#define s8  signed char	
	#endif

	#ifndef s16
		#define  s16 signed short 	
	#endif

	#ifndef s32
		#define  s32 signed long 	
	#endif 

	#ifndef u8
		#define u8  unsigned char	
	#endif

	#ifndef u16
		#define  u16 unsigned short 	
	#endif

	#ifndef u32
		#define  u32 unsigned long 	
	#endif 

    #ifndef LBYTE
        #define LBYTE  u8   
    #endif

    #ifndef PBIGNUM
        #define PBIGNUM  u8 *
    #endif

    #ifndef LPBYTE
        #define LPBYTE  u8 *
    #endif
    
    #ifndef LPWORD
		#define LPWORD  u32 *
    #endif
    
    #ifndef PVOID
        #define PVOID  void *
    #endif
    
    //定义ECC计算最大位数,512位对应64个字节
    #ifndef ECCMAXLEN
        #define ECCMAXLEN  0x40      
    #endif
	#define HASH_WORD_LEN_MAX		0x08				//定义最大HASH字个数
	#define HASH_BYTE_LEN_MAX		HASH_WORD_LEN_MAX*4	//定义最大HASH字节个数
	#define HASH_BYTELEN_SM3_256	0x20

	#ifndef memref
		#define memref u32
	#endif
	#ifndef jsize
		#define jsize u16
	#endif

	#define RESULT_ERROR   		0x45
	#define RESULT_OK      		0xBA
	#define RESULT_INFINITY     0x8C		//点乘结果为无穷远点
 
	#define RSA_MULT			0x00   		//模乘
	#define RSA_ADD				0x01   		//模加
	#define RSA_SUB				0x03   		//模减

	#define PAE_LEVEL0_F		0x5A		//Level 0:0x5A,"F"(function)
	#define PAE_LEVEL1_M		0x2D 		//Level 1:0x2D,"M"(middle)
	#define PAE_LEVEL2_S		0x96		//Level 2:0x96,"S"(secure)
	#define PAE_LEVEL3_H		0xE1		//Level 3:0xE1,"H"(high secure)
	
	typedef  struct               	
	{
		u32 BitLen;					//RSA模长 
		PBIGNUM  E;					//公钥E
		PBIGNUM  N; 				//模N
		PBIGNUM  D; 				//私钥D
		PBIGNUM  P;					//素因子P
		PBIGNUM  Q;					//素因子Q
		PBIGNUM  Dp;				//CRT私钥Dp
		PBIGNUM  Dq;				//CRT私钥Dq
		PBIGNUM  Iq;				//CRT私钥Iq
		u8 CRT;						//是否CRT模式标志,0 - 非CRT，1-CRT
		u8 ProtectLevel;			//RSA计算防护级别设置-F/M/S/H
	}RSA_KEY;

    // 彷射坐标点数据结构
    typedef  struct
    {
        LBYTE  Aff_XY[ECCMAXLEN*2] ; //彷射点x坐标和y坐标,顺序排放
    }AFFPOINT;  
    //ECC参数数据结构
    typedef  struct                  
    {
        u32 BitLen;                  //ECC模长
        PBIGNUM  p;                  //ECC曲线参数_素数p
        PBIGNUM  a;                  //ECC曲线参数_系数a
        PBIGNUM  b;                  //ECC曲线参数_系数b
        PBIGNUM  n;                  //ECC曲线参数_阶n
        AFFPOINT *G;                 //ECC曲线参数_基点G
        u8  h;                       //h=1,2,3,4
        u8  AIsMinus3;               //1：a=-3算法配置。a=-3时可选择加速实现模式。
        u32 *CombTable;              //疏状点乘时预计算点缓冲区指针
    }ECC_PARAM;
	#define ECC_BitLen 			param->BitLen
	#define ECC_p 				param->p
	#define ECC_a 				param->a
	#define ECC_b 				param->b
	#define ECC_n 				param->n
	#define ECC_G 				param->G
	#define ECC_h		 		param->h
	#define ECC_AIsMinus3 	 	param->AIsMinus3
	#define ECC_CombTable 		param->CombTable

    //ECC密钥对数据结构
    typedef  struct                     
    {
        ECC_PARAM *param;           //ECC曲线参数
        PBIGNUM   d;                //私钥d指针
        AFFPOINT *Q;                //公钥Q指针
        u8 ProtectLevel;            //ECC计算防护级别设置-F/M/S/H
    }ECC_KEY;

    //SM2密文数据结构
    typedef  struct                     
    {
    	LPBYTE PCC;
        LPBYTE XCoordinate;           	//X分量
        LPBYTE YCoordinate;           	//Y分量
        LPBYTE SM2_Hash;         		//杂凑值
        LPBYTE CipherText;             	//密文
    	u32    CipherTotalLen;         	//密文总长度=C1+C2+C3的长度
    }SM2_CIPHER;

   //HASH_ALG算法类型定义
    #define    ALG_MD5              0x01
    #define    ALG_RIPEMD160        0x02
    #define    ALG_SM3              0x03
    #define    ALG_SHA_1            0x04
    #define    ALG_SHA_224          0x05
    #define    ALG_SHA_256          0x06

    //HASH算法数据结构定义
    typedef struct
    {
        u32 Len[2];                 //保存收到的Message的总字节长度
        u8  Msg[64];                //暂存未处理的Message内容（不满64字节）
        u8  H[32];                  //hash摘要值，最长为256bit
        u8  Alg;                    //计算hash算法的类型，定义同上（见HASH_ALG）
        u8  First;                  //第一次运算标志
        u8  IV_Len;                 //IV计算长度
        u8  Digest_Len;             //输出摘要长度
    }HASH_CTX;

    #define ALG_TDES_2KEY           0x01
    #define ALG_TDES_3KEY           0x02
    #define ALG_DES                 0x03
    #define ALG_AES                 0x04
    #define ALG_SM4                 0x05
    #define ALG_SSF33               0x06
    #define ALG_SM7                 0x07
    #define ALG_SM1                 0x08
	  
#ifndef NULL
    #define NULL            ((void *)0)
#endif
    #define BCA_MODE_ECB        0x00
    #define BCA_MODE_CBC        0x01    
    
    #define BCA_DECRYPT         0x00
    #define BCA_ENCRYPT         0x01

#endif

/******************************************************************************
* \file  idxSeEmkDefines.h
* \brief Defines for the SE matcher library
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2018 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#ifndef __IDX_SE_EMK_DEFINES_H__
#define __IDX_SE_EMK_DEFINES_H__

#include <stdint.h>

#define PACKED_ATTRIBUTE

//~~~~~~~~~~~~~~~ HOST I/F related definition ~~~~~~~~~~~~~~~
#define GETVERSION_CMD     0x30
#define DELETERECORD_CMD   0x11
#define READ_DATA_CMD      0x50
#define PREPAREIMAGE_CMD   0x02
#define GETTEMPLATE_CMD    0x05
#define MERGETEMPLATES_CMD 0x06
#define MATCHTEMPLATES_CMD 0x07
#define STOREBLOB_CMD      0x18
#define LISTRECORDS_CMD    0x12
#define SETPARAMETER_CMD   0x09
#define GETPARAMETER_CMD   0x08

#define GETVERSION_CMD_DATA_SIZE     (0)
#define DELETERECORD_CMD_DATA_SIZE   (2)
#define READ_DATA_CMD_DATA_SIZE      (4)
#define PREPAREIMAGE_CMD_DATA_SIZE   (1)
#define GETTEMPLATE_CMD_DATA_SIZE    (1)
#define MERGETEMPLATES_CMD_DATA_SIZE (4)
#define MATCHTEMPLATES_CMD_DATA_SIZE (6)
#define STOREBLOB_CMD_DATA_SIZE      (5)

#define HDR_SIZE           ( 6 )   // including CRC
#define TEMPL_SZ_OFF       ( 0 )
#define MESSAGE_CHUNK      ( 0x40 )
#define MESSAGE_LAST       ( 0x80 )
#define RETURN_SIZE        ( 0x10 )
#define SE_MESSAGE_MASK    ( 0x40 )

#define FLAGS_PHASE_1      ( 0x00 )
#define FLAGS_PHASE_2      ( 0x10 )
#define MASK_LEN           ( 0 )
#define NUM_TEMPLATES      ( 1 )


//----------------- ERROR Codes ~~~~~~~~~~~~~~~~~~ 
#ifndef ERR_VERSION
#define ERR_VERSION            0x6800
#endif

#ifndef ERR_FLASH_ERASE
#define ERR_FLASH_ERASE        0x6501
#endif

#ifndef ERR_FLASH_WRITE
#define ERR_FLASH_WRITE        0x6502
#endif
#ifndef ERR_MATCHER_EXTRACT_INIT
#define ERR_MATCHER_EXTRACT_INIT    	0x6301
#endif
#ifndef ERR_MATCHER_EXTRACT_PROCESS
#define ERR_MATCHER_EXTRACT_PROCESS    0x6302
#endif

#define IDEX_ERR_NO_MATCH       0x6300   // templates do not match
#define IDEX_ERR_COMM           0x6741   // communication problem (EIO)
#define IDEX_ERR_COMM_2           0x6641   // handshake error 
#define IDEX_ERR_COMM_3           0x6642   // handshake error ,recover fail
#define IDEX_ERR_COMM_4           0x6643   // data transfer error ,



#define IDEX_ERR_BAD_QUALITY    0x6747   // quality is too bad
#define IDEX_ERR_TIMEOUT        0x6748   // user timeout
#define IDEX_ERR_REC_NOT_FOUND  0x6A83   // Record not found
#define IDEX_ERR_BAD_PARAMETERS 0x6A80   // Bad parameters
#define IDEX_ERR_NOT_SUPPORTED  0x6A81   // function not supported
#define IDEX_ERR_UNKNOWN        0x6969   // Unknown error
#define IDEX_SUCCESS            0x9000   // Success

// Template Info Definitions
#define IMG_QUAL_OFF       ( 0 )
#define TEMPL_QUAL_OFF     ( 2 )
#define NUM_FEAT_OFF       ( 4 )
#define TEMPL_SIZE_OFF     ( 6 )

// From idxBaseDefines.h
typedef struct PACKED_ATTRIBUTE __idex_UnitVersion
{
	unsigned char version ;
	unsigned char revision ;
	unsigned short build ;
} idex_UnitVersion_t ;

typedef enum __idex_SeType
{
    SE_SLE78 = 1,
    SE_THD89,
    SE_CIU9872B_01,        
}
idex_SeType_t;

typedef struct PACKED_ATTRIBUTE __idex_FlashTemplateEntry
{
	unsigned char finger;
	unsigned char touch;
	unsigned char blobIndex;
} idex_FlashTemplateEntry_t;

#ifndef __GNUC__
//#pragma pack(push)
//#pragma pack(1)
#endif

// This defines the maximum transfer size allowed in the mcuComms callback for send or receive - must be a multiple of 8 bytes
#define MAX_SE_TRANSFER_SZ    ( 224 )

typedef enum __idx_SeEmkSecurityLevel
{
    SE_EMK_SECURITY_LVL_4  = 4,
    SE_EMK_SECURITY_LVL_5  = 5,
    SE_EMK_SECURITY_LVL_6  = 6,
    SE_EMK_SECURITY_LVL_7  = 7,
    SE_EMK_SECURITY_LVL_8  = 8,
    SE_EMK_SECURITY_LVL_9  = 9,
    SE_EMK_SECURITY_LVL_10 = 10,
    SE_EMK_SECURITY_LVL_12 = 12,
    SE_EMK_SECURITY_LVL_15 = 15,
    SE_EMK_SECURITY_LVL_20 = 20,
}
idx_SeEmkSecurityLevel_t;

typedef enum __idx_SeEmkDeleteFingerMode
{
	SE_EMK_DELETE_ENROLLMENT_TEMPLATE = 0,
	SE_EMK_DELETE_VERIFICATION_TEMPLATE = 1,
}
idx_SeEmkDeleteFingerMode_t;

typedef enum __idx_SeEmkType
{
    SE_EMK_TYPE_IDEX_SECURE = 1,
}
idx_SeEmkType_t;

typedef struct PACKED_ATTRIBUTE __idx_SeEmkInfo
{
    idx_SeEmkType_t type;
    idex_UnitVersion_t version;
    uint8_t maxTouchesPerFinger;
    uint8_t maxEnrolledFingers;
    uint8_t minScratchPadRamSz;       // Minimum size in kBytes of scratch pad RAm
    uint8_t minTemplateFlashSz;       // Minimum size in kBytes of template Flash
}
idx_SeEmkInfo_t;


#ifndef __GNUC__
//#pragma pack(pop)
#endif

#endif //__IDX_SE_EMK_DEFINES_H__

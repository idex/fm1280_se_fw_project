#include <stdlib.h>
#include <stdint.h>
#include "idxSeEmkAPI.h"
#include "..\ctrl\hostif\serialInterface.h"

#include "idxHalLed.h"

uint8_t *pTest;
uint8_t errPoint = 0xff;

uint16_t idxSeEmkGetInfo(idx_SeEmkInfo_t* pSeEmkInfo){
	return IDEX_SUCCESS;
}
	
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * IMM ( enrolls & match API ) 
* info struct contains : { bOrdinal, bOptions, data[CL]} CL = command length(bytes)
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
typedef struct { uint8_t *info; uint8_t csize;} cmd_t; 

static uint8_t initc[]          = {0x00,0x00,0x02};
static uint8_t initcl[]         = {0x00,0x00,0x03};
static uint8_t uninit[]         = {0x01,0x00};
static uint8_t deleteRecord[]   = {0x11,0x00,0xff,0xff};
static uint8_t prepareImage[]   = {0x02,0x00,0x03};
static uint8_t getTemplate[]    = {0x05,0x00,0x00};
static uint8_t mergeTemplate[]  = {0x06,0x00,0x03,0x00};
static uint8_t matchTemplate[]  = {0x07,0x00,0x06,0x00,0x00};
static uint8_t enrollcmd[]      = {0x17,0x00,0x03,0x03,0x01,0x58,0x01,0x01,0x00,0x59,0x00};
//static uint8_t LedBlinkOn[]     = {0x09,0x00,0x87,0x10,0x88};
static uint8_t LedBlinkOn[]     = {0x09,0x00,0xb7,0x10,0x88};


void data_initial (void)
{
	initc[0]=0x00;initc[1]=0x00;initc[2]=0x02;
	initcl[0]=0x00;initcl[1]=0x00;initcl[2]=0x03;
	uninit[0]=0x01;uninit[1]=0x00;
	deleteRecord[0]=0x11;deleteRecord[1]=0x00;deleteRecord[2]=0xff;deleteRecord[3]=0xff;
	prepareImage[0]=0x02;prepareImage[1]=0x00;prepareImage[2]=0x03;
	getTemplate[0]=0x05;getTemplate[1]=0x00;getTemplate[2]=0x00;
	mergeTemplate[0]=0x06;mergeTemplate[1]=0x00;mergeTemplate[2]=0x03;mergeTemplate[3]=0x00;
	enrollcmd[0]=0x17;enrollcmd[0]=0x00;enrollcmd[0]=0x03;enrollcmd[0]=0x03;
	enrollcmd[0]=0x01;enrollcmd[0]=0x58;enrollcmd[0]=0x01;enrollcmd[0]=0x01;
	enrollcmd[0]=0x00;enrollcmd[0]=0x59;enrollcmd[0]=0x00;
	LedBlinkOn[0]=0x09;LedBlinkOn[0]=0x00;LedBlinkOn[0]=0xb7;LedBlinkOn[0]=0x10;LedBlinkOn[0]=0x88;
}





/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Send Commands 
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/





static uint16_t sendCmds(cmd_t cmds[], uint8_t numCmds){
  uint8_t i;
  uint16_t wCode;
  for ( i = 0; i < numCmds; i++ )
  { 
		uint8_t bOrdinal = cmds[i].info[0];   
    uint8_t bOptions = cmds[i].info[1];   
		uint8_t csize    = cmds[i].csize - 2;
		uint8_t *cdata   = csize ? &cmds[i].info[2] : NULL ; 
		
		wCode = idxMcuComms(bOrdinal, bOptions, cdata, csize, NULL,NULL);  
		if ( wCode != IDEX_SUCCESS ) return wCode;
	}
	return IDEX_SUCCESS;
}	

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * IMM (enroll) 2 cmds entry poin
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

uint16_t idxSeEmkMcuEnroll(void)
{
  
  uint16_t wCode;

  cmd_t cmds[] = {
    {initc,     sizeof(initc)}, 
    {enrollcmd, sizeof(enrollcmd)}};
	data_initial();

  idxCom.state->mode = 1;
	wCode = sendCmds(cmds, sizeof(cmds)/sizeof(cmds[0]));
	return wCode; 
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * IMM (verify) 4 cmds entry point
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
uint16_t idxSeEmkImmMatch(void)
{
  
  uint16_t wCode;

  cmd_t cmds[] = {
//    {initcl,        sizeof(initcl)},
    {prepareImage , sizeof(prepareImage)},
    {getTemplate,   sizeof(getTemplate)},
    {matchTemplate, sizeof(matchTemplate)}};
  data_initial();

  idxCom.state->mode = 1;
	wCode = sendCmds(cmds, sizeof(cmds)/sizeof(cmds[0]));
	return wCode; 
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * IMM (enroll) SE sends multiple cmds + LED control
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define NUM_FINGERS  ( 2 )
#define NUM_TOUCHES  ( 2 )
uint16_t idxSeEmkImmEnroll(void)
{
  
  uint16_t i,j;
  uint16_t wCode;
  cmd_t cmdsProlog[] = {
    {initcl,        sizeof(initcl)}, 
    {deleteRecord , sizeof(deleteRecord)},
	};
  cmd_t cmdsBody[] = {
    {LedBlinkOn,    sizeof(LedBlinkOn)},
    {prepareImage , sizeof(prepareImage)},
    {getTemplate,   sizeof(getTemplate)},
    {mergeTemplate, sizeof(mergeTemplate)},
	};
  cmd_t cmdsPostlog[] = {
		{uninit, sizeof(uninit)},
//    {storeTemplate, sizeof(storeTemplate)}
  };
  data_initial();

  wCode = sendCmds(cmdsProlog,sizeof(cmdsProlog)/sizeof(cmdsProlog[0]));
  if ( wCode != IDEX_SUCCESS ) return wCode;

  wCode = idxHalSetLedMode(IDX_HAL_CARD_LED_START,0);
  if ( wCode != IDEX_SUCCESS ) return wCode;
	
	for ( i = 0; i < NUM_FINGERS; i++ )
  {
		for ( j = 0; j < NUM_TOUCHES; j++ ){
		
			wCode = sendCmds(cmdsBody, sizeof(cmdsBody)/sizeof(cmdsBody[0]));
			if ( wCode != IDEX_SUCCESS ) return wCode;
		}
  }
  wCode = sendCmds(cmdsPostlog,sizeof(cmdsPostlog)/sizeof(cmdsPostlog[0]));
  if ( wCode != IDEX_SUCCESS ) return wCode;

  return IDEX_SUCCESS;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Sleeve-IMM (enroll) cmds test entry poin - SLE78
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
uint16_t idxSeEmkImmSleeveEnroll(void)
{return IDEX_SUCCESS;}

#if 0
/*****************************************************************************
 * IDM (dummy) API calls 
 *
 ******************************************************************************/
uint16_t idxSeEmkSingleEnroll(uint8_t *pScratchPadRam[],
                        uint8_t *pTemplateFlash,
                        uint32_t szTemplateFlash,        // Bytes
                        uint8_t touch,
                        uint8_t finger,
                        uint8_t numTouchesPerFinger,
                        uint8_t numEnrolledFingers,
                        idx_SeEmkSecurityLevel_t securityLevel)
{  return IDEX_SUCCESS ; }

uint16_t idxSeEmkSleeveEnroll(uint8_t *pScratchPadRam[],
                        uint8_t *pTemplateFlash,
                        uint32_t szTemplateFlash,        // Bytes
                        uint8_t numTouchesPerFinger,
                        uint8_t numEnrolledFingers,
                        idx_SeEmkSecurityLevel_t securityLevel)
{  return IDEX_SUCCESS ; }

uint16_t idxSeEmkEnroll(uint8_t *pScratchPadRam[],
                        uint8_t *pTemplateFlash,
                        uint32_t szTemplateFlash,        // Bytes
                        uint8_t numTouchesPerFinger,
                        uint8_t numEnrolledFingers,
                        idx_SeEmkSecurityLevel_t securityLevel)
{  return IDEX_SUCCESS ; }

uint16_t idxSeEmkMatch(uint8_t  *pScratchPadRam[],
                       uint8_t  *pTemplateFlash,
                       uint32_t szTemplateFlash,    // bytes
                       uint8_t  numTouchesPerFinger,
                       uint8_t  numEnrolledFingers,
                       uint8_t imgsrc,
                       uint16_t mwfTimeBudget,
                       idx_SeEmkSecurityLevel_t securityLevel)
{  return IDEX_SUCCESS ; }

uint16_t idxSeEmkGetInfo(idx_SeEmkInfo_t* pSeEmkInfo)
{
  return(IDEX_SUCCESS);
}

uint16_t idxSeEmkDeleteFinger(uint8_t *pTemplateFlash,
                              uint32_t szTemplateFlash,
                              uint8_t finger,
                              uint8_t numTouchesPerFinger,
                              int8_t mode)
{  return(IDEX_SUCCESS);}
#endif
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

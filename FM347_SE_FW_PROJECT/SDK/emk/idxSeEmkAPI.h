/******************************************************************************
* \file  idxSeEmkAPI.h
* \brief header for SE Enroll/Match Kit API
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2018 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#ifndef __IDX_SE_EMK_API_H__
#define __IDX_SE_EMK_API_H__

#include "idxSeEmkDefines.h"

/**
* @brief Identify the point where an error occurred.
*
* This is a debugging aid. It is set to a value when an error occurs (that causes
* a function to return a value other than \ref IDEX_SUCCESS) that allows the point
* of the failure to be determined.
*/
extern uint8_t errPoint;

// API FUNCTION DEFINITIONS


/**
* @brief Initialise the SE-EMK module
*
* @return IDEX_SUCCESS
*/
uint16_t idxSeEmkInit(void);

/**
* @brief Function to retrieve matcher version along with other info
*
*
* @param[out] pSeEmkInfo will contain the matcher info
*
* @return IDEX_SUCCESS
*/
uint16_t idxSeEmkGetInfo(idx_SeEmkInfo_t* pSeEmkInfo);


#ifndef IMM_ONLY

/**
* @brief Function used to perform enroll
*
*
* @param[in] pScratchPadRam is a pointer to the externally allocated (contiguous) RAM sector for use by the matcher
* @param[in] pTemplateFlash is a pointer to the template storage area in Flash
* @param[in] szTemplateFlash indicates the size of the template storage area in kBytes (used for erase callback)
* @param[in] numTouchesPerFinger indicates the number of touches in each template
* @param[in] numFingers indicates the number templates
* @param[in] securityLevel indicates the required security level
*
* @return IDEX_SUCCESS if successful,
*         or another error code if not successful
*/
uint16_t idxSeEmkEnroll(uint8_t  *pScratchPadRam[],
                        uint8_t  *pTemplateFlash,
                        uint32_t szTemplateFlash,
                        uint8_t  numTouchesPerFinger,
                        uint8_t  numEnrolledFingers,
                        idx_SeEmkSecurityLevel_t securityLevel);

uint16_t idxSeEmkSleeveEnroll(uint8_t  *pScratchPadRam[],
                        uint8_t  *pTemplateFlash,
                        uint32_t szTemplateFlash,
                        uint8_t  numTouchesPerFinger,
                        uint8_t  numEnrolledFingers,
                        idx_SeEmkSecurityLevel_t securityLevel);

uint16_t idxSeEmkSingleEnroll(uint8_t  *pScratchPadRam[],
                        uint8_t  *pTemplateFlash,
                        uint32_t szTemplateFlash,
                        uint8_t  touch,
                        uint8_t  finger,
                        uint8_t  numTouchesPerFinger,
                        uint8_t  numEnrolledFingers,
                        idx_SeEmkSecurityLevel_t securityLevel);

/**
* @brief Function used to perform match
*
*
* @param[in] pScratchPadRam is a pointer to the externally allocated (contiguous) RAM sector for use by the matcher
* @param[in] pTemplateFlash is a pointer to the template storage area in Flash
* @param[in] szTemplateFlash indicates the size of the template storage area in kBytes (used for erase callback)
* @param[in] numTouchesPerFinger indicates the number of touches in each template
* @param[in] numFingers indicates the number templates
* @param[in] securityLevel indicates the required security level
*
* @return IDEX_SUCCESS if successful,
*         IDEX_ERR_NO_MATCH if no match
*/
uint16_t idxSeEmkMatch(uint8_t *pScratchPadRam[],
                       uint8_t *pTemplateFlash,
                       uint32_t szTemplateFlash,
                       uint8_t  numTouchesPerFinger,
                       uint8_t  numEnrolledFingers,
                       uint8_t  imgsrc,
                       uint16_t mwfTimeBudget,
                       idx_SeEmkSecurityLevel_t securityLevel);

/**
* @brief Function used to delete enrollment of verification templates from the flash
*
*
* @param[in] pTemplateFlash is a pointer to the template storage area in Flash
* @param[in] szTemplateFlash indicates the size of the template storage area in kBytes (used for erase callback)
* @param[in] finger to remove from flash
* @param[in] numTouchesPerFinger indicates the number of touches in each template
* @param[in] mode indicates the delete mode (enrollment or verification template)
*
* @return IDEX_SUCCESS if successful,
*         IDEX_ERR_REC_NOT_FOUND if no record found
*/
uint16_t idxSeEmkDeleteFinger(uint8_t *pTemplateFlash,
                              uint32_t szTemplateFlash,        // Bytes
                              uint8_t finger,
                              uint8_t numTouchesPerFinger,
                              int8_t mode);
#else

uint16_t idxSeEmkMcuEnroll(void);
uint16_t idxSeEmkImmMatch(void);
uint16_t idxSeEmkImmEnroll(void);
uint16_t idxSeEmkImmSleeveEnroll(void);

#endif

                               
#endif //__IDX_SE_EMK_API_H__

/******************************************************************************
 Copyright 2014-2019 IDEX Biometrics ASA. All Rights Reserved.
 www.idexbiometrics.com

 IDEX Biometrics ASA is the owner of this software and all intellectual
 property rights in and to the software. The software may only be used together
 with IDEX fingerprint sensors, unless otherwise permitted by IDEX Biometrics
 ASA in writing.

 This copyright notice must not be altered or removed from the software.

 DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 Biometrics ASA has no obligation to support this software, and the software is
 provided "AS IS", with no express or implied warranties of any kind, and IDEX
 Biometrics ASA is not to be liable for any damages, any relief, or for any
 claim by any third party, arising from use of this software.

 Image capture and processing logic is defined and controlled by IDEX Biometrics
 ASA in order to maximize FAR/FRR performance.
******************************************************************************/
/******************************************************************************************
*	Copyright 1998 - 2016	Roger Bauchspies - All rights reserved
*	Copyright 2015 - 2019	IDEX Biometrics ASA - All Rights Reserved. www.idexbiometrics.com
*	CONFIDENTIAL INFORMATION - DO NOT DISTRIBUTE
*
*  This source code is the property of IDEX Biometrics ASA. The source and compiled
*  code may only be used together with IDEX fingerprint sensors.
*  This copyright notice must not be modified or removed from the source code.
******************************************************************************************/

#ifndef _IDEX_M_INTERFACE_H_

    #define _IDEX_M_INTERFACE_H_

	// PRAGMA_MESSAGE macro
	#ifndef PRAGMA_MESSAGE
		#define _IDEXSTRINGIZE(x) #x
		#define _IDEXSSTRINGIZE(x) _IDEXSTRINGIZE(x)

		#if _MSC_VER
			#define PRAGMA_MESSAGE(x) __pragma (message ("~~~~~-===[" _IDEXSSTRINGIZE(x) " : " __FILE__ " line " _IDEXSSTRINGIZE(__LINE__) "]>"))
		#else
			#define PRAGMA_MESSAGE(x)
		#endif
	#endif

	#if __C251__
		#ifndef PARM_4
			#define PARM_4
			#pragma parm4
		#endif
		#define message(a)
	#endif

	#include	<stdint.h>
	
    // *** to be enabled only for internal development testing, DO NOT OVERRIDE ***
	#ifndef IDEX_DEV_MODE												
		#define IDEX_DEV_MODE (0)
	#endif

	#ifndef kStatusCallbackEnabled	// can be overridden on compiler command line
		#if _MSC_VER
			// Status Callback - only in Windows builds
			#define kStatusCallbackEnabled	(1)
		#else
			// status callback disabled in all other builds
			#define kStatusCallbackEnabled	(0)
		#endif
	#endif
	
	#ifndef EnableOption1a
		#define     EnableOption1a                          (1)
	#endif
	
    // couple NVM Option 2 with Option1a can be decoupled too for a different context
    #if EnableOption1a
	    #ifndef EnableNVMOptimization
		    #define     EnableNVMOptimization                   (2)
	    #endif
		    // Enable/Disable MWF Based Latency Control
		    #define EnableMWFBasedControl                       (1) // Limit search based on commulative MWF value
		    #define MWFBasedControlThreadSafe					(1) // MWFBasedControl is thread-safe
		    #define EnableMatchResultBitProtection              (1) // protect against single bit error/attacks on the match result reported
		    #define EnableMatchEntropyCalc                      (1) // Match Entropy related
		    #define     EnableSRAMFlush                         (1) // Erase Verify HighRes Related data from MCU SRAM after usage
            PRAGMA_MESSAGE(Built with OPTION 1A ENABLED)
    #else
        // disable the following features for legacy
	    #ifndef EnableNVMOptimization
		    #define     EnableNVMOptimization                   (0)		
	    #endif
		    #define EnableMWFBasedControl                       (0) // Disable Limit search based on commulative MWF value for legacy	
		    #define MWFBasedControlThreadSafe					(0) // MWFBasedControl is thread-safe
		    #define EnableMatchResultBitProtection              (0) // Disable protect against single bit error/attacks on the match result reported for legacy
		    #define EnableMatchEntropyCalc                      (0) // Disable Match entropy for legacy - needs changes on MCU side so disabling for now 
		    #define     EnableSRAMFlush                         (0) // Erase Verify HighRes Related data from MCU SRAM after usage
            PRAGMA_MESSAGE(Built with OPTION 1A NOT ENABLED)
    #endif

	#define     DynamicHashTableConstruct                   (1) // construct HashTable for templates instead of saving them in NVM

    #if EnableNVMOptimization != 0
	    #define   Enable32BitWriteToDataMemory              (1)
	    #if Enable32BitWriteToDataMemory
            PRAGMA_MESSAGE(32-BIT DATA WRITE ENABLED)
        #else
            PRAGMA_MESSAGE(32-BIT DATA WRITE DISABLED)
	    #endif

	    #if EnableNVMOptimization == 1
            PRAGMA_MESSAGE(Built with NVM V1 and OPTION 1A ENABLED)
	    #elif EnableNVMOptimization == 2
            PRAGMA_MESSAGE(Built with NVM V2 and OPTION 1A ENABLED)
	    #endif
	
    #else
        PRAGMA_MESSAGE(Built with NVM - OPTION 1A NOT ENABLED)
    #endif // EnableNVMOptimization == 1

    #if EnableMWFBasedControl == 1
        PRAGMA_MESSAGE(Built with MWF Based Latency Control ENABLED)
    #endif // EnableNVMOptimization == 1
	
    #if EnableMatchResultBitProtection == 1
        PRAGMA_MESSAGE(Built with Match Result Protection ENABLED)
    #endif // EnableNVMOptimization == 1

    #if EnableMatchEntropyCalc == 1
        PRAGMA_MESSAGE(Built with Match Entropy Calculation ENABLED)
    #endif // EnableNVMOptimization == 1

	// 		---------------------------			Predefined SecurityLevel Settings			---------------------------
	#define		kIDEX_Security_50						(50)			// very high - maximum
	#define		kIDEX_Security_45						(45)
	#define		kIDEX_Security_40						(40)			// high
	#define		kIDEX_Security_35						(35)
	#define		kIDEX_Security_30						(30)			// moderate high
	#define		kIDEX_Security_25						(25)
	#define		kIDEX_Security_20						(20)			// medium	- cc max
	#define		kIDEX_Security_15						(15)
	#define		kIDEX_Security_12						(12)			// 
	#define		kIDEX_Security_10						(10)			// nominal
	#define		kIDEX_Security_9						(8)				// 
	#define		kIDEX_Security_8						(9)				// 
	#define		kIDEX_Security_7						(7)				// 
	#define		kIDEX_Security_6						(6)				// 
	#define		kIDEX_Security_5						(5)				// 
	#define		kIDEX_Security_4						(4)				// minimum
	
	// 		---------------------------			Matcher settings			---------------------------
	#define		kTMMatchMethodMask_S1					(0xf0)
	#define		kTMMatchMethodVerifyRange_S1			(0x00)			// this is used for testing, general use would be kTMStopAtFirstMatch_S1
	#define		kTMMatchMethodAbsID_S1					(0x10)			// Not currently intended for use, treat as placeholder
	#define		kTMStopAtFirstMatch_S1					(0x20)			// Use this for best performance. Stops upon finding the first match meeting the threshold
	
	#define		kTMMatchSecurityTypeMask_S1				(0x0f00)
	#define		kTMMatchSecurityType_0_Mask_S1			(0x0000)		// this is the normal test method, fast, effective and delivers standard benchmark results
	#define		kTMMatchSecurityType_1_Mask_S1			(0x0100)		// this is an advanced security feature. Should not be used for general matching, slower,

	// 		---------------------------		Registration settings		---------------------------
	#define		kRegistrationMask_S1						(0x0f)
	#define		kMatchNoRegistration_S1						(0x00)		// matching only, no registration in effect
	#define		kMatchWithDynamicFastRegistration_0_S1		(0x01)		// match and then determine if new data should be added to template, return template update status
	#define		kMatchWithDynamicMSRegistration_0_S1		(0x02)		// match and then determine if new data should be added to template, return template update status, medium confidence
	#define		kMatchWithDynamicMSRegistration_1_S1		(0x03)		// match and then determine if new data should be added to template, return template update status, medium confidence
	#define		kMatchWithDynamicMSRegistration_2_S1		(0x04)		// match and then determine if new data should be added to template, return template update status, medium confidence
	#define		kMatchWithDynamicMSRegistration_3_S1		(0x05)		// match and then determine if new data should be added to template, return template update status, medium confidence
	#define		kMatchWithDynamicHSRegistration_0_S1		(0x06)		// match and then determine if new data should be added to template, return template update status, High Confidence
	#define		kMatchWithDynamicHSRegistration_1_S1		(0x07)		// match and then determine if new data should be added to template, return template update status, High Confidence
	#define		kMatchWithDynamicHSRegistration_2_S1		(0x08)		// match and then determine if new data should be added to template, return template update status, High Confidence
	#define		kMatchWithDynamicHSRegistration_3_S1		(0x09)		// match and then determine if new data should be added to template, return template update status, High Confidence
	#define		kRegistrationAcceptAll_S1					(0x0a)		// registration uses all finger inputs be blindly included in template creation
	#define		kStartNewRegistrationTemplate_S1			(0x0b)		// begins a new registration template - afterward during kRegistrationAcceptAll_S1 only this template must be presented
	
	// 		---------------------------		User error codes		---------------------------
    #if EnableMatchResultBitProtection
	    #define		kMatcher_NoErrorNoMatchS1						(0)//(0x55)		// no error, no match/registration
	    #define		kMatcher_MatchFoundS1							(0x2f)		// Match is successful
	    #define     kMatcher_InterimResult                          (0x5a)
    #else
	    #define		kMatcher_NoErrorNoMatchS1						(0)		// no error, no match/registration
	    #define		kMatcher_MatchFoundS1							(1)		// Match is successful
    #endif // EnableMatchEntropyCalc

	// 		---------------------------		User error codes - IDEX_SetupRDS		---------------------------
	#define		kFailureToInitialize_ExistingNonNULL_PtrS1		(-10)	// RDS may already be initialized
	#define		kFailureToAllocateMemoryS1						(-11)	// possible cause is malloc failure
	#define		kExternalMemPtrNot4ByteAlignedS1				(-12)	// external memory ptr is not 4byte aligned
	#define		kExternalMemPtrNullS1							(-13)	// external memory ptr is NULL

	// 		---------------------------		User error codes - IDEX_SubmitFingerImageToMatcherRDS		---------------------------
	#define		kErrorFP_RDS_NotAllocatedS1						(-21)	// RDS not allocated or is corrupted
	#define		kErrorFP_RDS_RAW_ImageAddressAlteredS1			(-22)	// signals the address of RDS.availableMemToHoldSourceImagePtr has been altered externally
	#define		kMatcherErrorImageSizeS1						(-23)	// Source image size out of range
	#define		kProcessAbortedExternallyS1						(-24)	// an external request was received to exit matcher
	#define		kErrorCC_SE_FingerCntErrorS1					(-25)	// error in finger or enrollment counts
	#define		kErrorCC_FlashWriteErrorS1						(-26)	// error in flash write
	#define		kErrorCC_ProcessingStepErrorS1					(-27)	// error in SE/MCU Specific Defines request

	#define		kRawImagePtrNULL_S1								(-31)	// NULL Raw Image Ptr
	#define		kTemplateErrorNULL_PtrS1						(-32)	// Template NULL Ptr
	#define		kTemplatePtrNot4ByteAlignedS1					(-33)	// template ptr is not 4byte aligned
	#define		kTemplateErrorCheckSumS1						(-34)	// stored template failed checksum
	#define		kTemplateErrorVersionTypeS1						(-35)	// stored template is not compatible with matcher
	#define		kTemplateErrorMatcherConfigurationS1			(-36)	// stored template is not using same matcher configuration
	#define		kTemplateErrorDataSizeS1						(-37)	// stored template size is not as expected

	#define		kSensorImageDataFailureMCU_S1					(-40)	// Failure to decode MCU new sensor image data
	#define		kSE_To_MCU_CommFailure_S1						(-41)	// Failure of the communications between SE & MCU
	#define		kSE_To_MCU_VersionFailure_S1					(-42)	// Mismatch Library version SE & MCU
	#define		kSE_To_MCU_FlashError_S1						(-43)	// Failure with the MCU Flash
	#define		kSE_DataSizeError_S1							(-44)	// size of data error

	#define		kLackOfRidgeDefinitionSourceImageS1				(-50)	// insufficient source ridge information

	#define		kTemplateFullError								(-75)	// template is already full

	// 		---------------------------		SE/MCU Specific Defines		---------------------------
	#define		kDataForEnrollS1								(1)		// signals an enrollment operation
	#define		kDataForMatchS1									(2)		// signals a match operation
	
	// 		---------------------------		SE/MCU Specific Defines used internally		---------------------------
	#define		kProcessStageEnrollS1							(1)		// for internal use of specific stages in processing
	#define		kProcessStageMatch_1_S1							(2)		// for internal use of specific stages in processing
	#define		kProcessStageMatch_2_S1							(3)		// for internal use of specific stages in processing

    #if EnableOption1a
	    //#define     kProcessStageEnrollLowResS1                   (4) 
	    #define     kStageEnrollLowResMaxDataSize                   (4096) 
    #endif // EnableOption1a
	    #define		kGetNewSensorDataMaxSizeS1						(1024)

    #if EnableMWFBasedControl
        #define kmaxEnrollFingerCount (2) // Set it to max possible fingers for now ( in practice we use 2 only )
	    #define UseSingleMWFBudget    (1) // Use single Budget for all finger templates
    #endif // MWFBasedControl

    typedef struct _MWFScratch
    {
	    uint16_t MWFCalculated;
    #if EnableMWFBasedControl		
	    int16_t  MWFMaxAllowance;
	    int16_t  MWFRemainingPixelAllowance[kmaxEnrollFingerCount];
    #endif
    }
        MWFScratch;

    #if EnableMatchEntropyCalc

	typedef struct _ExtractEntropy
	{
		uint16_t NumRidgePixels;
		uint8_t  NumRidges;

	}
        Extract_Entropy;

	typedef struct _Compare_Entropy
	{
		uint8_t  NumMatchedPrimaryRidges;
		uint8_t  NumMatchedSecondaryRidges;
    #if UseSingleMWFBudget
		uint16_t MWFConsumed; 
    #else
		uint32_t MWFConsumed; // sum of all fingers worse case is 16bits x 10 Fingers = 20bits
    #endif // UseSingleMWFBudget
	}
        Compare_Entropy;

	typedef struct _Match_Entropy
	{
		Extract_Entropy ExtractEntropy;
		Compare_Entropy CompareEntropy;
		int8_t          MatchResult;

	}
        Match_Entropy;

    // because of the lack of uint64_t in SLE/SLC Secure Elements this function is only enabled for Windows builds
    #if _MSC_VER
        /* compute canonical matcher entropy value to be used as a matcher signature

            MTCH-488 spec for matcher signature:

            MatchResult  MWF Consumed  NumMatchedSecondaryRidges  NumMatchedPrimaryRidges  NumRidges  NumRidgePixels
            (8 bits)     (16 bits)     (8 bits)                   (8 bits)                 (8 bits)   (16 Bits)
            63...56      55...40       39...32                    31...24                  23...16    15...0
        */
        uint64_t IDEX_M_ComputeMatchSignature(Match_Entropy * pMatchEntropy);

        // macros to pull out each piece of the returned 64-bit match entropy value
        #define IDEX_M_GET_MATCHRESULT_FROM_MATCHSIGNATURE(me)                ((uint8_t)  ((me & 0xFF00000000000000L) >> 56))
        #define IDEX_M_GET_MWFCONCONSUMED_FROM_MATCHSIGNATURE(me)             ((uint16_t) ((me & 0x00FFFF0000000000L) >> 40))
        #define IDEX_M_GET_NUM_MATCHEDSECONDARYRIDGES_FROM_MATCHSIGNATURE(me) ((uint8_t)  ((me & 0x000000FF00000000L) >> 32))
        #define IDEX_M_GET_NUM_MATCHEDPRIMARYRIDGES_FROM_MATCHSIGNATURE(me)   ((uint8_t)  ((me & 0x00000000FF000000L) >> 24))
        #define IDEX_M_GET_NUM_RIDGES_FROM_MATCHSIGNATURE(me)                 ((uint8_t)  ((me & 0x0000000000FF0000L) >> 16))
        #define IDEX_M_GET_NUM_RIDGEPIXELS_FROM_MATCHSIGNATURE(me)            ((uint16_t)  (me & 0x000000000000FFFFL)       )
    #endif

    #endif // EnableMatchEntropyCalc

	// 		---------------------------		Data Structures Mobile		---------------------------
	typedef struct
	{
		uint8_t *	  templateTestResults;				  // contains test values against all templates tested for testing only. Set to NULL if not used
		uint8_t **	  templateDataBasePtr;				  // pointer to array of pointers to stored templates, each stored template must be 4 byte aligned

		uint8_t *	  RAW_ImagePtr;						  // ptr to memory holding raw sensor image, must be on 8 ByteBoundary and a multiple of 8
		uint8_t *	  newTemplatePtrReturn;				  // ptr to a template, NULL if not used, available until IDEX_FreeMemoryRDS

		uint8_t *	  availableMemToHoldSourceImagePtr;	  // base ptr of available to hold source image data, must be no larger than max image dimensions
		uint8_t *	  externalAllocatedMatcherBaseMemPtr; // base ptr available to hold source image data, must be no larger than max image dimensions, must be 4 byte aligned
		void *		  fpInternalUse;					  // internal use only, 
		
	#if 1 == IDEX_DEV_MODE
		void *		  Internal_Verification;			  // for IDEX internal development testing only
	#endif
		
		uint32_t	  matcherOperationRequestFlag;		  // what do we want the matcher to perform? Choose from options within Matcher & Registration settings above
		
		uint16_t	  RAW_InputImageWidth;				  // pixel width of source image
		uint16_t	  RAW_InputImageHeight;				  // pixel height of source image
		
		uint16_t	  matchAttemptTarget;				  // template number for targeted verification, 0 if entire database
		uint16_t	  NumberOfStoredUniqueFingers;		  // number of templates in database to be verified against, 0 if none, 1 if target one at matchAttemptTarget, If successful registration: incremented by 1
		
		uint16_t	  newTemplateSize;					  // size of template available from registration template creation or update
		uint8_t		  isTemplateFull;					  // true/false, true - signals the template is now full
		uint8_t		  abortProcess;						  // used to abort the matching process, matching will be terminated immediately - results will be indeterminate (true (1) aborts/false (0) continues)
		
		uint8_t		  isNewTemplateAvailable;			  // true/false, true - signals a new template is available
		uint8_t		  resultSecurityLevelMatch;			  // actual match security level found
		uint8_t		  targetSecurityLevelDRegistration;	  // set to the desired Dynamic Registration security level
		uint8_t		  targetSecurityLevelMatch;			  // set to the desired match security level

		// new items to track match against which sample in template (for DB purposes)
		uint8_t       fingerSampleMatchedIndex;			  // 0 = no match, >0 1-ordinal of the finger sample in the template that matched
		uint8_t *     pFingerSampleMatchedIndex;          // same as above, but for multiple test values

    #if kStatusCallbackEnabled
		uint8_t *     pRidgeDataBitmap;					  // ridge data bitmap buffer (~35K) - needs to be here for thread safety
    #endif

    #if EnableMWFBasedControl
		MWFScratch    MWFScratchBuffer;                   // Scratch buffer for MWF consumption calculations
    #endif // MWFBasedControl

    #if EnableMatchEntropyCalc
		Match_Entropy MatchEntropy;                       // Match Entropy from the last match attempted
    #endif//EnableMatchEntropyCalc
	}
        IDEX_MatcherDataS1;
	
	// 		---------------------------		Data Structures CC		---------------------------
	typedef struct
	{
		uint8_t						enrollTouchCount;					// the current source image count of the total per finger enrollment, { 1, 2, .., N}, N up to enrollMaxTouchCount
		uint8_t						enrollMaxTouchCount;				// the maximum number of enrollTouchCount to expect, { 4, 5, .., N}, N typically between 4 to 8
		uint8_t						enrollFingerCount;					// the current source finger count of the enrollment, { 1, 2, .., N}, N up to enrollMaxFingerCount
		uint8_t						enrollMaxFingerCount;				// the maximum number of enrollFingerCount to expect, { 1, 2, .., N}, N typically 2 but up to 4 can be expected
	}
        IDEX_FingerCntInfoS1;

	typedef struct
	{
		uint8_t *					SE_StoredTemplateFlashBasePtr;		// base ptr of consecutively stored templates, must be 4 byte aligned
		uint8_t *					externalAllocatedMatcherBaseMemPtr;// base ptr available to hold source image data, must be no larger than max image dimensions, must be 4 byte aligned

    #if EnableOption1a || EnableNVMOptimization != 0
		uint8_t *                   externalAllocatedMatcherBaseDATAMemPtr;// base ptr available to hold source image data in another RAM, must be no larger than max image dimensions, must be 4 byte aligned
    #endif // EnableOption1a || EnableNVMOptimization
		
		uint8_t *					SensorImageDataMemoryInternalPtr;	// base ptr to hold the SensorImageData obtained from the MCU

    /*#if EnableOption1a
		uint8_t *					LowResEnrollImageDataMemoryInternalPtr;	// base ptr to hold the Low Res data for optiona 1a  obtained from the MCU during enrollment
    #endif // EnableOption1a*/

		uint8_t *					SE_FlashLocation_1_Ptr;			// For internal use, communicates to EnrollMatchApp of flash locations for data write
		uint8_t *					SE_FlashLocation_2_Ptr;			// For internal use, communicates to EnrollMatchApp of flash locations for data write
		uint8_t *					Data_1_Ptr;						// For internal use, communicates to EnrollMatchApp of memory location
		uint8_t *					Data_2_Ptr;						// For internal use, communicates to EnrollMatchApp of memory location
		uint8_t *					Data_3_Ptr;						// For internal use, communicates to EnrollMatchApp of memory location
		
	#if 1 == IDEX_DEV_MODE
		void *						Internal_Verification;				// for IDEX internal development testing only
		uint8_t						InternalT_Num;						// for IDEX internal development testing only
		uint8_t						InternalTemp1;						// for IDEX internal development testing only
		uint16_t					InternalTemp2;						// for IDEX internal development testing only
	#endif
	#if 2 == IDEX_DEV_MODE
		uint8_t						InternalT_Num;						// for IDEX internal development testing only
		uint8_t						InternalT_Num1;						// for IDEX internal development testing only
	#endif

		uint16_t					DataSize_1;							// For internal use, communicates to EnrollMatchApp of data size
		uint16_t					DataSize_2;							// For internal use, communicates to EnrollMatchApp of data size
		uint16_t					DataSize_3;							// For internal use, communicates to EnrollMatchApp of data size
		uint16_t					sensorImageDataSize;				// size of data in SensorImageDataMemoryInternalPtr

		uint8_t						resultSecurityLevelMatch;			// actual match security level found
		uint8_t						targetSecurityLevelMatch;			// set to the desired match security level
		uint8_t						EnrollOrMatch;						// flag if the operation is an enrollment or a match
		uint8_t						ProcessStage;						// internal flag outlining stage of process for SE
		
		IDEX_FingerCntInfoS1		enrollMatchFingerInfo;			// see structure above 
		
    #if EnableMWFBasedControl
		MWFScratch                  MWFScratchBuffer;
    #endif // MWFBasedControl

    #if EnableMatchEntropyCalc
		Match_Entropy               MatchEntropy;
    #endif//EnableMatchEntropyCalc

		uint8_t						abortProcess;						// used to abort the matching process, matching will be terminated immediately - results will be indeterminate (true (1) aborts/false (0) continues)
		uint8_t						internalDataWM;						// for internal use only
		uint8_t						resultFingerMatched;				// finger matched from original enrollment order
		uint8_t						InternalUse1;						// reserved for internal use
	}
        IDEX_ccSE_MatcherDataS1;
	
	typedef struct
	{
		uint8_t *					RAW_ImagePtr;						// ptr to memory holding raw sensor image, must be on 8 ByteBoundary and a multiple of 8
		uint8_t *					SensorImageDataPtrReturn;			// ptr to a template, NULL if not used, available until IDEX_FreeMemoryRDS

		uint8_t *					availableMemToHoldSourceImagePtr;	// base ptr of available to hold source image data, must be no larger than max image dimensions
		uint8_t *					externalAllocatedMatcherBaseMemPtr;// base ptr available to hold source image data, must be no larger than max image dimensions, must be 4 byte aligned

		uint16_t					RAW_InputImageWidth;				// pixel width of source image
		uint16_t					RAW_InputImageHeight;				// pixel height of source image

		uint16_t					SensorImageDataSize;				// size of template available from registration template creation or update
		uint8_t						abortProcess;						// used to abort the matching process, matching will be terminated immediately - results will be indeterminate (true (1) aborts/false (0) continues)
		uint8_t						isNewTemplateAvailable;				// true/false, true - signals a new template is available

		uint8_t						Enroll;						        // flag if the operation is an enrollment or a match
	}
		IDEX_ccMCU_REx_MatcherDataS1;
	
	typedef struct
	{
		uint8_t *					MCU_StoredDataBasePtr;				// base ptr of consecutively stored templates, must be 4 byte aligned
		uint8_t *					externalAllocatedMatcherBaseMemPtr; // base ptr available to hold source image data, must be no larger than max image dimensions, must be 4 byte aligned

    #if EnableOption1a
		uint8_t *					RPS_Ptr;							// For internal use, communicates to EnrollMatchApp of flash locations for data write
		uint8_t *					RS_Ptr;							// For internal use, communicates to EnrollMatchApp of flash locations for data write
        uint8_t *					Lowres_Ptr;						// For internal use, communicates to EnrollMatchApp of flash locations for data write
    #if EnableNVMOptimization != 0
		uint8_t *					LowresCompressed_Ptr;   			// For pointing to compressed low res data specific to NVM Option1a
    #endif // EnableNVMOptimization == 1
		uint8_t *					Output_Ptr;						// For internal use, communicates to EnrollMatchApp of flash locations for data write
		uint8_t *					RPS_Flash_Ptr;			// For internal use, communicates to EnrollMatchApp of flash locations for data write
    #if DynamicHashTableConstruct == 0
		uint8_t *					RS_Flash_Ptr;			// For internal use, communicates to EnrollMatchApp of flash locations for data write
    #endif // DynamicHashTableConstruct
		uint8_t *                   Match_Results_Ptr;
    #else
		uint8_t *					Data_1_Ptr;						// For internal use, communicates to EnrollMatchApp of flash locations for data write
		uint8_t *					Data_2_Ptr;						// For internal use, communicates to EnrollMatchApp of flash locations for data write

		uint8_t *					MCU_FlashLocation_1_Ptr;			// For internal use, communicates to EnrollMatchApp of flash locations for data write
		uint8_t *					MCU_FlashLocation_2_Ptr;			// For internal use, communicates to EnrollMatchApp of flash locations for data write
    #endif
	#if 1 == IDEX_DEV_MODE
		void *						Internal_Verification;				// for IDEX internal development testing only
	#endif
    #if EnableOption1a
		uint16_t					RPS_Size;							// For internal use, communicates to EnrollMatchApp of flash locations for data write
        uint16_t					RS_Size;							// For internal use, communicates to EnrollMatchApp of flash locations for data write
        uint16_t					Lowres_Size;						// For internal use, communicates to EnrollMatchApp of flash locations for data write
    #if EnableNVMOptimization != 0
		uint16_t    				LowresCompressed_Size;   			// For pointing to compressed low res data specific to NVM Option1a
    #endif // EnableNVMOptimization == 1
        uint16_t					Output_Size;						// For internal use, communicates to EnrollMatchApp of flash locations for data write
    #else
		uint16_t					DataSize_1;							// For internal use, communicates to EnrollMatchApp of flash locations for data write
		uint16_t					DataSize_2;							// For internal use, communicates to EnrollMatchApp of flash locations for data write
    #endif

		uint8_t						abortProcess;						// used to abort the matching process, matching will be terminated immediately - results will be indeterminate (true (1) aborts/false (0) continues)
		uint8_t						flashPosition;						// flash position offset based on enrollment touch counts
		uint8_t						ProcessStage;						// flag for enroll or match operations
		uint8_t						unused;								// 4 byte align padding, reserved for future use
	}
		IDEX_ccMCU_M_MatcherDataS1;


    // --------------------------- Informational structures ---------------------------
    //                             pack on 32-bit boundaries

    // NOTE: used for both SE and MCU information for IDEX Distributed Matcher (IDM and IDM-L), so memory and template sizes are not included
    typedef struct
    {
        char    FuzeID[28];             // space for null-terminated FuzeID string "IDEXMTCHyymmdd-ttt-sssss" (NOTE: sizeof(FuzeID) must by divisible by 4)

        uint32_t buildDate;             // date of build in hex format, year, month, day, ex: 0x2017.05.01

        uint16_t baseCodeVersion;       // base code version
        uint8_t  baseConfigurationType; // base configurations
        uint8_t  baseOS_Platform_Type;  // any specifics for the environment

        uint8_t  templateVersion;       // template verion
        uint8_t  tuningVariant;         // tuning variant
        uint16_t capabilities;          // capabilities mask -- see kCapabilities... below

        uint16_t maxMCUTemplateSize;    // maximum MCU template size
        uint16_t maxSETemplateSize;     // maximum SE template size

        uint8_t  matcherVersion;        // build version
        uint8_t  matcherRevision;       // build revision
        uint16_t matcherBuildNumber;    // build number
    }
        IDEX_ccMatcherInfoS1;           // matcher build structure information, good for debug build ID

    // For IDEX MCU Matcher (IMM)
    typedef struct
    {
        char     FuzeID[28];            // space for null-terminated FuzeID string "IDEXMTCHyymmdd-ttt-sssss" (NOTE: sizeof(FuzeID) must by divisible by 4)

        uint32_t buildDate;             // date of build in hex format, year, month, day, ex: 0x2017.05.01

        uint16_t baseCodeVersion;       // base code version
        uint8_t  baseConfigurationType; // base configurations
        uint8_t  baseOS_Platform_Type;  // any specifics for the environment

        uint16_t maxTemplateSize;       // maximum template size
        uint8_t  templateVersion;       // template verion
        uint8_t  tuningVariant;         // tuning variant

        uint8_t  matcherVersion;        // build version
        uint8_t  matcherRevision;       // build revision
        uint16_t matcherBuildNumber;    // build number

        uint16_t capabilities;          // capabilities mask -- see kCapabilities... below
        uint8_t  unused[2];             // unused

        uint32_t memorySize;            // size of memory used
    }
        IDEX_MatcherInfoS1;             // matcher build structure information, good for debug build ID


	// values for buildOptions

    // watermarking is off unless explicitly enabled
    #if __C251__
        #define kEnableRidgeRegionWatermarkS1 (0)
        #define kEnableSensorLockWatermarkS1  (0)
    #else
        #ifndef kEnableRidgeRegionWatermarkS1
            #define kEnableRidgeRegionWatermarkS1 (0)
        #endif
        #ifndef kEnableSensorLockWatermarkS1
            #define kEnableSensorLockWatermarkS1  (0)
        #endif
    #endif

 	#if kEnableRidgeRegionWatermarkS1
        PRAGMA_MESSAGE(Ridge Region Watermarking Enabled)
	#else
        PRAGMA_MESSAGE(Ridge Region Watermarking NOT Enabled)
	#endif

	#if kEnableSensorLockWatermarkS1
        PRAGMA_MESSAGE(Sensor Lock Watermarking Enabled)
	#else
        PRAGMA_MESSAGE(Sensor Lock Watermarking NOT Enabled)
	#endif

	// Values for IDEX_MatcherInfoS1 and IDEX_ccMatcherInfoS1 capabilities field
	#define kCapabilitiesRidgeMarkEnabled					(0x0001)
	#define kCapabilitiesSensorLockWatermarkingEnabled		(0x0002)
	#define kCapabilitiesExternalAlloc						(0x0004)
	#define kCapabilitiesScoreDuplication					(0x0008)
	#define kCapabilities_EnableOption1A					(0x0010)
	#define kCapabilities_MCU_SE_EndianTranslationEnabled	(0x0020)
	#define kCapabilities_SE_MCU_EndianTranslationEnabled	(0x0040)
    #define kCapabilities_EnableNVMOptimization_1           (0x0080)
    #define kCapabilities_EnableNVMOptimization_2           (0x0100)

    #ifdef __cplusplus
    extern "C"
    {
    #endif

	typedef struct
	{
		uint32_t Offset;
		uint16_t Size;
	}
        OffsetSizePair;

	// 		---------------------------		Function Prototypes, IDEX MCU Matcher Specific		---------------------------
	void      IDEX_GetFP_MaxImageDimensions_S1(uint16_t * width, uint16_t * height);  // maximum image dimensions for input
	long     IDEX_GetFP_MemSize_S1(void);									          // memory size requirement for matcher
	uint16_t IDEX_GetFP_TemplateMaxBufferSize_S1(void);					              // IMM has a fixed max template size 
	void     IDEX_GetFP_MatcherInfo_S1(IDEX_MatcherInfoS1 * info);			          // return matcher build information, good for debug build ID

	void     IDEX_ZeroRDS(IDEX_MatcherDataS1 * fpRDS);                               // Zero the Matcher Environment
	long     IDEX_SetupRDS(IDEX_MatcherDataS1 * fpRDS);						         // Setup the Matcher Environment
	long     IDEX_SubmitFingerImageToMatcherRDS(IDEX_MatcherDataS1 * fpRDS);	     // all images presented to Matcher go through here
	void     IDEX_FreeMemoryRDS(IDEX_MatcherDataS1 * fpRDS);                         // free memory after Matcher is completed
	
	long     IDEX_M_SampleCode(uint8_t * pImage, int16_t width, int32_t height, uint8_t * pOutTemplate, int32_t * newTemplateSize, int32_t doDynamicEnroll, uint8_t * pScore);


	// 		---------------------------	Function Prototypes, IDEX Distributed Matcher, SE Specific ---------------------------
	uint16_t IDEX_ccSE_GetFP_MemSize_S1(void);	                                    // memory required

    #if EnableNVMOptimization
	    OffsetSizePair IDEX_ccSE_GetTemplateOffsetAndSize(uint8_t FingerId, uint8_t TouchId, uint8_t TouchMaxCnt, uint8_t TemplateOrVerify);
    #else
	    OffsetSizePair IDEX_ccSE_GetTemplateOffsetAndSize(uint8_t FingerId, uint8_t TouchId, uint8_t TouchMaxCnt);
    #endif // EnableNVMOptimization

    #if EnableOption1a || EnableNVMOptimization != 0
	    uint16_t IDEX_ccSE_GetFP_ScratchSize_S1(void);
    #endif // EnableOption1a || EnableNVMOptimization == 1

	int8_t IDEX_ccSE_GetBasePtrForSensorImageData(IDEX_ccSE_MatcherDataS1 * ccSE_Data);
	int8_t IDEX_ccSE_SubmitSensorDataToMatcherRDS(IDEX_ccSE_MatcherDataS1 * ccSE_Data);
	IDEX_ccMatcherInfoS1 * IDEX_GetFP_SEccMatcherInfo_S1(void);
	int8_t validateSELib(IDEX_ccMatcherInfoS1 * info);
    void IDEX_ccSE_GetProcess2Info(IDEX_ccSE_MatcherDataS1 * ccSE_Data);
    void IDEX_ccSE_GetMWFConsumed(IDEX_ccSE_MatcherDataS1 * ccSE_Data);
	
	// 		---------------------------	Function Prototypes, IDEX Distributed Matcher, MCU Specific ---------------------------
	long IDEX_ccMCU_REx_GetFP_MemSize_S1(void);
	long IDEX_ccMCU_REx_SetupRDS(IDEX_ccMCU_REx_MatcherDataS1 * fpRDS);
	long IDEX_ccMCU_REx_SubmitSensorImageToMatcherRDS(IDEX_ccMCU_REx_MatcherDataS1 * fpRDS);


	// 		---------------------------	Function Prototypes, MCU-CC Specific ---------------------------
#if EnableSRAMFlush
	void IDEX_ccMCU_M_EraseSRAM(IDEX_ccMCU_M_MatcherDataS1 *fpRDS);
#endif
	uint16_t IDEX_ccMCU_M_GetFP_MemSize_S1(void);
	int8_t IDEX_ccMCU_M_ProcessDataFromSE_1_RDS(IDEX_ccMCU_M_MatcherDataS1 * fpRDS_MCU_M);
	int IDEX_ccMCU_M_ProcessDataFromSE_2_RDS(IDEX_ccMCU_M_MatcherDataS1 * fpRDS_MCU_M);
	void IDEX_ccMCU_M_ProcessDataSetup_1_RDS(IDEX_ccMCU_M_MatcherDataS1 * fpRDS_MCU_M);
	void IDEX_ccMCU_M_ProcessDataSetup_2_RDS(IDEX_ccMCU_M_MatcherDataS1 * fpRDS_MCU_M);
	IDEX_ccMatcherInfoS1* IDEX_GetFP_MCUccMatcherInfo_S1(void);

	// 		---------------------------	Function Prototypes, Debug IDEX Distributed Matcher, SE Specific ---------------------------
	uint16_t IDEX_SE_DebugVerifyDataCheckSum(uint8_t * DataPtr, uint16_t DataSize);
	uint16_t IDEX_SE_DebugVerifyInfoCheckSum(void);


	// 		-----------------------------------------------------------------------------------------------------
	// 		---------------------------	Function Prototypes, etc for Status Callbacks ---------------------------
	// 		-----------------------------------------------------------------------------------------------------

	#if kStatusCallbackEnabled

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// IDEX_GetRidgeDataBitmap
		//
		// extract a ridge data bitmap (skeletonized) from an image
		//
		// Inputs:
		//
		//   pImage : pointer to the input image (8 bit grayscale)
		//
		//   width  : width in pixels of pImage
		//
		//   height : height in pixels of pImage
		//
		// Outputs:
		//
		//   ppRidgeDataBitmap : pointer to a pointer to the output skeletonized image (8 bit grayscale, 0 means no ridge, 0xFF means ridge
		//                     : Note that *ppRidgeDataBitmap must be deallocated by calling IDEX_FreeRidgeDataBitmap()
		//
		//   pWidth            : pointer to the width of *ppRidgeDataBitmap in pixels
		//
		//   pHeight           : pointer to the height of *ppRidgeDataBitmap in pixels
        //
        // Return Value: kMatcher_NoErrorNoMatchS1 if no error, otherwise one of the other matcher errors
		//
		long IDEX_GetRidgeDataBitmap( uint8_t * pImage, uint16_t width, uint16_t height, uint8_t ** ppRidgeDataBitmap, uint16_t * pWidth, uint16_t * pHeight );


		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// IDEX_FreeRidgeDataBitmap
		//
		// free the ridge data bitmap allocated by IDEX_GetRidgeDataBitmap
		//
		// Inputs: 
		//
		// pRidgeDataBitmap : bitmap allocated by IDEX_GetRidgeDataBitmap
		//
		void IDEX_FreeRidgeDataBitmap( uint8_t * pRidgeDataBitmap );


		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// IDEX_SetStatusCallback
		//
		// set a callback to receive 64 bits of status information from matcher for logging/debug interpretation is a private contract between the matcher and a known IDEX caller
		//
		// Inputs:
		//
		//   message  : one of the kStatusMessage* values, see below
		//
		//   status   : 64 bits of data formatted as follows, see kStatusMessage* for definition
		//
		//               D1               D2               D3
		//               1111111111111111 2222222222222222 33333333333333333333333333333333
		//              63       55       47               31                              0
		//
		//   userData : a 64-bit quantity that will be supplied by the caller and passed to the callback function
		//
		// Return Value: pointer to previous status callback
		//
		
		// pointer to status callback function
		typedef void(*pfnIDEX_GetStatus)(uint8_t message, uint64_t status, uint64_t userData);

		// install status callback, and provide userData
		// NOTE: this function is not thread-safe
		pfnIDEX_GetStatus IDEX_SetStatusCallback(pfnIDEX_GetStatus pfnGetStatus, uint64_t userData);

		// macros to pull out various pieces from the 64 bits of data sent via the callback
		#define IDEX_FP_getStatusD1(status) ((uint16_t) ((0xFFFF000000000000L & (status)) >> 48))
		#define IDEX_FP_getStatusD2(status) ((uint16_t) ((0x0000FFFF00000000L & (status)) >> 32))
		#define IDEX_FP_getStatusD3(status) ((uint32_t) ((0x00000000FFFFFFFFL & (status))))

		// additional functions to decimate canvas
		#ifndef DecimateCanvassForExploreRotatedMatch // can be overridden on compiler command line
			#define DecimateCanvassForExploreRotatedMatch (0)
		#endif

		#if DecimateCanvassForExploreRotatedMatch
			void IDEX_CompressBitmapS1(uint8_t * sourceBuffer);
		#endif

	#endif // kStatusCallbackEnabled
	
	// these need to be declared outside of the "#if kStatusCallbackEnabled" block above because they are used in macros

	//////////////////////////////////////////
	//
	// message values
	//

	// SensorLock messages
	#define kStatusMessageSensorLockImageCheckOK		((uint8_t) 1)	// D1: 0, D2: 0, D3: 0
	#define kStatusMessageSensorLockImageCheckFail		((uint8_t) 2)	// D1: 0, D2: 0, D3: 0
	#define kStatusMessageSensorLockMatchCheckOK		((uint8_t) 3)	// D1: 0, D2: 0, D3: 0
	#define kStatusMessageSensorLockMatchCheckFail		((uint8_t) 4)	// D1: 0, D2: 0, D3: 0
	#define kStatusMessageSensorLockMatchCheckFailData	((uint8_t) 5)	// D1: 0, D2: 0, D3: 0

	// Ridge Data Bitmap messages
	#define kStatusMessageRidgeData						((uint8_t) 6)	// D1: width of RidgeDataBitmap; D2: height of RidgeDataBitmap; D3: High 16 bits ridgePixelsFound, Low 16 bits ridgeEndPtsFound
	#define kStatusMessageRidgeDataBitmap				((uint8_t) 7)	// status = pointer to RidgeDataBitmap
	#define kStatusMessageStartMatch					((uint8_t) 8)	// D1: 0, D2: 0, D3: 0
	#define kStatusMessageMatchCompleted				((uint8_t) 9)	// D1: score, D2: index matched against, D3: imageSubmitResult
	#define kStatusMessageStartEnroll					((uint8_t) 10)	// D1: 0, D2: 0, D3: 0
	#define kStatusMessageEnrolledImage					((uint8_t) 11)	// D1: number of finger touches, D2: is template full ?, D3: imageSubmitResult
	#define kStatusMessageFlashWrite					((uint8_t) 12)	// D1: 0=MCU, 1=SE, D2: index(part), D3: bytes written
	#define kStatusMessageFlashWrite_MCU				(0)				// D1 value
	#define kStatusMessageFlashWrite_SE					(1)				// D1 value
	#define kStatusMessageFlashWrite_EraseBlockIndex	(0xFF)			// D2 value, D3 should be ignored


	// 		----------------------------------------------------------------------------------------------------------------
	// 		********		Sensor Data PreProcessing Flags															********
	// 		----------------------------------------------------------------------------------------------------------------
	#define		kEnableSensorWatermarkDetectionS1				(FALSE)			// enables code to process watermark for IDEX Sensor verification 
	#define		kEnablePreRidgeRegionDetectionS1				(FALSE)			// enables code to process sensor data containing a early ridge region found mask 
	#define     kEnableScoreDuplication                         (FALSE)         //  enabled code to implement score duplication inside the matcher

    #ifdef __cplusplus
    }
    #endif
	
#endif /* _IDEX_M_INTERFACE_H_ */

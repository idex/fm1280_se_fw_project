/******************************************************************************
* \file  dispatcher.c
* \brief code for testing SE EMK
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "idxSeBioAPI.h"
#include "bio-ctrl.h"
#include "dispatcher.h"
#include "hostif.h"
#include "serialInterface.h"                    // Debug
#include "idxSeEmkDefines.h"
#include "idxHalAlgo.h"





#include "cmac.h"
#include "kdf.h"
#include "crypto.h"
#include "main.h"
#include "idxHalLed.h"
#include "idxHalFlash.h"

#define FAR 




#ifdef sleeve_enrollment_support
vu8 sleeve_enable_flag;
vu8 process_in_waiting;
vu8 timer_a_int_flag;
vu8 blinkCtrl;

#endif
struct idxBioSystemConfig_s SystemConfig  __attribute__((at(0x00800a80)));

BaseSerialInterface idxCom __attribute__((at(0x00800ad0)));//16 byte
uint8_t COMBUF[256] __attribute__((at(0x00800b00)));
uint8_t buffer[256] __attribute__((at(0x00800c00)));

uint8_t ram1_buffer[4*1024] __attribute__((at(0x00800d00)));
uint8_t matcherBuffer[MEM_SIZE] __attribute__((at(0x00801d00)));

#define RAM1    (uint8_t*)ram1_buffer // RAM1   4KB



#define UNUSED(p) if(p){}




//  ****  Enrollment Parameters  ****
#define  kSE_Integrator_maxEnrollFingerCount        (2)            //  maximum number of fingers to be enrolled
#define  kSE_Integrator_maxEnrollTouchCount         (6)            //  maximum number of touches per finger enrolled
//  ****  Environment Parameters  ****
#define  kSE_Integrator_maxPerFingerTemplateSize    (20L << 10)    //  maximum space allocated for flash storage per enroll finger template
//  maximum space allocated for flash storage of enroll templates
#define  kSE_Integrator_maxFlashTemplateSize        (kSE_Integrator_maxEnrollFingerCount * kSE_Integrator_maxPerFingerTemplateSize)//total 40K
#define  kSE_Integrator_FlashSectorSize             (8L << 10)     //  size of flash sector
#define  kSE_Integrator_RAM_Size                    (6L << 10)    //  size of RAM available to the SE Matcher - Expected to be 12k
#define  kSE_Integrator_TargetSecurityLevel         (5)            //  range between 4 and 15, 8 typical

uint8_t apdup2;
uint8_t apdulc;


uint8_t *apdu_handler( void *arg );

extern uint16_t setFactoryKeys(void);
extern uint16_t getNextKeys(void);
extern uint16_t setNextKeys(uint8_t *buf, uint8_t mode);


extern void idxHalTimerDelayMsec(uint16_t msec);
extern void idxcom_initial(void);
extern void gpio_initial(void);
extern void ussleep(uint32_t count);
extern void Init_SystemConfig(void);
extern uint16_t idxHalFlashErase(uint8_t *pAddr, uint32_t bytesToErase);
extern void set_7816IO_to_GPIO(void);
#ifdef sleeve_enrollment_support

extern void testBlink(void);
extern uint16_t idxHalSetLedMode( idxHalLedModeStateType state, uint8_t index );
#endif
extern void scp03_related_data_initial(void);

extern  uint16_t BCTC_dataEncrypt(uint8_t * key,uint8_t *cdata, uint16_t Lc);
extern  uint16_t BCTC_dataDecrypt(uint8_t * key,uint8_t *cdata, uint16_t Lcc);

uint8_t get_p2( void )
{
  return apdup2;
}

uint8_t get_lc( void )
{
  return apdulc;
}

uint32_t get_update_address( void )
{
  return 0x08000000;
}

uint32_t get_update_size( void )
{
  return 256;
}

uint8_t * get_iv()
{
  unsigned int i;
  static uint8_t buff[32];
  for (i=0; i<32; i++) {
    buff[i] = 0;
  }
  return buff;
}

uint8_t * get_hash()
{
  unsigned int i;
  static uint8_t hash[8];
  for (i=0; i<8; i++) {
    hash[i] = 0;
  }
  return hash;
}



/*****************************************************************************
 * DEBUG 
 *
 *****************************************************************************/
extern uint8_t *pDbg;
// Encryption is in place ( 16 bytes ) Signature implies: (chain[16]) + (6 + encrypt[0:9]) + (encrypt[10:15] + padding[10])
//static uint8_t smallCmdData[16+10]; // Commands up to 16 bytes + signature

static void updateReplyInfo(uint16_t errCode, uint8_t *info, uint8_t isize){
  extern uint8_t errPoint;
  uint8_t wSize;
  uint8_t i;
  
  // ReplyHeader is initialized as a NULL in serialInterface.c
  if( NULL == idxCom.state->ReplyHeader )
  {
    idxCom.reset();  // init command/response buffers (pointers)
    ((uint8_t *)idxCom.state->ReplyHeader)[0] = 6;
  }
  wSize = ((uint8_t *)idxCom.state->ReplyHeader)[0];
  
  ((uint8_t *)idxCom.state->ReplyHeader)[1] = 0;
  ((uint8_t *)idxCom.state->ReplyHeader)[2] = errCode & 0xFF;
  ((uint8_t *)idxCom.state->ReplyHeader)[3] = ( errCode >> 8 ) & 0xFF;
  ((uint8_t *)idxCom.state->ReplyHeader)[4] = errPoint;
  ((uint8_t *)idxCom.state->ReplyHeader)[5] = 0x00;
  
  for(i = 0 ; i < isize ;i++){ 
    ((uint8_t *)idxCom.state->ReplyHeader)[wSize+i] = info[i];
  }
  if(isize)
    ((uint8_t *)idxCom.state->ReplyHeader)[0] = wSize +isize;
}

static void setReplyInfo(uint16_t errCode, uint8_t *info, uint8_t isize){
  extern uint8_t errPoint;
  idxCom.reset();  // init command/response buffers (pointers)
  ((uint8_t *)idxCom.state->ReplyHeader)[1] = 0;
  ((uint8_t *)idxCom.state->ReplyHeader)[2] = errCode & 0xFF;
  ((uint8_t *)idxCom.state->ReplyHeader)[3] = ( errCode >> 8 ) & 0xFF;
  ((uint8_t *)idxCom.state->ReplyHeader)[4] = errPoint;
  ((uint8_t *)idxCom.state->ReplyHeader)[5] = 0x00;
  if(isize){
   int i; 
   ((uint8_t *)idxCom.state->ReplyHeader)[0] = 6 + isize;
   for(i = 0 ; i < isize ; i++)
     ((uint8_t *)idxCom.state->ReplyHeader)[6+i] = info[i];     
  }
  else{
   ((uint8_t *)idxCom.state->ReplyHeader)[0] = 6;
  }
}

/*****************************************************************************
 * APDU-HANDLER - main function for dispatch the execution flow based on input
* Note: one exception is POS-VERIFY which should execute 2 commands  
 *****************************************************************************/


uint16_t fm1280_fast_finger_detect (void)
{
	uint16_t ret;
	uint8_t setParam40d[]	 ={0x28,0x0a,0x00};
	  ret = idxMcuComms( HOSTIF_CMD_SETPARAM, 0x00, (void*)setParam40d, HOSTIF_CMD_SETPARAM_DATA_SIZE, NULL, NULL );
	ussleep(200);
	if(IDX_BIO_OK != ret) return ret;
	setParam40d[0]=0x83;
	 ret = idxMcuComms( HOSTIF_CMD_ACQUIRE, 0x00, (void*)setParam40d, HOSTIF_CMD_ACQUIRE_DATA_SIZE, NULL, NULL );
	 ussleep(200);
	return ret;
}


#ifdef sleeve_enrollment_support
void FM1280_timer_init (void)
{
	reg_clk_ctrl0 |= TIMER_CLK_EN;
	reg_timera_config=BIT1|BIT8|BIT18|BIT17;//8Mhz clock, repeat enable, 2, t1 8+ t2 24 bit timer;
	reg_timer_int_config=TIMEA_INT_EN0;
	NVIC_ISER |=  TIMER_A_INT_BIT;
	reg_timera_preset0=0x000f4240;//500ms, 8000*500//0x00009c40;//5ms, 8000*5,
	reg_timera_counter0=0;
	reg_timer_start=TIMEA_START0;
}
void FM1280_timer_stop (void)
{
	NVIC_ICER=TIMER_A_INT_BIT;
	reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG0;
	reg_timer_stop=TIMEA_STOP0;
}




void fm1280_sleeve_enrollment(void)
{
	//u8 test_buffer[6];
	
	typedef struct info { uint8_t *apdu; uint8_t *dat;} info_t;
	uint8_t *apdu_handler( void *arg );
	uint16_t *pErrCode;
	uint8_t done;
	uint8_t inith[5]	 = {0x00,0x54,0x00,0x00,0x01};
	uint8_t initd[1]	 = {0x06};		  // mode = SLEEVE 
	info_t init;
	
	uint8_t uninith[]	  = {0x00,0x54,0x01,0x00,0x00};
	uint8_t dmenrolx1h[]  = {0x00,0x54,0x8b,0x26,0x00};
	uint8_t openscp03h[]  = {0x00,0x54,0xdd,0x00,0x00};
	
	info_t uninit, dmenrolx1, openscp03;
	uint8_t setParam[]	  = {0x00,0x54,0x09,0x00,0x03};
	uint8_t setParam20d[] = {0x20,0x00,0x00}; // - Switch off BrownOutDetection (set to 0x00)
	uint8_t setParam23d[] = {0x23,0xd2,0x80}; //  - Change Power Probe mask 2 to 0x80D2
	uint8_t setParam07d[] = {0x07,0x10,0x27}; // - Set timeout to 10 second 0x2710
	
	uint8_t setParam87d[] = {0x87,0x20,0x88}; // - MCU led blink - debugging
	uint8_t setParam2bd[] = {0x2b,0x00,0x00}; // - Set Vsense to default value 
	
	info_t setParam20, setParam23, setParam07, setParam87, setParam2b;
	
	
	init.apdu		 = inith;		init.dat	   = initd;
	uninit.apdu 	 = uninith; 	  uninit.dat	 = NULL;
	dmenrolx1.apdu	 = dmenrolx1h;	  dmenrolx1.dat  = NULL;
	openscp03.apdu	 = openscp03h;	openscp03.dat  = NULL;
	setParam20.apdu  = setParam;	setParam20.dat = setParam20d;
	setParam23.apdu  = setParam;	setParam23.dat = setParam23d;
	setParam07.apdu  = setParam;	setParam07.dat = setParam07d;
	setParam87.apdu  = setParam;	setParam87.dat = setParam87d;
	setParam2b.apdu  = setParam;	setParam2b.dat = setParam2bd;
	set_7816IO_to_GPIO();
	idxcom_initial();
	idxCom.reset(); 
	Init_SystemConfig();

	do 
		{
		  done = 0;
		  pErrCode = (uint16_t *)apdu_handler(&setParam2b);   // test MCU mode - plain or scp03 
		  if(pErrCode[1] != 0x0090)//big endian
			{
			if(pErrCode[1] == 0x8669) // this error means : scp03 in usebig endian
				{
				  pErrCode = (uint16_t *)apdu_handler(&openscp03);	
				  if(pErrCode[1] != 0x0090) break;
				  
				  pErrCode = (uint16_t *)apdu_handler(&setParam2b); // re-set Vsense-min 
				  if(pErrCode[1] != 0x0090) break;
				}
				else break;
			}
		  done = 1; // scp03 set
		  idxHalTimerDelayMsec(2);
		  pErrCode = (uint16_t *)apdu_handler(&init);
		  if(pErrCode[1] != 0x0090) break;
		  pErrCode = (uint16_t *)apdu_handler(&setParam20);
		  if(pErrCode[1] != 0x0090) break;
		  pErrCode = (uint16_t *)apdu_handler(&setParam23);
		  if(pErrCode[1] != 0x0090) break;
		  pErrCode = (uint16_t *)apdu_handler(&setParam07);
		  if(pErrCode[1] != 0x0090) break;
		  pErrCode = (uint16_t *)apdu_handler(&dmenrolx1);
		  if(pErrCode[1] != 0x0090) break;
			  
		  pErrCode = (uint16_t *)apdu_handler(&uninit);
		  if(pErrCode[1] != 0x0090) break;
		  
		  done = 2;
		}while(0);
	
	if(done){  // the check for scp03 was successful 
	  if(done == 2) setParam87d[1] = 0x10; // green light
	  else			setParam87d[1] = 0x20; // red light
	  apdu_handler(&setParam87);
	}

	while(1)
		{
		if(1==timer_a_int_flag)
			{
			timer_a_int_flag=0;
			}
		}

}
#endif

enum idxBioRet_e BCTC_VERIFY_INITIAL(void *arg)
{
	struct info { uint8_t *apdu; uint8_t *dat;} *info;
	enum idxBioInitializeOptions_e options;
	bool desiredSensorInit;
	enum idxBioRet_e ret = IDX_BIO_UNKNOWN;
	info = (struct info*)arg;
	apdup2 = info->apdu[3];
	apdulc = info->apdu[4];
	set_connection_mode( (uint8_t)(info->dat[0] & 0x05) );	// extract the mode from flags
	switch( info->dat[0] & 0x05)
	{
	  case 0x00:
	  {
		options = IDX_BIO_INITIALIZE_CONTACT;
		break;
	  }
	  case 0x01:
	  {
		options = IDX_BIO_INITIALIZE_CONTACTLESS;
		break;
	  }
	  default:
	  {
		options = IDX_BIO_INITIALIZE_BATTERY;
		break;
	  } 		 
	}
	desiredSensorInit = (( info->dat[0] & 0x02 ) != 0);
	if (desiredSensorInit != SystemConfig.Policies.SensorInit ) 
	  {
	  ret = IDX_BIO_NOT_SUPPTD;
	} else 
	{
	  ret = idxBioInitialize( options, &SystemConfig, sizeof(buffer), buffer );
	}
	return ret;
}

uint8_t *apdu_handler( void *arg )
{
  struct info { uint8_t *apdu; uint8_t *dat;} *info;
  uint16_t return_data;
	uint8_t *flash_test_pointer;
	uint32_t test_data;
	uint32_t test_data2;
	uint8_t reg_buffer[6];
	int i;
	  uint8_t BCTC_test_key[16];
	  //
  uint8_t BCTC_ENC_SOURCE[80]={	0xBE,0x73,0xC5,0x54,0x07,0xAE,0xE6,0x8E,0x2F,0x9D,0x75,0xCD,0x2C,0x85,0xC4,0x4E,
  									0x32,0xB6,0x49,0x9B,0x85,0x08,0x1E,0x9D,0x29,0xA4,0xDA,0x38,0x6B,0x94,0xD3,0xFF,
  									0x14,0xF4,0x63,0x67,0xEB,0x9D,0xB4,0x3F,0x2E,0xBB,0x47,0x8A,0xDA,0x37,0x9D,0xCB,
  									0x65,0xF9,0x3E,0xA2,0x59,0x3E,0xFB,0xC3,0x15,0x7B,0x39,0x58,0x19,0x99,0xD4,0x31,
  									0x9A,0x5A,0x65,0x11,0x1F,0x6E,0x7B,0x2A,0x2F,0xFD,0xBA,0xB9,0xA4,0xBF,0x38,0xFB
  };
  enum idxBioRet_e ret = IDX_BIO_UNKNOWN;
  info = (struct info*)arg;

  apdup2 = info->apdu[3];
  apdulc = info->apdu[4];
  return_data=0;



  switch( info->apdu[2] )
  {
    case APDU_CMD_INITIALIZE:
    {
      enum idxBioInitializeOptions_e options;
      bool desiredSensorInit;
      
      set_connection_mode( (uint8_t)(info->dat[0] & 0x05) );  // extract the mode from flags


      switch( info->dat[0] & 0x05)
      {
        case 0x00:
        {

          options = IDX_BIO_INITIALIZE_CONTACT;
          break;
        }
        case 0x01:
        {
          options = IDX_BIO_INITIALIZE_CONTACTLESS;
          break;
        }
        default:
        {
          options = IDX_BIO_INITIALIZE_BATTERY;
          break;
        }          
      }
      
      desiredSensorInit = (( info->dat[0] & 0x02 ) != 0);
      
      if (desiredSensorInit != SystemConfig.Policies.SensorInit ) 
	  	{
      	
        ret = IDX_BIO_NOT_SUPPTD;
      } else {
		 

        ret = idxBioInitialize( options, &SystemConfig, sizeof(buffer), buffer );
		
      }
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_UNINITIALIZE:
    {
      ret = idxBioUninitialize( sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_GETIMAGE:
    {
      uint16_t inSize = (uint16_t)get_lc();
      uint16_t output_size;
      memcpy( buffer, &info->dat[0], inSize );
      ret = idxBioGetImage( sizeof(buffer), buffer, &output_size );
      setReplyInfo( ret, buffer + (sizeof(buffer) >> 1), output_size );
      break;
    }
    case APDU_CMD_ACQUIRE:
    {
      ret = idxBioAcquire( sizeof(buffer), buffer, info->dat[0] );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_LIST:
    {
      uint16_t output_size;
	  memcpy(buffer, &info->dat[0],3); // List command has 3 bytes of data
      ret = idxBioListTemplates( sizeof(buffer), buffer, &output_size );
      setReplyInfo( ret, buffer + (sizeof(buffer) >> 1), output_size );
      break;
    }
    case APDU_CMD_GETPARAM:
    {
      uint8_t id;
      uint16_t val;
      id = info->dat[0];
      ret = idxBioGetParameter( id, &val, sizeof(buffer), buffer );
      setReplyInfo( ret,(uint8_t*)&val,sizeof(val) );
      break;
    }
    case APDU_CMD_SETPARAM:
    {
      uint8_t id = info->dat[0];
      uint16_t val = 0;
      val |= info->dat[1] & 0xFF;
      val |= ((uint16_t)info->dat[2] << 8 ) & 0xFF00; 
      ret = idxBioSetParameter( id, val,sizeof(buffer), buffer);
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_STOREPARAMS:
    {
      ret = idxBioStoreParams( sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_DELETERECORD:
    {
      uint16_t index = 0;
      index |= info->dat[0] & 0xFF;
      index |= ((uint16_t)info->dat[1] << 8 ) & 0xFF00;
      ret = idxBioDeleteRecord( index, sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }

	case 0x70://erase debug flash area, apdu: 00 54 70 00 00
	{
		return_data=0x9000;
		flash_test_pointer = (uint8_t *)debug_flash_start;
		idxHalFlashErase(flash_test_pointer,0x1000);
		updateReplyInfo( return_data, NULL, 0 );
		COMBUF[1]=0x00;
		COMBUF[0]=6;
		break;
  	}
	case 0x71://read debug flash area, apdu 00 54 71 00 04 | 00 b0 b2 00
	{
		test_data=0;
		test_data += (uint32_t)((info->dat[0]<<24)&0xff000000);
		test_data += (uint32_t)((info->dat[1]<<16)&0x00ff0000);
		test_data += (uint32_t)((info->dat[2]<<8)&0x0000ff00);
		test_data += (uint32_t)((info->dat[3]<<0)&0x000000ff);
		if(((uint32_t)idex_used_flash_start>test_data)||((uint32_t)idex_used_flash_end<test_data))
			{
			return_data=0x6a80;
			updateReplyInfo( return_data, NULL, 0 );
			break;
			}
		 idxCom.reset(); 
		flash_test_pointer=(uint8_t *)test_data;
		COMBUF[2]=0x00;
		COMBUF[3]=0x90;
		COMBUF[4]=info->dat[0];
		COMBUF[5]=info->dat[1];
		COMBUF[6]=info->dat[2];
		COMBUF[7]=info->dat[3];
		for(i=8;i<136;i++)
			{
			COMBUF[i]=  *flash_test_pointer;
			flash_test_pointer++;
			}
		COMBUF[1]=0x00;
		COMBUF[0]=136;
		break;
	}





	
#ifdef IMM_ONLY		
    case APDU_CMD_IMM_ENROLL:{
      ret = idxBioOnCardEnroll( NULL, 0, NULL );
      setReplyInfo(ret,NULL,0);
      break;
    }
    case APDU_CMD_IMM_SLEEVE_ENROLL:  // Sleeve enrolment for SLE78
    {
      ret = idxBioSleeveEnroll( NULL, 0, NULL );
      setReplyInfo(ret,NULL,0);
      break;
    }
    case APDU_CMD_IMM_VERIFY:  // Verify for IMM ( CIU9872B_01 )
    {
      ret = idxBioVerify( NULL, 0, NULL, NULL, 0, 0 );
      setReplyInfo(ret,NULL,0);
      break;
    }
    case APDU_CMD_POS_VERIFY:
    {
      ret = IDX_BIO_NOT_IMPLEMENTED;
      setReplyInfo(ret,NULL,0);
      break;
    }
#else
    case APDU_CMD_IDM_ENROLL:
    {
      struct idxBioMatcherWorkArea_s pWorkAreas[3];
      extern uint8_t errPoint;
      pWorkAreas[0].pointer = matcherBuffer;
      pWorkAreas[0].size = kSE_Integrator_RAM_Size;
      pWorkAreas[1].pointer = (uint8_t *)idex_template_flash_star_addr;
      pWorkAreas[1].size = kSE_Integrator_maxFlashTemplateSize;
      pWorkAreas[2].pointer = ram1_buffer;
      pWorkAreas[2].size = 4 * 1024;
      ret = idxBioOnCardEnroll( pWorkAreas, sizeof(buffer), buffer );
      setReplyInfo( ret,NULL,0 );
      break;
    }
    case APDU_CMD_IDM_SINGLE_ENROLL:
    {
      struct idxBioMatcherWorkArea_s pWorkAreas[3];
      pWorkAreas[0].pointer = matcherBuffer;
      pWorkAreas[0].size = kSE_Integrator_RAM_Size;
      pWorkAreas[1].pointer = (uint8_t *)idex_template_flash_star_addr;
      pWorkAreas[1].size = kSE_Integrator_maxFlashTemplateSize;
      pWorkAreas[2].pointer = ram1_buffer;
      pWorkAreas[2].size = 4 * 1024;
      ret = idxBioSingleEnroll( pWorkAreas, sizeof(buffer), buffer, info->dat[0] );
      setReplyInfo( ret,NULL,0 );
      break;
    }
    case APDU_CMD_SLEEVE_ENROLL:  // Sleeve enrolment for SLE78
    {
      struct idxBioMatcherWorkArea_s pWorkAreas[3];
      pWorkAreas[0].pointer = matcherBuffer;
      pWorkAreas[0].size = kSE_Integrator_RAM_Size;
      pWorkAreas[1].pointer = (uint8_t *)idex_template_flash_star_addr;
      pWorkAreas[1].size = kSE_Integrator_maxFlashTemplateSize;
      pWorkAreas[2].pointer = ram1_buffer;
      pWorkAreas[2].size = 4 * 1024;
      ret = idxBioSleeveEnroll( pWorkAreas, sizeof(buffer), buffer);
      setReplyInfo( ret,NULL,0 );
      break;
    }

//
//BCTC_VERIFY_INITIAL
	case APDU_CMD_IDM_BCTC_VERIFY_CL:
	case APDU_CMD_IDM_BCTC_VERIFY_CT:
	{
		struct idxBioMatcherWorkArea_s pWorkAreas[3];
		  uint16_t score;
		  uint16_t mwfTime = 0;
		  uint8_t imgsrc = info->dat[1];
		  
		  mwfTime |= info->dat[2] & 0xFF;
		  mwfTime |= ((uint16_t)info->dat[3] << 8 ) & 0xFF00;
		ret=BCTC_VERIFY_INITIAL(arg);
		if(IDX_BIO_OK!=ret)
			{
			setReplyInfo( ret, NULL, 0 );
			break;
			}
		//initial OK
		do
			{
			return_data = getNextKeys();
			if(return_data != IDEX_SUCCESS)
				{ 
				setReplyInfo( ret, NULL, 0 );
				break;
				} 
			return_data = getExtAuthData();    // extAuthorize
			if(return_data != IDEX_SUCCESS)
				{ 
				setReplyInfo( ret, NULL, 0 );
				break;
				}
			return_data = setNextKeys(matcherBuffer, apdup2);    // putKey + flashUpdate
			if(return_data != IDEX_SUCCESS)
				{ 
				setReplyInfo( ret, NULL, 0 );
				break;
				}
			}while(0);
	      //open SCP03 OK

		  pWorkAreas[0].pointer = matcherBuffer;
		  pWorkAreas[0].size = kSE_Integrator_RAM_Size;
		  pWorkAreas[1].pointer = (uint8_t *)idex_template_flash_star_addr;
		  pWorkAreas[1].size = kSE_Integrator_maxFlashTemplateSize;
		  pWorkAreas[2].pointer = ram1_buffer;
		  pWorkAreas[2].size = 4 * 1024;
		  ret = idxBioVerify( pWorkAreas, sizeof(buffer), buffer, &score, imgsrc, mwfTime );
		  updateReplyInfo( ret, NULL, 0 );
		  break;
	}
	
    case APDU_CMD_IDM_VERIFY:
    {
      struct idxBioMatcherWorkArea_s pWorkAreas[3];
      uint16_t score;
      uint16_t mwfTime = 0;
      uint8_t imgsrc = info->dat[0];

      mwfTime |= info->dat[1] & 0xFF;
      mwfTime |= ((uint16_t)info->dat[2] << 8 ) & 0xFF00;

      pWorkAreas[0].pointer = matcherBuffer;
      pWorkAreas[0].size = kSE_Integrator_RAM_Size;
      pWorkAreas[1].pointer = (uint8_t *)idex_template_flash_star_addr;
      pWorkAreas[1].size = kSE_Integrator_maxFlashTemplateSize;
      pWorkAreas[2].pointer = ram1_buffer;
      pWorkAreas[2].size = 4 * 1024;
      ret = idxBioVerify( pWorkAreas, sizeof(buffer), buffer, &score, imgsrc, mwfTime );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_POS_VERIFY:
    {
      uint16_t idmVerify(void);
      struct idxBioMatcherWorkArea_s pWorkAreas[3];
      uint16_t score;
      uint8_t imgsrc = 1;
      apdup2 = 0x16;
      pWorkAreas[0].pointer = matcherBuffer;
      pWorkAreas[0].size = kSE_Integrator_RAM_Size;
      pWorkAreas[1].pointer = (uint8_t *)idex_template_flash_star_addr;
      pWorkAreas[1].size = kSE_Integrator_maxFlashTemplateSize;
      pWorkAreas[2].pointer = ram1_buffer;
      pWorkAreas[2].size = 4 * 1024;
      ret = idxBioPosVerify( pWorkAreas, sizeof(buffer), buffer, &score, imgsrc, 0 );
      updateReplyInfo(ret, NULL, 0);
      break;
    }
    case APDU_CMD_DELETE_FINGER:
    {    
      struct idxBioMatcherWorkArea_s pWorkAreas[3];
      pWorkAreas[0].pointer = matcherBuffer;
      pWorkAreas[0].size = kSE_Integrator_RAM_Size;
      pWorkAreas[1].pointer = (uint8_t *)idex_template_flash_star_addr;
      pWorkAreas[1].size = kSE_Integrator_maxFlashTemplateSize;
      pWorkAreas[2].pointer = ram1_buffer;
      pWorkAreas[2].size = 4 * 1024;
      ret = idxBioDeleteFinger( pWorkAreas, info->dat[0] );
      setReplyInfo(ret,NULL,0);
      break;
    }   
#endif		
    case APDU_CMD_STOREBLOB:
    {
      uint16_t inSize = (uint16_t)get_lc();
      uint16_t output_size;
      memcpy( buffer, &info->dat[0], inSize ); // info->dat[0] contains "flags"
      ret = idxBioStoreBlob( sizeof(buffer), buffer, &output_size );
      setReplyInfo( ret, buffer + (sizeof(buffer) >> 1), output_size );
      break;
    }
    case APDU_CMD_LOCK:
    {
      uint16_t inSize = (uint16_t)get_lc();
      memcpy( buffer, &info->dat[0], inSize ); // info->dat[0] contains "flags"
      ret = idxBioLock( sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_GETSEVERSION:
    {
      uint16_t output_size;
      ret = idxBioGetVersions( sizeof(buffer), buffer, &output_size );
      setReplyInfo( ret, buffer + (sizeof(buffer) >> 1), output_size );
      break;
    }
    case APDU_CMD_UPDATESTART:
    {
      ret = idxBioUpdateStart( sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_UPDATEDETAILS:
    {
      uint16_t inSize =(uint16_t)get_lc();
      
      // Put apdu data in pBuffer
      uint32_t address = get_update_address(); 
      uint32_t size = get_update_size();
      uint8_t * iv = get_iv();
      BioUpdateOptions options = (BioUpdateOptions)info->dat[0];
      memcpy( buffer, &info->dat[0], inSize );
      ret = idxBioUpdateDetails( options, address, size, iv, sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_UPDATEDATA:
    {
      uint16_t inSize = (uint16_t)get_lc();
      
      // Put apdu data in pBuffer
      memcpy( buffer, &info->dat[0], inSize );
      ret = idxBioUpdateChunk( sizeof(buffer), buffer, 0 );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_UPDATEHASH:
    {
      //uint8_t Hash[8];
      uint8_t *pHash = get_hash();
      uint16_t inSize = (uint16_t)get_lc();
      
      // Put apdu data in pBuffer
      memcpy( buffer, &info->dat[0], inSize );
      ret = idxBioUpdateHash( pHash, sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_UPDATEEND:
    {
      ret = idxBioUpdateEnd( sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_CALIBRATE:
    {
      uint8_t options = info->dat[0];
      uint16_t output_size;
      ret = idxBioCalibrate( options, sizeof(buffer), buffer, &output_size );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_READDATA:
    {
      uint16_t inSize = (uint16_t)get_lc();
      uint16_t output_size;
      
      // Put apdu data in matcherBuffer, pBuffer has no enough size for decryption
      memcpy( matcherBuffer, &info->dat[0], inSize );
      ret = idxBioReadData( sizeof(matcherBuffer), matcherBuffer, &output_size );
      setReplyInfo( ret, matcherBuffer + (sizeof(matcherBuffer) >> 1), output_size );
      break;
    }
    case APDU_CMD_LOADIMAGE:
    {
      uint16_t inSize = (uint16_t)get_lc();      
      // Put apdu data in pBuffer
      memcpy( buffer, &info->dat[0], inSize );
      ret = idxBioLoadImage( sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );   
      break;
    }
/*********************************************************************
 *                Secure Channel Commands
 * Note : implemented as "debug" calls until a complete design for
 *        secure channel "use case" will be in place
 */    
    case APDU_CMD_RESET:
    {
      ret = idxBioReset( sizeof(buffer), buffer );
      updateReplyInfo( ret, NULL, 0 );
      break;
    }
    case APDU_CMD_INITUPDATE:
    {
#if 0
      ret = idxBioInitializeUpdate()
#endif
      break;
    }
    case APDU_CMD_EXTERNALAUTH:
    {
#if 0
      ret = idxBioExternalAuthenticate()
#endif
      break;
    }
    case APDU_CMD_PUTKEYHEADER:
    {
#if 0
      ret = idxBioPutKeyHeader()
#endif
      break;
    }
#if 0
    case 0x??:
    {

      idxBioFinalizeKeys( 256, buffer );
      break;
    }
#endif
	case APDU_DBG_SET_FACTORY_KEYS :
	{

		return_data = setFactoryKeys();
		setReplyInfo( return_data,NULL,0 );	   
		break;
	}	 
	case APDU_DBG_GET_NEXT_KEYS :
	{
		return_data = getNextKeys();
		setReplyInfo( return_data,NULL, 0 );      
		break;      
	}  
	case APDU_DBG_SET_NEXT_KEYS :
	{
		return_data = setNextKeys(matcherBuffer, 0);    // putKey + flashUpdate
		if(return_data != IDEX_SUCCESS)
		{
		setReplyInfo( ret,NULL, 0);      
		}
	}
	break;
	case APDU_DBG_OPEN_SCP03:
		{
		uint16_t err = 0xffff;
		do
			{
			return_data = getNextKeys();
			if(return_data != IDEX_SUCCESS){ err = 0x01; break;} 
			return_data = getExtAuthData();    // extAuthorize
			if(return_data != IDEX_SUCCESS){ err = 0x02; break;}
			return_data = setNextKeys(matcherBuffer, apdup2);    // putKey + flashUpdate
			if(return_data != IDEX_SUCCESS){ err = 0x03; break;}
			}while(0);
		setReplyInfo( return_data,(uint8_t*)&err,2);            
		break;      
		}    

	case APDU_ENCRYPT_TEST:
		{
		for(i=0;i<8;i++)
			{
			BCTC_test_key[i]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_test_key[i+8]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_ENC_SOURCE[i]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_ENC_SOURCE[i+8]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_ENC_SOURCE[i+16]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_ENC_SOURCE[i+24]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_ENC_SOURCE[i+32]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_ENC_SOURCE[i+40]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_ENC_SOURCE[i+48]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_ENC_SOURCE[i+56]=(uint8_t)(((i +1)<<4)+(i+1));
			}
		return_data=BCTC_dataEncrypt(BCTC_test_key,BCTC_ENC_SOURCE,64);
		ret=IDX_BIO_OK;
		setReplyInfo( ret, BCTC_ENC_SOURCE, return_data );
		break;
		}
	//	  uint8_t BCTC_test_key[16];
//  uint8_t BCTC_ENC_SOURCE[64];
//  uint8_t BCTC_ENC_RESULT[64];
//BE73C55407AEE68E2F9D75CD2C85C44E32B6499B85081E9D29A4DA386B94D3FF14F46367EB9DB43F2EBB478ADA379DCB65F93EA2593EFBC3157B39581999D4319A5A65111F6E7B2A2FFDBAB9A4BF38FB
	case APDU_DECRYPT_TEST:
		{
		for(i=0;i<8;i++)
			{
			BCTC_test_key[i]=(uint8_t)(((i +1)<<4)+(i+1));
			BCTC_test_key[i+8]=(uint8_t)(((i +1)<<4)+(i+1));
			}
		return_data=BCTC_dataDecrypt(BCTC_test_key,BCTC_ENC_SOURCE,80);
		ret=IDX_BIO_OK;
		setReplyInfo( ret, BCTC_ENC_SOURCE, return_data );
		break;
		}		
	case APDU_FAST_FINGER_DETECTION:
		{
			return_data=fm1280_fast_finger_detect();
			setReplyInfo( return_data,NULL,0 );	   
		break;
		}
//APDU_REG_READ
	case APDU_REG_READ:
		{
			test_data=0;
			test_data += (((uint32_t)(info->dat[0])<<24)&0xff000000);
			test_data += (((uint32_t)(info->dat[1])<<16)&0x00ff0000);
			test_data += (((uint32_t)(info->dat[2])<<8)&0x0000ff00);
			test_data += (((uint32_t)(info->dat[3])<<0)&0x000000ff);
			test_data2= *(uint32_t *)test_data;
			reg_buffer[0]=(uint8_t)((test_data2&0xff000000)>>24);
			reg_buffer[1]=(uint8_t)((test_data2&0x00ff0000)>>16);
			reg_buffer[2]=(uint8_t)((test_data2&0x0000ff00)>>8);
			reg_buffer[3]=(uint8_t)((test_data2&0x000000ff)>>0);
			setReplyInfo( return_data,reg_buffer,4 );    
		break;
		}


/*   
 ************************************************************************/    
	case APDU_CMD_GETVERSIONS:            // Not implemented, only need the version from the MCU, so need to go by default.
	case APDU_CMD_GETSENSORINFO:          // Not implemented, only need the version from the MCU, so need to go by default.       
	case APDU_CMD_GETSENSORFWVERSION:     // Not implemented, only need the version from the MCU, so need to go by default.
	default:
	{
		uint16_t rcvSize;
		uint16_t return_data;
		return_data = idxMcuComms( info->apdu[2], info->apdu[3], &info->dat[0], info->apdu[4], buffer, &rcvSize );
		updateReplyInfo( return_data, NULL, 0 );
		break;
	}
  }
  
  //return ret;
	return (uint8_t*)idxCom.state->ReplyHeader;
}

//------------------------------   end of file  ------------------------------------------------------

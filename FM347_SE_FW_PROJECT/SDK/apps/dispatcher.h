#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <stdint.h>

#define APDU_CMD_INITIALIZE			0x00
#define APDU_CMD_UNINITIALIZE		0x01
#define APDU_CMD_ACQUIRE				0x02
#define APDU_CMD_GETIMAGE       0x04
#define APDU_CMD_MATCHTEMPLATES	0x07
#define APDU_CMD_GETPARAM				0x08
#define APDU_CMD_SETPARAM				0x09
#define APDU_CMD_STOREPARAMS		0x0C
#define APDU_CMD_GETSENSORINFO	0x0E
#define APDU_CMD_DELETERECORD		0x11
#define APDU_CMD_LIST						0x12
#define APDU_CMD_ENROLL					0x17
#define APDU_CMD_STOREBLOB			0x18		
#define APDU_CMD_RESET					0x21
#define APDU_CMD_INITUPDATE			0x25
#define APDU_CMD_EXTERNALAUTH		0x26
#define APDU_CMD_PUTKEYHEADER		0x27
#define APDU_CMD_LOCK						0x2A
#define APDU_CMD_GETVERSIONS		0x30
#define APDU_CMD_UPDATESTART		0x31
#define APDU_CMD_UPDATEDETAILS	0x32
#define APDU_CMD_UPDATEDATA			0x33
#define APDU_CMD_UPDATEHASH			0x34
#define APDU_CMD_UPDATEEND			0x35
#define APDU_CMD_GETSENSORFWVERSION	0x36
#define APDU_CMD_CALIBRATE			0x45
#define APDU_CMD_READDATA       0x50
#define APDU_CMD_LOADIMAGE			0x52

#define APDU_CMD_IMM_ENROLL          0x80
#define APDU_CMD_POS_VERIFY          0x81
#define APDU_CMD_IMM_SLEEVE_ENROLL   0x82
#define APDU_CMD_IMM_VERIFY          0x83
#define APDU_CMD_IDM_BCTC_VERIFY_CL	0x84
#define APDU_CMD_IDM_BCTC_VERIFY_CT	0x8c

#define APDU_CMD_GET_INTERNAL_BUFFER 0x85
#define APDU_CMD_SET_INTERNAL_BUFFER 0x86
#define APDU_CMD_IDM_ENROLL			     0x87
#define APDU_CMD_IDM_VERIFY			     0x88
#define APDU_CMD_IDM_GETINFO         0x89
#define APDU_CMD_IDM_SINGLE_ENROLL	 0x8a
#define APDU_CMD_SLEEVE_ENROLL       0x8b
#define APDU_CMD_READ_TIMES_BUFFER   0x8d
#define APDU_CMD_GETSEVERSION        0x8e
#define APDU_CMD_DELETE_FINGER       0x8f


#define APDU_DBG_SET_NVM_KEYS        0xf0 // 0xe0
#define APDU_DBG_GEN_RN              0xe1
#define APDU_DBG_TIMER2              0xe2
#define APDU_DBG_ERASE_NVM           0xe3
#define APDU_DBG_GET_BUF             0xe4
#define APDU_DBG_GET_REG             0xe5
#define APDU_DBG_WRITE_NVM           0xe6
#define APDU_DBG_AES                 0xe7
#define APDU_DBG_CRC                 0xe8

#define APDU_DBG_SET_FACTORY_KEYS    0xe0
#define APDU_DBG_GET_NEXT_KEYS       0xea
#define APDU_DBG_SET_NEXT_KEYS       0xeb
#define APDU_DBG_OPEN_SCP03          0xdd

#define APDU_ENCRYPT_TEST 0xF1
#define APDU_DECRYPT_TEST 0xF2

#define APDU_FAST_FINGER_DETECTION  0xf3
#define APDU_REG_READ 	0xf4

uint8_t *apdu_handler( void *arg );

#endif


#ifndef BIO_CTRL_H
#define BIO_CTRL_H

#include "idxSeEmkAPI.h"

#define LED_COLOR_GREEN ( 0x01 )
#define LED_COLOR_RED 	( 0x02 )

extern uint8_t ctrlErr;

void idxBioCtrlInit(void);

/**
* @brief Function for exchanging messages with the MCU
*
* @param[in] bOrdinal indicates the command required
* @param[in] bOptions indicates the transfer options for this (part of the) command
* @param[in] pSendData is a pointer to the data to be sent to the MCU
* @param[in] sendDataSz indicates the size of the data to be sent
* @param[out] pReceiveData is a pointer to where the data received from the MCU should put
* @param[out] receivedDataSz indicates the actual size of the reply data
*
* @return IDEX_SUCCESS if successful,
*         or another error code (taken from wCode in reply header) if not successful
*/
uint16_t idxMcuComms(const uint8_t bOrdinal,
                  const uint8_t bOptions, 
                  const void  *pSendData, 
                  const uint16_t sendDataSz, 
                  void *pReceiveData,
                  uint16_t *receivedDataSz);

uint16_t mcuComms2(uint8_t bOrdinal,
                  uint8_t bOptions, 
                  void  *pSendData, 
                  uint16_t sendDataSz, 
                  void *pReceiveData,
                  uint16_t *receivedDataSz);

#endif // BIO_CTRL_H

/******************************************************************************
* \file  idxSeEmkAPI.c
* \brief code for SE Enroll/Match Kit API fro Secure Ridge Matcher
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2018 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#include <stdint.h>
#include "idxSeEmkDefines.h"

const idex_UnitVersion_t SEversion = {0xff,0xff,0xffff};
const uint8_t SEtype = 0xff;

//--------------------------------   end of file -------------------------------

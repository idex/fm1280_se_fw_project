/** ***************************************************************************
 * \file   idxSDK.h
 * \brief  Interface for IDEX SDK initialization
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
******************************************************************************/
#ifndef _IDX_SDK_H_
#define _IDX_SDK_H_

/** \brief Initialize the IDEX SDK
*
* Perform initialization of the SDK. This initializes the bare minimum that
* is possible and should be called shortly after the card O/S starts and
* before any attempt is made to use any other routine in the SDK.
*/
void idxSdkInit(void);

#endif /* _IDX_SDK_H_ */

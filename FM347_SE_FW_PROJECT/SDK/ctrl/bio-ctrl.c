/******************************************************************************
* \file  bio-ctrl.c
* \brief code for testing SE EMK
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "serialInterface.h"
#include "idxSeEmkAPI.h"
#include "bio-ctrl.h"
#include "idxHalTimer.h"
#include "idxHalLed.h"
#include "idxHalFlash.h"
#include "idxHalUart.h"
#include "idxHalSystem.h"
#include "crypto.h"
#include "idxSeBioDrvAPI.h" // Only for RESPONSE_CRC_KLUDGE - remove when this is removed
#define HDR_SIZE          ( 6 )   // including CRC
#define SETPARAMETER_CMD_DATA_SIZE (3)

static uint8_t cdata[16+10]    __attribute__((aligned(4))); // commands with less than 16 bytes


uint8_t ctrlErr;
extern volatile uint8_t blinkCtrl;
extern hostif_State SerialState;

extern BaseSerialInterface idxCom;
#ifdef sleeve_enrollment_support
extern vu8 sleeve_enable_flag;
extern vu8 process_in_waiting;
extern vu8 timer_a_int_flag;
extern vu8 blinkCtrl;
#endif



extern session_t session;
extern void program_tracker(uint8_t *data_buff, uint8_t data_len);
extern void set_7816IO_to_GPIO(void);
extern void led1on(void);
extern void led2on(void);
extern void SleeveLEDOff(void);
extern void ussleep(uint32_t count);
#ifdef sleeve_enrollment_support
extern void FM1280_timer_init (void);
#endif

void idxBioCtrlInit()
{
  ctrlErr = 0xff;
}

uint16_t mcuComms2(uint8_t bOrdinal,
                  uint8_t bOptions, 
                  void  *pSendData, 
                  uint16_t sendDataSz, 
                  void *pReceiveData,
                  uint16_t *receivedDataSz)
{
	uint16_t i;
	 uint16_t wCode;
	 uint16_t wSize;
	 uint16_t dSize = sendDataSz;
	 uint8_t tmp[8];

	 idx_CommandHeader *pCmdHdr;
	 
	 idxCom.reset();  // initializes pointers to COMM buffers, Headers & Data	
	 pCmdHdr  = idxCom.state->CommandHeader;	
	
	 if(session.state == SEC_CH_OPEN){
	   session.msgCounter++;
	   }
	 
	 if(bOptions & BOPTION_ENCRYPTED){

	   dSize = dataEncrypt(pSendData, sendDataSz);
	 }
	 if(bOptions & BOPTION_SIGNED){
	   sign(bOrdinal, bOptions, pSendData, dSize);
	   if(dSize == 0) pSendData = tmp;	// no initial data so signature saved in tmp 
	   memcpy((uint8_t*)pSendData+dSize, session.macChain, 8);
	   dSize += 8;

	 }
	
	 if(pSendData != idxCom.state->CommandData){ // if data is not in COM buffer
	   for( i = 0 ; i < dSize ; i++){
		 idxCom.state->CommandData[i] = ((uint8_t*)pSendData)[i];
	   }
	 }	
	 // Build Command Header
	 pCmdHdr->wSize    = HDR_SIZE + dSize;
	 pCmdHdr->bOrdinal = bOrdinal;
	 pCmdHdr->bOptions = bOptions;
	 // Send Command = {Hdr + Data}  
	

	 idxCom.SendCommand(idxCom.state->CommandData);
	 // Retrieve relevant info from Reply message = {Hdr + Data}
	 wSize = RD_U16(&idxCom.state->ReplyHeader->wSize);
	 wCode = RD_U16(&idxCom.state->ReplyHeader->wCode);
	
	 if(bOptions & BOPTION_SIGNED){
	   uint8_t cmac[16];
	   memcpy(cmac,idxCom.state->ReplyData,8); // save for matching
	   wSize -= 8; // drop cmac from received response
	 }
	 
	 if( wSize > HDR_SIZE ){
	   uint16_t dataSize = wSize - HDR_SIZE;
	   if( pReceiveData != NULL ){	// there is buffer for decrypting
		 for(i = 0 ; i < dataSize ; i++){
		   ((uint8_t*)pReceiveData)[i] = ((uint8_t *)idxCom.state->ReplyData)[i];		 
		 }
		 if( (bOptions & BOPTION_ENCRYPTED) && ( ( dataSize & 0x0f ) == 0 ) ){
		   dataSize = dataDecrypt((uint8_t*)pReceiveData,wSize - HDR_SIZE);
		 }
	   }
	   if( receivedDataSz != NULL ) *receivedDataSz = dataSize;
	 }
	 return wCode;

}




/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * MCU-COMMS - TH89 & SLE78
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
uint16_t idxMcuComms(const uint8_t bOrdinal,
                  const uint8_t bOptions, 
                  const void  *pSendData, 
                  const uint16_t sendDataSz, 
                  void *pReceiveData,
                  uint16_t *receivedDataSz)
{
  uint16_t i;
  uint16_t wCode;
  uint16_t wSize;
  uint16_t rDataSize;
  idx_CommandHeader *pCmdHdr;

if(session.state == SEC_CH_OPEN){
  uint8_t arg1	= bOrdinal;
  uint8_t arg2	= bOptions | 0x06; // HOSTIF_BOPTION_ENCRYPT | HOSTIF_BOPTION_SIGNED ;
  uint8_t *arg3 = (uint8_t*)pSendData;
  uint16_t arg4 = sendDataSz;
  return mcuComms2(arg1,arg2,arg3,arg4,pReceiveData,receivedDataSz);
}

  idxCom.reset();  // initializes pointers to COMM buffers, Headers & Data   

  pCmdHdr  = idxCom.state->CommandHeader;
  // Build command Data

  if(sendDataSz){
    for( i = sendDataSz-1 ;;){
      ((uint8_t *)idxCom.state->CommandData)[i] = ((uint8_t*)pSendData)[i];
      if(i)i--;
      else break;
    }
  }
  // Build Command Header
  pCmdHdr->wSize    = HDR_SIZE + sendDataSz;
  pCmdHdr->bOrdinal = bOrdinal;
  pCmdHdr->bOptions = bOptions;
  // Send Command = {Hdr + Data}

  idxCom.SendCommand(idxCom.state->CommandData);

  // Retrieve relevant info from Reply message = {Hdr + Data}
  wSize = RD_U16(&idxCom.state->ReplyHeader->wSize);
  wCode = RD_U16(&idxCom.state->ReplyHeader->wCode);
  // Guard against (wSize - HDR_SIZE) overflowing to a large positive value
  rDataSize = (wSize >= HDR_SIZE) ? (wSize - HDR_SIZE) : 0;
  if( pReceiveData != NULL ){
    for(i = 0 ; i < rDataSize ; i++){
      ((uint8_t*)pReceiveData)[i] = ((uint8_t *)idxCom.state->ReplyData)[i];
    }
  }
  if( receivedDataSz != NULL ) *receivedDataSz = rDataSize;
  return wCode;
} 


uint16_t ledStart(void){
  if(1){
    uint16_t wCode;
    cdata[0]=135; cdata[1]=0x10; cdata[2]=0x88;
    wCode = idxMcuComms(SETPARAMETER_CMD,0x00,(void *)cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);  
	ussleep(200);
    if( IDEX_SUCCESS != wCode) { ctrlErr = 0xa0; return wCode;}
  }
  return IDEX_SUCCESS;
}

uint16_t idxHalLedStop(void)
{
  uint16_t wCode;
  cdata[0]=135; cdata[1]=0x00; cdata[2]=0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD,0x00,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);
  ussleep(200);
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb8; return wCode;}	
	return wCode;
}

uint16_t idxHalLed2ndFinger(void){
  // 2nd Finger - LED control here
  uint16_t wCode;
  cdata[0] = 135; cdata[1] =0x80; cdata[2] = 0x88;
  wCode = idxMcuComms(SETPARAMETER_CMD,0x00,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);  
  	idxHalTimerDelayMsec(1500);
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb6; return wCode;}	

  cdata[0] = 135; cdata[1]=0x00; cdata[2]=0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD,0x00,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);
  ussleep(200);
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb7; return wCode;}	
  
  return wCode;
}

uint16_t idxHalLedTouch( uint8_t LedColor ){
  // Touch - LED control here
  uint16_t wCode;
  cdata[0] = 135;	cdata[1] = LedColor; cdata[2] = 0;
  wCode = idxMcuComms(SETPARAMETER_CMD,0x00,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);  
  ussleep(200);
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb4; return wCode;}	
	idxHalTimerDelayMsec(1000);
  cdata[0] = 135;	cdata[1] = 0x00; cdata[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD,0,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);  
  ussleep(200);
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb5; return wCode;}	
  return wCode;
}

uint16_t idxHalLedLastTouch(void){
  // Last Touch - LED control here
  uint16_t wCode;
  cdata[0] = 135;	cdata[1] = 0x01; cdata[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD,0x00,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);  
  ussleep(200);
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb1; return wCode;}	
	//sleepCnt(5000);
	idxHalTimerDelayMsec(500);
  cdata[0] = 135;	cdata[1] = 0x00; cdata[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD,0,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);
  ussleep(200);
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb2; return wCode;}	
  return wCode;
}

uint16_t idxHalLedBlink(void)
{
  uint16_t wCode;
  cdata[0] = 135;	cdata[1] = 0x10; cdata[2] = 0x88;	
  wCode = idxMcuComms(SETPARAMETER_CMD,0x00,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);  
  ussleep(200);
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb0; return wCode;}

  return wCode;
}

uint16_t ledOnOff(uint8_t LedColor){
  uint16_t volatile cnt;
  uint16_t wCode;
  uint16_t duration;
  { 
    cdata[0] = ( LedColor == LED_COLOR_GREEN ) ? 198 : 200;
    wCode = idxMcuComms(GETPARAMETER_CMD,0x00,cdata,1,&duration,NULL);  
    if( IDEX_SUCCESS != wCode) { ctrlErr = 0xb9; return wCode;}	
    //duration = DELAY_1250_MSEC;
  }
  if (RD_U16(&duration) < 100) return   IDEX_SUCCESS;
  cdata[0] = 135;	cdata[1] = LedColor; cdata[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD,0x00,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);  
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xba; return wCode;}	
	idxHalTimerDelayMsec(1000);
  cdata[0] = 135;	cdata[1] = 0x00; cdata[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD,0x00,cdata,SETPARAMETER_CMD_DATA_SIZE,NULL,NULL);  
  if( IDEX_SUCCESS != wCode) { ctrlErr = 0xbb; return wCode;}	
  return wCode;
}

/**********************************************************************************************
* Turn on First finger indicator LED and start flashing the first touch indicator LED on sleeve.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlStart(void)
{
//fm1280
idxHalTimerPeriodicRun(0,NULL);

//-------------------------- SLE78 ----------------------------------  
  return IDEX_SUCCESS;
}

/**
* Stop flashing all the LEDs on sleeve.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlStop(void)
{
//-------------------------- SLE78 ----------------------------------  
//-------------------------- ??? ----------------------------------    
#ifdef sleeve_enrollment_support
	SleeveLEDOff();
	idxHalTimerStopRun();
#endif
    return IDEX_SUCCESS;
}

/**
* Flash finger indicator LED after enrollment finished and turn on 3 touch indicator LEDs.
* first finger finished, flashing first finger indicator LED.
* sencond finger finished, flashing both of the two finger indicator LEDs.
*
* @param[in]  index      LED index number on sleeve.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlEnrollFinished(uint8_t index)
{
//-------------------------- SLE78 ----------------------------------   
//-------------------------- ??? ----------------------------------  
	return IDEX_SUCCESS;  
}

/**
* Turn on the second finger indicator LED on sleeve after first finger enrolled.
* Start flashing first touch indicator LED.
*
* @param[in]  index      LED index number on sleeve.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlNextFinger(void)
{
//-------------------------- SLE78 ----------------------------------  
//-------------------------- ??? ----------------------------------  
#ifdef sleeve_enrollment_support
	uint32_t i;
	for ( i = 0; i < 16; i++ )
	{
	blinkCtrl |= 1*4; // Set counter to 1
	while( blinkCtrl & 0xfc)__wfi(); // Test counter to see if it is 0
	blinkCtrl ^= 2;  // toggle 2nd bit (i.e. new touch)
	}
#endif
return IDEX_SUCCESS;

}

/**
* Turn off all touch indicator LED first then flash the next touch indicator LED on sleeve after each touch.
*
* @param[in]  index      LED index number on sleeve.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlNextTouch(uint8_t index)
{
//-------------------------- SLE78 ----------------------------------  
#ifdef sleeve_enrollment_support

	blinkCtrl |= 8*4; // Set counter to 8
	while( blinkCtrl & 0xfc)__wfi(); // Test counter to see if it is 0
	blinkCtrl ^= 2;  // toggle 2nd bit (i.e. new touch)
	#endif
	return IDEX_SUCCESS;	
}

#ifdef sleeve_enrollment_support

void testBlink(void)
{
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_FINGER,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);

	idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_FINGER,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);
	 idxHalSetLedMode(IDX_HAL_SLEEVE_LED_NEXT_TOUCH,0);
	 idxHalTimerDelayMsec(3000);

	 idxHalSetLedMode( IDX_HAL_SLEEVE_LED_STOP, 0 );



}

#endif




/**
* Function for controlling LEDs.
*
* @param[in]  state      Enrollment state machine.
* @param[in]  index      LED index number on sleeve.
*
* @return      success or error code.
*/
uint16_t idxHalSetLedMode( idxHalLedModeStateType state, uint8_t index )
{
	switch( state )
	{
  case IDX_HAL_CARD_LED_START:
    return ledStart();
	case IDX_HAL_SLEEVE_LED_START:
		return sleeveLEDCtrlStart();
  case IDX_HAL_CARD_LED_NEXT_FINGER:
    return idxHalLed2ndFinger();
	case IDX_HAL_SLEEVE_LED_NEXT_FINGER:
		return sleeveLEDCtrlNextFinger();
  case IDX_HAL_CARD_LED_TOUCH_SUCCESS:
    return idxHalLedTouch(LED_COLOR_GREEN);
	case IDX_HAL_SLEEVE_LED_NEXT_TOUCH:
		return sleeveLEDCtrlNextTouch(index);
  case IDX_HAL_CARD_LED_TOUCH_FAIL:
    return idxHalLedTouch(LED_COLOR_RED);
  case IDX_HAL_CARD_LED_LAST_TOUCH:
    return idxHalLedLastTouch();
  case IDX_HAL_SLEEVE_LED_ENROLL_FINISHED:
		return sleeveLEDCtrlEnrollFinished(index);
  case IDX_HAL_CARD_LED_STOP:
    return idxHalLedStop();
	case IDX_HAL_SLEEVE_LED_STOP:
		return sleeveLEDCtrlStop();
  case IDX_HAL_CARD_LED_BLINK:
    return idxHalLedBlink();
	case IDX_HAL_MATCH_STATE_SUCCESS:
    return IDEX_SUCCESS;         // ledOnOff(LED_COLOR_GREEN)
	case IDX_HAL_MATCH_STATE_FAIL:
    return IDEX_SUCCESS;         // ledOnOff(LED_COLOR_RED)
	default: break;
	}
	
  return IDEX_SUCCESS;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Sleeve-IDM (enroll) cmds test entry poin - TH89
 * This should be moved to IDM SE EMK
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

void setReplyData(uint8_t *buf, uint8_t bsize){
  uint16_t i;
  uint8_t *hdr  = (uint8_t *)idxCom.state->ReplyHeader;  
  uint8_t *dbuf = idxCom.state->ReplyData;
  hdr[0] = 6 + bsize; 
  for (i = 0 ; i < bsize ; i++) dbuf[i] = buf[i];
  idxCom.state->ReplyData += bsize;
}
void setErrorData(uint8_t *buf, uint8_t bsize){
  uint16_t i;
  uint8_t *hdr  = (uint8_t *)idxCom.state->ReplyHeader;  
  uint8_t *dbuf = idxCom.state->ReplyData;
  hdr[0] = 6 + bsize; hdr[2] = 0x00; hdr[3] = 0x63;
  for (i = 0 ; i < bsize ; i++) dbuf[i] = buf[i];
  idxCom.state->ReplyData += bsize;
}
void addReplyData(uint8_t *buf, uint8_t bsize){
  uint16_t i;
  uint8_t *hdr  = (uint8_t *)idxCom.state->ReplyHeader;  
  uint8_t *dbuf = idxCom.state->ReplyData;
  hdr[0] += bsize; 
  for (i = 0 ; i < bsize ; i++) dbuf[i] = buf[i];
  idxCom.state->ReplyData += bsize;
}


//-----------------------------  end of file ------------------------------------

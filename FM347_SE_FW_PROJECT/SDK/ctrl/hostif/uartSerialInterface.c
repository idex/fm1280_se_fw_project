/**************************************************************************//*
* \file  uartSerialInterface.c
* \brief header for object for communication with Idex serial interface
* \author IDEX ASA
* \version 0.0.0
******************************************************************************
* \section License
* <b>(C) Copyright 2012, 2019 IDEX ASA, http://www.idex.no</b<http://www.idex.no%3c/b>>
*****************************************************************************/

/****************************************************************************
*
* This source code is property of IDEX ASA. The source and compiled
* code may only be used together with IDEX fingerprint sensor applications.
*
* This copyright notice must not be modified or removed from the source code.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: IDEX ASA has no
* obligation to support this Software. IDEX ASA is providing the
* Software "AS IS", with no express or implied warranties of any kind.
*
* IDEX ASA is not to be liable for any damages, any relief,
* or for any claim by any third party, arising from use of this software.
*
*****************************************************************************/

#include <stdint.h>
#include <stdbool.h>

#include <string.h> // For memcpy

#include "idxHalGpio.h"
#include "idxHalSystem.h"

#include "idxBaseErrors.h"

#include "idxSeBioDrvAPI.h"

#include "serialInterface.h"
#include "_serialInterface.h"

#define UNUSED(p) if(p){}
#define TIMEOUT_MS ( 5000 )

static bool initialized; // Set false in idxSerialInterfaceInit()

//--------------------------------------------------------------------
static hostif_State SerialState;
#if 0
/* Note that this intiailization is now done explicitly in idxSerialInterfaceInit()*/
{
  NULL,               /* CommandHeader */
  NULL,               /* CommandData   */
  NULL,               /* ReplyHeader   */
  NULL,               /* ReplyData     */
  NULL,               /* env           */
  MODE_UNINITIALIZED, /* mode          */
  FALSE,              /* erase         */
  FALSE               /* enroll        */
};
#endif

/*
* This buffer shouldn't be necessary if the SE BIO API is applied 
* consistently. But to get something going...
*/
#define HOST_IF_BUFFER_SIZE 256
static uint8_t hostifBuffer[HOST_IF_BUFFER_SIZE];

#ifndef IDX_BIO_BUFFER_DATA_OFFSET
#define IDX_BIO_BUFFER_DATA_OFFSET 6
#endif

static void reset(void)
{
  if (!initialized) {
    idxBioDrvInit();
    initialized = 1;
  }
  
  SerialState.CommandHeader = (idx_CommandHeader *)&hostifBuffer[0];
  SerialState.ReplyHeader   = (idx_ReplyHeader *)&hostifBuffer[0];
  SerialState.CommandData   = &hostifBuffer[IDX_BIO_BUFFER_DATA_OFFSET];     
  SerialState.ReplyData     = &hostifBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
}

static int SendCommand(uint8_t *cmd_data)
{
  uint16_t wSize, wCode;
  enum idxBioDrvRet_e ret;

  if (!initialized) {
    idxBioDrvInit();
    initialized = 1;
  }

  // We happen to know that Header and Data are in the same buffer
  // as set by the reset() function above. Check this...
  
  if ((SerialState.CommandHeader != (idx_CommandHeader *)&hostifBuffer[0])     ||
      (SerialState.ReplyHeader   != (idx_ReplyHeader *)&hostifBuffer[0])       ||
      (SerialState.CommandData   != (uint8_t *)&hostifBuffer[IDX_BIO_BUFFER_DATA_OFFSET]) ||
      (SerialState.ReplyData     != (uint8_t *)&hostifBuffer[IDX_BIO_BUFFER_DATA_OFFSET]))
  {
    return 0;
  }
  
  wSize = SerialState.CommandHeader->wSize;
  if (cmd_data != SerialState.CommandData)
  {
    uint16_t size = wSize - sizeof(idx_CommandHeader);
    memcpy(SerialState.CommandData, cmd_data, size);
  }
  
  // At this point the Command header and Command data are in the same buffer

  // Reorganize the command header into the format expected by idxBioDrvSend()
  hostifBuffer[0] = 0x84;
  hostifBuffer[1] = 0x00;
  // Leave bOrdinal in hostifBuffer[2]
  // Leave bOptions in hostifBuffer[3]
  // idxBioDrvSend() expects wSize to have not yet counted the CRChdr bytes,
  // since these are added inside this function.
  if (wSize >= 2) wSize -= 2;
  hostifBuffer[4] = wSize & 0xff;
  hostifBuffer[5] = wSize >> 8;

#if 1

  ret = idxBioDrvSend(HOST_IF_BUFFER_SIZE, hostifBuffer);
  if (IDX_BIO_DRV_OK == ret) { 
    ret = idxBioDrvReceive(HOST_IF_BUFFER_SIZE, hostifBuffer);
  }

  if (IDX_BIO_DRV_OK == ret) {
    // Reorganize the response header from the format returned by
    // idxBioDrvReceive().  Note that wSize and wCode are only decoded from
    // buffer if reception succeeded.
    
    // XXXXXXXXXXXXX Also note the documentation of idxBioDrvReceive()
    // doesn't agree with the current implementation - I think it's the
    // documentation that's wrong - in that wSize is returned in
    // bytes 4 and 5, and the wCode in bytes 2 and 3 - matching the
    // format of data passed into idxBioDrvSend() XXXXXXXXXXXXXXXXX
    
    wCode = (((uint16_t) hostifBuffer[3]) << 8) | hostifBuffer[2];
    wSize = (((uint16_t) hostifBuffer[5]) << 8) | hostifBuffer[4];
    // idxBioDrvReceive() returns wSize not counting the CRChdr bytes,
    // which it checked and effectively removed.
    wSize += 2;
    ret = 0; // Unclear what return values are expected
  } else {
    wCode = IDEX_ERR_COMM;
    wSize = 0;
    ret = -1; // Unclear what return values are expected
  }
#else
  // For doing some testing on the simulator...
  hostifBuffer[0] = 0x42; // Two unused bytes
  hostifBuffer[1] = 0x43;
  hostifBuffer[2] = 0x00; // wCode
  hostifBuffer[3] = 0x90;
  hostifBuffer[4] = 0x04; // wSize
  hostifBuffer[5] = 0x00;
  
  wCode = (((uint16_t) hostifBuffer[3]) << 8) | hostifBuffer[2];
  wSize = (((uint16_t) hostifBuffer[5]) << 8) | hostifBuffer[4];
  ret = 0;
#endif
  
#if (RESPONSE_CRC_KLUDGE)
  // Kludge things to look like what comes back on the SLE78
  hostifBuffer[4] = hostifBuffer[0]; // Put back the CRC
  hostifBuffer[5] = hostifBuffer[1];
  hostifBuffer[0] = wSize & 255;
  hostifBuffer[1] = wSize >> 8;
  hostifBuffer[2] = wCode & 255;
  hostifBuffer[3] = wCode >> 8;
#else
  // In a sane world update these 16-bit values to be in native
  // endianess
  SerialState.ReplyHeader->wSize = wSize;
  SerialState.ReplyHeader->wCode = wCode;
#endif

  return ret;
}

/*****************************************************************************
 * @brief Define the Communication IF 
 */
BaseSerialInterface idxCom;
/* Note that this intiailization is now done explicitly in idxSerialInterfaceInit()
{
  &SerialState,
  reset,
  SendCommand,
  TIMEOUT_MS
};
*/

void idxSerialInterfaceInit()
{
  /*
  * On platforms with no initialized data capability, explicitly
  * initialize
  */
  idxCom.state       = &SerialState;
  idxCom.reset       = reset;
  idxCom.SendCommand = SendCommand;
  idxCom.timeout_ms  = TIMEOUT_MS;

  memset(&SerialState, 0, sizeof(SerialState));
  SerialState.mode = MODE_UNINITIALIZED;

  initialized = false;
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


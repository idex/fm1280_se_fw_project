#ifndef HOSTIF_H
#define HOSTIF_H

#define HOSTIF_CMD_INITIALIZE      0x00
#define HOSTIF_CMD_UNINITIALIZE    0x01
#define HOSTIF_CMD_ACQUIRE         0x02
#define HOSTIF_CMD_GETIMAGE        0x04
#define HOSTIF_CMD_MATCHTEMPLATES  0x07
#define HOSTIF_CMD_GETPARAM        0x08
#define HOSTIF_CMD_SETPARAM        0x09
#define HOSTIF_CMD_STOREPARAMS     0x0C
#define HOSTIF_CMD_GETSENSORINFO   0x0E
#define HOSTIF_CMD_DELETERECORD    0x11
#define HOSTIF_CMD_LIST            0x12
#define HOSTIF_CMD_ENROLL          0x17
#define HOSTIF_CMD_STOREBLOB       0x18    
#define HOSTIF_CMD_RESET           0x21
#define HOSTIF_CMD_GETUID          0x24
#define HOSTIF_CMD_INITUPDATE      0x25
#define HOSTIF_CMD_EXTERNALAUTH    0x26
#define HOSTIF_CMD_PUTKEY          0x27
#define HOSTIF_CMD_FLASHUPDATE     0x28
#define HOSTIF_CMD_LOCK            0x2A
#define HOSTIF_CMD_GETVERSIONS     0x30
#define HOSTIF_CMD_UPDATESTART     0x31
#define HOSTIF_CMD_UPDATEDETAILS   0x32
#define HOSTIF_CMD_UPDATEDATA      0x33
#define HOSTIF_CMD_UPDATEHASH      0x34
#define HOSTIF_CMD_UPDATEEND       0x35
#define HOSTIF_CMD_GETSENSORFWVERSION  0x36
#define HOSTIF_CMD_CALIBRATE       0x45
#define HOSTIF_CMD_READDATA        0x50
#define HOSTIF_CMD_LOADIMAGE       0x52
#define HOSTIF_CMD_BIODATAFLUSH    0x55

#define HOSTIF_CMD_INITIALIZE_DATA_SIZE      (1)
#define HOSTIF_CMD_UNINITIALIZE_DATA_SIZE    (0)
#define HOSTIF_CMD_ACQUIRE_DATA_SIZE         (1)
#define HOSTIF_CMD_GETPARAM_DATA_SIZE        (1)
#define HOSTIF_CMD_SETPARAM_DATA_SIZE        (3)
#define HOSTIF_CMD_STOREPARAMS_DATA_SIZE     (0)
#define HOSTIF_CMD_DELETERECORD_DATA_SIZE    (2)
#define HOSTIF_CMD_LIST_DATA_SIZE            (3)
#define HOSTIF_CMD_RESET_DATA_SIZE           (0)
#define HOSTIF_CMD_PUTKEY_DATA_SIZE          (82)
#define HOSTIF_CMD_FLASHUPDATE_DATA_SIZE     (1)
#define HOSTIF_CMD_UPDATESTART_DATA_SIZE     (0)
#define HOSTIF_CMD_UPDATEDETAILS_DATA_SIZE   (41)
#define HOSTIF_CMD_UPDATEEND_DATA_SIZE       (0)
#define HOSTIF_CMD_CALIBRATE_DATA_SIZE       (1)
#define HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE    (2)

#define HOSTIF_BOPTION_ENCRYPT     0x04
#define HOSTIF_BOPTION_SIGNED      0x02


#endif

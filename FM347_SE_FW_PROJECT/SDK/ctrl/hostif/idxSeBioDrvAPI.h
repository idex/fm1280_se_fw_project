/** ***************************************************************************
 * \file   idxSeBioDrvAPI.h
 * \brief  Define the IDEX SE-biometric Driver API
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2018, 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *
 * \mainpage IDEX SE biometric driver API
 *
 * The SE biometric driver API provides a recommended API to provide
 * access to a "biometric host interface" that provides communication between
 * the Secure Element and a biometric subsystem MCU. Physically this interface
 * may be a dedicated electrical interface, such as SPI, or it might be
 * provided by shared usage of an ISO7816 interface.
 *
 * Typically, when an application running on the SE determines that it needs
 * access to the biometric subsystem, for example to perform biometric
 * verification, it will call a related API - the "SE biometric API". The
 * biometric API handler will prepare messages that need to be sent over
 * the host interface and will call idxBioEncryptSignSend() to send them
 * and idxBioReceiveVerifyDecrypt() to receive their responses.
 *
 * idxBioEncryptSignSend() and idxBioReceiveVerifyDecrypt() need to be provided
 * by the user. They are primarily concerned with encryption/decryption and
 * signature generation/checking. In order to actually send or receive data
 * over the host interface their implementations  may call the routines
 * idxBioDrvSend() and idxBioDrvReceive() (provided by this API).
 *
 * \section WTXsupport WTX support
 *
 * In order to properly maintain WTX signaling during biometric commands,
 * which may execute for a longer than the WTX interval, an integration must
 * provide some infrastructure for WTX handling.
 *
 * Several approaches have been used for this.
 *
 * In integrations that use SPI for the host interface protocol the approach
 * has been to have the SE remain the WTX timing master amd use a side-band
 * handshake signal to indicate when the biometric subsystem should be
 * suspended (and enter a low power state) to allow WTX signalling to occur.
 * Such integrations will use idxBioDrvSuspend() and idxBioDrvResume().
 *
 * An experimental implementation using T=1 protocol over ISO7816 uses no
 * side-band handshaking signals, instead conveying the need to suspend by
 * messgaes over the communication channel. In this case the WTX timing
 * responsibility is shared, the SE being responsible to send the suspension
 * message at times when the T=1 protocol requires that it will send the next
 * block, while the MCU has that responsibility when it is expected to send
 * the next block. Such integrations might implement idxBioDrvSuspend() to
 * send the required suspension message. Since the actual WTX signalling is
 * initiated from within the protocol handler this implementation has no
 * reason to provide idxBioDrvResume(), since the channel will have already
 * been resumed before the idxBioDrvSuspend() returns.
 *
 * Another implementation, denoted "2-wire UART" sends raw IDEX host interface
 * commands using the UART. In this implementation two different approaches
 * are taken:
 * 
 *   If the card is operating contactless then the system works very like
 *   the SPI implementation, described above. At times when there is no actual
 *   data transfer occuring, the I/O and CLK lines are repurposed as
 *   handshaking lines. The SE remains the timing master and uses the
 *   side-band handshaking line to put the biometric subsystem in a low-power
 *   state. In this mode idxBioDrvSuspend() and idxBioDrvResume() are used.
 *
 *   If the card is operating contact-based then the approach with the
 *   handshaking lines cannot be used since the clock signal is required.
 *   In this mode messages are used to control WTX signalling. The WTX
 *   timing responsibility is shared. At times when the SE is expected to
 *   send next (to send a host interface command) the SE may instead call
 *   idxBioDrvTerminate() to terminate the channel and put the biometric
 *   subsystem into a low power state so that the WTX communication can
 *   occur. At times that the biometric subsystem is due to send a
 *   a response, it may also send a message to suspend the channel, but
 *   since, in this case, the channel is still required, the channel is
 *   restarted after the WTX signalling has been sent as a result of a call
 *   made from the HAL layer.
 */
#ifndef _IDX_SE_BIO_DRV_API_H_
#define _IDX_SE_BIO_DRV_API_H_

// Kludge - make response data look like that returned from SPI
#define RESPONSE_CRC_KLUDGE (1)

#include <stdint.h>

/** \brief Return type for driver API calls.
 */
enum idxBioDrvRet_e
{
  IDX_BIO_DRV_OK = 0,     //!< Successful call
  IDX_BIO_DRV_TERMINATED, //!< Channel was terminated (success)
  IDX_BIO_DRV_ERROR       //!< Unspecified error
};

/** \brief Indicate WTX due now
 *
 * May be used as the \b timeRequired parameter to idxBioDrvWTX() to
 * indicate that WTX signalling is due now.
 */
#define IDX_BIO_DRV_WTX_NOW ((uint16_t) (~0))

/** \brief Initialize driver internal state.
 *
 * idxBioDrvInit() should be called once before the first call to any
 * of the other functions defined by this API.
 */
void idxBioDrvInit(void);

/** \brief Send a command over the biometric host interface
 *
 * It is assumed that data in the buffer is preceded by a header formatted
 * as follows:
 *
 *  * Constant byte 0x84
 *  * Constant byte 0x00
 *  * Byte \a bOrdinal.
 *  * Byte \a bOptions.
 *  * Unsigned 16-bit \a wSize.
 *
 * \a wSize will reflect the size of the data that follows the header, any
 * padding that was added during encryption, any signature that has been added,
 * plus four - bOrdinal, bOptions, and wSize itself.
 *
 * The data in the buffer is modified to attain the required format for transmission
 * over the particular host interface being used.
 *
 * For example, in the case of transmission over SPI, \a wSize will be read from its
 * position in the buffer, incremented by two, and written into the first two bytes
 * of the buffer. \a bOptions is modified to set the least signficiant bit. The fifth
 * and sixth bytes will be cleared, and a CRC calculated over the whole command.
 * \a wSize will incremented by a further two, a CRC is calculated over the first
 * four bytes of the buffer and written into the fifth and sixth bytes.
 *
 * For physical interfaces where WTX signalling cannot interrupt communication,
 * the driver then determines whether it has sufficient time to send the command
 * over the host interface before the next WTX, waiting if necessary for WTX
 * signaling to take place before it starts to send the data.
 *
 * \param[in] BufferSize   Size of available buffer at \b pBuffer.
 *                         This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer      Pointer to communication buffer.
 * \return                 \ref IDX_BIO_DRV_OK normally, any other value
 *                         indicating an error.
 */
enum idxBioDrvRet_e idxBioDrvSend(uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Receive a response over the biometric host interface
 *
 * Wait, in a low power state, until data is received, then place it in the
 * buffer at a predetermined offset. The received data will be in a format
 * specific to the particular host interface being used.
 *
 * Once data has been received it is reorganized so that the actual data
 * follows a header with the following format: 
 *
 * * Two unused bytes
 * * Unsigned 16-bit \a wSize, indicating the total amount of data including any
 *   padding (due to encryption) and the R-MAC (if present).
 * * 16-bit unsigned value, \a wCode.
 *
 * Typically, the caller will proceed to verify any signature, decrypt as
 * appropriate, and remove any padding, before passing the resulting data
 * back to the biometric API handler (or other component).
 *
 * \param[in] BufferSize   Size of available buffer at \b pBuffer.
 *                         This call requires a buffer of at least \e TBD bytes.
 * \param[out] pBuffer     Pointer to communication buffer.
 * \return                 \ref IDX_BIO_DRV_OK normally, any other value
 *                         indicating an error.
 */
enum idxBioDrvRet_e idxBioDrvReceive(uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Terminate the Biometric subsystem
 *
 * In integrations, such as those using 2-wire UART (ISO7816) in Contact Based
 * mode, where cooperation on WTX signalling is managed by message exchange
 * over the communication protocol, idxBioDrvTerminate() may be implemented to
 * provide an interface that may be called (at times where the SE is due to
 * send next) to send messages that result in the channel between SE and MCU
 * being terminated, and the biometric subsystem being placed in a low power
 * state.
 *
 * If the reason for termination is that WTX signalling is due to be sent,
 * it will sent after this routine returns. If further biometric commands are
 * then required, a future call to idxBioDrvSend() will reopen the channel.
 */
enum idxBioDrvRet_e idxBioDrvTerminate(void);

/** \brief Suspend the Biometric subsystem
 *
 * In integrations where the SE remains the WTX timing master, when
 * it is necessary to send a WTX. This places the biometric subsystem
 * into a low power state. In such integrations, WTX signalling is
 * performed, by the caller, following the return from idxBioDrvSuspend()
 * and before a call to idxBioDrvResume().
 *
 * In integrations, such as those using T=1 ISO7816 as the interface between
 * the biometric MCU and the SE, where cooperation on WTX signalling is managed
 * by message exchange over the communication protocol, idxBioDrvSuspend() may
 * be implemented to provide an interface that may be called (in periods where
 * the SE is currently the WTX timing master) to initiate the exchange of
 * messages that result in the channel between SE and MCU being suspended ready
 * for WTX signalling. In such integrations the call to idxBioDrvWTX() that
 * performs the actual WTX signalling will typically be made from the
 * communication protocol handler either as a result of a call to
 * idxBioDrvSuspend() on the SE, or as a result of similar actions  on the MCU
 * if it should currently be the WTX timing master. If initiated as a result of
 * a call to idxBioDrvSuspend() on the SE, the call to idxBioDrvWTX() will
 * occur and the communication channel will also have been re-established before
 * idxBioDrvSuspend() returns, so that a call to idxBioDrvResume() serves no
 * purpose.
 */
enum idxBioDrvRet_e idxBioDrvSuspend(void);

/** \brief Resume the Biometric subsystem
 *
 * Used in integrations where the SE remains the WTX timing master,
 * following an earlier call to idxBioDrvSuspend(). This allows the
 * biometric subsystem to return to operation and make progress on
 * biometric operations.
 */
enum idxBioDrvRet_e idxBioDrvResume(void);

/** \brief Interface to WTX timer
 *
 * idxBioDrvWTX() provides a recommended interface that may be used by
 * implementations of idxBioDrvSend() and idxBioDrvReceive() to access the SE's
 * WTX timer.
 *
 * idxBioDrvWTX() may be called in three situations.
 *
 * -# Where communication over the physical host interface cannot be
 *    interrupted by WTX signaling, it may be called with \b timeRequired,
 *    indicating how long is required before a WTX transfer can occur.
 * -# In order to determine time until the next scheduled WTX transfer,
 *    it may be called with \b timeRequired set to zero.
 * -# In order to actually perform WTX signalling it may be called with
 *    \b timeRequired set to \ref IDX_BIO_DRV_WTX_NOW.
 *
 * Unless \b timeRequired is zero or \ref IDX_BIO_DRV_WTX_NOW,
 * idxBioDrvWTX() should not return until there is \b timeRequired before
 * the next WTX. If necessary it should wait for (or cause) the WTX to be
 * signaled and return after WTX signaling.
 * 
 * If \b timeRequired is zero, idxBioDrvWTX() will always return immediately
 * regardless of the time remaining until the next WTX signalling is due.
 *
 * If \b timeRequired is \ref IDX_BIO_DRV_WTX_NOW, idxBioDrvWTX()
 * will not return until after WTX signaling has occurred.
 *
 * The time (possibly after a WTX transfer has occurred) until the \e next
 * WTX transfer is required is returned at *\b pTimeToWTX.
 *
 * \param[in] timeRequired Unless it has the value \ref IDX_BIO_DRV_WTX_NOW,
 *                         it indicates the time required by the caller
 *                         before a WTX can be handled. If
 *                         \ref IDX_BIO_DRV_WTX_NOW it indicates that a
 *                         WTX transfer should occur now.
 * \param[out] pTimeToWTX  Indicates the time until the next WTX transfer
 *                         is due.
 * \return                 \ref IDX_BIO_DRV_OK normally, any other value
 *                         indicating an error.
 *
 * \todo Determine resolution and data types (number of bits) of parameters.
 * Milliseconds seems too course, microseconds too fine (and allowing only
 * 65ms for a 16-bit type, which is not enough to span typical contact-mode
 * waiting times). Perhaps units of 16 microseconds, allowing a range of
 * just over one second in 16-bits?
 */
enum idxBioDrvRet_e idxBioDrvWTX(uint16_t timeRequired, uint16_t *pTimeToWTX);

#endif /*_IDX_SE_BIO_DRV_API_H_*/

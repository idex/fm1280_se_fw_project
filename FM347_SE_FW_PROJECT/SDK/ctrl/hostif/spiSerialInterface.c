/**************************************************************************//*
* \file  spiSerialInterface.c
* \brief header for object for communication with Idex serial interface
* \author IDEX ASA
* \version 0.0.0
******************************************************************************
* \section License
* <b>(C) Copyright 2012, 2019 IDEX ASA, http://www.idex.no</b<http://www.idex.no%3c/b>>
*****************************************************************************/

/****************************************************************************
*
* This source code is property of IDEX ASA. The source and compiled
* code may only be used together with IDEX fingerprint sensor applications.
*
* This copyright notice must not be modified or removed from the source code.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: IDEX ASA has no
* obligation to support this Software. IDEX ASA is providing the
* Software "AS IS", with no express or implied warranties of any kind.
*
* IDEX ASA is not to be liable for any damages, any relief,
* or for any claim by any third party, arising from use of this software.
*
*****************************************************************************/
#include <stdint.h>
#include <string.h> // For memcpy


#include "idxHalGpio.h"
#include "idxHalSystem.h"
#include "idxHalSpi.h"
#include "idex_cmds.h"


#include "idxBaseErrors.h"
#include "idxSeEmkDefines.h"
#include "serialInterface.h"
#include "_serialInterface.h"
#include "hw.h"
#include "main.h"

#define UNUSED(p) if(p){}
#define TIMEOUT_MS ( 5000 )

#define FAR
typedef uint8_t  UINT8;
typedef uint16_t UINT16;
typedef uint32_t UINT32;

typedef uint8_t * PUINT8;
typedef uint16_t * PUINT16;
// SPI clock divider
#define IDEX_fsys_div_2     ((u32)0) 
#define IDEX_fsys_div_4     BIT5
#define IDEX_fsys_div_8     BIT6
#define IDEX_fsys_div_16    BIT6|BIT5
#define IDEX_fsys_div_32    BIT7
#define IDEX_fsys_div_64    BIT7|BIT5
#define IDEX_fsys_div_128	BIT7|BIT6
#define IDEX_fsys_div_256	BIT7|BIT6|BIT5

#define FRAME_BYTE_GAP_CFG         0xFFFF0000
#define INTERFACE_INT_EN           BIT31
#define HSPI_SEL                   BIT30
#define INTERFACE_MASTER_SEL       BIT29


#ifndef IDX_BIO_BUFFER_DATA_OFFSET
#define IDX_BIO_BUFFER_DATA_OFFSET 6
#endif

//static bool initialized; // Set false in idxSerialInterfaceInit()
extern uint8_t COMBUF[]; 
vu8 HSI_change;
vu8 HSI_status;
vu8 hsiIntFlag;
vu32 system_frequency;
vu8 communication_wdt_flag;
vu32 communication_wdt_counter;

vu8 Idex_free_timer_flag;
vu32 Idex_free_timer_counter;

vu8 reset_dedicated_flag;

extern vu8 idex_wtx_count;
extern vu8 idex_wtx_flag;

void idex_mosi_return_to_high(void);

extern void wait_for_HSI_rising(void);
  
static uint16_t HBHandshake_Start1(idxHalSpiSpeedType spiSpeed)
	{ 
  uint16_t ret;
  uint32_t spi_frequency;
  switch(spiSpeed)
  	{
  	case IDX_HAL_SPI_SPEED_HIGH:
		spi_frequency=IDEX_fsys_div_4;

		break;
	default:
		spi_frequency=IDEX_fsys_div_16;
		break;
  	}
  
  //frequency = idxHalSystemDesiredFrequency(IDX_HAL_SYSTEM_FREQUENCY_LOWEST);
  //idxHalSystemSetFrequency(frequency);
  //ret = idxHalGpioInit(IDX_HAL_GPIO_CONFIG_SPI_5PIN);
  ret =HBHandshake_Start();
  	//return 0x9000;
  if(0 == ret)
  {
    //frequency = idxHalSystemDesiredFrequency(IDX_HAL_SYSTEM_FREQUENCY_HIGH);
    //idxHalSystemSetFrequency(frequency);
    idxHalSpiInit(spi_frequency);
  }
  return ret;
}

typedef enum {
  EV_HSI_FALLING_EDGE = 10,
  EV_HSI_RISING_EDGE,
  EV_START_HANDSHAKE
} HsEvent_t;

// WTX state machine (triggered by WTX events)
typedef enum {
  WTX_IDLE,
  WTX_SIGNALLING,
} WtxFsmState_t;

static WtxFsmState_t wtxFsmState = WTX_IDLE;

// handshake state machine (triggered by HSI/RDY events)
typedef enum {
  HS_IDLE,
  HS_WAIT_HSI_FALLING_EDGE,
  HS_WAIT_HSI_RISING_EDGE,
  HS_WAIT_RDY_RISING_EDGE,
  HS_WAIT_RESUME
} HsFsmState_t;

static HsFsmState_t hsFsmState = HS_IDLE;
static HsFsmState_t hsFsm(uint8_t event);

// communication state machine triggered from handshake state machine
typedef enum {
  COM_IDLE,
  COM_SENDING_CMD_HDR,
  COM_RECEIVING_CMD_ACK,
  COM_RECEIVING_CMD_ACK_NO_DATA,
  COM_SENDING_CMD_DATA,
  COM_SENDING_LAST_CMD_DATA,
  COM_RECEIVING_RSP_HDR,
  COM_SENDING_RSP_ACK,
  COM_RECEIVING_RSP_DATA
} ComFsmStateId_t;

typedef struct {
  ComFsmStateId_t id;
  uint16_t        dataSize;
  uint8_t          *ptrData;
  uint8_t         replyAck;
//  uint8_t         test;
} ComFsmState_t;

static ComFsmState_t comFsmState;
hostif_State SerialState;
extern BaseSerialInterface idxCom;
static ComFsmStateId_t comFsm(void);
static int SendCommand(uint8_t *cmd_data);


extern void program_tracker(uint8_t *data_buff, uint8_t data_len);

volatile uint8_t cmdRdy;
extern volatile uint8_t  RDY_Signal_Detected;
volatile uint8_t wtxInt = 0;
static HsFsmState_t hsFsm(uint8_t event);
static void hsiCallback(void *param );
extern void idxHalTimerDelayMsec(uint16_t msec);//done

//--------------------------------------------------------------------


void SPI_IF_reset(void)
{
	 SerialState.CommandHeader = (idx_CommandHeader *)&COMBUF[0];
	  SerialState.ReplyHeader	= (idx_ReplyHeader *)&COMBUF[0];	   // OK to overlap Command and Reply headers
	SerialState.CommandData	= &COMBUF[6];	  
	 SerialState.ReplyData 	= &COMBUF[6];	   // OK to overlap Command and Reply data buffers
}



void idx_system_data_initial(void)
{
	idex_wtx_flag=0;
	system_frequency=0;
	communication_wdt_flag=0;
	communication_wdt_counter=0;
	Idex_free_timer_flag=0;
	Idex_free_timer_counter=0;
	reset_dedicated_flag=0;
}



void idxcom_initial(void)
{

	SerialState.CommandHeader=NULL;			/* CommandHeader */
	SerialState.CommandData=NULL;				/* CommandData	 */
	SerialState.ReplyHeader=NULL;				/* ReplyHeader	 */
	SerialState.ReplyData=NULL;				/* ReplyData	 */
	SerialState.env=NULL;				/* env			 */
	SerialState.mode=MODE_UNINITIALIZED; /* mode 		 */
	SerialState.erase=FALSE;				/* erase		 */
	SerialState.enroll=FALSE;				/* enroll		 */


	idxCom.state=&SerialState;
	idxCom.reset=&SPI_IF_reset;
	idxCom.SendCommand=&SendCommand;
	idxCom.timeout_ms=TIMEOUT_MS;
}




void ussleep(uint32_t count)
{
	volatile uint32_t i;
	for(i=0;i<count;i++)
	{
	__asm__ ("nop");
	__asm__ ("nop");
	__asm__ ("nop");	
	__asm__ ("nop");
	__asm__ ("nop");

	}

}

void gpio_initial(void)
{
	reg_pad_function_config = (reg_pad_function_config & (~(BIT0 | BIT1| BIT2))) |  BIT0;
	reg_pad_mode_config = (reg_pad_mode_config & 0xfffffff0) | BIT0 | BIT3;
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT16 | BIT17 | BIT18))) | BIT16;
	reg_pad_mode_config = (reg_pad_mode_config & 0xfff0ffff) ;
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT10 | BIT9 | BIT8))) | BIT8;
	reg_pad_mode_config = (reg_pad_mode_config & 0xfffff0ff)|BIT10|BIT8 ;

	reg_gpio_dout |= GPIO5_DOUT;  //clk output high

}




u16 HandShake_HW(void)
{
	u16 ret=0;//IDEX_ERR_COMM
	u32 timeout=100;//first set to ms
	//hsi/hso initial to gpio

	reg_pad_function_config = (reg_pad_function_config & (~(BIT0 | BIT1| BIT2))) |  BIT0;
	reg_pad_mode_config = (reg_pad_mode_config & 0xfffffff0) | BIT0 | BIT3;
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT16 | BIT17 | BIT18))) | BIT16;
	reg_pad_mode_config = (reg_pad_mode_config & 0xfff0ffff) ;
	//hsi interrupt config
	
	reg_pad_interrupt_config = (reg_pad_interrupt_config & 0xfffffff0) |POSEDGE_INT;// POSEDGE_INT;
	reg_pad_interrupt_flag =0;
	NVIC_ICPR = GPIO_INT_BIT;//clear gpio interrupt flag
	NVIC_ISER = GPIO_INT_BIT;//enable gpio interrupt
	
	//clk output high
	reg_gpio_dout |= GPIO5_DOUT;  
	//check if hsi is high
	while(0<timeout)
		{
		timeout--;
		if((reg_gpio_din&GPIO1_DIN) == GPIO1_DIN)break;
		idxHalTimerDelayMsec(10);
		}
	if(timeout==0)
		{
		//try to recover
		for(timeout=0;timeout<50;timeout++)
			{
			reg_gpio_dout &= ~GPIO5_DOUT;  
			ussleep(20);
			reg_gpio_dout |= GPIO5_DOUT;  
			ussleep(20);
			if((reg_gpio_din&GPIO1_DIN) == GPIO1_DIN)break;
			}
		if(timeout>48)		return IDEX_ERR_COMM_2;

		}
	timeout=10000;//second set to us
	reg_gpio_dout &= ~GPIO5_DOUT;  
	//check hsi goto low;
	while(0<timeout)
		{

		timeout--;
		if((reg_gpio_din&GPIO1_DIN) != GPIO1_DIN)break;
		ussleep(20);
		}
	if(timeout==0)return IDEX_ERR_COMM_2;//IDEX_ERR_COMM
	reg_gpio_dout |= GPIO5_DOUT;
	//check hsi goto high
	timeout=10000;//second set to us
	while(0<timeout)
		{

		timeout--;
		if((reg_gpio_din&GPIO1_DIN) == GPIO1_DIN)break;
		ussleep(20);
		}

	if(timeout==0)return IDEX_ERR_COMM_2;
	return ret;
}



void HW_spi_Master_transfer_DM_SS_soft(u8* tx_data_buf, u32 tx_data_length, u8* rx_data_buf, u32 rx_data_length )
{
	u32 i=0;
	u32 j=0;
	u32 counter=tx_data_length;
	u8 temp=0;
	u8 send_byte=0;
	u8 *pointer_tx;
	u8 *pointer_rx;
	pointer_tx=tx_data_buf;
	pointer_rx=rx_data_buf;
	j=rx_data_length;
	if(tx_data_length<rx_data_length)counter=rx_data_length;
	if(counter)
		{
		spcr|=SPI_CLR_TXBUF|SPI_CLR_RXBUF|SPI_CLR_MASTER_ERR;
		for(i=0;i<counter;i++)
			{
			send_byte=(0==tx_data_length)?(0):(*pointer_tx);
			spi_txbuf = send_byte;
			while((spsr & SPI_RXBUF_FULL_FLAG) == 0);
			temp=spi_rxbuf;
			if(j)
				{
				*pointer_rx=temp;
				pointer_rx++;
				j--;
				}
			pointer_tx++;
			}
		}
}


u32 hw_interface_init_software(u32 interface_sel, u8 on_off_switch, u32 interface_config ) 
{
	u32 mask_value;

			mask_value = ~(/*PAD04_MASK | */PAD06_MASK | PAD07_MASK | PAD08_MASK) ;
		    if(on_off_switch == 0)			// off
		    {
			   reg_reset_ctrl |= SPI_SOFT_RST ;
			   reg_hspcr1 = 0x00 ; 	
			   reg_clk_ctrl0 &= ~(SPI_CLK_EN | HSPI_CLK_EN);
			   NVIC_ICER = SPI_INT_BIT;
			   NVIC_ICPR = SPI_INT_BIT;
		    }
		    else										// on
		    {
			    NVIC_ICPR = SPI_INT_BIT;
			    NVIC_ICER = SPI_INT_BIT;		
       			
				{	
				   reg_clk_ctrl0 |= SPI_CLK_EN;
				   spcr =   SPI_CLR_TXBUF
					      | SPI_CLR_RXBUF
					      | SPI_CLR_MASTER_ERR
					      | SPI_CLR_SLAVE_ERR
					      | SPI_MASTER_SSN_SOFT_CFG
					      //| SPI_MASTER_SSN_SOFT_CFG_EN
					      | (interface_config & (  SPI_MASTER_BAUD_CFG 
					                             | SPI_DATA_FRAME_CFG
					                             | SPI_CLK_POLARITY_CFG 
					                             | SPI_CLK_PHASE_CFG    
					                             | SPI_MASTER_SAMPLE_MODE_CFG
                                                 | SPI_SLAVE_SEND_MODE_CFG  
					                             | SPI_SLAVE_INPUT_FILTER_EN ) );
				   if(interface_config & INTERFACE_MASTER_SEL)
					  spcr |= SPI_WORKING_MODE_CFG ;   
					
				   //if((interface_config & INTERFACE_MASTER_SEL) )				// master
			       {
			          reg_pad_function_config = (reg_pad_function_config & mask_value) |
									        (  (SPI_PAD_CONFIG << 16) 
					                         | (SPI_PAD_CONFIG << 12) 
					                         | (SPI_PAD_CONFIG << 8 ) 
					                         /*|  SPI_PAD_CONFIG      */) ;			
				      reg_pad_mode_config = (reg_pad_mode_config & mask_value) | PAD07_PU_EN | PAD06_PU_EN | PAD06_OEB;
				   }			       
                   spcr |= SPI_ACTIVE ;
			    }        
		    }
		    return 0;

}

void hw_spi_m_init (u32 SPI_speed_cfg)
{
    //Init SPI Master
    hw_interface_init_software(0x01, 0x01, (0x07|BIT30|BIT29|SPI_speed_cfg)); 
    reg_pad_mode_config = (reg_pad_mode_config & (~ (BIT11 | BIT10))) | BIT10;  //使能MISO下拉，防止电流倒灌1601


}
void idex_wtx_debug_test(void)
{
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT12 | BIT13 | BIT14))) | BIT12;
	reg_pad_mode_config = (reg_pad_mode_config & 0xffff0fff) ;//output
	reg_gpio_dout &=~ GPIO4_DOUT;//output "0"
	ussleep(30);
	reg_gpio_dout |= GPIO4_DOUT;//output "1"
	ussleep(30);
	reg_gpio_dout &=~ GPIO4_DOUT;//output "0"
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT12 | BIT13 | BIT14))) | BIT12|BIT13;

}

void program_tracker(u8 *data_buff, u8 data_len)
{
	u8 i=0;
	u8 j=0;
	u8 temp=0;
	if(data_len==0)return;
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT12 | BIT13 | BIT14))) | BIT12;
	reg_pad_mode_config = (reg_pad_mode_config & 0xffff0fff) ;//output
	reg_gpio_dout &=~ GPIO4_DOUT;//output "0"
	ussleep(30);
	reg_gpio_dout |= GPIO4_DOUT;//output "1"
	ussleep(30);
	reg_gpio_dout &=~ GPIO4_DOUT;//output "0"
	for(j=0;j<data_len;j++)
		{
		temp= data_buff[j];
		for(i=0;i<8;i++)
			{
			if(temp&(1<<(7-i)))
				{
				reg_gpio_dout |= GPIO4_DOUT;//output "1"
				}
			else
				{
				reg_gpio_dout &=~ GPIO4_DOUT;//output "0"
				}
			ussleep(3);//2.48us
			}
		}
	reg_gpio_dout &=~ GPIO4_DOUT;//output "0"
	ussleep(30);
	reg_gpio_dout |= GPIO4_DOUT;//output "1"
	ussleep(30);
	reg_gpio_dout &=~ GPIO4_DOUT;//output "0"
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT12 | BIT13 | BIT14))) | BIT12|BIT13;
}



//static uint8_t hostifBuffer[250 + 60];

/*****************************************************************************
 * @brief Checks the CRC of an incoming message
 * @param[in] idx_ReplyHeader *Answer incoming header
 * @param[in] uint8_t *pBuffer extra data
 * @return int check result: SUCCESS or ERR_COM
 */
static uint16_t CheckRespCRC(hostif_State *state)
{
   uint16_t ret = IDEX_SUCCESS;
   uint16_t crc = crc16_ccitt((uint8_t *)state->ReplyHeader, sizeof(*state->ReplyHeader) - sizeof(crc));
   
   uint16_t diff = state->ReplyHeader->crc - RD_U16(&crc);
   if(diff){
      ret = IDEX_ERR_COMM;
   }
   
   return ret ;
}      
/*****************************************************************************
 * @brief Adds the CRC to an outgoing message
 * @param[in] idx_CommandHeader *Command command header
 * @param[in] uint8_t *pBuffer command data
 * @return int check result: SUCCESS
 */
static int AddCmdCRC(hostif_State *state)
{
   uint16_t crc;
   uint8_t *src_crc = (uint8_t *)&state->CommandHeader->crc;
   
   // Adjust wSize Endianess as it is required for CRC calculation
   state->CommandHeader->wSize = RD_U16(&state->CommandHeader->wSize);

   crc = crc16_ccitt((uint8_t *)state->CommandHeader, sizeof(*state->CommandHeader)-sizeof(crc));   //CRChdr is calculated over fixed 4 bytes
   
   WR_U16(src_crc, crc);   // set CRC ( and adjusting the endianess )
   
   return IDEX_SUCCESS ;
}

static int AddDataCRC(hostif_State *state)
{
   uint16_t cmdHdrCrc;
   uint16_t cmdDataCrc;
   uint16_t crcHdrSize  = sizeof(*state->CommandHeader) - sizeof(cmdHdrCrc);
   uint8_t *pCmdHdrCrc  = (uint8_t *)&state->CommandHeader->crc;
   // Here wSize is sizeof(CmdHdr) + sizeof(CmdHdrCrc) + sizeof(CmdData)
   uint16_t crcDataSize = state->CommandHeader->wSize;
   uint8_t *pCmdDataCrc = (uint8_t *)state->CommandHeader + state->CommandHeader->wSize;


   // Adjust wSize Endianess as it is required for CRC calculation
   state->CommandHeader->wSize = RD_U16(&state->CommandHeader->wSize);

   // Make CmdHrdCrc = 0
   state->CommandHeader->crc = 0;

   // Calculate CRC over CmdHdr + (CmdHdrCrc=0) + CmdData
   cmdDataCrc = crc16_ccitt((uint8_t *)state->CommandHeader, crcDataSize);   //CRChdr is calculated over fixed 4 bytes

   // add cmdDataCrc
   WR_U16(pCmdDataCrc, cmdDataCrc);   // set CRC ( and adjusting the endianess )

   // Back to SE native representation, increment by 2, then again to target representation
   state->CommandHeader->wSize  = RD_U16(&state->CommandHeader->wSize);
   state->CommandHeader->wSize += 2;
   state->CommandHeader->wSize  = RD_U16(&state->CommandHeader->wSize);

   // Calculate CRC over CmdHdr only
   cmdHdrCrc = crc16_ccitt((uint8_t *)state->CommandHeader, crcHdrSize);   //CRChdr is calculated over fixed 4 bytes

   // add cmdHdrCrc
   WR_U16(pCmdHdrCrc, cmdHdrCrc);   // set CRC ( and adjusting the endianess )

   return IDEX_SUCCESS ;
}


u32 get_TA0_counter(void)
{
	u8 i;
	u32 counter0=0x00000000;
	u32 counter1=0x10000000;
	for(i=0;i<100;i++)
		{
		counter0=reg_timera_counter0;
		counter1=reg_timera_counter0;
		counter0&=0xffffffc0;
		counter1&=0xffffffc0;
		if(counter1==counter0)break;
		}
	return counter0;
}


//#define reg_clk_ctrl2                            (REG_T(CLK_GEN_BASE+0x08)) // 3F3F_0404h
//  #define CPU_FIX_FREQ_CFG                     (BIT13|BIT12|BIT11|BIT10|BIT9|BIT8)
//  #define FUNC_FIX_FREQ_CFG                    (BIT5|BIT4|BIT3|BIT2|BIT1|BIT0)  



void idxHalSystemSleep(void)
{
	if(card_rte.curr_io == IO_CL)
		{
		uint32_t bak_clk_reg0=0;
		uint32_t bak_clk_reg2=0;
		bak_clk_reg0=reg_clk_ctrl0;
		bak_clk_reg2=reg_clk_ctrl2;
		reg_clk_ctrl0 &=0xFF010002;
		reg_clk_ctrl2=(reg_clk_ctrl2&(~(CPU_FIX_FREQ_CFG|FUNC_FIX_FREQ_CFG)))|SYSTEM_LOW_FREQ;
		reg_clk_ctrl0 &=0xFe010002;
		SCR = 0x00;
		__wfi();
		reg_clk_ctrl2=bak_clk_reg2;
		reg_clk_ctrl0=bak_clk_reg0;
		}
	else
		{
		__wfi();
		}
}

static int SendCommand(uint8_t *cmd_data)
{
	uint16_t ret=0;
   hostif_State *state = &SerialState; 
   idxHalSpiSpeedType spiSpeed = state->mode == MODE_UNINITIALIZED ? IDX_HAL_SPI_SPEED_LOW : IDX_HAL_SPI_SPEED_HIGH; // high speed ( 6Mhz)
   int cmd_data_len = state->CommandHeader->wSize - sizeof(*state->CommandHeader);
   communication_wdt_flag=0;
   communication_wdt_counter=0;
   communication_wdt_flag=1;
   if(idxCom.state->CommandHeader->bOrdinal == 0x11) {
     idxCom.state->erase = 1; // erase flash on MCU
   }
   else idxCom.state->erase = 0; // erase flash on MCU
   

  // don't copy if data have been already build in buffer 
   if(cmd_data_len && (state->CommandData != cmd_data)){  
         memcpy(state->CommandData, cmd_data, cmd_data_len);
   }

   if(state->CommandHeader->bOptions & 1){
     AddDataCRC(state);   // Add Data and Header CRC
   }
   else{
     AddCmdCRC(state);    // Header CRC is mandatory
   }




	cmdRdy = 0;
	 hsiIntFlag = 0;
	hsFsmState = HS_WAIT_HSI_RISING_EDGE; // skip falling edge state
	if(state->CommandHeader->bOrdinal == 0x00) spiSpeed = IDX_HAL_SPI_SPEED_LOW; // low speed for init command 

	//while(SYSTEM_LOW_FREQ==(reg_clk_ctrl2&CPU_FIX_FREQ_CFG));
	if( state->mode == MODE_CONTACTLESS){
		if(TA0_lessthan_4ms<=get_TA0_counter())
		//__wfi();	//wait for WTX

		idxHalSystemSleep();
		//spiSpeed = IDX_HAL_SPI_SPEED_LOW; 
		}
	
	ret=HBHandshake_Start1(spiSpeed);
	  if(ret!=0)
		{
			state->ReplyHeader->wCode = ret;
			return 0;  // Handshake mechanism
		}
	  HSI_change=0;

	idxHalGpioSetIRQ(IDX_HAL_GPIO_HSI,1, POSEDGE_INT);   //replaces HBHSConfig_RDY()
	hsFsm(EV_HSI_RISING_EDGE); 

	while(cmdRdy == 0)
	{
		//__wfi();	  // wait for HSI interrupt
		idxHalSystemSleep();
		//if(1000<communication_wdt_counter) return IDEX_ERR_COMM_4;
		 if(HSI_change) 
		 	{
			HSI_change =0;
			hsFsm(EV_HSI_RISING_EDGE); 
			}
		}

	idxHalGpioSetIRQ(IDX_HAL_GPIO_HSI,0, POSEDGE_INT); 
	communication_wdt_flag=0;
	communication_wdt_counter=0;

   return 0;
}
/*****************************************************************************/

static int com_send_cmd_hdr(hostif_State *state){
  return idxHalSpiTransfer((uint8_t *)state->CommandHeader, sizeof(*state->CommandHeader), 0, 0, idxCom.timeout_ms);
}

static int com_receive_cmd_ack(hostif_State *state, uint8_t *ack){
  UNUSED(state);
  return idxHalSpiTransfer(0, 0, ack, 1, idxCom.timeout_ms);
}

static ComFsmStateId_t comFsm(void){
  hostif_State *state = &SerialState;
  int ret;
  UNUSED(ret);
  switch(comFsmState.id){
  case COM_IDLE :
    break;
  case COM_SENDING_CMD_HDR :
  {
    comFsmState.dataSize = RD_U16(&state->CommandHeader->wSize) - sizeof(*state->CommandHeader);
    ret = com_send_cmd_hdr(state);
    if (comFsmState.dataSize ) comFsmState.id = COM_RECEIVING_CMD_ACK;
    else                       comFsmState.id = COM_RECEIVING_CMD_ACK_NO_DATA;
    
    break;
  }
  case COM_RECEIVING_CMD_ACK_NO_DATA :
  {   uint8_t ack;
    ret = com_receive_cmd_ack(state, (uint8_t*)state->CommandHeader);
    ack = ((uint8_t*)state->CommandHeader)[0];	// TH89 wants to use its special buffer !
    if(ack == 0x06) comFsmState.id = COM_RECEIVING_RSP_HDR;
    else{
      state->CommandData[0] = ack;  // check ack and exit
      comFsmState.id = COM_SENDING_CMD_HDR;
    }
    break;
  }
  case COM_RECEIVING_CMD_ACK :
  {   uint8_t ack;
    ret = com_receive_cmd_ack(state, (uint8_t*)state->CommandHeader);
    ack = ((uint8_t*)state->CommandHeader)[0];	// TH89 wants to use its special buffer !
    if(ack == 0x06){
      if(comFsmState.dataSize <= MAX_PACKET_SIZE) comFsmState.id = COM_SENDING_LAST_CMD_DATA;
      else comFsmState.id = COM_SENDING_CMD_DATA;
      comFsmState.ptrData = state->CommandData;
    }
    else{
      state->CommandData[0] = ack;  // check ack and exit
      comFsmState.id = COM_SENDING_CMD_HDR;
    }
    break;
  }
  case COM_SENDING_CMD_DATA :
      idxHalSpiTransfer(comFsmState.ptrData, MAX_PACKET_SIZE, 0, 0, idxCom.timeout_ms);
      comFsmState.ptrData  += MAX_PACKET_SIZE;
      comFsmState.dataSize -= MAX_PACKET_SIZE;
       
      if( comFsmState.dataSize <= MAX_PACKET_SIZE) comFsmState.id = COM_SENDING_LAST_CMD_DATA;
    break;
  case COM_SENDING_LAST_CMD_DATA :
      idxHalSpiTransfer(comFsmState.ptrData, comFsmState.dataSize, 0, 0, idxCom.timeout_ms);
     comFsmState.id = COM_RECEIVING_RSP_HDR;
    break;
  case COM_RECEIVING_RSP_HDR :
    idxHalSpiTransfer(0, 0, (uint8_t *)state->ReplyHeader, sizeof(*state->ReplyHeader), idxCom.timeout_ms);
    if(( CheckRespCRC(state) != IDEX_SUCCESS) || (state->ReplyHeader->wSize == 0))
    {
      comFsmState.replyAck = 0x15;  // NACK
    }
    else{ // SUCCESS

      comFsmState.replyAck = 0x06;  // ACK
    }
    comFsmState.id = COM_SENDING_RSP_ACK;
    break;
  case COM_SENDING_RSP_ACK :
  {
    state->ReplyData[0] = comFsmState.replyAck;
    state->ReplyData[1] = 0;
    idxHalSpiTransfer(state->ReplyData, 2, 0, 0, idxCom.timeout_ms);
    if(comFsmState.replyAck == 0x15){
      comFsmState.id    = COM_IDLE;
      break;
    }

    comFsmState.dataSize = RD_U16(&state->ReplyHeader->wSize)- sizeof(*state->ReplyHeader);
    if( comFsmState.dataSize ){
      comFsmState.id    = COM_RECEIVING_RSP_DATA;
      comFsmState.ptrData = state->ReplyData;
    }
    else 
		{
		comFsmState.id    = COM_IDLE;
    	}
    break;
  }
  case COM_RECEIVING_RSP_DATA :
    if(comFsmState.dataSize > MAX_PACKET_SIZE){
      idxHalSpiTransfer(0, 0, comFsmState.ptrData, MAX_PACKET_SIZE, idxCom.timeout_ms) ;
        comFsmState.ptrData     += MAX_PACKET_SIZE;
        comFsmState.dataSize -= MAX_PACKET_SIZE;
    }
    else {
      idxHalSpiTransfer(0, 0, comFsmState.ptrData, comFsmState.dataSize, idxCom.timeout_ms) ;
        comFsmState.id = COM_IDLE;

    }
    break;
  default :
    comFsmState.id = COM_IDLE;
    break;
  }
  return comFsmState.id;
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void sendWTX(void)
{
	cl_wtx (&g_cl_ctx, false);
}
//static void hsiCallback(void){ hsFsm(EV_HSI_RISING_EDGE);}
static void hsiCallback(void *param ){ 
   UNUSED(param);
  hsiIntFlag = 1;
}

//void testCallback(void){ hsFsm(EV_HSI_RISING_EDGE);}
static HsFsmState_t hsFsm(uint8_t event){  
//uint8_t test_data=0x50;
    switch(hsFsmState){
    case HS_IDLE :
      if(event == EV_START_HANDSHAKE){
        idxHalGpioRegisterListener(hsiCallback);
        idxHalGpioSetIRQ(IDX_HAL_GPIO_HSI, 1, POSEDGE_INT);
        idxHalGpioSet(IDX_HAL_GPIO_HSO, IDX_HAL_GPIO_LOW);
        hsFsmState = HS_WAIT_HSI_FALLING_EDGE;
      }
      break;
    case HS_WAIT_HSI_FALLING_EDGE : // BMCU has acknowledged HSO low
      if(event == EV_HSI_FALLING_EDGE){
        idxHalGpioSetIRQ(IDX_HAL_GPIO_HSI, 1, POSEDGE_INT);
        idxHalGpioSet(IDX_HAL_GPIO_HSO, IDX_HAL_GPIO_HIGH); 
        hsFsmState = HS_WAIT_HSI_RISING_EDGE;
      }
      break;
    case HS_WAIT_HSI_RISING_EDGE : // BMCU ended handshake
      if(event == EV_HSI_RISING_EDGE){        
        comFsmState.id = COM_SENDING_CMD_HDR; // init Communication State Machine and start it
        comFsm();
        hsFsmState = HS_WAIT_RDY_RISING_EDGE;
      }
      break;      
    case HS_WAIT_RDY_RISING_EDGE :
    //cmdRdy = 1;return hsFsmState;
      if( wtxFsmState == WTX_SIGNALLING)
	  	{
	        sendWTX(); // WTX service
	        wtxFsm(EV_WTX_STOP);
	        hsFsmState = HS_WAIT_RDY_RISING_EDGE;
		}
      else if(event == EV_HSI_RISING_EDGE){
        ComFsmStateId_t comState = comFsm();

        if( comState == COM_IDLE){    // trigger Commm state machine

          hsFsmState = HS_IDLE;
            cmdRdy = 1;
        }
      }
      break;
    case HS_WAIT_RESUME :
      if(event == EV_HSI_FALLING_EDGE){
        hsFsmState = HS_WAIT_RDY_RISING_EDGE; // wtx signalling finished go back to business
      }
      break;
    default :
      break;

    }
  return hsFsmState;
}

void idex_mosi_return_to_high(void)
{
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT12 | BIT13 | BIT14))) | BIT12;
	reg_pad_mode_config = (reg_pad_mode_config & 0xffff0fff) ;//output


	reg_gpio_dout |= GPIO4_DOUT;//output "1"



}
void idex_mosi_retuento_SPI_mode(void)
{
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT14 | BIT13 | BIT12))) | BIT13| BIT12;

}

void idxHalGpio4WireSPISwitchHSO2CLK(void)
{
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT16 | BIT17 | BIT18))) | BIT16| BIT17;

}
void idx_spi_clk2gpio(void)
{
	reg_pad_function_config = (reg_pad_function_config & (~ (BIT16 | BIT17 | BIT18))) | BIT16;
	reg_pad_mode_config = (reg_pad_mode_config & 0xfff0ffff) ;
	}

// executed with highest priority (higher then HSI callback)
// It should be re-entrant as it is self referenced
#if 1
void wtxFsm(WtxEvent_t event)
{
//uint8_t test_buffer[3];
  hostif_State *state = &SerialState;
if(1==communication_wdt_flag) communication_wdt_counter++;
  switch(wtxFsmState){
  case WTX_IDLE :
    if(event == EV_WTX_START)
		{
		//test_buffer[0]=state->mode;
		//test_buffer[1]=state->erase;
		//program_tracker(test_buffer,2);

      if ((state->mode == MODE_CONTACT) || (state->mode == MODE_UNINITIALIZED) || (state->erase == 1) )
	  	{
        sendWTX();
      }
      else {
        idxHalGpioStateType hsiState;
        idxHalGpioGet(IDX_HAL_GPIO_HSI, &hsiState); 
			 // idex_wtx_count++;
		//program_tracker((u8 *) (&idex_wtx_count),1);
        if( (hsiState == IDX_HAL_GPIO_LOW) && ( comFsmState.id == COM_RECEIVING_RSP_HDR)){
            wtxFsmState = WTX_SIGNALLING;
		
            idxHalGpioSet(IDX_HAL_GPIO_HSO, IDX_HAL_GPIO_LOW); 
            //wtxInt = 1;
           // idex_wtx_count++;
		//program_tracker((u8 *) (&idex_wtx_count),1);
        }
        else {
          sendWTX(); // this is executed in (pri=6) context masking HSI int (pri =5)
        }
      }
    }
    break;
  case WTX_SIGNALLING:
    if(event == EV_WTX_STOP){
		//ussleep(800);
      idxHalGpioSet(IDX_HAL_GPIO_HSO, IDX_HAL_GPIO_HIGH); 
	idxHalGpio4WireSPISwitchHSO2CLK();
	  //idex_wtx_debug_test();

      wtxFsmState = WTX_IDLE;
    }
    break;
  default :
    break;
  }
}
#endif




/*****************************************************************************
 * @brief Define the Communication IF 
 */






void idxSerialInterfaceInit()
{
	/*
	*	This source is only used on platforms supporting initialized
	* data, so do nothing
	*/
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


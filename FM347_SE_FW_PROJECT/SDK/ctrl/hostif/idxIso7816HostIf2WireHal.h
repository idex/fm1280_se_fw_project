/** ***************************************************************************
 * \file   idxIso7816HostIf2WireHal.h
 * \brief  HAL layer for 2-wire UART protocol handler
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *
 * Provides hardware- (or driver-) specific implementations of the routines
 * depended on by \ref idxIso7816HostIf2Wire.c.
 *
 *****************************************************************************/
#ifndef _ISO7816_HOST_IF_2WIRE_HAL_H_
#define _ISO7816_HOST_IF_2WIRE_HAL_H_

#include <stdint.h>
#include "idxSeBioDrvAPI.h" /* For enum idxBioDrvRet_e */

struct idxBioDrvBuf_s {
  uint16_t Size;       //< Overall size of buffer
  uint16_t InitialPos; //< Position of first data in buffer
  uint16_t Number;     //< Number of bytes in the buffer
  uint8_t *pBuffer;    //< Pointer to the actual data
};

enum idxBioDrv_e {
  IDX_BIO_DRV_ANY        = 0,
  
  IDX_BIO_DRV_ACK      = 1, // ACK (else header or data)
  IDX_BIO_DRV_HDR      = 2, // Header (not data)
  IDX_BIO_DRV_FIRST    = 4, // First of header or data
  IDX_BIO_DRV_MSG_LAST = 8  // Last of the entire message
};

#ifndef TEST_HOST_PC
#define TEST_HOST_PC 0
#endif

#if (TEST_HOST_PC)
  /* If not on TEST_HOST_PC, use idxBioDrvInit() */
#include <stdbool.h>
  void init_iso7816_hostif_2wire(bool OnSE, bool CRCDat);
#endif

  void idxInitHostIf2WireHal(void);
  int idxContactbased(void); /* Returns logical bool (no stdbool on SE) */
  
  uint16_t idxHostIfCRC(uint16_t wSize, uint8_t *pBuffer);
  uint16_t idxTimeToTransfer(uint16_t bytes);

  enum idxBioDrvRet_e idxStartPrivateChannel(void);
  enum idxBioDrvRet_e idxFinishPrivateChannel(void);

  enum idxBioDrvRet_e idxSendBuffer(enum idxBioDrv_e MsgPos, struct idxBioDrvBuf_s *pBuf);
  enum idxBioDrvRet_e idxReceiveBuffer(enum idxBioDrv_e MsgPos, struct idxBioDrvBuf_s *pBuf);

  uint16_t idxGetClockCalibration(void);
  void idxSetClockCalibration(uint16_t CalibrationValue);

  // This is a routine provided by the protocol handler, which
  // allows the HAL to ask whether the channel is in a "ready"
  // state - i.e. the channel is open and a command can be sent.
  // It is used to determine, when the contact-mode waiting time
  // expires, whether the SE can send a special command header to
  // terminate the channel
  bool idxIso7816HostIfReady(void);
  
#endif //_ISO7816_HOST_IF_2WIRE_HAL_H_

/**************************************************************************//*
* \file  serialinterface.h
* \brief header for object for communication with Idex serial interface
* \author IDEX ASA
* \version 0.0.0
******************************************************************************
* \section License
* <b>(C) Copyright 2012, 2019 IDEX ASA, http://www.idex.no</b<http://www.idex.no%3c/b>>
*****************************************************************************/

/****************************************************************************
*
* This source code is property of IDEX ASA. The source and compiled
* code may only be used together with IDEX fingerprint sensor applications.
*
* This copyright notice must not be modified or removed from the source code.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: IDEX ASA has no
* obligation to support this Software. IDEX ASA is providing the
* Software "AS IS", with no express or implied warranties of any kind.
*
* IDEX ASA is not to be liable for any damages, any relief,
* or for any claim by any third party, arising from use of this software.
*
*****************************************************************************/
#ifndef __SERIAL_INTERFACE_H__
#define __SERIAL_INTERFACE_H__

#include <stdint.h>

#define MAX_PACKET_SIZE   512
#define BOPTION_ENCRYPTED 0x04
#define BOPTION_SIGNED    0x02

/**************************************************************************************
 * Endianess Macros 
 * 
 * 
 * 
 * Bio-MCU (Cortex-M4) is Little Endian
 */ 
#define RD_U16(pU16)      ( ( *((uint16_t *)pU16) >> 8 ) | ( *((uint16_t *)pU16) << 8 ) )
#define RD_U32(pU32)      ( *((uint8_t *)pU32) |  (*((uint8_t *)pU32 + 1 ) << 8) | (*((uint8_t *)pU32 + 2 ) << 16) | (*((uint8_t *)pU32 + 3 ) << 24))
#define WR_U16(pU16, val) ( *((uint8_t *)pU16+0) = (uint8_t)(val), *((uint8_t *)pU16+1) = (uint8_t)((val) >> 8))
#define WR_U32(pU32, val) ( *((uint8_t *)pU32+0) = (uint8_t)(val), *((uint8_t *)pU32+1) = (uint8_t)((val) >> 8), *((uint8_t *)pU32+2) = (uint8_t)((val) >> 16), *((uint8_t *)pU32+3) = (uint8_t)((val) >> 24))

typedef struct __idx_CommandHeader
{
   uint16_t   wSize ;           // size of the command, including the whole header
   uint8_t    bOrdinal ;        // command ordinal
   uint8_t    bOptions ;        // command options
   uint16_t   crc ;  
} idx_CommandHeader ;



typedef struct __idx_ReplyHeader
{
   uint16_t    wSize ;            // size of the reply, including the whole header
   uint16_t    wCode ;            // return code of the command
   uint16_t    crc ;
} idx_ReplyHeader ;



enum  {
  MODE_CONTACT      = 0x00,
  MODE_CONTACTLESS  = 0x01,
  MODE_SLEEVE       = (0x04 | MODE_CONTACT) ,      // 00000101b
  MODE_UNINITIALIZED= 0xff
};


typedef struct _hostif_state
{
   idx_CommandHeader *CommandHeader ;      // pointer to the header of a command
   uint8_t           *CommandData ;
   idx_ReplyHeader   *ReplyHeader ;
   uint8_t           *ReplyData ;
   void              *env ;
   uint8_t           mode;    // contact or contactless
   uint8_t           erase;   // signals the MCU erase execution  
   uint8_t           enroll;  // signals if it is an enroll ( add WTX sleep() ) 
} hostif_State;





typedef enum {
  EV_WTX_START = 1,
  EV_WTX_STOP
} WtxEvent_t;

void wtxFsm(WtxEvent_t ev);

void set_connection_mode( uint8_t mode );

void idxSerialInterfaceInit(void);

typedef struct _BaseSerialInterface
{
  hostif_State * state;
  void (*reset)(void) ;
  int (*SendCommand)(uint8_t *cmd_data) ;
  uint16_t timeout_ms;
} BaseSerialInterface;







#endif // __SERIAL_INTERFACE_H__

/**************************************************************************//*
* \file  serialInterface.c
* \brief header for object for communication with Idex serial interface
* \author IDEX ASA
* \version 0.0.0
******************************************************************************
* \section License
* <b>(C) Copyright 2012, 2019 IDEX ASA, http://www.idex.no</b<http://www.idex.no%3c/b>>
*****************************************************************************/

/****************************************************************************
*
* This source code is property of IDEX ASA. The source and compiled
* code may only be used together with IDEX fingerprint sensor applications.
*
* This copyright notice must not be modified or removed from the source code.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: IDEX ASA has no
* obligation to support this Software. IDEX ASA is providing the
* Software "AS IS", with no express or implied warranties of any kind.
*
* IDEX ASA is not to be liable for any damages, any relief,
* or for any claim by any third party, arising from use of this software.
*
*****************************************************************************/

#include "serialInterface.h"
#include "_serialInterface.h"
extern BaseSerialInterface idxCom;

/*****************************************************************************
 * @brief Calculate the CRC of an incoming message
 * @param[in] x    pointer to incoming message
 * @param[in] size of incoming message
 * @returns Computed CRC
 */
int16_t crc16_ccitt( uint8_t *x, int size )
{
   uint16_t crc_new = 0 ;
   int i ;

   for( i=0; i<size; i++ )
   {
      crc_new  = (uint8_t)(crc_new >> 8) | (crc_new << 8);
      crc_new ^= x[i] ;
      crc_new ^= (uint8_t)(crc_new & 0xff) >> 4;
      crc_new ^= crc_new << 12;
      crc_new ^= (crc_new & 0xff) << 5;
   }
   
   return crc_new ;
}

void set_connection_mode( uint8_t mode )
{
		idxCom.state->mode = mode;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

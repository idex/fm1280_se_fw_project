/******************************************************************************
* @file  idxBaseErrors.h
* @brief header file for the IDEX hostif
* @author IDEX ASA
* @version 0.0.0
*******************************************************************************
* Copyright 2013-2017 IDEX ASA. All Rights Reserved. www.idex.no
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is provided
* "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
* not to be liable for any damages, any relief, or for any claim by any third
* party, arising from use of this software.
*
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR/FRR performance.
******************************************************************************/
#ifndef __IDEXBASEERROR_H__
#define __IDEXBASEERROR_H__


//#define IDEX_ERR_NO_MATCH       0x6300   // templates do not match
#define IDEX_ERR_COMM           0x6741   // communication problem (EIO)
//#define IDEX_ERR_IT             0x6742   // interrupt problem (ETIME)
//#define IDEX_ERR_CALIB          0x6743   // wrong calibration data (EAGAIN)
//#define IDEX_ERR_TOO_SMALL      0x6745   // image too small
//#define IDEX_ERR_ABORTED        0x6746   // acquisition aborted
//#define IDEX_ERR_BAD_QUALITY    0x6747   // quality is too bad
//#define IDEX_ERR_TIMEOUT        0x6748   // user timeout
//#define IDEX_ERR_UNKNOWN        0x6969   // UNKNOWN ERROR
//#define IDEX_ERR_BAD_CONDITIONS 0x6985   // Conditions of use not satisfied
//#define IDEX_ERR_ALLOWED        0x6986   // Command is not allowed
//#define IDEX_ERR_BAD_PARAMETERS 0x6A80   // Bad parameters
//#define IDEX_ERR_NOT_SUPPORTED  0x6A81   // function not supported
//#define IDEX_ERR_REC_NOT_FOUND  0x6A83   // Record not found
//#define IDEX_ERR_NO_SPACE       0x6A84   // Not enough space left on device
//#define IDEX_ERR_SIZE           0x6A87
//#define IDEX_ERR_STATE          0x6985   // wrong state
//#define IDEX_ERR_KEY_EXIST      0x6A89   // keys are already created
//#define IDEX_ERR_BAD_COMMAND    0x6D00   // command is not valid or not implemented
#define IDEX_SUCCESS            0x9000

#endif // __IDEXBASEERROR_

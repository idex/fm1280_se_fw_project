/** ***************************************************************************
 * \file   idxSDK.c
 * \brief  IDEX SDK initialization
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
******************************************************************************/
#include "idxSDK.h"

#include "serialinterface.h"
#include "idxSeEmkAPI.h"
#include "bio-ctrl.h"

void idxSdkInit()
{
  idxSerialInterfaceInit();
  (void) idxSeEmkInit();
  idxBioCtrlInit();
}



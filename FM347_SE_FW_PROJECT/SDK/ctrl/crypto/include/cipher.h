/******************************************************************************
* \file  cipher.h
* \brief cipher definitions
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#ifndef __CIPHER_H
#define __CIPHER_H
#include <stdint.h>
uint8_t *fm1280_enCBC(uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t key_size,uint16_t size);

void fm1280_encmac(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t key_size,uint16_t size);
void fm1280_dcCBC(uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t key_size,uint16_t size);


#endif /* __CIPHER_H */

/******************************************************************************
* @file  cmac.h
* @brief Cipher-Based Message Authentication Code head file.
* @author IDEX ASA
* @version 1.0.0
*
*******************************************************************************
 Copyright 2014-2017 IDEX ASA. All Rights Reserved. www.idex.no


 IDEX ASA is the owner of this software and all intellectual property rights
 in and to the software. The software may only be used together with IDEX
 fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.


 This copyright notice must not be altered or removed from the software.


 DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 ASA has no obligation to support this software, and the software is provided
 "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 not to be liable for any damages, any relief, or for any claim by any third
 party, arising from use of this software.


 Image capture and processing logic is defined and controlled by IDEX ASA in
 order to maximize FAR/FRR performance.
******************************************************************************/

#ifndef __CMAC_H__
#define __CMAC_H__
#include <stdint.h>

#include "cipher.h"

typedef struct __cmac_context_t
{
  uint8_t k1[16] ;
  uint8_t k2[16] ;
  uint8_t c[16] ;
} cmac_context_t ;

void cmac_init( cmac_context_t *mac_ctx, uint8_t *key ) ;
void cmac_compute( uint8_t *out, uint8_t *key, uint8_t *iv, uint8_t *buffer, int len);
void cmac_reset( cmac_context_t *mac_ctx );

#endif // __CMAC_H__

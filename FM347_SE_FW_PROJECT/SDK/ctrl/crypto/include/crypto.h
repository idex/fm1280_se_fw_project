/******************************************************************************
* @file  crypto.h
* @brief Secure channel infrastructure functions 
* @author IDEX ASA
* @version 1.0.0
*
*******************************************************************************
 Copyright 2014-2017 IDEX ASA. All Rights Reserved. www.idex.no


 IDEX ASA is the owner of this software and all intellectual property rights
 in and to the software. The software may only be used together with IDEX
 fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.


 This copyright notice must not be altered or removed from the software.


 DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 ASA has no obligation to support this software, and the software is provided
 "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 not to be liable for any damages, any relief, or for any claim by any third
 party, arising from use of this software.


 Image capture and processing logic is defined and controlled by IDEX ASA in
 order to maximize FAR/FRR performance.
******************************************************************************/

#ifndef __CRYPTO_H__
#define __CRYPTO_H__

#include <stdint.h>

typedef enum {
  SEC_CH_CLOSED = 0x01,
  SEC_CH_OPEN   = 0x02,  
} SecChStatus_t;  

typedef struct {
  uint8_t Kenc[16];
  uint8_t Kmac[16];
  uint8_t Kdec[16];
  uint8_t Krnd[32];
  uint8_t crc[2];     // CRC place holder 
} scp03_keys_t;

typedef struct {
  uint16_t state;
  uint8_t senc[16];
  uint8_t smac[16];
  uint8_t srmac[16];
  uint8_t sdec[16];
  uint8_t context[16];
  uint8_t macChain[16];
  uint16_t msgCounter;
  uint16_t page;
  uint8_t kcnt[2];
  uint8_t kstate[2];
} session_t;

uint16_t dataEncrypt(uint8_t *cdata, uint16_t Lc);
uint16_t dataDecrypt(uint8_t *cdata, uint16_t Lcc);
void sign(uint8_t bOrdinal,uint8_t bOptions, uint8_t *cdata, uint16_t Lc);


uint16_t getExtAuthData(void);

uint16_t setMcuKeys(void);
void setSeKeys(void);



#endif // __CRYPTO_H__

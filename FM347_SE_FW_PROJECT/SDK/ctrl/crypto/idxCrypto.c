/******************************************************************************
* \file  idxCrypto.c
* \brief IDEX Crypto infrastructure implementation
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "bio-ctrl.h"
#include "hostif.h"
#include "serialInterface.h"                    // Debug
#include "idxHalFlash.h"

#include "crypto.h"
//#include "aes.h"
#include "cipher.h"
#include "cmac.h"
#include "kdf.h"
#include "fm_proto_types.h"



//#include "main.h"
#define UNUSED(p) if(p){}

/********************************************************************************
 * Enternal Definitions
 */

#define KEY_STATE_OFFSET ( 88 )
#define KEY_CNT_OFFSET   ( 96 )
#define KEY_MAX_CNT      ( 16 )

#define debug_flash_start 0x00b0b200

#define pNvmKey1 0x00b10000
#define pNvmKey2 0x00b10200


//#define pNvmKeys  ((uint8_t *)(0x00b10000))
//static uint8_t (*pNvmKey)[512] = (uint8_t (*)[512])(0x00b10000);

/********************************************************************************
 * Global Definitions
 * Note: ukey is the "universal key" used for tests
 */
uint8_t ukey[16];//={0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f};
uint8_t *pDbg;
session_t session;// = {SEC_CH_CLOSED};
static uint8_t validate[8];// = {0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55};
static uint8_t cntValue[8];//static uint8_t cntValue[8] = {0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88};


extern BaseSerialInterface idxCom;

//static void swapBytes(uint16_t *u16);
static void genNewKeys(uint8_t *key);
static void genKrnd(uint8_t *Krnd);
static void genHostChallenge(uint8_t *hChallenge);
extern u32 crc_cal(u8 crc_mode, u32 crc_cal_start_addr, u32 crc_cal_length, u32* crc_data_value);
extern void RNG_GetRandNum(uint8_t *key,int length);
extern void ussleep(uint32_t count);
extern void aes_para_initial(void);

/********************************************************************************
 * Generate new basic keys, Krnd and host challenge 
 */
//#define RANDOM_KEYS
static void genNewKeys(uint8_t *key){
#ifdef RANDOM_KEYS  


RNG_GetRandNum( key,48 );

#else  
  memcpy(key,    ukey, 16);
  memcpy(key+16, ukey, 16);
  memcpy(key+32, ukey, 16);
#endif
}

void initial_ukey(void)
{
	uint8_t i;
	for (i=0;i<16;i++)
		{
		ukey[i]= 0x40+i;
		}
}
void initial_session(void)
{
	session.state=SEC_CH_CLOSED;
}

void initial_validate(void)
{
	uint8_t i;
	for (i=0;i<8;i++)
		{
		validate[i]= 0x55;
		}
}

void initial_cntValue(void)
{
	uint8_t i;
	for (i=1;i<=8;i++)
		{
		cntValue[i-1]= (i<<4)+i;
		}

}


void scp03_related_data_initial(void)
{
	initial_ukey();
	initial_session();
	initial_validate();
	initial_cntValue();
}

u8 gen_CRC16_CCITT(u8 *crc, u8 *buf, u32 crc_len)
{
    u32 crc_result  = 0;
	u16 crc16_iv;
    //base on source_start_addr, increasing
    //ISO144433-A, CRC16
    crc_cal(1, (u32)buf, crc_len, (u32 *)&crc_result);
	crc16_iv = (u16)crc_result;
	crc16_iv ^= 0xFFFF;
	crc[0] = (u8)crc16_iv;
	crc[1] = (u8)(crc16_iv>>8);

    return 0;
}



static void genHostChallenge(uint8_t *hChallenge){
  uint16_t i;
  for(i = 0 ; i < 8 ; i++) hChallenge[i] = 0x11 + ( ( i<<4 ) + i);  
  if(0){
    //uint16_t idxErr = (uint16_t)tmcGenRandomNum_SU(8,hChallenge);
    RNG_GetRandNum(hChallenge,8);
  }  
}

static void genKrnd(uint8_t *Krnd){
  memcpy(Krnd,    ukey, 16);
  memcpy(Krnd+16, ukey, 16); 
  if(0){
    //uint16_t idxErr = (uint16_t)tmcGenRandomNum_SU(32,Krnd);
    RNG_GetRandNum(Krnd,32);
  }  
}
/********************************************************************************
 *  SetFactoryKeys() = Erase + prgm(keys) + prgm(validate) + putKey()
 *
 */
 uint16_t setFactoryKeys(void)
	{
	  uint8_t NvmProgramWords(uint8_t *pbDest, uint8_t *pbSrc, uint16_t usLen);
	  uint8_t NvmFastEraseSector(uint8_t *pbDest);
	  uint16_t ret;
	  uint16_t crc = 0;
	  uint8_t *pdata;
	  uint8_t *sector;
	  sector= (uint8_t *)pNvmKey1; // sector #1   
	  idxCom.reset();
	  pdata = idxCom.state->CommandData;
	
	  genNewKeys(pdata);				 // Kenc, Kmac, Kdek (random or default)
	  genKrnd(pdata+48);				 // Krnd (random or default)
	  gen_CRC16_CCITT((uint8_t*)&crc, pdata, 80);  // TH89 CRC module (need byte swapping)
	  memcpy(pdata+80,(uint8_t*)&crc, 2);
	idxHalFlashErase(sector,256);
	idxHalFlashErase(sector+512,256);
	  // Program keys to sector #1
	ret= idxHalFlashWrite(sector,pdata,88);
	if(IDEX_SUCCESS != ret)return ret;
	ret=idxHalFlashWrite(sector+88,validate,8);
	if(IDEX_SUCCESS != ret)return ret;
	  ret = idxMcuComms( HOSTIF_CMD_PUTKEY, 0, pdata, 82, NULL, NULL );
	  ussleep(200);
	  return ret;
	}

/********************************************************************************
 *  getNextKeys() Write keys to SE nvm (standard or random generated)
 *
 */

uint8_t doubleWordEraseCheck(uint8_t * pbSrc)
{
	uint32_t data=0;
	data= * (uint32_t *)pbSrc;
	if(0xffffffff==data)return 0;
	return 1;
}

 uint16_t getNextKeys(void)
	{
	uint16_t i,j;
	uint8_t  firstPage;
	uint16_t ret;
	uint8_t *ctx;// = session.context;
	uint8_t cryptogram[16];
	uint8_t cdata[9]={0};  // keyID[1] + hChallenge[8] 
	uint8_t uidType = 0; // MCU ID
	uint8_t uid[16];
	uint8_t cardInfo[8+8+3]; // MCU-challenge[8], MCU-cryptogram[8], counter[3]={cnt,~cnt,0xab} 
	uint8_t k1state,k2state;
	uint8_t k1cnt,k2cnt;
	uint8_t *nvmSector1;
	uint8_t *nvmSector2;
	uint8_t *curNvmSector;
	nvmSector1 = (uint8_t*)pNvmKey1;
	nvmSector2 = (uint8_t*)pNvmKey2;
	ctx = session.context;
	genHostChallenge(&cdata[1]); 
	ret = idxMcuComms( HOSTIF_CMD_GETUID, 0, &uidType, 1, uid, NULL );	  // MCU ID
	ussleep(200);
	if(ret != IDEX_SUCCESS){return ret;}
	uidType = 1; // sensor ID
	ret = idxMcuComms( HOSTIF_CMD_GETUID, 0, &uidType, 1, &uid[12], NULL ); // sensor ID
	ussleep(200);
	if(ret != IDEX_SUCCESS){return ret;}
	ret = idxMcuComms( HOSTIF_CMD_INITUPDATE, 0, cdata, 9, cardInfo, NULL );
	ussleep(200);
	if(ret != IDEX_SUCCESS){return ret;}
	// Evaluate the state (validated/erased) and key counters
	k1state = doubleWordEraseCheck(nvmSector1 + KEY_STATE_OFFSET);
	k2state = doubleWordEraseCheck(nvmSector2 + KEY_STATE_OFFSET);	
	if( (k1state == 0) && (k2state == 0) )
		{
		return 0x6504;  // return if no key available
		}
	for(k1cnt = 0 ; k1cnt < KEY_MAX_CNT ; k1cnt++)
		{
		if( 0 == doubleWordEraseCheck( nvmSector1 + KEY_CNT_OFFSET + k1cnt * 8) )
			{ 
			break;
			}
		}
	if(k1cnt >= KEY_MAX_CNT)
		{
		return 0x6505;
		}	 
	for(k2cnt = 0 ; k2cnt < KEY_MAX_CNT ; k2cnt++)
		{
		if( 0 == doubleWordEraseCheck( nvmSector2 + KEY_CNT_OFFSET + k2cnt * 8) )
			{ 
			break;
			}
	}
	if(k2cnt >= KEY_MAX_CNT)
		{
		return 0x6506;
		}	 
	
	// Main selection block based on combination SE vs MCU
	// Rule : if cnt from MCU is 1, then validated,cnt=0 should be used, 
	//		  if cnt from MCU is not 1, then validated, cnt=1 (or 2, 3, 4,...) should be used
	if( cardInfo[16] == 1 )
		{	// cardInfo[16] = mcuCnt
		if( k1state && (k1cnt == 0))
			{ 
			session.page = 0;
			}
		else if( k2state && (k2cnt == 0))
			{ 
			session.page = 1;
			}
		else 
			{
			// We shouldn't be here. Try to recover with the assumption: kstate > 0 ==> keys have been validated
			//return 0x6507;
			if(k1state > 0) session.page = 0;
			else			  session.page = 1;
			}	  
		}
	else 
		{	// mcuCnt > 1
		if( k1state && (k1cnt > 0))
			{ 
			session.page = 0;
			}
		else if( k2state && (k2cnt > 0))
			{ 
			session.page = 1;	
			}
		else 
			{
			// We shouldn't be here. Try to recover with the assumption: kstate > 0 ==> keys have been validated	
			//return 0x6508;
			if(k1state > 0) session.page = 0;
			else			  session.page = 1;
			}	  
		}
	session.kstate[0] = k1state;
	session.kstate[1] = k2state;
	session.kcnt[0]   = k1cnt;
	session.kcnt[1]   = k2cnt;
// Test MCU cryptogram
	for( i = 0 ; i < 8 ; i++) ctx[i]   = cdata[i+1]; 
	for( i = 0 ; i < 8 ; i++) ctx[i+8] = cardInfo[i];
	for( i = 0 ; i < 16; i++) ctx[i]   ^= uid[i];
	for( firstPage = 1 ; ; )
		{
		curNvmSector =( (session.page==0)? (uint8_t*)pNvmKey1 : (uint8_t*)pNvmKey2);
		genkeySENC(curNvmSector, &session);	// Kenc
		genkeySMAC(curNvmSector+16, &session);   // Kmac
		genkeySRMAC(curNvmSector+16, &session);  // Kmac
		genCardCryptogram(cryptogram, &session); // should be tested against mcu_crypto
		for( j = 0 ; j < 8 ; j++)
			{
			if(cardInfo[8+j] != cryptogram[j]) break;
			}
		if( j == 8 ) break;	 // Success on either Page #1 or Page #2. Exit the loop 
		else if( firstPage )
			{  // If failure on first attempt test 2nd page
			session.page ^= 1;	 
			if( 0 == session.kstate[session.page]) return 0x6519; // 2nd page is NOT valid
			firstPage = 0;		 // Jump to check the cryptogram for 2nd page
			}
		else return 0x6509;	// Both pages have ( validated ) keys but cryptograms failed 
		}
	session.state = SEC_CH_CLOSED;	// init sec channel status	  
	  
	return ret;
	}


/********************************************************************************
 *  SetNextKeys() Write keys to SE nvm (standard or random generated)
 *
 */


uint16_t setNextKeys(uint8_t *buf, uint8_t mode)
{
	uint16_t ret;
	uint8_t addOff;
	uint16_t crc = 0;
	uint8_t Kdec[16];
	uint8_t *curNvmSector; 
	uint8_t *newNvmSector; 
	if(0==session.page)
		{
		curNvmSector=(uint8_t*)pNvmKey1;
		newNvmSector=(uint8_t*)pNvmKey2;
		}
	else
		{
		curNvmSector=(uint8_t*)pNvmKey2;
		newNvmSector=(uint8_t*)pNvmKey1;
		}

	memcpy(Kdec, curNvmSector + 32 , 16);
	// Erase NVM for next session 
	idxHalFlashErase(newNvmSector,256);
  // Increment the counter for current session

	addOff = session.kcnt[session.page] * 8;  // First write is for 0 to 1 increment  
	if(IDEX_SUCCESS!=idxHalFlashWrite(curNvmSector + KEY_CNT_OFFSET + addOff, cntValue,sizeof(cntValue)))//idxHalFlashWrite
		{
		return 0x650a;
		}

  // Generate keys for next session
	genNewKeys(buf);                // Kenc, Kmac, Kdek (random or default)
	genKrnd(buf+48);                // Krnd[32] (random or default)
	gen_CRC16_CCITT((uint8_t*)&crc, buf, 80); 
	memcpy(buf+80,(uint8_t*)&crc, 2);

  // Program keys for next seession (SE flash)
	if(IDEX_SUCCESS!=idxHalFlashWrite(newNvmSector, buf,88))//idxHalFlashWrite
		{
		return 0x650b;
		}

  // The counter is implicitly set to 0 by NOT writting counter area
  
  // Set "validated" 
	if(IDEX_SUCCESS!=idxHalFlashWrite(newNvmSector + KEY_STATE_OFFSET, validate,sizeof(validate)))//idxHalFlashWrite
		{
		return 0x650b;
		}
 
  // encode the key fields  
	fm1280_enCBC(Kdec, buf,    NULL, sizeof(Kdec),16);   
	fm1280_enCBC(Kdec, buf+16, NULL, sizeof(Kdec),16);   
	fm1280_enCBC(Kdec, buf+32, NULL, sizeof(Kdec),16);   
	fm1280_enCBC(Kdec, buf+48, NULL, sizeof(Kdec),32);   

	memcpy(buf+80,(uint8_t*)&crc,2);
	ret = mcuComms2( HOSTIF_CMD_PUTKEY, HOSTIF_BOPTION_ENCRYPT|HOSTIF_BOPTION_SIGNED, buf, 82, NULL, NULL );
	ussleep(200);
	if(ret != IDEX_SUCCESS){return ret;}  
	buf[0] = mode;  // set flash update mode
	ret = mcuComms2( HOSTIF_CMD_FLASHUPDATE, HOSTIF_BOPTION_ENCRYPT|HOSTIF_BOPTION_SIGNED, buf, 1, NULL, NULL );
	ussleep(200);
	return ret;
}



/********************************************************************************
 *  SetSeKeys() Write keys to SE nvm (standard or random generated)
 *
 */
void setSeKeys(void){
  uint8_t seKeys[48];  
  genNewKeys(seKeys);  
	
	(void) idxHalFlashWrite((uint8_t *)pNvmKey1, seKeys, sizeof(seKeys));
}
/********************************************************************************
 *  SetMcuKeys() Compute CRC for the keys block and send PUT-KEY to mcu
 *  Note: This is the very first set of keys for MCU so no need secure channel
 */
uint16_t setMcuKeys(void){
  uint16_t ret;
  uint16_t crc = 0;
  uint8_t *pdata;  
  idxCom.reset();
  pdata = idxCom.state->CommandData;

  memcpy(pdata,(uint8_t *)pNvmKey1,48);
  genKrnd(pdata+48);
  
  //CRC16((uint8_t*)&crc, pdata, 80);  // TH89 CRC module (need byte swapping)
   gen_CRC16_CCITT((uint8_t*)&crc, pdata, 80);
  //swapBytes(&crc);  
  memcpy(pdata+80,(uint8_t*)&crc, 2);
  
  pDbg = idxCom.state->CommandData;
  ret = idxMcuComms( HOSTIF_CMD_PUTKEY, 0, idxCom.state->CommandData, 82, NULL, NULL );
  ussleep(200);
  return ret;
}



/********************************************************************************
 * GetExtAuthData() 
 * 
 * Data has to be build into another buffer 
 * Note: GM (encrypt part) of this command expects session.msgCounter to be 1
 * The next command within this session also expects session.msgCounter to be 1
 * so the counter is reset after this command.
 */
uint16_t getExtAuthData(void){
  void sign(uint8_t bOrdinal,uint8_t bOptions, uint8_t *cdata, uint16_t Lc);
  uint16_t ret;
  uint8_t cryptogram[16+8+16+2]; // encoded[16]+ signed[8] 
  session.msgCounter = 0;
  memset(session.macChain,0,16);
  memset(cryptogram,0,sizeof(cryptogram));
  genHostCryptogram(cryptogram,&session);
  session.msgCounter = 1;  // recall that ExtAuth asks for this ( at this time state = SEC_CH_CLOSE  
  ret = mcuComms2( HOSTIF_CMD_EXTERNALAUTH, HOSTIF_BOPTION_SIGNED|HOSTIF_BOPTION_ENCRYPT, cryptogram, 8, NULL, NULL );
  ussleep(200);
  if(ret == IDEX_SUCCESS){
    session.state      = SEC_CH_OPEN;
    session.msgCounter = 0;
  }
  return ret;
}

/********************************************************************************
 *  Unpad Buffer - Remove ( possible message padding)
 *  Note: this code replicates MCU code
 */
int unpad_message(uint8_t *message, int size)
{
  int i;
  uint8_t *msg = message + size - 1;

  for (i = 0; i < size; i++)
  {
    if (*msg == 0x80)       break;
    else if (*msg != 0x00)  return -1;  // Found non-zero byte before 0x80
    msg--;
  }

  if (i >= 16)   return -1;  // Too much padding

  return size - i - 1;  // Returns length of unpadded message
}

/********************************************************************************
 * Data Encryption: plain-text, padding , encrypted (S-ENC , ICV) 
 *   |__Command data -plain text(Lc)__|
 *
 *   |__Command Data -plain text(Lc)__|__'80'__|__(000..)00__|  multiple of 16
 *
 *   \_________________________  ____________________________/
 *                             \/
 *               ICV --> |__AES-CBC__| <-- S-ENC 
 *
 * Note: ICV is message-counter encrypted with S-ENC   
 */

 uint16_t BCTC_dataEncrypt(uint8_t * key,uint8_t *cdata, uint16_t Lc)
{
	uint8_t  icv[16];
	uint8_t  padding;
	uint16_t newLc;
	uint8_t  *p;
	
	if(Lc == 0) return 0;	
	p	 = cdata + Lc;
	*p++ = 0x80;
	Lc++;
	 
	padding = ( Lc & 0x0f ) ? 1 : 0;
	newLc	= padding ? ( Lc & ~0x0f) + 16 : Lc;
	if(padding){
	  while( p < cdata + newLc) *p++ = 0x00; 
	}
	// compute ICV
	{  
	  memset( icv, 0, 16);
	   //icv[14] = (uint8_t)((session.msgCounter&0xff00)>>8);
	   //icv[15]=(uint8_t)(session.msgCounter&0x00ff);
	
	  fm1280_enCBC(key, icv, NULL,16, 16 ); 
	}
	fm1280_enCBC(key,cdata,icv,16,newLc);
	//fm1280_enCBC(key,cdata,NULL,16,newLc);
	return newLc;
}

 uint16_t BCTC_dataDecrypt(uint8_t * key,uint8_t *cdata, uint16_t Lcc) {
	uint8_t icv[16];
	uint16_t Lc;
 
	if(Lcc == 0) return 0; 
	// compute ICV
	{
	  memset(icv, 0, 16);
	  // msg counter big endian format
	  icv[0]  = 0x80;
	  //icv[14] = (uint8_t)((session.msgCounter&0xff00)>>8);
	  //icv[15]=(uint8_t)(session.msgCounter&0x00ff);
	  fm1280_enCBC(key, icv, NULL, 16,16); // iv = zeros
	}
	fm1280_dcCBC(key, cdata, icv,16, Lcc);
 
	Lc = unpad_message(cdata, Lcc);
	return Lc;
 }



 uint16_t dataEncrypt(uint8_t *cdata, uint16_t Lc){
  uint8_t  icv[16];
  uint8_t  padding;
  uint16_t newLc;
  uint8_t  *p;
  
  if(Lc == 0) return 0;   
  p    = cdata + Lc;
  *p++ = 0x80;
  Lc++;
   
  padding = ( Lc & 0x0f ) ? 1 : 0;
  newLc   = padding ? ( Lc & ~0x0f) + 16 : Lc;
  if(padding){
    while( p < cdata + newLc) *p++ = 0x00; 
  }
  // compute ICV
  {  
    memset( icv, 0, 16);
	 icv[14] = (uint8_t)((session.msgCounter&0xff00)>>8);
	 icv[15]=(uint8_t)(session.msgCounter&0x00ff);

    fm1280_enCBC(session.senc, icv, NULL,16, 16 ); 
  }
  fm1280_enCBC(session.senc,cdata,icv,16,newLc);
  return newLc;
}
uint16_t dataDecrypt(uint8_t *cdata, uint16_t Lcc) {
   uint8_t icv[16];
   uint16_t Lc;

   if(Lcc == 0) return 0; 
   // compute ICV
   {
     memset(icv, 0, 16);
     // msg counter big endian format
     icv[0]  = 0x80;
	 icv[14] = (uint8_t)((session.msgCounter&0xff00)>>8);
	 icv[15]=(uint8_t)(session.msgCounter&0x00ff);
     fm1280_enCBC(session.senc, icv, NULL, sizeof(session.senc),16); // iv = zeros
   }
   fm1280_dcCBC(session.senc, cdata, icv,sizeof(session.senc), Lcc);

   Lc = unpad_message(cdata, Lcc);
   return Lc;
}
/********************************************************************************
 * Message Signing:
 *                 |_'84'_|_INS P1 P2_|_Lc__|__Command data - plain text(Lc)__|
 *
 *   |_MAC chaining|_'84'_|_INS P1 P2_|_Lcc_|__Command data - plain text(Lc)__|
 *
 *   \____________________________  _________________________________________/
 *                                \/
 *                      |_C-MAC calculation_| <-- S-MAC 
 *
 *   |_'84'_|_INS P1 P2_|_Lcc__|__Command data - plain text(Lcc-8)__|_msb(C-MAC)_|
 */
void sign(uint8_t bOrdinal,uint8_t bOptions, uint8_t *cdata, uint16_t Lc){
  uint8_t tmp[16];
  uint8_t first16bitBlk[16];
  uint16_t wSize = Lc + 4 + 8;  // Header[4] + C-MAC[8]
  uint16_t i;
  fm1280_encmac(tmp,session.smac, session.macChain, NULL, sizeof(session.smac),16 );
  first16bitBlk[0] = 0x84;
  first16bitBlk[1] = 0x00;
  first16bitBlk[2] = bOrdinal;
  first16bitBlk[3] = bOptions;
  first16bitBlk[4] = wSize & 0xff;
  first16bitBlk[5] = wSize >> 8;
  if ( Lc == 0 ){ // no command data
    cmac_compute( session.macChain, session.smac, tmp, first16bitBlk, 6);
  }
  else if ( Lc < 10 ){   // here 0 < Lc < 10 
    for(i = 6 ; i < 6 + Lc ; i++) first16bitBlk[i] = cdata[i-6];
    cmac_compute( session.macChain, session.smac, tmp, first16bitBlk, 6 + Lc);
  }
  else{  //  Lc > 10 
    for(i = 6 ; i < 16 ; i++) first16bitBlk[i] = cdata[i-6];  // fill up the first block
	fm1280_enCBC(session.smac, first16bitBlk, tmp, sizeof(session.smac),16 );
    cmac_compute(session.macChain, session.smac, first16bitBlk, &cdata[10], Lc - 10);
    
  }  
}
//---------------------------------  End of file -----------------------------------------

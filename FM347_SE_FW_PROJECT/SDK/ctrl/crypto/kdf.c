/******************************************************************************
* @file  kdf.c
* @brief Key Derivation Function implementation.
* @author IDEX ASA
* @version 1.0.0
*
*******************************************************************************
 Copyright 2014-2017 IDEX ASA. All Rights Reserved. www.idex.no


 IDEX ASA is the owner of this software and all intellectual property rights
 in and to the software. The software may only be used together with IDEX
 fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.


 This copyright notice must not be altered or removed from the software.


 DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 ASA has no obligation to support this software, and the software is provided
 "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 not to be liable for any damages, any relief, or for any claim by any third
 party, arising from use of this software.


 Image capture and processing logic is defined and controlled by IDEX ASA in
 order to maximize FAR/FRR performance.
******************************************************************************/
#include <stdint.h>
#include <stdlib.h>

#include "cmac.h"
#include "crypto.h"
#include "kdf.h"
#include "idxHalFlash.h"


// context = (Host challenge || MCU challenge) Xor (MCU_ID || SENSOR_ID)
// SKmac  =          KDF(Kmac,  derivation constant='06', Length='0080', context)
// SKrmac =          KDF(Kmac,  derivation constant='07', Length='0080', context)
// SKenc  =          KDF(Kenc,  derivation constant='04', Length='0080', context)
// Card challenge  = KDF(Kenc,  derivation constant='02', Length='0040', context = ( COUNTER || AID ))
// Card Cryptagram = KFD(SKmac, derivation constant='00', Length='0040', context = ( HChallenge || CChallenge))
// Host Cryptagram = KFD(SKmac, derivation constant='01', Length='0040', context = ( HChallenge || CChallenge))
static void kdf_cmac_scp03(uint8_t *K,uint8_t derivationConst,uint8_t L,uint8_t *context,uint8_t *derived_key)
{
  uint16_t i;
  uint8_t in[32];
  for( i = 0; i < 11 ; i++) in[i] = 0x00;	// label = 11 * [0x00] + const
  in[11] = derivationConst;
  in[12] = 0;         // separator
  in[13] = 0x00;      // 'L' = {0x0020, 0x0040, 0x0080, 0x0100}
  in[14] = L;
  in[15] = 0x01;       // 'i' = counter
  for( i = 0; i < 16 ; i++)  (&in[16])[i] = context[i];
  //idxHalFlashWrite((uint8_t *)(0x00b0b300),in,32);

  cmac_compute(derived_key, K, NULL, in, 32) ; // iv = zeros
}

void genkeySENC(uint8_t *kenc, session_t *session){
  kdf_cmac_scp03(kenc, KDF_CONST_SENC, 0x80, session->context, session->senc);
}
void genkeySMAC(uint8_t *kmac, session_t *session){
  kdf_cmac_scp03(kmac, KDF_CONST_SMAC, 0x80, session->context, session->smac);
}

void genkeySRMAC(uint8_t *kmac, session_t *session){
  kdf_cmac_scp03(kmac, KDF_CONST_SRMAC, 0x80, session->context, session->srmac);
}

void genCardCryptogram(uint8_t *cryptogram, session_t *session){
  kdf_cmac_scp03(session->smac, KDF_CONST_CARD_CRYPTOGRAM, 0x40, session->context, cryptogram);
}
void genHostCryptogram(uint8_t *cryptogram, session_t *session){
  kdf_cmac_scp03(session->smac, KDF_CONST_HOST_CRYPTOGRAM, 0x40, session->context, cryptogram);
}


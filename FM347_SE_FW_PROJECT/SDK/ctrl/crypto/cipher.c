/******************************************************************************
* \file  cipher.c
* \brief code cipher
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#include <stdint.h>
#include <stdlib.h>

//#define WINDOWS

#define UNUSED(p) if(p){}
  
#include "idxHalAlgo.h"
#include "cipher.h"
#include "FM_CryptoLib_struct.h"


extern void Copy_dat( uint8_t *dst, uint8_t *src, uint16_t len ) ;
extern uint8_t	BCA_ClockOn (void);
extern uint8_t	BCA_ClockOff (void);
extern u8	BCA_SetKey (uint8_t Alg, uint8_t * Key, uint8_t KeyLen, uint8_t Mode, uint8_t EncDec);
extern u8	BCA_EncDec (uint8_t Alg, uint8_t * Iv, uint8_t * In, uint8_t * Out, uint32_t Len);


/******************************************************************************
 *
 * size = multiple of 16 
 */
uint8_t *fm1280_enCBC(uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t key_size,uint16_t size)
{

	uint8_t out_buffer[16];
	uint8_t iv_buffer[16];
	uint32_t u32_size=0;
	int i;
	uint8_t * iv_pointer;
	uint8_t * in_pointer;
	u32_size=((uint32_t)size&0x0000ffff);
	BCA_ClockOn();
	iv_pointer=iv;
	in_pointer=in;

	if(NULL!=iv_pointer)
		{
		Copy_dat(iv_buffer,iv,16);
		iv_pointer=iv_buffer;
		}

	for(i=0;i<(u32_size>>4);i++)
		{
		if(NULL==iv_pointer)
			{
			BCA_SetKey(ALG_AES,K,key_size,BCA_MODE_ECB,BCA_ENCRYPT);
			}
		else
			{
			BCA_SetKey(ALG_AES,K,key_size,BCA_MODE_CBC,BCA_ENCRYPT);

			}
		BCA_EncDec(ALG_AES,iv_pointer,in_pointer,out_buffer,16);
		Copy_dat(in+(16*i),out_buffer,16);
		Copy_dat(iv_buffer,in_pointer,16);
		iv_pointer=iv_buffer;
		in_pointer +=16;
		}

	BCA_ClockOff();
	return   in;

	
}




void fm1280_encmac(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t key_size,uint16_t size)
{
	uint8_t iv_buffer[16];
	int i;
	uint8_t * iv_pointer;
	uint8_t * in_pointer;

	uint32_t u32_size=0;
	
	u32_size=((uint32_t)size&0x0000ffff);
	iv_pointer=iv;
	in_pointer=in;
	if(NULL!=iv_pointer)
		{
		Copy_dat(iv_buffer,iv,16);
		iv_pointer=iv_buffer;
		}

	BCA_ClockOn();
	for(i=0;i<(u32_size>>4);i++)
		{
		if(NULL==iv_pointer)
			{
			BCA_SetKey(ALG_AES,K,key_size,BCA_MODE_ECB,BCA_ENCRYPT);
			}
		else
			{
			BCA_SetKey(ALG_AES,K,key_size,BCA_MODE_CBC,BCA_ENCRYPT);
			}
		BCA_EncDec(ALG_AES,iv_pointer,in_pointer,out,16);
		Copy_dat(iv_buffer,out,16);
		iv_pointer=iv_buffer;
		in_pointer +=16;
		}
	BCA_ClockOff();

}


void fm1280_dcCBC(uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t key_size,uint16_t size)
{
	uint32_t u32_size=0;
	int i;
	uint8_t * iv_pointer;
	uint8_t * in_pointer;
	uint8_t out_buffer[16];
	uint8_t iv_buffer[16];
	u32_size=((uint32_t)size&0x0000ffff);

	in_pointer=in+u32_size-16;
	Copy_dat(iv_buffer,in_pointer-16,16);
	iv_pointer=iv_buffer;

  	BCA_ClockOn();
	for(i=0;i<(u32_size>>4)-1;i++)
		{
		if(NULL==iv)
			{
			BCA_SetKey(ALG_AES,K,key_size,BCA_MODE_ECB,BCA_DECRYPT);
			  }
		  else
			  {
			  BCA_SetKey(ALG_AES,K,key_size,BCA_MODE_CBC,BCA_DECRYPT);

			  }
		  BCA_EncDec(ALG_AES,iv_pointer,in_pointer,out_buffer,16);
		  Copy_dat(in_pointer,out_buffer,16);
		  in_pointer-=16;
		  Copy_dat(iv_buffer,in_pointer-16,16);
		iv_pointer=iv_buffer;
		}
	if(NULL!=iv_pointer)
		{
		Copy_dat(iv_buffer,iv,16);
		iv_pointer=iv_buffer;
		}

 	 if(NULL==iv)
	  	{
	  	BCA_SetKey(ALG_AES,K,key_size,BCA_MODE_ECB,BCA_DECRYPT);
		}
	else
		{
		BCA_SetKey(ALG_AES,K,key_size,BCA_MODE_CBC,BCA_DECRYPT);
  		}
		BCA_EncDec(ALG_AES,iv_pointer,in,out_buffer,16);
		Copy_dat(in,out_buffer,16);

  BCA_ClockOff();
}






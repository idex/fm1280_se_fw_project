/******************************************************************************
* \file  cmac.c
* \brief implement rfc4493 procedure
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "aes.h"
#include "cipher.h"
#include "cmac.h"
#include "idxHalFlash.h"

#define RB_128 0x87

void vector_shift( unsigned char *out, unsigned char *in, int len )
{
  unsigned char carry = 0 ;
  int i;
  for( i=15; i>=0; i-- )
  {
    out[i] = (in[i] << 1) | carry ;
    carry = (in[i] & 0x80) ? 1 : 0 ;
  }
}

void vector_xor( unsigned char *vector, unsigned char *pattern, int len )
{
  int i;
  for( i=0; i<len; i++ )
  {
    vector[i] ^= pattern[i] ;
  }
}

//uint16_t aes(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t mode);


void cmac_reset( cmac_context_t *mac_ctx )
{
  memset(mac_ctx->c, 0, 16);
}

void cmac_init( cmac_context_t *mac_ctx, uint8_t *key ){
  uint8_t vector[16];
  uint8_t msb;
  memset( vector, 0, 16 );
  fm1280_enCBC(key, vector, NULL, 16,16);
  msb = (int8_t)vector[0] < 0 ? 1 : 0;
  vector_shift(mac_ctx->k1, vector, 16);
  memset(vector, 0, 16);
  vector[15] = RB_128;
  if( msb ){
    vector_xor(mac_ctx->k1, vector, 16);
  }
  vector_shift(mac_ctx->k2, mac_ctx->k1, 16);
  if( (int8_t)mac_ctx->k1[0] < 0){
    vector_xor(mac_ctx->k2, vector, 16);
  }
  memset( mac_ctx->c, 0, 16 );
}


/*************************************************************************************** 
 * cmac1() generate C-MAC and optional encrypt data 
 * K    = key used for AES
 * buf  = pointer to message ( NOTE: should provide at least 16 + 8 bytes extension) 
 * len  = length of message in Bytes
 */
void cmac_compute( uint8_t *out, uint8_t *key, uint8_t *iv, uint8_t *buffer, int len){
//uint8_t i;
  uint16_t nBlks = (len + 15 )  >> 4;  // ceil() In our case can't be 0 
  uint8_t padding = len & 0x0f;
	uint8_t lastBlk[16];
  cmac_context_t mac_ctx ;

  cmac_init( &mac_ctx, key ) ;
  //idxHalFlashWrite((uint8_t *)(0x00b0b500),(uint8_t *)&mac_ctx,48);

  // Now deal with the last block
  if(padding){  // padding and xor-ing
    uint8_t *p = lastBlk + padding;
    memcpy(lastBlk, buffer + (nBlks-1)*16, padding);
    *p++ = 0x80;
    while( p < lastBlk + 16) *p++ = 0x00; 
    vector_xor( lastBlk, mac_ctx.k2, 16);
  }
  else{  // Last block full ( no padding)
    memcpy(lastBlk, buffer + (nBlks-1)*16, 16);
    vector_xor( lastBlk, mac_ctx.k1, 16);
  }
  // AES CBC   
  if( nBlks == 1 ) {
	fm1280_encmac(out, key, lastBlk, iv, 16,nBlks * 16);
  }
  else{
	fm1280_encmac(out, key, buffer, iv,16,(nBlks-1)*16 ); 
	fm1280_encmac(out, key, lastBlk, out,16,16);

  }
}
//------------------------------- End of file ----------------------------------

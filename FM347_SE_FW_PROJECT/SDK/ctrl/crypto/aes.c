/******************************************************************************
* \file  aes.c
* \brief code AES
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#include <stdlib.h>
#include "aes.h"

// bmode = b0-b3   a=encrypt , 5 = decrypt
//         b4-b7   a=AES128 , 5 = AES192, 3 = AES256
//         b8-b15  0x22
//         b16-b20 5
//         b21-b23 5 = sec level normal, other = sec level high
//         b24-b27 5 = no checking compute, other =  checking compute
// return 0xaa success, other = error

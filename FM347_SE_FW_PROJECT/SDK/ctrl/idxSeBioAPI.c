/******************************************************************************
* \file  bio-api.c
* \brief code for testing SE EMK
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "idxSeEmkAPI.h"
#include "hostif.h"
#include "idxSeBioAPI.h"
#include "bio-ctrl.h"
#include "serialInterface.h"
#include "main.h"

#define UNUSED(p) if(p){}

#define FAR

/* The endianess should be consider as following
 * The COMBUF is always little endian 
 */ 
static void wrU16(uint8_t *pU16, uint16_t val)
{
  *pU16++ = val & 0xff;
  *pU16   = val >> 8;
}

#if 0
static uint16_t rdU16(uint8_t *pU16)
{
  uint16_t val = (uint16_t)*pU16 | ((uint16_t)*(pU16 +1) << 8 ) ; 
  return val;
}
#endif

enum ParamType
{
  MAX_NUM_TOUCHES_PER_FINGER = 1,
  MAX_ENROLLED_FINGERS
};

//static struct idxBioEnrolFingerInfo_s finfo[2];
//static const struct idxBioParamListEntry_s params[] = { {0, 0}, { 2, 6}, { 2, 6 } };
extern BaseSerialInterface idxCom;
extern uint8_t matcherBuffer[];
void idxtest(void){;}

extern uint8_t get_p2( void );
extern uint8_t get_lc( void );

extern struct idxBioSystemConfig_s SystemConfig;
extern void program_tracker(uint8_t *data_buff, uint8_t data_len);
extern void ussleep(uint32_t count);
extern uint16_t idxHalFlashErase(uint8_t *pAddr, uint32_t bytesToErase);
#if 1
void Init_SystemConfig(void)
{
  SystemConfig.Policies.SensorInit = 1;
  SystemConfig.Policies.PermitUpdate = 1;
  SystemConfig.Policies.UseCaseSecurity = IDX_BIO_VERIFY_MANUFACTURE;
  SystemConfig.Policies.RequireSecureMatcher = 0;	// Implicit IDM
  SystemConfig.Policies.MatchSecurity = IDX_BIO_MATCH_SECURITY_MIN;
  SystemConfig.Policies.EnrollPermissions = IDX_BIO_ENROLL_ON_CARD;
  SystemConfig.Policies.oceFingers = IDX_BIO_OCE_FINGERS_TWO;
  SystemConfig.Policies.oceTouches = 6;
  SystemConfig.Policies.PermitIncrementalFingerEnroll = 0;
  SystemConfig.Policies.PermitIncrementalImageEnroll = 0;
  SystemConfig.Policies.ReEnrollment = IDX_BIO_OCE_RE_ENROLL_RESET_ALL;
  SystemConfig.Policies.PermitContactlessOCE = 0;
  
  SystemConfig.PageSize = 256;
  SystemConfig.TemplateBase = 0x00b01000;
  SystemConfig.TemplateSize = 256;
  
  //SystemConfig.idxBioWriteFlash = writeFlash;
  
  SystemConfig.MaxFingers = 2;
  SystemConfig.EnrolFingerInfo = NULL;
  SystemConfig.idxBioEnrollUi  = NULL;
}
#endif

enum idxBioRet_e idxBioInitialize( enum idxBioInitializeOptions_e Options,
          const struct idxBioSystemConfig_s *SystemConfig,
          uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  uint16_t recvDataSize = 0;

  switch( Options )
  {
    case IDX_BIO_INITIALIZE_CONTACT:
    {
      cdata[0] = SystemConfig->Policies.SensorInit ? 0x02 : 0x00;
      break;
    }
    case IDX_BIO_INITIALIZE_CONTACTLESS:
    {
		
      cdata[0] = SystemConfig->Policies.SensorInit ? 0x03 : 0x01;

      break;
    }
    case IDX_BIO_INITIALIZE_BATTERY:
    {
      cdata[0] = 0x03;
      break;
    }
    default:
    {
      ret = IDX_BIO_NOT_IMPLEMENTED;
    }
  }


  if ( ret == IDX_BIO_OK )
  	{
    ret = (BioRet)idxMcuComms( HOSTIF_CMD_INITIALIZE, bOptions, (void*)cdata, HOSTIF_CMD_INITIALIZE_DATA_SIZE, (void*)rdata, &recvDataSize );
	ussleep(200);
  	}
  return ret;
}

enum idxBioRet_e idxBioUninitialize( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_UNINITIALIZE, bOptions, (void*)cdata, HOSTIF_CMD_UNINITIALIZE_DATA_SIZE, NULL, NULL );
  return ret;
}

enum idxBioRet_e idxBioGetImage( uint16_t BufferSize, uint8_t *pBuffer, uint16_t *pOutputDataSize )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint16_t cdata_size = (uint16_t)get_lc();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  uint16_t recvDataSize = 0;

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_GETIMAGE, bOptions, (void*)cdata, cdata_size, (void*)rdata, &recvDataSize );
  if ( pOutputDataSize != NULL )
    *pOutputDataSize = recvDataSize;
  return ret;	
}

enum idxBioRet_e idxBioAcquire( uint16_t BufferSize, uint8_t *pBuffer, uint8_t flags )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint16_t *test_size=0;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )
  
  cdata[0] = flags;

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_ACQUIRE, bOptions, (void*)cdata, HOSTIF_CMD_ACQUIRE_DATA_SIZE, matcherBuffer, test_size );
  return ret; 
}

enum idxBioRet_e idxBioLed( uint16_t ledControlFlags,
         uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED( BufferSize )
  UNUSED( pBuffer )
  UNUSED( ledControlFlags )

  return ret;
}

enum idxBioRet_e idxBioSelfTest( uint16_t BufferSize, uint8_t *pBuffer,
        uint16_t *pOutputDataSize )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED( BufferSize )
  UNUSED( pBuffer )
  UNUSED( pOutputDataSize )

  return ret;
}

enum idxBioRet_e idxBioCalibrate( uint8_t Options,
				 uint16_t BufferSize, uint8_t *pBuffer,
				 uint16_t *pOutputDataSize )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )
  
  cdata[0] = Options;

  if ( ret == IDX_BIO_OK )
    ret = (BioRet)idxMcuComms( HOSTIF_CMD_CALIBRATE, bOptions, (void*)cdata, HOSTIF_CMD_CALIBRATE_DATA_SIZE, &rdata, pOutputDataSize );

  return ret;
}

enum idxBioRet_e idxBioDeleteRecord( uint16_t index, 
        uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )
  
  cdata[0] = index & 0xFF;
  cdata[1] = ( index >> 8 ) & 0xFF;
  idxHalFlashErase((uint8_t *)idex_used_flash_start,idex_template_flash_size);//erase 40k template area
  idxHalFlashErase((uint8_t *)debug_flash_start,debug_flash_size);//erase 15k debug area
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_DELETERECORD, bOptions, (void*)cdata, HOSTIF_CMD_DELETERECORD_DATA_SIZE, NULL, NULL );
  return ret;
}

enum idxBioRet_e idxBioUpdateStart( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_UPDATESTART, bOptions, (void*)cdata, HOSTIF_CMD_UPDATESTART_DATA_SIZE, NULL, NULL );
  return ret;
}

enum idxBioRet_e idxBioUpdateDetails( enum idxBioUpdateOptions_e Options,
             uint32_t Address, uint32_t Size,
             const uint8_t iv[32],
             uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  
  uint8_t bOptions = get_p2();
  uint16_t cdata_size = (uint16_t)get_lc();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )
  UNUSED( Options )
  UNUSED( Address )
  UNUSED( Size )
  UNUSED( iv )
  
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_UPDATEDETAILS, bOptions, (void*)cdata, cdata_size, NULL, NULL );
  return ret;
}

enum idxBioRet_e idxBioUpdateChunk( uint16_t BufferSize, uint8_t *pBuffer,
           uint16_t InputDataSize )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint16_t cdata_size = (uint16_t)get_lc();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )
  UNUSED( InputDataSize )
  
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_UPDATEDATA, bOptions, (void*)cdata, cdata_size, NULL, NULL );
  return ret;
}

enum idxBioRet_e idxBioUpdateHash( const uint8_t Hash[8],
          uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint16_t cdata_size = (uint16_t)get_lc();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )
  UNUSED( Hash )
  
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_UPDATEHASH, bOptions, (void*)cdata, cdata_size, NULL, NULL );
  return ret;
}

enum idxBioRet_e idxBioUpdateEnd( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_UPDATEEND, bOptions, (void*)cdata, HOSTIF_CMD_UPDATEEND_DATA_SIZE, NULL, NULL );
  return ret;
}

enum idxBioRet_e idxBioStoreBlob( uint16_t BufferSize, uint8_t *pBuffer, uint16_t *pOutputDataSize )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint16_t cdata_size = (uint16_t)get_lc();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  uint16_t recvDataSize = 0;

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_STOREBLOB, bOptions, (void*)cdata, cdata_size, (void*)rdata, &recvDataSize );
  if ( pOutputDataSize != NULL )
    *pOutputDataSize = recvDataSize;
  return ret;  
}

enum idxBioRet_e idxBioPairSensor( uint8_t Random[IDX_BIO_PAIR_RANDOM_SIZE],
          uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED( BufferSize )
  UNUSED( pBuffer )
  UNUSED( Random )

  return ret;
}

enum idxBioRet_e idxBioLock( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t options = get_p2();
  uint16_t cdata_size = (uint16_t)get_lc();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_LOCK, options, (void*)cdata, cdata_size, NULL, NULL );
  return ret;
}

enum idxBioRet_e idxBioOnCardEnroll( const struct idxBioMatcherWorkArea_s *pWorkAreas,
            uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;

  uint8_t p2 = get_p2();
  uint8_t maxEnrollTouches =  p2 & 0x0F;  // These will come later from SystemConfig
  uint8_t maxEnrollFingers = ( p2 >> 4 ) & 0x0F;
  UNUSED(BufferSize)
  UNUSED(pBuffer)

#ifdef IMM_ONLY	
  UNUSED(pWorkAreas)	
  ret =  (BioRet)idxSeEmkMcuEnroll();

#else  // here IDM EMK API calls  
  {
    uint8_t * matcherBuffer = pWorkAreas[0].pointer;
    uint8_t FAR * pFLASH = pWorkAreas[1].pointer;
    uint32_t flashSize   = pWorkAreas[1].size;
    uint8_t *RAMS[2];
    
    RAMS[0] = matcherBuffer;
    RAMS[1] = pWorkAreas[2].pointer; // RAM areas used by matcher

    ret = (BioRet)idxSeEmkEnroll( RAMS,                // uint8_t  *pScratchPadRam,
                          pFLASH,              // uint8_t  *pTemplateFlash,
                          flashSize,                    // uint32_t szTemplateFlash,
                          maxEnrollTouches,              // uint8_t numTouchesPerFinger,
                          maxEnrollFingers,              // uint8_t numEnrolledFingers,
                          SE_EMK_SECURITY_LVL_5);        // idx_SeEmkSecurityLevel_t securityLevel,
              
    // Do not override idxSeEmkEnroll ret value
    pBuffer[0] = 0x00; pBuffer[1] = 0x00;
    idxMcuComms( HOSTIF_CMD_BIODATAFLUSH, 0, pBuffer, HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE, NULL, NULL );
  }
#endif

  return ret;
}

enum idxBioRet_e idxBioSleeveEnroll( const struct idxBioMatcherWorkArea_s *pWorkAreas,
            uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
 	uint8_t p2 = get_p2();
	uint8_t maxEnrollTouches =  p2 & 0x0F;  // These will come later from SystemConfig
	uint8_t maxEnrollFingers = ( p2 >> 4 ) & 0x0F;
	UNUSED(BufferSize)
	UNUSED(pBuffer)

#ifdef IMM_ONLY	

  ret =  (BioRet)idxSeEmkImmSleeveEnroll();

#else // here IDM EMK API calls
  {  
    uint8_t * matcherBuffer = pWorkAreas[0].pointer;
    uint8_t FAR * pFLASH = pWorkAreas[1].pointer;
    uint32_t flashSize   = pWorkAreas[1].size;
    uint8_t *RAMS[2];
    
    RAMS[0] = matcherBuffer;
    RAMS[1] = pWorkAreas[2].pointer; // RAM areas used by matcher

    ret = (BioRet)idxSeEmkSleeveEnroll( RAMS,                // uint8_t  *pScratchPadRam,
                          pFLASH,              // uint8_t  *pTemplateFlash,
                          flashSize,                    // uint32_t szTemplateFlash,
                          maxEnrollTouches,              // uint8_t numTouchesPerFinger,
                          maxEnrollFingers,              // uint8_t numEnrolledFingers,
                          SE_EMK_SECURITY_LVL_5);        // idx_SeEmkSecurityLevel_t securityLevel,
              
    // Do not override idxSeEmkSleeveEnroll ret value
    pBuffer[0] = 0x00; pBuffer[1] = 0x00;
    idxMcuComms( HOSTIF_CMD_BIODATAFLUSH, 0, pBuffer, HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE, NULL, NULL );
  }
#endif

  return ret;
}

enum idxBioRet_e idxBioSingleEnroll( const struct idxBioMatcherWorkArea_s *pWorkAreas,
            uint16_t BufferSize, uint8_t *pBuffer, uint8_t ranges )
{
	//uint8_t test_buff[6];
  enum idxBioRet_e ret = IDX_BIO_OK;
 	uint8_t p2 = get_p2();
	uint8_t touch =  p2 & 0x0F;  // These will come later from SystemConfig
	uint8_t finger = ( p2 >> 4 ) & 0x0F;
	uint8_t maxEnrollTouches =  ranges & 0x0F;  // These will come later from SystemConfig
	uint8_t maxEnrollFingers = ( ranges >> 4 ) & 0x0F;
	UNUSED(BufferSize)
	UNUSED(pBuffer)
		//test_buff[0]=0x01;
	//program_tracker(test_buff,1);
#ifdef IMM_ONLY	

  //ret =  idxSeEmkImmEnroll();

#else  // here IDM EMK API calls  
  {	
    uint8_t * matcherBuffer = pWorkAreas[0].pointer;
    uint8_t FAR * pFLASH = pWorkAreas[1].pointer;
    uint32_t flashSize = pWorkAreas[1].size;
    uint8_t *RAMS[2];

    RAMS[0] = matcherBuffer;
    RAMS[1] = pWorkAreas[2].pointer; // RAM areas used by matcher

    ret = (BioRet)idxSeEmkSingleEnroll( RAMS,                   // uint8_t  *pScratchPadRam,
                          pFLASH,            // uint8_t  *pTemplateFlash,
                          flashSize,                    // uint32_t szTemplateFlash,
                          touch,
                          finger,
                          maxEnrollTouches,              // uint8_t numTouchesPerFinger,
                          maxEnrollFingers,              // uint8_t numEnrolledFingers,
                          SE_EMK_SECURITY_LVL_5 );        // idx_SeEmkSecurityLevel_t securityLevel,
              
    // Do not override idxSeEmkSingleEnroll ret value
    pBuffer[0] = 0x00; pBuffer[1] = 0x00;
    idxMcuComms( HOSTIF_CMD_BIODATAFLUSH, 0, pBuffer, HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE, NULL, NULL );
  }
#endif

	return ret;
}

#define BIOVERIFY_STORE_DATA_SIZE 32
enum idxBioRet_e idxBioPosVerify( const struct idxBioMatcherWorkArea_s *pWorkAreas,
            uint16_t BufferSize, uint8_t *pBuffer,
            uint16_t *pScore, uint8_t imgsrc, uint16_t mwfTimeBudget )
{
		uint16_t ret = IDEX_SUCCESS;
		pBuffer[0] = 0x03;
		ret = idxMcuComms( HOSTIF_CMD_INITIALIZE, 0, pBuffer, 1, NULL, NULL );
	  if (ret != IDEX_SUCCESS) return (BioRet)ret;
		return idxBioVerify( pWorkAreas, BufferSize, pBuffer, pScore, imgsrc, 0 );
}
enum idxBioRet_e idxBioVerify( const struct idxBioMatcherWorkArea_s *pWorkAreas,
            uint16_t BufferSize, uint8_t *pBuffer,
            uint16_t *pScore, uint8_t imgsrc, uint16_t mwfTimeBudget )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
	uint8_t p2 = get_p2();
	uint8_t maxEnrollTouches =  p2 & 0x0F;  // These will come later from SystemConfig
	uint8_t maxEnrollFingers = ( p2 >> 4 ) & 0x0F;
	UNUSED(BufferSize)
	UNUSED(pBuffer)
	UNUSED(pScore)

#ifdef IMM_ONLY	

  ret =  (BioRet)idxSeEmkImmMatch();

#else  // here IDM EMK API calls  
  {
    uint8_t tmpDataBuff[BIOVERIFY_STORE_DATA_SIZE];
    uint8_t * matcherBuffer = pWorkAreas[0].pointer;
    uint8_t FAR * pFLASH = pWorkAreas[1].pointer;
    uint32_t flashSize = pWorkAreas[1].size;
    uint8_t *pRAM1 = pWorkAreas[2].pointer;
    
    uint8_t *RAMS[2];

    RAMS[0] = matcherBuffer;
    RAMS[1] = pRAM1; // RAM areas used by matcher

    ret = (BioRet)idxSeEmkMatch( RAMS,                 // uint8_t *pScratchPadRam,
                        pFLASH,              // uint8_t *pTemplateFlash,
                        flashSize,                      // uint16_t szTemplateFlash,
                        maxEnrollTouches,               // uint8_t numTouchesPerFinger,
                        maxEnrollFingers,               // uint8_t numEnrolledFingers,
                        imgsrc,
                        mwfTimeBudget,                  // matcher work function
                        SE_EMK_SECURITY_LVL_5);          // idx_SeEmkSecurityLevel_t securityLevel,
            
    // Store ReplyHeader and first 10 bytes of ReplyData (signature + (possible) CRC for data)	
    memcpy(tmpDataBuff, (uint8_t*)idxCom.state->ReplyHeader, BIOVERIFY_STORE_DATA_SIZE);
    // Do not override idxSeEmkMatch ret value
    pBuffer[0] = 0x00; pBuffer[1] = 0x00;
    idxMcuComms( HOSTIF_CMD_BIODATAFLUSH, 0, pBuffer, HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE, NULL, NULL );
    // Restore reply header and first 10 bytes of reply data (signature + (possible) CRC for data)	
    memcpy((uint8_t*)idxCom.state->ReplyHeader, tmpDataBuff, BIOVERIFY_STORE_DATA_SIZE);
  }
#endif
  return ret;
}




enum idxBioRet_e idxBioTemplateDetails( enum idxBioTemplateOptions_e Options,
               const struct idxBioEnrolFingerInfo_s *pFingerInfo,
               uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED( BufferSize )
  UNUSED( pBuffer )
  UNUSED( pFingerInfo )

  switch( Options )
  {
    case IDX_BIO_TEMPLATE_DEFAULT:
    {
      break;
    }
    case IDX_BIO_TEMPLATE_BIO:
    {
      break;
    }
    case IDX_BIO_TEMPLATE_SE:
    {
      break;
    }
    default:
    {
      ret = IDX_BIO_NOT_SUPPTD;
    }
  }
  return ret;
}

enum idxBioRet_e idxBioTemplateChunk( uint16_t BufferSize, uint8_t *pBuffer,
             uint16_t InputDataSize )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED( BufferSize )
  UNUSED( pBuffer )
  UNUSED( InputDataSize )

  return ret;
}

enum idxBioRet_e idxBioUpdateTemplateEnd( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED( BufferSize )
  UNUSED( pBuffer )

  return ret;
}

enum idxBioRet_e idxBioResetEnrollment( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED( BufferSize )
  UNUSED( pBuffer )

  return ret;
}

enum idxBioRet_e idxBioReset( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  UNUSED( rdata )
 	idxHalFlashErase((uint8_t *)(scp03_key_flash_addr),scp03_key_flash_size);//erase SCP03Key store area
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_RESET, bOptions, (void*)cdata, HOSTIF_CMD_RESET_DATA_SIZE, NULL, NULL );
  ussleep(200);

  return ret;
}

enum idxBioRet_e idxBioGetParameter( uint8_t id, uint16_t *pValue,
            uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();  
  uint8_t *cdata = pBuffer;  
  uint8_t *rdata = pBuffer + (BufferSize >> 1); 
  uint16_t recvDataSize = 0;
  cdata[0] = id;
  
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_GETPARAM, bOptions, (void*)cdata, HOSTIF_CMD_GETPARAM_DATA_SIZE, (void*)rdata, &recvDataSize );
  *pValue = *(uint16_t*)&rdata[0];
  ussleep(200);
  return ret;
}

enum idxBioRet_e idxBioSetParameter( uint8_t id, uint16_t Value,
            uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;  
  uint8_t *rdata = pBuffer + (BufferSize >> 1); 
  UNUSED( rdata )
  cdata[0] = id;
  cdata[1] = Value & 0xFF;
  cdata[2] = ( Value >> 8 ) & 0xFF;	
  
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_SETPARAM, bOptions, (void*)cdata, HOSTIF_CMD_SETPARAM_DATA_SIZE, NULL, NULL );
ussleep(200);
  return ret;
}

enum idxBioRet_e idxBioStoreParams( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint8_t *cdata = pBuffer;  
  uint8_t *rdata = pBuffer + (BufferSize >> 1); 
  UNUSED( rdata )

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_STOREPARAMS, bOptions, (void*)cdata, HOSTIF_CMD_STOREPARAMS_DATA_SIZE, NULL, NULL );
  ussleep(200);

  return ret;
}

enum idxBioRet_e idxBioReadData( uint16_t BufferSize, uint8_t *pBuffer, uint16_t *pOutputDataSize )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint16_t cdata_size = (uint16_t)get_lc();
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  uint16_t recvDataSize = 0;
  
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_READDATA, bOptions, (void*)cdata, cdata_size, (void*)rdata, &recvDataSize );
  if ( pOutputDataSize != NULL )
    *pOutputDataSize = recvDataSize;
  return ret;	
}

enum idxBioRet_e idxBioLoadImage( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  uint16_t cdata_size = (uint16_t)get_lc();
  uint8_t *cdata = pBuffer;  
  uint8_t *rdata = pBuffer + (BufferSize >> 1); 
  UNUSED( rdata )
  
  ret = (BioRet)idxMcuComms( HOSTIF_CMD_LOADIMAGE, bOptions, (void*)cdata, cdata_size, NULL, NULL );
  ussleep(200);

  return ret;  
}
  
enum idxBioRet_e idxBioGetVersions( uint16_t BufferSize, uint8_t *pBuffer,
           uint16_t *pOutputDataSize )
{
  extern const idex_UnitVersion_t SEversion;
  extern const uint8_t SEtype; 
  //uint8_t seInfo[5];   
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t bOptions = get_p2();
  //uint8_t *cdata = pBuffer;  
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  
  idx_SeEmkInfo_t info;
  uint16_t replySize;
  //uint16_t retSize;
  idex_UnitVersion_t version;

  replySize = 0;
  memcpy(&rdata[replySize], (uint8_t*)&SEversion, sizeof(idex_UnitVersion_t));
  replySize += sizeof(idex_UnitVersion_t);
  
  rdata[replySize] = SEtype;
  replySize += 1;

  idxSeEmkGetInfo(&info);
  version = info.version;      
  rdata[replySize]     = IDX_BIO_VENDOR_IDEX;
  rdata[replySize + 1] = version.version;
  rdata[replySize + 2] = version.revision;
  wrU16(&rdata[replySize + 3], version.build);
  replySize += 5;

  if ( pOutputDataSize != NULL )
    *pOutputDataSize = replySize;

  return ret;
}

enum idxBioRet_e idxBioListTemplates( uint16_t BufferSize, uint8_t *pBuffer, uint16_t *pOutputDataSize )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  uint16_t recvDataSize = 0;

  ret = (BioRet)idxMcuComms( HOSTIF_CMD_LIST, 0, (void*)cdata, HOSTIF_CMD_LIST_DATA_SIZE, (void*)rdata, &recvDataSize );
  if ( pOutputDataSize != NULL )
    *pOutputDataSize = recvDataSize;
  ussleep(200);
  return ret;  
}

#ifndef IMM_ONLY
enum idxBioRet_e idxBioDeleteFinger( const struct idxBioMatcherWorkArea_s *pWorkAreas,
									uint8_t options )
{
  enum idxBioRet_e ret = IDX_BIO_OK;

  uint8_t FAR * pFLASH = pWorkAreas[1].pointer;
  uint32_t flashSize = pWorkAreas[1].size;
  
  uint8_t maxEnrollTouches =  options & 0x0F;  // These will come later from SystemConfig
  uint8_t fingerID = ( options >> 4 ) & 0x0F;

  ret = (BioRet)idxSeEmkDeleteFinger( pFLASH,
                      flashSize,
                      fingerID,
                      maxEnrollTouches,
                      SE_EMK_DELETE_ENROLLMENT_TEMPLATE);
  
  return (BioRet)ret;
}
#endif

/********************************************************************************
 *  Secure Channel Commands
 */
enum idxBioRet_e idxBioInitializeUpdate( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED(BufferSize)
  UNUSED(pBuffer)
  return ret;
}

enum idxBioRet_e idxBioExternalAuthenticate( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED(BufferSize)
  UNUSED(pBuffer)
  return ret;
}

enum idxBioRet_e idxBioPutKeyHeader( enum idxBioSecureOptions_e Options,
            uint16_t BufferSize, uint8_t *pBuffer)
{
  enum idxBioRet_e ret = IDX_BIO_OK;
  UNUSED(pBuffer)
  UNUSED(Options)
  UNUSED(BufferSize)
	
  return ret;
}

enum idxBioRet_e idxBioFinalizeKeys( uint16_t BufferSize, uint8_t *pBuffer )
{
  enum idxBioRet_e ret = IDX_BIO_OK;
	
  UNUSED(BufferSize)
  UNUSED(pBuffer)
	
  return ret;
}
/*  Secure Channel Commands
 ********************************************************************************/

/******************************************************************************
* @file  hostif-dispatcher.c.c
* @brief source file to parse serial commands and send over SPI
* @author IDEX ASA
* @version 0.0.0
*******************************************************************************
* Copyright 2013-2017 IDEX ASA. All Rights Reserved. www.idex.no
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is provided
* "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
* not to be liable for any damages, any relief, or for any claim by any third
* party, arising from use of this software.
*
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR/FRR performance.
******************************************************************************/
#include <string.h>
//#include "..\idex_serial\idex-hostif-state.h"
#include "serialInterface.h"

//#include "halglobal.h"
#include "idex_cmds.h"

#include "idxHalGpio.h"
#include "idxHalSpi.h"
#include "idxHalSystem.h"

#include "idxBaseErrors.h"
#include "idxSeEmkDefines.h"
#include "hw.h"
#include "idxHalTimer.h"
#include "main.h"
#ifndef u32
#define u32 uint32_t
#endif

#ifndef u8
#define u8 uint8_t
#endif
#ifndef u16
#define u16 uint16_t
#endif

#define HSI_CONTROL_BITS BIT3|BIT2|BIT1|BIT0
#define HSO_CONTROL_BITS BIT19|BIT18|BIT17|BIT16

#define HSI_INPUT_NOPULL BIT0
#define HSI_INPUT_PULL_UP BIT3|BIT0
#define HSI_INPUT_PULL_DOWN BIT2|BIT0
#define HSI_OUTPUT_NOPULL 0
#define HSI_OUTPUT_PULL_UP BIT3
#define HSI_OUTPUT_PULL_DOWN BIT2

#define HSO_INPUT_NOPULL BIT16
#define HSO_INPUT_PULL_UP BIT19|BIT16
#define HSO_INPUT_PULL_DOWN BIT18|BIT16
#define HSO_OUTPUT_NOPULL 0
#define HSO_OUTPUT_PULL_UP BIT19
#define HSO_OUTPUT_PULL_DOWN BIT18

#define HS_INPUT_REG reg_gpio_din
#define HS_OUTPUT_REG reg_gpio_dout

#define HSI_INPUT_HIGH  GPIO1_DIN
#define HSO_INPUT_HIGH GPIO5_DIN

#define HSI_OUTPUT_HIGH GPIO1_DOUT
#define HSO_OUTPUT_HIGH GPIO5_DOUT
#define UNUSED(p) if(p){}


volatile unsigned char  Idex_MCU_Initialised;
volatile unsigned char  Idex_RDY_Signal_Detected;
volatile unsigned char  Idex_WTX_Event = 0;
volatile unsigned char  bDelayTimeout;
unsigned char  EnableTimeout;



#ifdef sleeve_enrollment_support
extern vu8	sleeve_enable_flag;
extern vu8 blinkCtrl;
extern vu8 process_in_waiting;
#endif

extern vu8 Idex_free_timer_flag;
extern vu32 Idex_free_timer_counter;

//static idxHalGpioListenerFuncType callback = (idxHalGpioListenerFuncType)NULL;

extern void delay_one_us(unsigned int num);
//extern uint8_t hw_spi_m_transceive (uint32_t cmd, uint8_t *tx_buf, uint32_t tx_len, uint8_t *rx_buf, uint32_t rx_len);
extern uint8_t simu_spi_m_transceive ( uint8_t *tx_buf, uint32_t tx_len, uint8_t *rx_buf, uint32_t rx_len);

extern void hw_spi_m_init (u32 SPI_speed_cfg);
extern void simu_spi_m_init (void);
extern void HW_spi_Master_transfer_DM_SS_soft(u8* tx_data_buf, u32 tx_data_length, u8* rx_data_buf, u32 rx_data_length );
extern void hw_spi_handshaking(void);
extern u16 HandShake_HW(void);
extern void ussleep(uint32_t count);
extern void idex_mosi_return_to_high(void);
extern void idex_mosi_retuento_SPI_mode(void);
#ifdef sleeve_enrollment_support
extern void FM1280_timer_init (void);
extern void FM1280_timer_stop (void);
#endif
extern void idx_spi_clk2gpio(void);

uint32_t SE_GPIO_READ(void)
{
return HS_INPUT_REG;
}
void SE_GPIO_WRITE(uint32_t data)
{
HS_OUTPUT_REG=data;

}

void SE_GPIO_SetOutPut(uint32_t GPIO_NAME,uint32_t mode)
{

	reg_pad_mode_config=(reg_pad_mode_config&(~GPIO_NAME))|mode;

}
void SE_GPIO_SetInput(uint32_t GPIO_NAME,uint32_t mode)
{

		reg_pad_mode_config=(reg_pad_mode_config&(~GPIO_NAME))|mode;



}


void set_7816IO_to_GPIO(void)
{
	reg_pad_function_config = (reg_pad_function_config & (~PAD05_SEL)) |  BIT4;//7816IO to gpio2
	reg_pad_mode_config = (reg_pad_mode_config & 0xffffff0f) | BIT4;//GPIO input with no pull high, led off
	 reg_gpio_dout |= GPIO2_DOUT;
}

void led1on(void){
  reg_pad_mode_config = (reg_pad_mode_config & 0xffffff0f) ;   // GPIO2 = output   
  reg_gpio_dout |= GPIO2_DOUT;   //GPIO2 output High;
}  

void led2on(void){
	reg_pad_mode_config = (reg_pad_mode_config & 0xffffff0f) ;	 // GPIO2 = output	 
	reg_gpio_dout &= ~GPIO2_DOUT;   //GPIO2 output low;

}  

void ledsOff(void){
  reg_pad_mode_config = (reg_pad_mode_config & 0xffffff0f) | BIT4;//GPIO input with no pull high, led off
  reg_gpio_dout |= GPIO2_DOUT;
  //reg_gpio_dout &= ~GPIO2_DOUT; 
}  

void SleeveLEDOff( void )
{
	ledsOff();
}

#ifdef sleeve_enrollment_support

void blinkControl(void)
{

if( blinkCtrl & 0xfc)
  	{
	blinkCtrl -= 4;
	blinkCtrl |= 1;
  	}
if(blinkCtrl & 1)
  	{ 
	blinkCtrl &= ~1;
	if( blinkCtrl & 2)
		{
		led1on();
		}
	else
		{
		led2on();
		}
  	}
else
	{
	blinkCtrl |= 1;
	ledsOff();
	}
}

#endif






/************************************************* 
  Function:     idex_wtx_entry_mgmt
  Description: 
  Input: None
  Return: 
************************************************/
void idex_wtx_entry_handle(void)
{
  return;
#if 0  
  if(Idex_MCU_Initialised)            // No HSO toggling until MCU is initialised
  {
  #if RDY_LINE_INTR_BASED
    Idex_WTX_Event = 1;              // Used to filter false RDY interrupt on WTX wakeup

    GPIOINTEN &= ~IDEX_GPIO1;        // Disable GPIO1 interrupt
    INTIOCON &= ~0x08;          // Disable GPIO interrupt
    IOSEL0 &= ~(0x07 << 9);     // Switch GPIO1 pin to GPIO mode
    GPIODIR &= ~IDEX_SPI_CLK;        // Select direction to OP
    TMC_GPIO_Write((TMC_GPIO_Read() & ~IDEX_SE_HSO));  // Set HSO low        
    while(( TMC_GPIO_Read() & IDEX_SE_HSI) == 0);      // Wait till RDY goes high 
  #else
    TMC_GPIO_Write((TMC_GPIO_Read() & ~IDEX_SE_HSO));          
  #endif
  }
#endif
}

/************************************************* 
  Function:     idex_wtx_exit_handle
  Description: 
  Input: None
  Return: 
************************************************/
void idex_wtx_exit_handle(void)
{
  return;
#if 0
  if(Idex_MCU_Initialised)
  {
    TMC_GPIO_Write((TMC_GPIO_Read() | IDEX_SE_HSO));          
  #if RDY_LINE_INTR_BASED
    IOSEL0 |= (0x01 << 9);     // Revert to SPI CLK mode
    GPIOINTEN |= IDEX_GPIO1;       // Enable GPIO1 interrupt 
    INTIOCON |= 0x08;         // Enable global GPIO interrupt
  #endif  
  }
#endif
}

/************************************************* 
  Function: HBHandshake_Start
  Description: Initial command handshake
  Input: none
  Return: none
************************************************/
u16 HBHandshake_Start(void)
	{
	//hw_spi_handshaking();
	return HandShake_HW();
	}


/**********************************************************************************
 *  HAL - Implementation
 *
 ***********************************************************************************/

uint8_t idxHalGpioInit(idxHalGpioConfigureType config){
  return HBHandshake_Start();
}

void idxHalGpioSet(idxHalGpioPinType pin, idxHalGpioStateType state)
	{



	switch(pin)
	{
		case IDX_HAL_GPIO_HSO:
				idx_spi_clk2gpio();
			if(IDX_HAL_GPIO_LOW == state)
				{
				//SE_GPIO_WRITE( SE_GPIO_READ() & ~HSO_OUTPUT_HIGH );
				reg_gpio_dout &= ~GPIO5_DOUT;  
				}
			else
				{
				//SE_GPIO_WRITE( SE_GPIO_READ() | HSO_OUTPUT_HIGH );
				reg_gpio_dout  |= GPIO5_DOUT;  
				}
			break;
		default:
			break;

	}



}

void idxHalGpioGet(idxHalGpioPinType pin, idxHalGpioStateType *state)
	{

  if( pin == IDX_HAL_GPIO_HSI)
  	{
  	*state = ((reg_gpio_din&GPIO1_DIN) == GPIO1_DIN)? IDX_HAL_GPIO_HIGH : IDX_HAL_GPIO_LOW;
    //( SE_GPIO_READ() & HSI_INPUT_HIGH ) ? IDX_HAL_GPIO_HIGH : IDX_HAL_GPIO_LOW;
  }
  else if( pin == IDX_HAL_GPIO_HSO){
    *state = ( SE_GPIO_READ() & HSO_INPUT_HIGH ) ? IDX_HAL_GPIO_HIGH : IDX_HAL_GPIO_LOW;  
  }

}

void idxHalGpioSetIRQ(idxHalGpioPinType pin, uint8_t on_off,  uint32_t edge)
	{
	  switch (pin)
	  	{
	  	case IDX_HAL_GPIO_HSI:
			reg_pad_function_config = (reg_pad_function_config & (~(BIT0 | BIT1| BIT2))) |  BIT0;
			reg_pad_mode_config = (reg_pad_mode_config & 0xfffffff0) | BIT0 | BIT3;
			reg_pad_interrupt_config = (reg_pad_interrupt_config & 0xfffffff0) |edge;
			reg_pad_interrupt_flag =0;
			NVIC_ICPR = GPIO_INT_BIT;//clear gpio interrupt flag
			if(on_off)
				{NVIC_ISER= GPIO_INT_BIT;//enable gpio interrupt
				}
			else
				{NVIC_ICER = GPIO_INT_BIT;//disable gpio interrupt
				}
			
			break;
		default:break;
	  	}
	}

void idxHalGpioRegisterListener(idxHalGpioListenerFuncType listener)
	{

	}


void idxHalSpiSetSpeed(idxHalSpiSpeedType speed)
{}
void idxHalSpiInit(uint32_t spi_speed)
{
	hw_spi_m_init(spi_speed);
}
int idxHalSpiTransfer(uint8_t *pBufTx, uint16_t txLen, uint8_t *pBufRx,
                      uint16_t rxLen, uint16_t timeoutMs)//need timeout control later
{
	HW_spi_Master_transfer_DM_SS_soft(pBufTx,txLen,pBufRx,rxLen);
return 0;
}

uint32_t idxHalSystemDesiredFrequency(idxHalSystemFrequencyType frequencyLevel)
{
uint32_t frequency;

if(IDX_HAL_SYSTEM_FREQUENCY_LOWEST==frequencyLevel)
{
}
if(IDX_HAL_SYSTEM_FREQUENCY_HIGH==frequencyLevel)
{

}
return frequency;
}

void idxHalSystemSetFrequency(uint32_t frequency)
{}


void idxHalSystemGetFrequency(uint32_t *frequency)
{}

extern void nvm_erase( u8 *addr );
extern void nvm_write( u8 *dst, u8 *src, u16 len );







uint16_t idxHalFlashErase(uint8_t *pAddr, 
                          uint32_t bytesToErase)
{
	uint8_t temp_buffer[256];//any byte can not be read before write.
	uint32_t length=bytesToErase;
	uint32_t offset=0;
	memset(temp_buffer,0xff,256);

	while(0<length)
		{
		if(256<=length)
			{
			nvm_erase(pAddr+offset);
			nvm_write((pAddr+offset), temp_buffer, 256);
			offset+=256;
			length-=256;
			}
		else
			{
			if (0==length)break;
			nvm_erase(pAddr+offset);
			nvm_write((pAddr+offset), temp_buffer, 256);
			break;
			}
		}
	return IDEX_SUCCESS;
}






uint16_t idxHalFlashWrite(uint8_t *pAddr, 
                          const uint8_t *pData, 
                          uint16_t bytesToWrite)
{
	uint8_t temp_buffer[256];//any byte can not be read before write.
	uint32_t length=bytesToWrite;
	uint32_t offset=0;
	//uint16_t i=0;

	uint32_t flash_start_address_in_page;
	uint32_t rest_length_in_page;
	uint8_t *flash_page_start;


	flash_start_address_in_page=(uint32_t)pAddr&0x000000ff;
	flash_page_start=pAddr-flash_start_address_in_page;
	rest_length_in_page=256-flash_start_address_in_page;


	if(rest_length_in_page>=length)
		{
		memcpy(temp_buffer,flash_page_start,256);
		memcpy(temp_buffer+flash_start_address_in_page,pData,length);
		nvm_erase(flash_page_start);
		nvm_write(flash_page_start,temp_buffer,256);
		return IDEX_SUCCESS;
		}
	else
		{
		memcpy(temp_buffer,flash_page_start,256);
		memcpy(temp_buffer+flash_start_address_in_page,pData,rest_length_in_page);
		nvm_erase(flash_page_start);
		nvm_write(flash_page_start,temp_buffer,256);
		offset=rest_length_in_page;
		length-=rest_length_in_page;
		flash_page_start+=256;
		}

	while(0<length)
		{
		if(256<=length)
			{
			memcpy(temp_buffer,(uint8_t *)(pData+offset),256);
			nvm_erase(flash_page_start);
			nvm_write(flash_page_start,temp_buffer,256);
			length-=256;
			offset+=256;
			flash_page_start+=256;
			}
		else
			{
			memcpy(temp_buffer,flash_page_start,256);
			memcpy(temp_buffer,pData+offset,length);
			nvm_erase(flash_page_start);
			nvm_write(flash_page_start,temp_buffer,256);
			break;
			}
		}

	return IDEX_SUCCESS;

}



/*
	reg_clk_ctrl0 |= TIMER_CLK_EN;
	reg_timera_config=BIT1|BIT8|BIT18|BIT17;//8Mhz clock, repeat enable, 2, t1 8+ t2 24 bit timer;
	reg_timer_int_config=TIMEA_INT_EN0;
	NVIC_ISER |=  TIMER_A_INT_BIT;
	reg_timera_preset0=0x000f4240;//500ms, 8000*500//0x00009c40;//5ms, 8000*5,
	reg_timera_counter0=0;
	reg_timer_start=TIMEA_START0;

*/

void idex_sleeve_timer_start(void)
{
	reg_clk_ctrl0 |= TIMER_CLK_EN;
	reg_timera_config=BIT8|BIT18|BIT17;//8Mhz clock, repeat enable,  t0 32 bit timer;
	reg_timer_int_config |=TIMEA_INT_EN0;
	NVIC_ISER |=  TIMER_A_INT_BIT;
	reg_timera_preset0=0x00001f40;//1ms, 8000*1//0x00001f40;
	reg_timera_counter0=0;
	reg_timer_start |=TIMEA_START0;


}
void idex_sleeve_timer_stop(void)
{
	reg_timer_stop |= TIMEA_STOP0;
	reg_timer_int_config&=~TIMEA_INT_EN0;
	NVIC_ICER = TIMER_A_INT_BIT;
	reg_timera_counter0=0;
	reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG0;



}



void idxhalfreetimerstop(void)
{
	if(card_rte.curr_io == IO_CL)
		{
		reg_timer_stop |= TIMEB_STOP0;
		reg_timer_int_config&=~TIMEB_INT_EN0;
		NVIC_ICER =  TIMER_B_INT_BIT;
		reg_timerb_counter0=0;
		reg_timer_ovflow_flag &= ~TIMEB_INT_FLAG0;
		}
	else
		{
		if(card_rte.curr_io == IO_CT)
			{
			reg_timer_stop |= TIMEA_STOP0;
			reg_timer_int_config&=~TIMEA_INT_EN0;
			NVIC_ICER = TIMER_A_INT_BIT;
			reg_timera_counter0=0;
			reg_timer_ovflow_flag &= ~TIMEA_INT_FLAG0;
			}
		}
	Idex_free_timer_flag=0;
	Idex_free_timer_counter=0;


}

void idxHalTimerFreeRun(void)
{
	Idex_free_timer_flag=0;
	Idex_free_timer_counter=0;
	if(card_rte.curr_io == IO_CL)
		{
		reg_clk_ctrl0 |= TIMER_CLK_EN;
		reg_timerb_config=BIT8|BIT18|BIT17;//8Mhz clock, repeat enable,  t0 32 bit timer;
		reg_timer_int_config |=TIMEB_INT_EN0;
		NVIC_ISER |=  TIMER_B_INT_BIT;
		reg_timerb_preset0=0x00001f40;//500ms, 8000*500//0x00009c40;//5ms, 8000*5,
		reg_timerb_counter0=0;
		reg_timer_start|=TIMEB_START0;
		Idex_free_timer_flag=1;
		}
	else
		{
		if(card_rte.curr_io == IO_CT)
			{
			reg_clk_ctrl0 |= TIMER_CLK_EN;
			reg_timera_config=BIT8|BIT18|BIT17;//8Mhz clock, repeat enable,  t0 32 bit timer;
			reg_timer_int_config |=TIMEA_INT_EN0;
			NVIC_ISER |=  TIMER_A_INT_BIT;
			reg_timera_preset0=0x00001f40;//500ms, 8000*500//0x00009c40;//5ms, 8000*5,
			reg_timera_counter0=0;
			reg_timer_start |=TIMEA_START0;
			Idex_free_timer_flag=2;
			}
		}

}

uint16_t idxHalTimerGetTicks(void)
{
 return (uint16_t)Idex_free_timer_counter;
}




void idxHalTimerPeriodicRun(uint16_t msec, tcallback cb){
  UNUSED(msec);
  UNUSED(cb);
#ifdef sleeve_enrollment_support
	FM1280_timer_init();
#endif
}

void idxHalTimerStopRun(){
#ifdef sleeve_enrollment_support
	FM1280_timer_stop();
#endif

}



void idxHalTimerDelayMsec(uint16_t msec)//done
{
uint32_t i;
for(i=0;i<msec;i++)
{

#ifdef sleeve_enrollment_support
if(process_in_waiting==0xc3)
	{
	if(	sleeve_enable_flag==0x5a)
		break;
	}
#endif

ussleep(1000);
}
}






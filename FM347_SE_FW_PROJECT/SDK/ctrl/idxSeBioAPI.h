/** ***************************************************************************
 * \file   idxSeBioAPI.h
 * \brief  Define the IDEX SE-biometric API
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2018, 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *
 * \mainpage IDEX SE biometric API
 *
 * The SE biometric API provides an API to access the biometric subsystem
 * from code running on a a secure element (SE). The biometric subsystem
 * includes a biometric microcontroller (MCU) to which a fingerprint
 * sensor is attached. In configurations that support secure matching the
 * biometric subsystem also encompasses significant code that runs on the
 * SE itself.
 *
 * The biometric is organized into a series of use-cases:
 * * \ref BioManufacture
 * * \ref BioConfigure
 * * \ref BioVerification
 * * \ref BioReset
 * * \ref BioDebug
 *
 * In addition there are a number of \ref BioGeneral.
 *
 * \section BasicUsage Basic usage
 *
 * Any usage of the SE biometric API must start with a call to
 * idxBioInitialize() which establishes a pointer to the configuration
 * structure, and defines the power environment (contact-mode / contactless,
 * etc.) in which the card is operating. When the biometric subsystem
 * is no longer required, idxBioUninitialize() should be called.
 *
 * \subsection SecureChannel Secure channel
 * 
 * In many cases, it is expected that a secure channel will be set up between
 * the SE and biometric subsystem's MCU. The SE biometric API is designed
 * such that the API handler never has access to unencrypted keys which are
 * entirely managed by SE code outside of the SE biometric API handler.
 * Similarly, actual encryption, decryption, and signing operations are
 * implemented in external functions idxBioEncryptSignSend() and
 * idxBioReceiveVerifyDecrypt() provided by the SE code to support the
 * SE biometric API handler.
 *
 * After the call to idxBioInitialize() a secure channel may be established,
 * this requires a call to idxBioInitializeUpdate(), followed by a call to
 * idxBioExternalAuthenticate(). When the required biometric operations are
 * completed a call to idxBioPutKey() is required, which installs a new
 * key-set for a subsequent use of the secure channel, and uninitializes
 * the current channel.
 *
 * \section Policies Policies
 *
 * Detailed operation of the SE biometric API handler is controlled by a
 * series of policies, \ref idxBioPolicies_s. It is anticipated that this
 * structure is located on flash memory (not RAM) and it is not intended
 * to be modified while the biometric subsystem is in operation.
 * 
 * \section Threads Multiple threads
 *
 * The SE biometric API is not intended to be used in a multi-threaded
 * environment. It is required that once an API function is called that it
 * must return to the caller before any other API function is called.
 *
 * \section MemManage Memory management
 *
 * The SE biometric API handler, and the routines it calls (such as the
 * biometric manager) require access to RAM. The amount of RAM that is
 * statically allocated is minimized, as is the stack usage.
 *
 * \subsection MemManageComms Communications buffer
 *
 * All API routines that communicate with the biometric subsystem's
 * MCU require a communication buffer, and this is passed in the call
 * to each routine. Most API calls that either supply data to the
 * biometric subsystem or return data from it pass that data in the
 * supplied communication buffer. Since the API handler needs to add
 * headers before the data, and to avoid moving data, the input or
 * output data is located some bytes from the start of the buffer.
 * \ref IDX_BIO_BUFFER_DATA_OFFSET represents the offset of the data
 * within the buffer.
 *
 * \image html BufferDiagram.svg "Buffer parameters"
 *
 * The maximum buffer size required is \ref IDX_BIO_BUFFER_MAX_SIZE.
 * Individual API routines may be able to operate with a buffer smaller
 * than the maximum size, this is documented for each function.
 *
 * The buffer may be used internally by the API handler for multiple
 * data transfers. It must be assumed that input data supplied in
 * the buffer is destroyed by a call to the API.
 *
 * \subsection MemManageScratch Matcher working RAM
 * 
 * Releases that support match on the SE require working RAM to be
 * available for the duration of calls that use the matcher (including
 * On Card Enroll). A data structure, idxBioMatcherWorkArea_s, is passed
 * to these calls to define pointer(s) to the area(s) of memory that
 * may be used. The available size is also included, allowing systems
 * where the available RAM is dynamic to be supported.
 *
 * However, the number of working RAM areas, their minimum size, and any
 * special attributes of those areas are expected to be fixed and agreed in
 * advance between the customer integrating the API handler in to SE code and
 * IDEX. The size information is only intended for a simple yes/no decision on
 * whether sufficient RAM is available. There should be no expectation that
 * the matcher will somehow "figure out" how to operate within the confines
 * of the working RAM areas that the SE is able to free up at the time that
 * the relevant API functions are called.
 *
 *****************************************************************************/
#ifndef _IDX_SE_BIO_API_H_
#define _IDX_SE_BIO_API_H_

#include <stdint.h>
#include <stdbool.h>


/** \brief Assumed offset of data in buffer
 *
 * For API routines that pass data (either input or output data) the
 * assumed offset of the data within the buffer.
 *
 * \note The Biometric API handler may access data up to
 * IDX_BIO_BUFFER_DATA_OFFSET bytes \b before the supplied data pointer.
 */
#define IDX_BIO_BUFFER_DATA_OFFSET 6

/** \brief Largest required buffer size
 */
#define IDX_BIO_BUFFER_MAX_SIZE 256

/** \brief Size of random data to supply to idxBioPairSensor()
 *
 * \todo IDX_BIO_PAIR_RANDOM_SIZE is currently TBD
 */
#define IDX_BIO_PAIR_RANDOM_SIZE

/** Return status from SE Biometric API functions */
enum idxBioRet_e
{
  /* Values that may be returned from the IDEX biometric host interface */
  
  IDX_BIO_OK          = 0x9000, //!< Indicates success

  IDX_BIO_MISMATCH    = 0x6300, //!< Templates do not match
  IDX_BIO_COMMS       = 0x6741, //!< Communication problem (EIO)
  IDX_BIO_INTERRUPT   = 0x6742, //!< Interrupt problem (ETIME)
  IDX_BIO_CALIBRATE   = 0x6743, //!< Wrong calibration data (EAGAIN)
  IDX_BIO_BAD_STATE   = 0x6744, //!< Wrong state
  IDX_BIO_TOO_SMALL   = 0x6745, //!< Image too small
  IDX_BIO_ABORTED     = 0x6746, //!< Acquisition aborted
  IDX_BIO_QUALITY     = 0x6747, //!< Quality is too bad
  IDX_BIO_TIMEOUT     = 0x6748, //!< User timeout
  IDX_BIO_UNKNOWN     = 0x6969, //!< Unknown error
  IDX_BIO_CONDITIONS  = 0x6985, //!< Conditions of use not satisfied
  IDX_BIO_ILLEGAL_CMD = 0x6986, //!< command not allowed
  IDX_BIO_SNS_POWER   = 0x6987, //!< not enough power for sensor
  IDX_BIO_BIO_POWER   = 0x6988, //!< not enough power
  IDX_BIO_BAD_PARAMS  = 0x6A80, //!< Bad parameters
  IDX_BIO_NOT_SUPPTD  = 0x6A81, //!< Function not supported/implemented
  IDX_BIO_NOT_FOUND   = 0x6A83, //!< Record not found
  IDX_BIO_NO_SPACE    = 0x6A84, //!< Not enough space left on device
  IDX_BIO_DATA_SIZE   = 0x6A87, //!< Wrong data size for command
  IDX_BIO_HAVE_KEYS   = 0x6A89, //!< Keys are already created
  IDX_BIO_BAD_COMMAND = 0x6D00, //!< Command not recognized (or not available)

  /* Values returned directly from the SE Biometric handler */
  IDX_BIO_NO_KEYS     = 0x6301, //!< No keys have (ever) been installed
  IDX_BIO_INCONSISTENT= 0x6302, //!< Returned data is inconsistent
  IDX_BIO_BAD_RMAC    = 0x6F01,  //!< Computed R-MAC does not match
  IDX_BIO_NOT_IMPLEMENTED = 0x6A8A, //!< Function not supported/implemented
};
typedef enum idxBioRet_e BioRet;

/** \brief Options for \ref idxBioInitializeUpdate() function
 *
 */
enum idxBioSecureOptions_e
{
  IDX_BIO_SECURE_NONE        = 0, //!< No security
  IDX_BIO_SECURE_MAC         = 1, //!< Sign commands and responses
  IDX_BIO_SECURE_ENCRYPT     = 2, //!< Encrypt commands and responses
  IDX_BIO_SECURE_ENCRYPT_MAC = 3  //!< Encrypt and sign commands and responses
};

/** \brief Options for \ref idxBioTemplateDetails() function
 *
 */
enum idxBioTemplateOptions_e
{
  IDX_BIO_TEMPLATE_DEFAULT = 0, //!< Default - place where templates are normally stored
  IDX_BIO_TEMPLATE_BIO     = 1, //!< Send the template to the biometric subsystem
  IDX_BIO_TEMPLATE_SE      = 2, //!< Send the template to the SE
};

/** \brief Options for \ref idxBioUpdateDetails() function
 *
 */
typedef enum idxBioUpdateOptions_e
{
  IDX_BIO_UPDATE_SUBSYSTEM  = (0 << 1), //!< Update the biometric subsystem MCU
  IDX_BIO_UPDATE_SENSOR_RAM = (1 << 1), //!< Update the sensor - RAM
  IDX_BIO_UPDATE_SENSOR_OTP = (2 << 1)  //!< Update the sensor - OTP
} BioUpdateOptions;

/** \brief Options for \ref idxBioInitialize() function
 *
 */
enum idxBioInitializeOptions_e
{
  IDX_BIO_INITIALIZE_CONTACT     = 0, //!< Initialize in contact-mode
  IDX_BIO_INITIALIZE_CONTACTLESS = 1, //!< Initialize in contactless
  IDX_BIO_INITIALIZE_BATTERY     = 2  //!< Initialize in battery (enrollment sleeve) mode
};

/** \brief Options for \ref idxBioCalibrate() function
 *
 * A bit field for options to pass to calibration, values may be OR-ed
 * with one-another to form the required value.
 */
enum idxBioCalibrateOptions_e
{
  IDX_BIO_CALIBRATE_NONE         =      0, //!< No calibration (return existing data)
  IDX_BIO_CALIBRATE_PERFORM      = (1   ), //!< Perform calibration
  IDX_BIO_CALIBRATE_RETURN       = (1<<1), //!< Return calibration data
  IDX_BIO_CALIBRATE_STORE        = (1<<3), //!< Store calibration to NVRAM
  IDX_BIO_CALIBRATE_DO_NOT_FORCE = (1<<6)  //!< Only calibrate if not previously calibrated
};

/** \brief Vendor definitions for \ref idxBioGetVersions
 *
 */
enum idxBioVendor_e
{
  IDX_BIO_VENDOR_NONE         =      0, //!< Component is missing
  IDX_BIO_VENDOR_IDEX         =      1, //!< Component is supplied by IDEX
};

/** \brief Entry in a parameter list
 *
 * The last entry is marked by a \ref paramId of zero (parameter
 * zero is not updated).  
 */
struct idxBioParamListEntry_s
{
  uint8_t  paramId;    //!< Number of the parameter to update
  uint16_t paramValue; //!< Value to set the parameter to
};

/** \brief Structure holding per-finger enrollment parameters
 *
 * It is intended that this structure be located in
 * flash.
 *
 * If \b name is zero (NULL) the ENROLL_DEFAULT_NAME option bit
 * will be set for this finger.
 *
 * Before enrolling each finger the parameters are updated as
 * specified by \ref paramList. This is intended to allow LED
 * guidance sequences to be altered for each finger. 
 */ 
struct idxBioEnrolFingerInfo_s
{
  uint8_t  fingerId;  //!< Finger ID for the template
  char    *name;      //!< Name to give to the finger
  /** \brief List of parameters to update before enrolling this finger
   *
   * The list is terminated by an entry with \b paramId zero.
   */
  struct idxBioParamListEntry_s *paramList;
};

/** \brief Descriptor for a work area
 *
 */
struct idxBioMatcherWorkArea_s
{
  void *pointer; //!< Pointer to start of area
  uint32_t size; //!< Size of the area (in bytes)
};

/** \brief Describe a number of working areas
 *
 */
struct idxBioMatcherWorkAreas_s
{
  uint8_t numWorkAreas; //!< The number of distinct work areas

  /** \brief Descriptors for work areas
   *
   * An array of \ref numWorkAreas descriptors.
   */
  struct idxBioMatcherWorkArea_s *workAreas;
};
  
/** \defgroup BioEncryptDriver Biometric encryption and hardware interface driver interface
 * @{
 */

/** \brief Send data, after any required encryption and signing.
 *
 * The Biometric API handler depends on this routine whose required behaviour
 * is documented here.
 *
 * Before calling this routine the API handler will have filled out the first
 * \ref IDX_BIO_BUFFER_DATA_OFFSET bytes of the communication buffer with a
 * header. In the current version this header is 6 bytes as follows:
 *
 *  * Constant byte 0x84
 *  * Constant byte 0x00
 *  * Byte \a bOrdinal, specifying to the command being sent (the value of this
 *    byte does not need to be interpreted by the implementation of this function).
 *  * Byte \a bOptions, with options bits for the command (the value of this byte
 *    does not need to be interpreted by the implementation of this function).
 *  * Unsigned 16-bit \a wSize, with the intial value \ref IDX_BIO_BUFFER_DATA_OFFSET
 *    plus the size of data (if any) to be sent with the command.
 *
 * This description is written assuming that encryption and signing is performed
 * in-place in the buffer in which the data is supplied. An actual implementation
 * may choose to encrypt into a separate buffer, but it is the responsibility of
 * the routine to allocate storage for such a buffer.
 *
 * If \b Options calls for encryption the input data will be encrypted. If (as is normally
 * the case) the encryption requires the input data to be a multiple of a fixed sized block,
 * padding will be added after the input data to achieve this. \a wSize is incremented to
 * account for this additional data. The data and the padding is then encrypted.
 *
 * If \b Options calls for signing, \a wSize is incremented by eight (the size of the
 * signature) and the C-MAC is calculated over:
 *
 *  * The 16-byte \b Chaining value
 *  * The data in the buffer from \b pBuffer[0] to \b pBuffer[\a wSize-8]
 *
 * The computed signature is placed in the output buffer at \b pBuffer[\a wSize-8], which
 * will then contain \a wSize bytes.
 *
 * The buffer is then modified so that it contains data in the correct format for
 * the specific physical interface being used. (IDEX supplies routines to achieve this
 * in source code which may be called from within the implementation of this function.)
 *
 * The data in the format for the physical interface is then passed to the driver
 * for the physical interface in order to send it.
 *
 * \param[in] Options    Indicates encrypt/sign/both/none.
 * \param[in] BufferSize Size of available buffer at \b pBuffer.
 * \param[in] pBuffer    Pointer to communication buffer.
 *
 * \return Normally \ref IDX_BIO_OK, else \ref IDX_BIO_NO_SPACE to indicate insufficient
 * space in the buffer, \ref IDX_BIO_CONDITIONS if an encrypted channel is
 * required and it has not been correctly initialized.
 */
enum idxBioRet_e idxBioEncryptSignSend(enum idxBioSecureOptions_e Options,
				       uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Receive data, then perform any required verification and decryption.
 *
 * The Biometric API handler depends on this routine whose required behaviour
 * is documented here.
 *
 * This description is written assuming that data is received into the buffer
 * passed to this routine. An actual implementation may choose to receive data into a
 * separate buffer, but it is the responsibility of the routine to allocate storage for
 * such a buffer.
 *
 * Data is received into the buffer in a format defined for the specific physical
 * interface being used. In general, it may be advantageous to place the first received
 * byte into a byte some number of bytes into the buffer (to minimize data movement
 * in subsequent steps).
 *
 * The buffer is then modified from the specific physical interface format into the format
 * documented below. (IDEX supplies routines to achieve this in source code which may be
 * called from within the implementation of this function.) Following this reorganization
 * the actual data carried in the response (if any) will be located at offset
 * \ref IDX_BIO_BUFFER_DATA_OFFSET bytes in the buffer, and it will be preceded by a
 * header formatted, in the current version, as follows:
 *
 * * Two unused bytes
 * * Unsigned 16-bit \a wSize, indicating the total amount of data including any
 *   padding (due to encryption) and the R-MAC (if present).
 * * 16-bit unsigned value, \a wCode.
 *
 * If \b Options calls for signing (which is validated by this routine) the R-MAC is
 * computed over:
 *
 *  * 16-byte \b Chaining value
 *  * The data in the buffer from \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET] to
 *    \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET + \a wSize - 9]
 *  * \a wCode
 *
 * The computed R-MAC is compared to the last eight bytes of the data (at
 * \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET + \a wSize - 8] to
 * \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET + \a wSize - 1]) if this mismatches
 * an error code (\ref IDX_BIO_BAD_RMAC) is returned, else \a wSize is decremented
 * by 8 (the size of R-MAC) and the routine proceeds.
 *
 * If \b Options calls for decryption, the input data, of length (\a wSize) is decrypted.
 * The padding at the end of the decrypted data is removed and \a wSize is
 * decremented accordingly.
 *
 * \param[in]  Options    Indicates encrypt/sign/both/none.
 * \param[in]  BufferSize Size of available buffer at \b pBuffer.
 * \param[out] pBuffer    Pointer to communication buffer.
 *
 * \return Normally \ref IDX_BIO_OK, else \ref IDX_BIO_NO_SPACE to indicate insufficient
 * space in the buffer, \ref IDX_BIO_CONDITIONS if an encrypted channel is
 * required and it has not been correctly initialized, \ref IDX_BIO_BAD_RMAC if the
 * computed R-MAC does not match the received value.
 */
enum idxBioRet_e idxBioReceiveVerifyDecrypt(enum idxBioSecureOptions_e Options,
					    uint16_t BufferSize, uint8_t *pBuffer);

/** @} */

/** \defgroup BioCallbacks Biometric subsystem callbacks
 * @{
 */

/** \name Flash access callbacks
 * @{
 */

/** \brief Write data to flash
 *
 * \param[in]  pAddr Start address to write to in SE flash.
 * \param[in]  pData Start of data to be written.
 * \param[in]  bytesToWrite  Size of data to be written.
 * \param[out] pBytesWritten Actual size of data written.
 *
 * \todo Resolve return type conflict with callback required by IDEX
 * distributed matcher.
 */
typedef enum idxBioRet_e idxBioWriteFlash_t(uint8_t *pAddr, const uint8_t *pData,
					    uint16_t bytesToWrite, uint16_t *pBytesWritten);

/** @} */

/** \name User interface callbacks
 * @{
 */

/** \brief SE-controlled user interface callback
 *
 * This optional callback may be supplied to allow user guidance
 * indications that are controlled by the SE. It is called both
 * before and after enrolling each touch of each finger.
 *
 * \param[in] fingerId   The finger ID (from the
 *                       \ref idxBioEnrolFingerInfo_s structure) of the
 *                       finger that is about to be or has just completed
 *                       the enrollment processing.
 * \param[in] touch      The "touch" number (counting from zero).
 * \param[in] status     Before dealing with the indicated \b touch of
 *                       the \b fingerId, \b status will have the value zero,
 *                       after dealing with it \b status will have a valid
 *                       return code, \ref IDX_BIO_OK indicating success, other
 *                       values indicating some sort of error condition.
 */
typedef void idxBioEnrollUi_t(uint8_t fingerId, uint8_t touch, enum idxBioRet_e status);

/** @} */

/** @} */

/** \brief Bitset indicating security requirements for each use-case
 *
 */
enum idxBioUseCaseSecurity_e
{
  IDX_BIO_VERIFY_MANUFACTURE             = (1     ), //!< Verify manufacture use-case.
  IDX_BIO_ENCRYPT_MANUFACTURE            = (1 << 1), //!< Encrypt manufacture use-case.
  IDX_BIO_VERIFY_CONFIGURE_BIOMETRICS    = (1 << 2), //!< Verify configure biometrics use-case.
  IDX_BIO_ENCRYPT_CONFIGURE_BIOMETRICS   = (1 << 3), //!< Encrypt configure biometrics use-case.
  IDX_BIO_VERIFY_BIOMETRIC_VERIFICATION  = (1 << 4), //!< Verify biometric verification use-case.
  IDX_BIO_ENCRYPT_BIOMETRIC_VERIFICATION = (1 << 5), //!< Encrypt biometric verification use-case.
  IDX_BIO_VERIFY_SYSTEM_RESET            = (1 << 6), //!< Verify biometric system reset use-case.
  IDX_BIO_ENCRYPT_SYSTEM_RESET           = (1 << 7), //!< Encrypt biometric system reset use-case.
  IDX_BIO_VERIFY_DEBUG                   = (1 << 8), //!< Verify biometric system debug use-case.
  IDX_BIO_ENCRYPT_DEBUG                  = (1 << 9)  //!< Encrypt biometric system debug use-case.
};

/** \brief Control permitted types of enrollment
 *
 * A bitset of permitted enrollment types
 */
enum idxBioEnrollPermissions_e
{
  IDX_BIO_ENROLL_NONE     =        0, //!< No enrollment allowed (possibly not too useful)
  IDX_BIO_ENROLL_ON_CARD  = (1 << 0), //!< Permit On Card Enroll (OCE)
  IDX_BIO_ENROLL_DOWNLOAD = (1 << 1)  //!< Permit template download
};

/** \brief Number of fingers to be enrolled  */
enum idxBioOceFingers_e
{
  IDX_BIO_OCE_FINGERS_NONE = 0, //!< Unused
  IDX_BIO_OCE_FINGERS_ONE  = 1, //!< One finger is to be enrolled
  IDX_BIO_OCE_FINGERS_TWO  = 2, //!< Two fingers are to be enrolled
  IDX_BIO_OCE_FINGERS_1OR2 = 3  //!< One or two fingers are to be enrolled
};

/** \brief Control re-enrollment
 *
 * \note This API provides no mechanism for partial re-enrollment, if an
 * enroll has been started but not completed the only route forwards is one of
 * the reset functions, and start the enrollment again.
 */
enum idxBioOceReEnroll_e
{
  IDX_BIO_OCE_RE_ENROLL_FORBIDDEN,    //!< Re-enrollment is forbidden
  IDX_BIO_OCE_RE_ENROLL_RESET_ALL,    //!< Re-enrollment is permitted via idxBioReset()
  IDX_BIO_OCE_RE_ENROLL_RESET_ENROLL  //!< Re-enrollment is permitted via idxBioResetEnrollment() or idxBioReset()
};

/** \brief Determine match security level
 */
enum idxBioMatchSecurity_e
{
  IDX_BIO_MATCH_SECURITY_MIN,    //!< Minimum security level
  IDX_BIO_MATCH_SECURITY_MEDIUM, //!< Medium security level
  IDX_BIO_MATCH_SECURITY_HIGH    //!< High security level
};

/** \brief System Policies
 *
 */
struct idxBioPolicies_s
{
  /** \brief idxBioInitialize() should initialize the sensor.
   *
   * Determines whether idxBioInitialize() should initialize the sensor, or
   * whether this should be delayed until a call that needs the sensor to
   * be operational.
   */
  bool SensorInit;

  /** \brief Permit Field-Update of the biometric subsystem.
   *
   */
  bool PermitUpdate;

  /** \brief Identify use-cases requiring a secure channel.
   *
   * A bit-set identifying, for each use-case, whether verification
   * (CMAC and RMAC signing) and/or encryption is required.
   *
   * If either security measure is required then all commands
   * in that use-case will fail (with \ref IDX_BIO_CONDITIONS) if
   * a secure channel has not been successfully established.
   */
  enum idxBioUseCaseSecurity_e UseCaseSecurity;

  /** \brief Require the use of a secure matcher.
   *
   * The matcher library selection is generally made when the biometric library
   * is built. On builds that have no secure matcher available setting this policy
   * will cause all attempts at biometric verification to fail.
   *
   * In builds that have both secure and insecure matchers setting this policy
   * will cause a secure matcher to be selected.
   */
  bool RequireSecureMatcher;
  
  /** \brief Determine match security level.
   *
   * \note This policy over-writes biometric subsystem parameter
   * \a MATCH_SECURITY at the start of biometric verification and
   * on-card enrollment.
   */
  enum idxBioMatchSecurity_e MatchSecurity;
   
  /** \brief Permited enrollment schemes
   *
   * Permits the use of commands to download templates to the biometric subsystem.
   *
   * \note A download of a "template" with \b pFingerInfo set to zero is still
   * permitted even if \ref IDX_BIO_ENROLL_DOWNLOAD is not set.
   * This facilitates downloading the field-update commands to the biometric
   * subsystem.
   */
  enum idxBioEnrollPermissions_e EnrollPermissions;

  /** \brief Determine the number of fingers to enroll */
  enum idxBioOceFingers_e oceFingers;

  /** \brief Determine the number of enrolled touches per finger
   *
   * \note This policy over-writes biometric subsystem parameter
   * \a TOUCHES_PER_ENROLL at the start of on-card enrollment.
   */
  uint8_t oceTouches;

  /** \brief Allow enrollment of additional fingers.
   *
   * If, when On Card Enrollment is entered, at least one finger has been enrolled,
   * and the number enrolled is less than the maximum number of fingers to be enrolled,
   * as indicated by \ref oceFingers, then if this is \b true, additional fingers may
   * be enrolled. If this is false then all existing templates need to be deleted and
   * the enrollment started afresh if more fingers are to be enrolled.
   */
  bool PermitIncrementalFingerEnroll;

  /** \brief Allow enrollment of additional fingers.
   *
   * If, when On Card Enrollment is entered, at least one finger has been enrolled,
   * and the most recently enrolled finger has less than the number of enrolled touches
   * required by \ref oceTouches, then if this is \b true, additional touches for this
   * finger may be enrolled. (Enrollment may then proceed to the next finger under the
   * control of \ref PermitIncrementalFingerEnroll.) If this is false then all existing
   * templates for the given finger need to be deleted and the enrollment of the finger
   * started afresh if more touches are to be enrolled.
   */
  bool PermitIncrementalImageEnroll;

  /** \brief Control whether (and when) re-enrollment is permitted.
   *
   */
  enum idxBioOceReEnroll_e ReEnrollment;
  
  /** \brief Determine whether on-card enrollment is permitted with contactless power. */
  bool PermitContactlessOCE;

};

/** \brief System configuration structure
 *
 */
struct idxBioSystemConfig_s
{
  struct idxBioPolicies_s Policies; //!< Policies controlling actions of the API handler
  
  /* Associated with writing to flash */
  uint32_t PageSize;     //!< Size of (write) page
  uint32_t TemplateBase; //!< Base of area available for template storage
  uint32_t TemplateSize; //!< Size of area available for template storage

  idxBioWriteFlash_t *idxBioWriteFlash; //!< Callback to write data to flash
  
  /* Associated with On Card Enroll */

  uint16_t MaxFingers; //!< Maximum number of fingers that may be enrolled.

  /** \brief Per finger enrollment information
   *
   * A pointer to an array of \ref MaxFingers finger enrollment information
   * structures.
   */
  struct idxBioEnrolFingerInfo_s *EnrolFingerInfo;

  /** \brief Enrollment user interface callback
   *
   * May be zero if not required (for example, if On-Card-Enroll is not
   * supported or there are no user-interface elements under direct
   * control of the secure element).
   */
  idxBioEnrollUi_t *idxBioEnrollUi;
  
};

/** \defgroup BioGeneral General biometric API functions
 *
 * API calls associated with basic operation of the Biometric API such as
 * initializing the Biometric API handler, and establishing a secure channel.
 *
 * @{
 */

/** \brief Initialize the Biometric API handler
 *
 * \param[in] Options      Determine mode to initialize the biometric
 *                         subsystem in.
 * \param[in] SystemConfig Structure holding callbacks and configuration
 *                         constants for the integration of the
 *                         Biometric API into the application. The structure
 *                         pointed to by \b SystemConfig is required to remain
 *                         valid an unmodified throughout the lifetime of the
 *                         biometric system (until \ref idxBioUninitialize()
 *                         is called.)
 * \param[in] BufferSize   Size of available buffer at \b pBuffer.
 *                         This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer      Pointer to communication buffer.
 *
 * If the \ref idxBioPolicies_s.SensorInit policy is true, the sensor will be
 * initialized immediately.
 *
 * \note This call does not copy the configuration structure at
 * \b SystemConfig (it merely copies the pointer). The configuration
 * structure is required to remain valid thoughout the session.
 */
void idxtest(void);
 
enum idxBioRet_e idxBioInitialize(enum idxBioInitializeOptions_e Options,
				  const struct idxBioSystemConfig_s *SystemConfig,
				  uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Uninitialize the Biometric API handler
 *
 * \param[in] BufferSize   Size of available buffer at \b pBuffer.
 *                         This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer      Pointer to communication buffer.
 *
 * \note If a secure channel has been established, and the keys are to
 * be changed then a PutKey message must be sent and idxBioFinalizeKeys()
 * called before this function.
 */
enum idxBioRet_e idxBioUninitialize(uint16_t BufferSize, uint8_t *pBuffer);

enum idxBioRet_e idxBioGetImage( uint16_t BufferSize, uint8_t *pBuffer,
					uint16_t *pOutputDataSize);
						
enum idxBioRet_e idxBioAcquire( uint16_t BufferSize, uint8_t *pBuffer, uint8_t flags );
/** \brief Establish encrypted channel over the biometric host interface
 *
 * Initiates the process of mutual authentication and establishes a secure
 * channel over the biometric host interface. The call to
 * \ref idxBioInitializeUpdate() must be followed by a call to
 * \ref idxBioExternalAuthenticate() to complete establishment of the
 * secure channel before any further calls to the SE biometric API.
 *
 * The \b BioIdentity is formed from the identity of the biometric
 * microcontroller and the sensor and allows the host to compute Context,
 * and hence the session keys, which is then used to verify the
 * \b Cryptogram returned by the biometric subsystem.
 *
 * Normally verification should first be attempted with the most recent
 * Keyset, with the older one tried if that fails. However if
 * \b PreferOldKeySet is set there is doubt about whether the latest keys
 * are available to the biometric subsystem, and the older keyset should
 * be tried first.
 *
 * In the special case that no keys have ever been installed,
 * \ref idxBioInitializeUpdate() may return \ref IDX_BIO_NO_KEYS. In this case
 * \b HostChallenge is ignored, all of the output parameters of 
 * \ref idxBioInitializeUpdate() are undefined and should be ignored by the
 * caller, \ref idxBioExternalAuthenticate() should not be called
 * but a PutKey message may be sent and idxBioFinalizeKeys() called to install
 * an initial keyset.
 *
 * \param[in]  HostChallenge   Random data used as the host challenge.
 * \param[out] BioChallenge    The challenge returned by the biometric system
 *                             is returned in this array.
 * \param[out] Cryptogram      The cryptogram returned by the biometric system
 *                             is returned in this array.
 * \param[out] BioIdentity     Eight bytes formed from the identity of the
 *                             biometric microcontroller and the sensor are
 *                             returned in this array.
 * \param[out] PreferOldKeySet Set \b true if the information returned by the
 *                             biometric system indicates doubt that the most
 *                             recently saved KeySet was stored correctly. 
 * \param[in] BufferSize       Size of available buffer at \b pBuffer.
 *                             This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer          Pointer to communication buffer.
 */
enum idxBioRet_e idxBioInitializeUpdate(
					uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Complete establishment of the encrypted channel
 *
 * Before calling this routine, the caller must first have called
 * \ref idxBioInitializeUpdate() and used the returned values to verify the MCU
 * cryptogram, calculate the Host cryptogram, calculate session keys, and set the initial
 * Chaining value to all zeros. This routine is then called to send the
 * computed host cryptogram to the biometric subsystem, allowing it to
 * authenticate the host.
 *
 * \param[in] Cryptogram       The host cryptogram which is sent to the biometric
 *                             system to allow it to authenticate the host.
 * \param[in] BufferSize       Size of available buffer at \b pBuffer.
 *                             This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer          Pointer to communication buffer.
 *
 */
enum idxBioRet_e idxBioExternalAuthenticate(
					    uint16_t BufferSize, uint8_t *pBuffer);


/** \brief Obtain header bytes for PutKey message.
 *
 * This API provides no call to send a PutKey message to send a new key-set
 * to the biometric subsystem's MCU. This is because the biometric API handler,
 * by design, has no access to the unencrypted KeySet.
 *
 * It is therefore necessary for code outside of the biometric API to
 * construct the required message and send it with idxBioEncryptSignSend().
 *
 * This call, idxBioPutKeyHeader(), is provided to fill in the header
 * bytes described in the documentation of idxBioEncryptSignSend().
 *
 * \b Options may have the value \ref IDX_BIO_SECURE_ENCRYPT_MAC (which
 * is the normal case) or \ref IDX_BIO_SECURE_NONE (in the special case
 * that the very first key-set is being installed over an insecure channel).
 * Other values of \b Options are not supported.
 *
 * Data in the remainder of the buffer is expected to have the following
 * format:
 *
 *  * 16-byte Key-ENC
 *  * 16-byte Key-MAC
 *  * 16-byte Key-DEK
 *  * 32-byte Random data
 *  *  2-byte Key-check CRC
 *
 * Each of the three 16-byte keys shall have been encrypted using the
 * (existing) Key-DEK in ECB mode with an initial vector set to all zeros.
 * The Random data shall have been encrypted using the (existing) Key-DEK
 * in CBC mode with an initial vector set to all zeros. The CRC is the
 * CRC16-CCITT computed over the 80 bytes of keys and random data before
 * encryption, and it shall not itself have been encrypted.
 *
 * This function will set the initial value of \a wSize to 82, \a bordinal
 * will be set to the appropriate host interface command and \a bOptions
 * will have a value consistent with \b Options.
 *
 * \param[in] Options Indicates encrypt/sign/both/none.
 * \param[in] Buffer  Array of bytes into which the header is written.
 */
enum idxBioRet_e idxBioPutKeyHeader(enum idxBioSecureOptions_e Options,
				    uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Finalize the loading of a new key-set.
 *
 * This sends a command to the biometric subsystem MCU to load the new keyset
 * sent in an earlier PutKey message into flash and erase the current keyset.
 *
 * \param[in] BufferSize       Size of available buffer at \b pBuffer.
 *                             This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer          Pointer to communication buffer.
 * 
 * \sa idxBioPutKeyHeader()
 */
enum idxBioRet_e idxBioFinalizeKeys(uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Control LEDs attached to biometric system
 * 
 * Control the three LEDs attached to the biometric system (integrated into
 * the card). The LEDs remain in the state set by this function indefinitely,
 * so it is usual to call this function again - for example to turn off the
 * LEDs - or call another function (such as \ref idxBioVerify()) that controls
 * the LEDs.
 *
 * \param[in] ledControlFlags A bit field as described in the Biometric
 *                            Host interface protocol document in the
 *                            description of the \a LED_CONTROL_FLAGS
 *                            parameter.
 * \param[in] BufferSize      Size of available buffer at \b pBuffer.
 *                            This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer         Pointer to communication buffer.
 *
 * \note This call does not control any LEDs under control of the SE,
 * for example those integrated into an enrollment sleeve.
 *
 * \note The host commands to satisfy this API call are sent unencrypted and
 * unsigned. (There are no bits in the \ref idxBioUseCaseSecurity_e bitset for
 * general Biometric API functions.)
 */
enum idxBioRet_e idxBioLed(uint16_t ledControlFlags,
			   uint16_t BufferSize, uint8_t *pBuffer);

/** @} General Biometric API Functions */

/** \defgroup BioManufacture Manufacturing use-case
 *
 * Functionality to prepare the biometric subsystem during card manufacture.
 *
 * Functionality provided in this use-case allows:
 * * Access to sensor self-test results (idxBioSelfTest()).
 * * Ability to calibrate the sensor (idxBioCalibrate()).
 * * Update of software in the biometric subsystem MCU and sensor
 *   (idxBioUpdateDetails(), idxBioUpdateHash(), idxBioUpdateEnd()).
 * * Installation of keys for a secure channel between the biometric subsystem MCU
 *   and the sensor, on systems supporting this functionality, (idxBioPairSensor()).
 *
 * \note That installation of an initial key-set for the secure channel between the
 * SE and the biometric subsystem MCU is provided by idxBioPutKey().
 *
 * @{
 */

/** \brief Perform a self-test and retrieve the resulting data.
 *
 * Performs a self-test, placing the resulting data in the output
 * buffer, \b pBuffer, of size \b BufferSize,
 * the returned data starts at \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET],
 * \b pOutputDataSize is set to the actual length of data returned.
 *
 * \param[in]    BufferSize      The \e total size of the data buffer.
 *                               This call requires a buffer of at least \e TBD bytes.
 * \param[in]    pBuffer         Pointer to communication buffer.
 * \param[inout] pOutputDataSize Pointer at which to save the size of the output data.
 *                               Output data will start at
 *                               \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET]
 */
enum idxBioRet_e idxBioSelfTest(uint16_t BufferSize, uint8_t *pBuffer,
				uint16_t *pOutputDataSize);

/** \brief Perform a calibration and/or return calibration data.
 *
 * If the \b Options bitset includes \ref IDX_BIO_CALIBRATE_PERFORM a calibration                                                                            
 * is performed, which may be stored in NVRAM if \ref IDX_BIO_CALIBRATE_STORE is set.
 * The calibration may be conditionally performed only if no calibration has
 * previously been performed by setting \ref IDX_BIO_CALIBRATE_DO_NOT_FORCE.
 * If \ref IDX_BIO_CALIBRATE_RETURN is set the calibration data is returned (if
 * it is clear then *\b pOutputDataSize will be set to zero.)
 *
 * \param[in]    Options         Options for calibration.
 * \param[in]    BufferSize      The \e total size of the data buffer.
 *                               This call requires a buffer of at least \e TBD bytes.
 * \param[in]    pBuffer         Pointer to communication buffer.
 * \param[inout] pOutputDataSize Pointer at which to save the size of the output data,
 *                               Output data will start at
 *                               \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET]
 */
enum idxBioRet_e idxBioCalibrate(uint8_t Options,
				 uint16_t BufferSize, uint8_t *pBuffer,
				 uint16_t *pOutputDataSize);

enum idxBioRet_e idxBioDeleteRecord( uint16_t index, 
				uint16_t BufferSize, uint8_t *pBuffer );

enum idxBioRet_e idxBioUpdateStart(uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Start field-update of an update section.
 *
 * Sets the address and size of the update section that is to be transferred.
 *
 * Data is the transferred using \ref idxBioUpdateChunk(). When all of the data
 * for the section has been transferred, \ref idxBioUpdateHash() should be called.
 * Finally, \ref idxBioUpdateEnd() is called to complete the update.
 *
 * (If this is the first call to \ref idxBioUpdateDetails() an \a UpdateStart host
 * interface command will be sent first.)
 *
 * \todo It is assumed that the encryption (with the static key, in the case there's
 * no secure channel) and the computation of the hash over the image is done on
 * the "real" host - the PC or whatever that is driving the process. (In fact, we
 * probably did it when preparing the image to send to the factory, so that the
 * clear-text image isn't ever shipped to anyone?) Therefore, the SE
 * doesn't need to do this encryption/hashing. If this isn't the case then the
 * encryption callbacks need more parameters to allow selection of the special
 * keys used for these functions.
 *
 * \param[in] Options    Determine destination for the update section.
 * \param[in] Address    Address at which to write the update section.
 * \param[in] Size       Size of the update section.
 * \param[in] iv         Initial vector for update section data encryption.
 * \param[in] BufferSize Size of available buffer at \b pBuffer.
 *                       This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer    Pointer to communication buffer.
 */
enum idxBioRet_e idxBioUpdateDetails(enum idxBioUpdateOptions_e Options,
				     uint32_t Address, uint32_t Size,
				     const uint8_t iv[32],
				     uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Supply a chunk of data
 *
 * Supplies a chunk of data which is being downloaded to the the biometric
 * subsystem when downloading update code images.
 *
 * \param[in] BufferSize    The \e total size of the data buffer.
 *                          This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer       Pointer to communication buffer.
 * \param[in] InputDataSize The length of the data chunk to be downloaded,
 *                          starting at \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET]
 */
enum idxBioRet_e idxBioUpdateChunk(uint16_t BufferSize, uint8_t *pBuffer,
				   uint16_t InputDataSize );

/** \brief Check the hash of the update section
 *
 * \param[in] Hash       Hash of the update section.
 * \param[in] BufferSize Size of available buffer at \b pBuffer.
 *                       This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer    Pointer to communication buffer.
 */
enum idxBioRet_e idxBioUpdateHash(const uint8_t Hash[8],
				  uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Complete an update
 *
 * Complete the update.
 *
 * For updates targeting \ref IDX_BIO_UPDATE_SUBSYSTEM all keys in the biometric
 * subsystem (but not its sensor) are deleted. For updates targeting
 * \ref IDX_BIO_UPDATE_SENSOR_OTP all keys in the sensor are deleted. If the
 * sensor is paired with the biometric subsystem then either operation will
 * result in the biometric subsystem being permanently unable to communicate
 * with the sensor. For this reason, it is generally necessary to call
 * \ref idxBioReset() before performing an update.
 *
 * The entire biometric subsystem is reset after the update is completed, and
 * this call implicitly performs a \ref idxBioUninitialize().
 *
 * \param[in] BufferSize Size of available buffer at \b pBuffer.
 *                       This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer    Pointer to communication buffer.
 */
enum idxBioRet_e idxBioUpdateEnd(uint16_t BufferSize, uint8_t *pBuffer);

enum idxBioRet_e idxBioStoreBlob( uint16_t BufferSize, uint8_t *pBuffer,
					uint16_t *pOutputDataSize );

/** \brief Pair sensor with biometric subsystem
 *
 * Establish a secure channel between the biometric subsystem and the sensor that
 * it uses. Not all implementations support such a secure channel, and on such
 * systems \ref IDX_BIO_NOT_SUPPTD will be returned. Pairing may only be
 * performed once, and where the sensor is already paired \ref IDX_BIO_HAVE_KEYS
 * will be returned.
 *
 * \param[in] Random     Data used as a source of entropy in generating random keys.
 * \param[in] BufferSize Size of available buffer at \b pBuffer.
 *                       This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer    Pointer to communication buffer.
 *
 * \note The functionality anticipated by this API call is still being defined
 * thus the API call is likely to be redefined.
 */
enum idxBioRet_e idxBioPairSensor(uint8_t Random[IDX_BIO_PAIR_RANDOM_SIZE],
				  uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Removes a number of host interface commands
 *
 * Removes a number of biometric host interface commands from the biometric
 * subsystem MCU. These are Reset, UpdateStart, and Debug.
 *
 * \param[in] BufferSize Size of available buffer at \b pBuffer.
 *                       This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer    Pointer to communication buffer.
 */
enum idxBioRet_e idxBioLock(uint16_t BufferSize, uint8_t *pBuffer);

/** @} Manufacturing use-case */

/** \defgroup BioConfigure Configure biometrics use-case
 *
 * Commands in this use-case provide the ability to install biometric templates
 * on the card ready for subsequent verification. Templates are either
 * installed via On-Card-Enroll (idxBioOnCardEnroll()) or are downloaded
 * to the card (idxBioTemplateDetails(), idxBioTemplateChunk(), and
 * idxBioUpdateTemplateEnd()).
 *
 * Installing templates, where data is to be stored on the SE, requires
 * that callback \ref idxBioWriteFlash_t is provided to allow the storage of
 * the templates.
 *
 * Where the On-Card-Enroll functionality is used a callback,
 * \ref idxBioEnrollUi_t, may be used to provide feedback to the user (for
 * example, using LEDs on an enrollment sleeve that are connected to signals
 * controlled by the SE).
 *
 * @{
 */

/** \brief Perform on-card enrollment
 *
 * Perform on-card enrollment, store the enrolled templates in NVM (either
 * in the biometric system or - for secure matchers - in the SE). If
 * templates have already been saved for some fingers and partial enrollment
 * is enabled then the enrollment sequence will start with the appropriate
 * finger.
 *
 * If the optional \b idxBioEnrollUi callback is supplied (in the
 * \ref idxBioSystemConfig_s structure) then the enrollment sequence is
 * managed on the SE and the callback is called to allow locally controlled
 * user interface elements (for example, LEDs on an enrollment sleeve) to be
 * controlled.
 *
 * User interface elements in the biometric subsystem (for example, LEDs
 * integrated into the card) are controlled by the biometric subsystem
 * under the control of the LED sequence parameters. These may be updated,
 * prior to starting enrollment of each finger, using the \b paramList
 * entry in the \b fingerInfo structure for the specific finger.
 *
 * \param[in] pWorkAreas  Pointer to a structure describing available working
 *                        RAM areas.
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 *
 */
enum idxBioRet_e idxBioOnCardEnroll(const struct idxBioMatcherWorkArea_s *pWorkAreas,
				    uint16_t BufferSize, uint8_t *pBuffer);

enum idxBioRet_e idxBioSleeveEnroll( const struct idxBioMatcherWorkArea_s *pWorkAreas,
            uint16_t BufferSize, uint8_t *pBuffer );

enum idxBioRet_e idxBioSingleEnroll( const struct idxBioMatcherWorkArea_s *pWorkAreas,
				    uint16_t BufferSize, uint8_t *pBuffer, uint8_t ranges );

/** \brief Start the process of installing a template
 *
 * Starts the process of installing a template (obtained via off-card enrollment)
 * into the biometric subsystem.
 *
 * Templates are normally stored in the default location for templates in
 * the particular implementation (which may be on the SE for some secure
 * matchers, or in the biometric subsystem - generally for insecure matchers).
 *
 * Some matchers require a template that is split between the biometric
 * subsystem and the SE. In this case, two separate downloads will be required,
 * one for the SE part, and the other for the biometric subsystem part, with
 * the \b Options argument determining where the template is to be stored.
 *
 * Downloading a template to the biometric subsystem may also be required as
 * part of a field update sequence. In this case \b pFingerInfo may be set
 * to zero to indicate that this is not actually a biometric template.
 *
 * \param[in] Options     Options
 * \param[in] pFingerInfo Per-finger arguments.
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 *
 * \note Any \b paramList in the \b fingerInfo is ignored.
 */
enum idxBioRet_e idxBioTemplateDetails(enum idxBioTemplateOptions_e Options,
				       const struct idxBioEnrolFingerInfo_s *pFingerInfo,
				       uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Supply a chunk of template data
 *
 * Supplies a chunk of data which is being downloaded to the the biometric
 * subsystem when downloading templates.
 *
 * \param[in] BufferSize    The \e total size of the data buffer.
 *                          This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer       Pointer to communication buffer.
 * \param[in] InputDataSize The length of the data chunk to be downloaded,
 *                          starting at \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET]
 */
enum idxBioRet_e idxBioTemplateChunk(uint16_t BufferSize, uint8_t *pBuffer,
				     uint16_t InputDataSize);

/** \brief Complete a template download process
 *
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 */
enum idxBioRet_e idxBioUpdateTemplateEnd(uint16_t BufferSize, uint8_t *pBuffer);

/** @} Configure biometrics use-case */

/** \defgroup BioVerification Biometric verification use-case
 *
 * Actual biometric verification (deciding whether a fingerprint acquired from the
 * sensor matches one of the installed templates) is provided by idxBioVerify().
 *
 * In most cases, control of LEDs integrated into the card to indicate verification
 * status is controlled via parameters of the biometric subsystem. Where this is
 * insufficient, idxBioLed() provides direct control of the LEDs.
 * @{
 */

/** \brief Perform a biometric verification
 *
 * Acquires a fingerprint and performs a biometric verification.
 *
 * \param[in] pWorkAreas  Pointer to a structure describing available working
 *                        RAM areas.
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 * \param[in] pScore      Location at which the match score will be saved.
 *
 * \note Because of the importance of the match score it is returned twice over
 * the biometric host interface (in the upper and lower 16-bits of a 32-bit
 * word). The smaller score value is returned. If the upper 16 bits do not match
 * the lower (and the routine would otherwise have returned \ref IDX_BIO_OK) the
 * return code will have the value \ref IDX_BIO_INCONSISTENT.
 *
 * \return \ref IDX_BIO_OK indicates a successful match against a stored template,
 *         \ref IDX_BIO_MISMATCH indicates completion of the the command without
 *              error, but that the acquired fingerprint does not match any stored
 *              template. Other return values indicate error conditions.
 */
enum idxBioRet_e idxBioVerify(const struct idxBioMatcherWorkArea_s *pWorkAreas,
			      uint16_t BufferSize, uint8_t *pBuffer,
			      uint16_t *pScore, uint8_t imgsrc, uint16_t mwfTimeBudget );
enum idxBioRet_e idxBioPosVerify( const struct idxBioMatcherWorkArea_s *pWorkAreas,
            uint16_t BufferSize, uint8_t *pBuffer,
            uint16_t *pScore, uint8_t imgsrc, uint16_t mwfTimeBudget );
/** @} Biometric verification use-case */

/** \defgroup BioReset Biometric system reset use-case
 *
 * Provides the possibility to reset either the entire biometric
 * subsystem (idxBioReset()) or just the enrollment status (idxBioResetEnrollment()),
 * allowing the card to be reinitialized.
 * @{
 */

/** \brief Reset enrollment status
 *
 * Delete stored templates, returning the biometric subsystem to its
 * un-enrolled state. In addition to calling this routine, the caller must
 * also erase the flash storage on the SE dedicated to template storage.
 * (See idxBioSystemConfig_s.TemplateBase and
 * idxBioSystemConfig_s.TemplateSize).
 *
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 *
 * \note If the policy \b ReEnrollment is set to \ref IDX_BIO_OCE_RE_ENROLL_FORBIDDEN
 * or \ref IDX_BIO_OCE_RE_ENROLL_RESET_ALL then this call will have no effect and
 * will return \ref IDX_BIO_CONDITIONS.
 */
enum idxBioRet_e idxBioResetEnrollment(uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Reset biometric subsystem
 *
 * Invalidates all sensitive data on the biometric system.
 * In addition to calling this routine, the caller must
 * also erase the flash storage on the SE dedicated to template storage.
 * (See idxBioSystemConfig_s.TemplateBase and
 * idxBioSystemConfig_s.TemplateSize).
 * 
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 *
 * \note If the policy \b ReEnrollment is set to \ref IDX_BIO_OCE_RE_ENROLL_FORBIDDEN
 * then this call will have no effect and will return \ref IDX_BIO_CONDITIONS.
 */
enum idxBioRet_e idxBioReset(uint16_t BufferSize, uint8_t *pBuffer);

/** @} Biometric System reset use-case */

/** \defgroup BioDebug Biometric system debug use-case
 *
 * Provides the ability to read and write biometric subsystem parameters
 * (idxBioGetParameter(), and idxBioSetParameter()) and the ability to write
 * them into biometric subsystem MCU flash (idxBioStoreParams()).
 * @{
 */

/** \brief Get biometric subsystem parameter
 *
 * Reads a biometric parameter. In the special case of reading
 * \a LAST_MATCH_SCORE (which should return the most recent
 * match score twice - in both the lower and upper 16 bits). The smalle
 * score value is returned. If the upper 16 bits do not match
 * the lower (and the routine would otherwise have returned \ref IDX_BIO_OK)
 * then the return code will have the value \ref IDX_BIO_INCONSISTENT.
 *
 * \param[in] id     Identifier for the parameter to read.
 * \param[in] pValue Pointer to the value to be returned.
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 * 
 * \return \ref IDX_BIO_OK indicates that the parameter is successfully read,
 *         \ref IDX_BIO_NOT_FOUND if an attempt is made to read a parameter that
 *         does not exist, \ref IDX_BIO_CONDITIONS in the special case described
 *         for \a LAST_MATCH_SCORE.
 */
enum idxBioRet_e idxBioGetParameter(uint8_t id, uint16_t *pValue,
				    uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Set a biometric subsystem parameter
 *
 * Sets a biometric parameter.
 *
 * \param[in] id    Identifier for the parameter to written.
 * \param[in] Value Value to be written to the parameter.
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 * 
 * \return \ref IDX_BIO_OK indicates that the parameter is successfully written,
 *         \ref IDX_BIO_NOT_FOUND if an attempt is made to read a parameter that
 *         does not exist, \ref IDX_BIO_BAD_PARAMS if an attempt is made to
 *         write to a read-only parameter.
 */
enum idxBioRet_e idxBioSetParameter(uint8_t id, uint16_t Value,
				    uint16_t BufferSize, uint8_t *pBuffer);

/** \brief Store parameters in the biometric subsystem flash
 *
 * Save the current state of all parameters into the Biometric MCU flash.
 *
 * \param[in] BufferSize  Size of available buffer at \b pBuffer.
 *                        This call requires a buffer of at least \e TBD bytes.
 * \param[in] pBuffer     Pointer to communication buffer.
 *
 * \return IDX_BIO_OK indicates that the parameters are successfully written,
 *         Other values indicate an error condition.
 */
enum idxBioRet_e idxBioStoreParams(uint16_t BufferSize, uint8_t *pBuffer);

enum idxBioRet_e idxBioReadData( uint16_t BufferSize, uint8_t *pBuffer,
					uint16_t *pOutputDataSize );

enum idxBioRet_e idxBioLoadImage( uint16_t BufferSize, uint8_t *pBuffer );

/** \brief Return version information.
 *
 * Returns version information in the buffer, \b pBuffer,  of size \b BufferSize,
 * the returned data starts at \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET],
 * \b pOutputDataSize is set to the actual length of data returned.
 *
 * Version data is returned in a series of 5-byte fields:
 *
 * | Vendor |  Version | Component                                                 |
 * | -----: | -------: | :-------------------------------------------------------- |
 * |      0 |  1 to  4 | Sensor firmware version                                   |
 * |      5 |  6 to  9 | Biometric subsystem software version                      |
 * |     10 | 11 to 14 | Biometric matcher library in biometric subsystem (if any) |
 * |     15 | 16 to 19 | Biometric matcher library in SE (if any)                  |
 *
 * If a given component is missing, all bytes for that component have the value zero.
 *
 * The vendor byte has values as defined by \ref idxBioVendor_e.
 *
 * Version information for components supplied by Idex has the following format:
 *
 * |   Byte  | Meaning       |
 * | ------: | :------------ |
 * |       0 | Major Version |
 * |       1 | Revision      |
 * | 2 and 3 | Build number  |
 *
 * \param[in]    BufferSize      The \e total size of the data buffer.
 *                               This call requires a buffer of at least \e TBD bytes.
 * \param[in]    pBuffer         Pointer to communication buffer.
 * \param[inout] pOutputDataSize Pointer at which to save the size of the output data,
 *                               Output data will start at
 *                               \b pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET]
 *
 */
enum idxBioRet_e idxBioGetVersions(uint16_t BufferSize, uint8_t *pBuffer,
				   uint16_t *pOutputDataSize);

enum idxBioRet_e idxBioListTemplates( uint16_t BufferSize, uint8_t *pBuffer,
					uint16_t *pOutputDataSize);

/** \brief Delete the enrolled template from the flash
 *
 * \param[in] pWorkAreas  Pointer to a structure describing available working
 *                        RAM areas.
 * \param[in] options     Contains delete finger options
 *
 * \return IDX_BIO_OK indicates that the finger successfully has been removed,
 *         Other values indicate an error condition.
 */
enum idxBioRet_e idxBioDeleteFinger(const struct idxBioMatcherWorkArea_s *pWorkAreas,
			      uint8_t options);

/** @} Biometric system debug use-case */

#endif /* _IDX_SE_BIO_API_H_ */

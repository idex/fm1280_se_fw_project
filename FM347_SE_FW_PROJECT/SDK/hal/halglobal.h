/*
 *  Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 *  All rights reserved.
 * 
 *  FileName: halglobal.h
 *  SCFID: 
 *  Feature: global header of hal and communication
 *  Version: V0.1
 * 
 *  History: 
 *    2012-01-02 by Luqian
 *      1. Original version 0.1
 * */
#ifndef	__HALGLOBAL_H__
#define	__HALGLOBAL_H__
/*
#include "type.h"
#include "THD89_hal.h"
#include "halinter.h"
#include "THD89.h"
#include "irqhandler.h"
#include "com_api.h"
#include "com_cfg.h"
#include "core_cm0.h"
#include "cryptolib.h"
#include "M1Lib.h"
#include "NVM.h"
#include "SPI.h"
#include "SystemClockLib.h"
#include "selftest.h"
#include "SecurityUtils.h"
extern ST_COMCMD g_stCMD;

void rtnBackup(void);
void BackupPC(void);
*/
void IdleMode(void);

#endif

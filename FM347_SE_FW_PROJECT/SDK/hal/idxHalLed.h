/** ***************************************************************************
 * \file   idxHalLed.h
 * \brief  header for customized GPIO control API defines, in case customer add a new platform.
 *         we expect customer to implement these APIs. These APIs are used by high
 *         level applications.
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#ifndef _IDX_HAL_LED_H_
#define _IDX_HAL_LED_H_

#include <stdint.h>


typedef enum {
  IDX_HAL_CARD_LED_START = 1,
  IDX_HAL_SLEEVE_LED_START,
  IDX_HAL_CARD_LED_NEXT_FINGER,
  IDX_HAL_SLEEVE_LED_NEXT_FINGER,
  IDX_HAL_CARD_LED_TOUCH_SUCCESS,
  IDX_HAL_SLEEVE_LED_NEXT_TOUCH,
  IDX_HAL_CARD_LED_TOUCH_FAIL,  
  IDX_HAL_CARD_LED_LAST_TOUCH,
  IDX_HAL_SLEEVE_LED_ENROLL_FINISHED,
  IDX_HAL_CARD_LED_STOP,
  IDX_HAL_SLEEVE_LED_STOP,
  IDX_HAL_CARD_LED_BLINK,
  IDX_HAL_MATCH_STATE_SUCCESS,
  IDX_HAL_MATCH_STATE_FAIL
} idxHalLedModeStateType;

/**
* @brief LED Control Function Declarations
*
* @return IDEX_SUCCESS if successful,
*         or another error code (taken from wCode in reply header) if not successful
*/
uint16_t idxHalLedStart(void);
uint16_t idxHalLed2ndFinger(void);
uint16_t idxHalLedTouch(uint8_t LedColor);
uint16_t idxHalLedLastTouch(void);
uint16_t idxHalLedStop(void);
uint16_t idxHalLedOff(void);
uint16_t idxHalLedBlink(void);

/**
* @brief Function for controlling LEDs
*
* @param[in] state Enrollment state machine.
* @param[in] index LED index number on sleeve.
*
* @return IDEX_SUCCESS if successful,
*         or another error code (taken from wCode in reply header) if not successful
*/
uint16_t idxHalSetLedMode( idxHalLedModeStateType state, uint8_t index );

#endif /* _IDX_HAL_LED_H_ */

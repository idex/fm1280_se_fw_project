/** ***************************************************************************
 * \file   idxHalGpio.h
 * \brief  Header for customized GPIO control API defines, in case customer add a new platform.
 *         we expect customer to implement these APIs. These APIs are used by high
 *         level applications.
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_GPIO_H_
#define _IDX_HAL_GPIO_H_

#include <stdint.h>
#include <stdbool.h>

/*!
 * @defgroup hal_gpio Hal GPIO module
 * hal_gpio module provides APIs to initialize and control GPIO pin.
 * @{
 */
/*!
 * @brief General GPIO TYPE
 */
typedef enum idxHalGpioPinType
{
	IDX_HAL_GPIO_HSO,
	IDX_HAL_GPIO_HSI,
	IDX_HAL_GPIO_IF_SELECT,
	IDX_HAL_GPIO_TYPE_MAX
}idxHalGpioPinType;

/*!
 * @brief GPIO IRQ Edge
 */
typedef enum {
	IDX_HAL_GPIO_IRQ_RISING_EDGE,
	IDX_HAL_GPIO_IRQ_FALLING_EDGE,
	IDX_HAL_GPIO_IRQ_BOTH
}idxHalGpioIrqEdgeType;

/*!
 * @brief General GPIO state
 */
typedef enum idxHalGpioStateType
{
	IDX_HAL_GPIO_LOW,
	IDX_HAL_GPIO_HIGH
}idxHalGpioStateType;

typedef enum idxHalGpioConfigureType {
	IDX_HAL_GPIO_CONFIG_SPI_5PIN,
	IDX_HAL_GPIO_CONFIG_SPI_4PIN,
	IDX_HAL_GPIO_CONFIG_AUTO
} idxHalGpioConfigureType;

/* @brief To register a callback function listener with instance
 *
 * @param [i] param    The registered param to be transfered back to the listener function
 * @param [i] state    The state of GPIO PIN
 *
 * @return none
 *  */
typedef void (*idxHalGpioListenerFuncType)(void *param);

/**
* Initialize GPIO pin on customer's platform.
* Mainly three steps:
* 1. Enable gpio pin clock;
* 2. Then set default state if it's output pin;
* 3. Do initialization with mode, pull, speed according with configured mode
*
* @param [i] config   Input configurations of GPIO connectivity type
*  of @idxHalGpioConfigureType
*
* @return none
*/
uint8_t idxHalGpioInit(idxHalGpioConfigureType config);

/*!
* @brief Set specific GPIO pin to required state.
* @param[in]  pin   To specify which hal gpio pin to control. Please refer to idxHalGpioPinType.
* @param[in]  state To set hal gpio pin state. Please refer to idxHalGpioStateType..
* @return     NONE.
*/
void idxHalGpioSet(idxHalGpioPinType pin, idxHalGpioStateType state);

/*!
* @brief Get specific GPIO pin's state.
* @param[in]  pin    To specify which hal gpio pin to control. Please refer to idxHalGpioPinType.
* @param[out] *state To get hal gpio pin state.
* @return     NONE.
*/
void idxHalGpioGet(idxHalGpioPinType pin, idxHalGpioStateType *state);

/**
* @brief Set GPIO irq enable or disabled
*
* @param pin  [in]  To specify which hal gpio pin to control. Please refer to @idxHalGpioPinType.
* @param en   [in]  Enable or disable GPIO IRQ
* @param edge [in]  Edge of irq
*
* @return     NONE.
*/
void idxHalGpioSetIRQ(idxHalGpioPinType pin, uint8_t on_off,  uint32_t edge);


/*!
* @brief To support 4 wire SPI communication, HSO and SPI_CLK will use same pin,
* so this API will help to switch pin function between SPI Clock and HSO.
* @param[in]  val,  true : switch from HSO to SPI_CLK.
*                   false: switch from SPI_CLK to HSO.
* @return     NONE.
*/
void idxHalGpio4WireSPISwitchHSO2CLK(void);

void idxHalGpioRegisterListener(idxHalGpioListenerFuncType listener);

/** @}*/
#endif /* _IDX_HAL_GPIO_H_ */

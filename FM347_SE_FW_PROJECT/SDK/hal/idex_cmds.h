/******************************************************************************
* @file  hostif-dispatcher.c.c
* @brief source file to parse serial commands and send over SPI
* @author IDEX ASA
* @version 0.0.0
*******************************************************************************
* Copyright 2013-2017 IDEX ASA. All Rights Reserved. www.idex.no
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is provided
* "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
* not to be liable for any damages, any relief, or for any claim by any third
* party, arising from use of this software.
*
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR/FRR performance.
******************************************************************************/
#ifndef	__IDEXDEMOCMD_H__
#define	__IDEXDEMOCMD_H__
//#pragma pack(push)
//#pragma pack(1)
#include "stdint.h"



// Possbile clock speeds
#define IDEX_fsys_72MHz 		0x00		//default
#define IDEX_fsys_36MHz 		0x10
#define IDEX_fsys_24MHz 		0x20
#define IDEX_fsys_18MHz 		0x30
#define IDEX_fsys_14_4MHz 	0x40
#define IDEX_fsys_12MHz 		0x50
#define IDEX_fsys_10_3MHz 	0x60
#define IDEX_fsys_9MHz 		0x70
#define IDEX_fsys_8MHz 		0x80
#define IDEX_fsys_7_2MHz 	0x90
#define IDEX_fsys_6_5MHz 	0xA0
#define IDEX_fsys_6MHz 		0xB0
#define IDEX_fsys_5_5MHz 	0xC0
#define IDEX_fsys_5_1MHz 	0xD0
#define IDEX_fsys_4_8MHz 	0xE0
#define IDEX_fsys_4_5MHz 	0xF0





#define IDEX_SYSTEM_LOW_SPEED			IDEX_fsys_24MHz
#define IDEX_SYSTEM_HIGH_SPEED		IDEX_fsys_72MHz
#define IDEX_SYSTEM_LOWEST_SPEED	IDEX_fsys_4_5MHz


#define IDEX_ACK_LOCATION 300

//GPIO pin definitions/



#define TIMEOUT_COUNT			3000
#define TIMEOUT_LIMIT			5000		// Don NOT modify



uint16_t HBHandshake_Start(void);

void idex_wtx_entry_handle(void);
void idex_wtx_exit_handle(void);


extern volatile unsigned char	Idex_MCU_Initialised;
extern volatile unsigned char Idex_RDY_Signal_Detected;
extern volatile unsigned char Idex_WTX_Event;
extern volatile unsigned char bDelayTimeout;
extern unsigned char EnableTimeout;

#endif

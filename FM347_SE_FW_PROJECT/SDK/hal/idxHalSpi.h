/** ***************************************************************************
 * \file   idxHalSpi.h
 * \brief  Header for customized SPI defines, in case customer add a new platform.
 *         we expect customer to implement these APIs. These APIs are used by high
 *         level applications.
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_SPI_H_
#define _IDX_HAL_SPI_H_

#include <stdint.h>

/**
 * @defgroup idxHalSpi Hal SPI module
 * idxHalSpi module provides low level SPI APIs for communication implementation
 * @{
 */

/*!
 * @brief General SPI speed level will be implemented by different targets.
 */
typedef enum {
    IDX_HAL_SPI_SPEED_LOW = 0,
    IDX_HAL_SPI_SPEED_MIDDLE,
    IDX_HAL_SPI_SPEED_HIGH
}idxHalSpiSpeedType;

/*!
 * @brief SPI Master or Slave.
 */
typedef enum {
    IDX_HAL_SPI_MASTER_MODE,
    IDX_HAL_SPI_SLAVE_MODE
}idxHalSpiModeType;

/**
* initialize SPI bus on customer's platform.
*             To initialize SPI, three main steps involved:
*             First, please check the MISO/MOSI/SCK/CS pin number.
*             Then enable corresponding clock and GPIO.
*             Configure SPI feature based on slave requirement.
*
* @return     NONE.
*/
void idxHalSpiInit(uint32_t spi_speed);

/**
* This API starts SPI transaction using data from pBufTx for transmit and 
* pBufRx for receive
*
* @param[in]  pBufTx     data buffer stored sending data on SPI bus.
* @param[in]  txLen      sending data size
* @param[in]  pBufRx     data buffer to store the reading data on SPI bus.
* @param[in]  rxLen      reading data size
* @param[in]  timeoutMs  transfer timeout in ms
*
* @return     zero success, non-zero failed.
* @retval     ERR_BAD_PARAM Invalid transfer size.
*             ERR_BUSY      Last SPI transmitting is not finished.
*             ERR_DEVICE    Communication failed between devices.
*/
int idxHalSpiTransfer(uint8_t *pBufTx, uint16_t txLen, uint8_t *pBufRx,
                      uint16_t rxLen, uint16_t timeoutMs);

/**
* Set speed of SPI
* @param[in]  speed  Options of value:
                            IDX_HAL_SPI_SPEED_HIGH,
                            IDX_HAL_SPI_SPEED_MIDDLE,
                            IDX_HAL_SPI_SPEED_LOW,
* @return     NONE.
*/
void idxHalSpiSetSpeed(idxHalSpiSpeedType speed);

/**
* Set SPI into master or slave mode
* @param[in]  mode  Options of value:
                            IDX_HAL_SPI_MASTER_MODE,
                            IDX_HAL_SPI_SLAVE_MODE
* @return     NONE.
*/
void idxHalSpiSetMode(idxHalSpiModeType mode);

/**
* @brief This function enables SPI interrupt, once transaction completed,
* an SERIAL event must be pushed to bio stack
*
* @param en [in]  TRUE enable or FALSE disabled
*
* @return none
*/
void idxHalSpiSetIRQ(uint8_t en);

/** @}*/
#endif /* _IDX_HAL_SPI_H_ */

/** ***************************************************************************
 * \file   idxHalUart.h
 * \brief  Interface to Hardware Abstraction Layer for uart
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_UART_H_
#define _IDX_HAL_UART_H_

// TODO - resolve need for parm4 pragmas on SLE78

#include <stdint.h>

/**
* @brief Function for refresh ISO7816
*
*
* @return IDEX_SUCCESS if successful,
*         or another error code if not successful
*/ 
uint16_t idxHalUartISO7816Refresh(void);

#endif /* _IDX_HAL_UART_H_ */

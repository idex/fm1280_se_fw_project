/** ***************************************************************************
 * \file   idxHalSystem.h
 * \brief  Header of APIs for biometric applications/extensions
 *         to operate system clock and sleep.
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef __HAL_SYSTEM_H
#define __HAL_SYSTEM_H

#include <stdint.h>

/**
 * @defgroup hal_system Hal system module
 * hal_system module provides system initialization/sleep/control frequency APIs for implementation
 * @{
 */

/*!
 * @brief Define different system frequency.
 */
typedef enum
{
    IDX_HAL_SYSTEM_FREQUENCY_HIGH,
	IDX_HAL_SYSTEM_FREQUENCY_LOWEST
}idxHalSystemFrequencyType;

uint32_t idxHalSystemDesiredFrequency(idxHalSystemFrequencyType frequencyLevel);

/**
* Initialize system on customer's platform: e.g. CLOCK/GPIO/TIMER/...
*
* @return     NONE.
*/
void idxHalSystemInit(void);

/**
* Force system to enter sleep mode.
*
* @return     NONE.
*/
void idxHalSystemSleep(void);

/**
* Restore operating frequency to a value returned by an earlier call to idxHalSystemDesiredFrequency() or idxHalSystemGetFrequency().
* The ability to set the system operating frequency is not supported on all Secure Elements,
* and this call will have no effect on those Secure Elements that cannot explicitly set the operating frequency.
* NOTE: This call may affect system clocks so that peripherals such as UART/SPI/I2C may need to be
*       reconfigured after this call in order to set the required data-rate.
* @param[in]  frequencyLevel  To specify frequency level of system clock.
* @return     NONE.
*/
void idxHalSystemSetFrequency(uint32_t frequency);

/**
* Get system operating frequency level.
* @param[in]  frequencyLevel  To specify frequency level of system clock.
* @return     NONE.
*/
void idxHalSystemGetFrequency(uint32_t *frequency);
/** @}*/
#endif /* __HAL_SYSTEM_H */

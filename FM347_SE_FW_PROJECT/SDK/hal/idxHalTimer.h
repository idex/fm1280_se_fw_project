/** ***************************************************************************
 * \file   idxHalTimer.h
 * \brief  Interface to Hardware Abstraction Layer for Timer
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
 
#ifndef _IDX_HAL_TIMER_H_
#define _IDX_HAL_TIMER_H_

#include <stdint.h>

typedef void (*tcallback)(void);

void idxHalTimerFreeRun(void);   // start timer (free running)

uint16_t idxHalTimerGetTicks(void);

void idxHalTimerStopRun(void);

void idxHalTimerDelayMsec(uint16_t msec);

void idxHalTimerDelayUsec(uint16_t usec);

void idxHalTimerTimeOut(uint16_t msec, tcallback cb);

void idxHalTimerPeriodicRun(uint16_t msec, tcallback cb);

#endif /* _IDX_HAL_TIMER_H_ */

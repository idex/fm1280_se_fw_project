/** ***************************************************************************
 * \file   idxHalAlgo.c
 * \brief  Hardware Abstraction Layer for Algo Engines
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#ifndef _IDX_HAL_ALGO_H_
#define _IDX_HAL_ALGO_H_

#include <stdint.h>
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * CRC16-CCITT  - Generate CRC16 for len bytes of data 
 * Args: iv = ptr to initial value, buf = ptr to data, len = data length
 * Return : IDEX Error code
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

uint16_t CRC16_CCITT(uint8_t *iv, uint8_t *buf, uint32_t len);


/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * RNGenerate - Generate DataLen bytes of random data 
 * Args: Randomdata = ptr to generated random data, Datalen  
 * Return : IDX Error code
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

uint16_t RNGenerate(uint8_t *RandomData, uint16_t DataLen);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * AES128 encrypt/decrypt - generate (de)encrypted data (AES-128 algorithm
 * Args:  K = pointer to 128 bit key, iv = pointer to IV 
 * Return : IDX Error code
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

uint16_t AES128encrypt(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv);

uint16_t AES128decrypt(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv);

#endif //_IDX_HAL_ALGO_H_

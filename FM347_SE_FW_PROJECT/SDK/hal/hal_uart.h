/**
 * file          $filename$
 * version       $version$
 * commit        $id$
 * date          $date$
 * @brief        header for customized SPI defines, in case customer add a new platform.
 *               we expect customer to implement these APIs. These APIs are used by high
 *               level applications.
 * $license$
 *
 */

#ifndef __HAL_UART_H
#define __HAL_UART_H

/**
 * @defgroup hal_uart Hal UART module
 * hal_uart module provides low level UART APIs for communication implementation
 * @{
 */

/*!
 * @brief Define different uart baudrate.
 */
typedef enum hal_uart_baudrate_type
{
    HAL_UART_BAUDRATE_9600 = 9600,
    HAL_UART_BAUDRATE_115200 = 115200
}hal_uart_baudrate_t;

/*!
 * @brief To register a callback function listener, please follow below function type.
 */
typedef void (*hal_uart_listener_func_t)(void);

/**
* initialize UART on customer's platform.
*             To initialize UART, three main steps involved:
*             First, please check the UART_RX/UART_TX pin number.
*             Then enable corresponding clock and GPIO.
*             Configure UART feature based on requirement.
*
* @return     NONE.
*/
void hal_uart_init();

/*!
 * @brief UART send data to low level physical port
 *
 * @param data point to address of data to be sent
 * @param len  length of data
 *
 * @return     zero success, non-zero failed.
 * @retval     ERR_BAD_PARAM Invalid length.
 */
int32_t hal_uart_send(uint8_t *data, uint32_t len);

/*!
 * @brief UART receive data from low level physical port
 *
 * @param data point to address of data to be sent
 * @param len  length of data
 *
 * @return     zero success, non-zero failed.
 * @retval     ERR_BAD_PARAM Invalid length.
 */
int32_t hal_uart_receive(uint8_t *data, uint32_t len);

/**
* Register UART TX listener function when transaction is completed.
* @param[in]  listener  Callback function pointer
* @return     NONE.
*/
void hal_uart_register_tx_listener(hal_uart_listener_func_t listener);

/**
* Register UART RX listener function when transaction is completed.
* @param[in]  listener  Callback function pointer
* @return     NONE.
*/
void hal_uart_register_rx_listener(hal_uart_listener_func_t listener);
/** @}*/
#endif /* __HAL_UART_H */

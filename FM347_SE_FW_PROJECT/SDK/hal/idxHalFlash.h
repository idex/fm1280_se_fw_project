/** ***************************************************************************
 * \file   idxHalFlash.h
 * \brief  Interface to Hardware Abstraction Layer for flash
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_FLASH_H_
#define _IDX_HAL_FLASH_H_

#include <stdint.h>

// Pending sorting out error codes...
#ifndef IDX_ISO_SUCCESS
#define IDX_ISO_SUCCESS 0x9000
#endif

#ifndef IDX_ISO_ERR_BAD_PARAMETERS
#define IDX_ISO_ERR_BAD_PARAMETERS 0x6A80
#endif

#ifndef IDX_ERR_FLASH_ERASE
#define IDX_ERR_FLASH_ERASE        0x6501
#endif

#ifndef IDX_ERR_FLASH_WRITE
#define IDX_ERR_FLASH_WRITE        0x6502
#endif

/**
* @brief Function for erasing a contiguous block of data to Flash
*
* @param[in] pAddr is a pointer to the start of the Flash area to be erased
* @param[in] bytesToErase indicates the number of bytes to erase from to Flash
*
* @return IDEX_SUCCESS if successful,
*         or another error code if not successful
*/                          
uint16_t idxHalFlashErase(uint8_t *pAddr, 
                          uint32_t bytesToErase);
/**
* @brief Function for writing a contiguous block of data to Flash
*
* @param[in] pAddr is a pointer to the start of the Flash area to be written
* @param[in] pData is a pointer to the data to be written to Flash
* @param[in] bytesToWrite indicates the number of bytes to write to Flash
*
* @return IDEX_SUCCESS if successful,
*         or another error code if not successful
*/
uint16_t idxHalFlashWrite(uint8_t *pAddr, 
                          const uint8_t *pData, 
                          uint16_t bytesToWrite);


#endif /* _IDX_HAL_FLASH_H_ */

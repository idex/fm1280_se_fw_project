/*
* led.h
* Control Sleeve LED flashing by I2C interface.
*
*******************************************************************************
* Copyright
* 2013-2018 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#ifndef __LED_H_
#define __LED_H_

/*!
 * @brief  LED index on sleeve.
 */
typedef enum {
    LED_INDEX_UPPERTHUMB,
    LED_INDEX_LOWERTHUMB,
    LED_INDEX_TIP,
    LED_INDEX_FIRST_FINGER,
    LED_INDEX_SECOND_FINGER,
    LED_INDEX_ALL,
} LED_Index;

/*!
 * @brief  LED state.
 */
typedef enum {
    LED_OFF,
    LED_ON
} LED_State;

/**
* Initialize Sleeve LED. Config I2C interface and set LED brightness.
*
* @return     void.
*/
void SleeveLEDInit(void);

/**
* Toggle LED state. on -> off; off -> on.
*
* @param[in]  index      LED index number on sleeve.
*
* @return      void
*/
void SleeveLEDToggle(uint8_t index);

/**
* Set specified LED on or off flag.
*
* @param[in]  index      LED index number on sleeve.
* @param[in]  onOff      ON or OFF
*
* @return     void
*/
void SleeveLEDSet(LED_Index index, LED_State onOff);

/**
* Change LED state by writing general register, turn on or off.
*
* @return      void
*/
void SleeveLEDUpdate();

/**
* Update LED state according to enrolled finger number or touch sequence.
* Turn off LED to save power after timeout.
*
* @return      void
*/
void SleeveLEDHandler(void);

#endif /* __LED_H_ */

/*
* led.h
* Control Sleeve LED flashing by I2C interface.
*
*******************************************************************************
* Copyright
* 2013-2018 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#include <stdint.h>
#include "LED.h"
#include "I2C.h"

//I2C sleeve configuration
#define I2C_SLAVE_ADDRESS   0x36

//FAN5702 registers address
#define I2C_SLAVE_REGISTER_GENERAL    0x10
#define I2C_SLAVE_REGISTER_CONFIG     0x20
#define I2C_SLAVE_REGISTER_CHA        0xA0
#define I2C_SLAVE_REGISTER_CH3        0x30
#define I2C_SLAVE_REGISTER_CH4        0x40
#define I2C_SLAVE_REGISTER_CH5        0x50
#define I2C_SLAVE_REGISTER_CH6        0x60

//Set EN pin to 1 to function as a PWM dimming pin and define LED current level
#define LED_CURRENT_08_MA 0x7
#define LED_CURRENT_15_MA 0x6
#define LED_CURRENT_20_MA 0x4
#define LED_CURRENT_30_MA 0x5

//LED channels
#define CHANNEL_ENA 0x1
#define CHANNEL_EN3 0x2
#define CHANNEL_EN4 0x4
#define CHANNEL_EN5 0x8
#define CHANNEL_EN6 0x10

//LED dimming code, according to FAN5702 brightness control table.
//Biometric card may brownout if the brightness is higher than 10%.
#define DIMMING_CODE_FULL_BRIGHTNESS       0x3F
#define DIMMING_CODE_92_PERCENY_BRIGHTNESS 0x3E
#define DIMMING_CODE_80_PERCENY_BRIGHTNESS 0x3C
#define DIMMING_CODE_68_PERCENY_BRIGHTNESS 0x3A
#define DIMMING_CODE_58_PERCENY_BRIGHTNESS 0x38
#define DIMMING_CODE_50_PERCENY_BRIGHTNESS 0x36
#define DIMMING_CODE_40_PERCENY_BRIGHTNESS 0x34
#define DIMMING_CODE_30_PERCENY_BRIGHTNESS 0x31
#define DIMMING_CODE_20_PERCENY_BRIGHTNESS 0xED
#define DIMMING_CODE_10_PERCENY_BRIGHTNESS 0x27
#define DIMMING_CODE_08_PERCENY_BRIGHTNESS 0x12
#define DIMMING_CODE_04_PERCENY_BRIGHTNESS 0x1F
#define DIMMING_CODE_01_PERCENY_BRIGHTNESS 0x0E
#define DIMMING_CODE_COMPLETE_DARK         0xC0

/*!
 * @brief  LED state flag.
 */
LED_State LEDBuffer[6] = {0, 0, 0, 0, 0, 0};
uint8_t updateLED = 0;


//LED blink maxmium 3 mintues. depending on timer B interrupt frequency.
#define MAX_LED_BLINKING_TIMES 720
int timeoutCounter = 0;

uint8_t fingerIndex = 0;
uint8_t enrollFinished = 0;
uint8_t fingerNum = 1;


/*!
 * @brief Control LED flashing sequence. map flashing LED to each touch.
 *        To change LED flashing sequence, change this LED index array.
 */
uint8_t enrollPattern[8] = {
    LED_INDEX_LOWERTHUMB,
    LED_INDEX_TIP,
    LED_INDEX_LOWERTHUMB,
    LED_INDEX_TIP,
    LED_INDEX_LOWERTHUMB,
    LED_INDEX_TIP,
    LED_INDEX_LOWERTHUMB,
    LED_INDEX_TIP
};

/**
* Initialize Sleeve LED. Config I2C interface and set LED brightness.
*
* @return     void.
*/
void SleeveLEDInit(void)
{
    GPIO_I2C_Init();

    //Set LED brightness to 4% to avoid card brownout.
    GPIO_I2C_WriteData(I2C_SLAVE_ADDRESS, I2C_SLAVE_REGISTER_CH6, DIMMING_CODE_04_PERCENY_BRIGHTNESS);
    GPIO_I2C_WriteData(I2C_SLAVE_ADDRESS, I2C_SLAVE_REGISTER_CH5, DIMMING_CODE_04_PERCENY_BRIGHTNESS);
    GPIO_I2C_WriteData(I2C_SLAVE_ADDRESS, I2C_SLAVE_REGISTER_CH4, DIMMING_CODE_04_PERCENY_BRIGHTNESS);
    GPIO_I2C_WriteData(I2C_SLAVE_ADDRESS, I2C_SLAVE_REGISTER_CH3, DIMMING_CODE_04_PERCENY_BRIGHTNESS);
    GPIO_I2C_WriteData(I2C_SLAVE_ADDRESS, I2C_SLAVE_REGISTER_CHA, DIMMING_CODE_04_PERCENY_BRIGHTNESS);
}

/**
* Set specified LED on or off flag.
*
* @param[in]  index      LED index number on sleeve.
* @param[in]  onOff      ON or OFF
*
* @return     void
*/
void SleeveLEDSet(LED_Index index, LED_State onOff)
{
    if (index == LED_INDEX_ALL) {
        if (LEDBuffer[LED_INDEX_UPPERTHUMB] != onOff)
        {
            LEDBuffer[LED_INDEX_UPPERTHUMB] = onOff;
            updateLED = 1;
        }
        if (LEDBuffer[LED_INDEX_LOWERTHUMB] != onOff)
        {
            LEDBuffer[LED_INDEX_LOWERTHUMB] = onOff;
            updateLED = 1;
        }
        if (LEDBuffer[LED_INDEX_TIP] != onOff)
        {
            LEDBuffer[LED_INDEX_TIP] = onOff;
            updateLED = 1;
        }
        if (LEDBuffer[LED_INDEX_FIRST_FINGER] != onOff)
        {
            LEDBuffer[LED_INDEX_FIRST_FINGER] = onOff;
            updateLED = 1;
        }
        if (LEDBuffer[LED_INDEX_SECOND_FINGER] != onOff)
        {
            LEDBuffer[LED_INDEX_SECOND_FINGER] = onOff;
            updateLED = 1;
        }
    }
    else if (LEDBuffer[index] != onOff) {
        LEDBuffer[index] = onOff;
        updateLED = 1;
    }
}

/**
* Get specified LED state, on or off.
*
* @param[in]  index      LED index number on sleeve.
*
* @return      ON or OFF
*/
LED_Index SleeveLEDGet(LED_Index index)
{
    return LEDBuffer[index];
}

/**
* Change LED state by writing general register, turn on or off.
*
* @return      void
*/
void SleeveLEDUpdate()
{
    unsigned char ledStatus = 0;
    if (0 == updateLED)
        return;

    //Set LED current level to 15mA.
    ledStatus = ledStatus | (LED_CURRENT_15_MA << 5);
    if (LEDBuffer[LED_INDEX_FIRST_FINGER])
    {
        ledStatus = ledStatus|CHANNEL_EN3;
    }

    if(LEDBuffer[LED_INDEX_SECOND_FINGER])
    {
        ledStatus = ledStatus|CHANNEL_EN4;
    }

    if(LEDBuffer[LED_INDEX_UPPERTHUMB])
    {
        ledStatus = ledStatus|CHANNEL_ENA;
    }

    if(LEDBuffer[LED_INDEX_LOWERTHUMB])
    {
        ledStatus = ledStatus|CHANNEL_EN5;
    }

    if(LEDBuffer[LED_INDEX_TIP])
    {
        ledStatus = ledStatus|CHANNEL_EN6;
    }

    //Write General register to turn on/off LED.
    GPIO_I2C_WriteData(I2C_SLAVE_ADDRESS, I2C_SLAVE_REGISTER_GENERAL, ledStatus);

    updateLED = 0;
}

/**
* Toggle LED state. on -> off; off -> on.
*
* @param[in]  index      LED index number on sleeve.
*
* @return      void
*/
void SleeveLEDToggle(uint8_t index)
{
    LED_State onOff;

    onOff = SleeveLEDGet(index);
    SleeveLEDSet(index, !onOff);
}

/**
* Update LED state according to enrolled finger number or touch sequence.
* Turn off LED to save power after timeout.
*
* @return      void
*/
void SleeveLEDHandler(void) {
    timeoutCounter++;
    if (timeoutCounter > MAX_LED_BLINKING_TIMES)
    {
        // Timeout after idle 3 minutes. Close LED and enter low power mode.
        SleeveLEDSet(LED_INDEX_ALL, 0);
        SleeveLEDUpdate();
        return;
    }
    if (enrollFinished)//Toggle the First or Sencond finger LED
    {
        if (1 == fingerNum)
        {
            SleeveLEDToggle(LED_INDEX_FIRST_FINGER);
        }
        else if (2 == fingerNum)
        {
            SleeveLEDToggle(LED_INDEX_FIRST_FINGER);
            SleeveLEDToggle(LED_INDEX_SECOND_FINGER);
        }
    }
    else
    {
        //Toggle the indicator LED
       SleeveLEDToggle(enrollPattern[fingerIndex%8]);
    }

    SleeveLEDUpdate();
}

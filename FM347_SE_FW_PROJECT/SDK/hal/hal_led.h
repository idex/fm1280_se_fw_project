/**
 * @file         $filename$
 * @version      $version$
 * @commit       $id$
 * @date         $date$
 * @brief        header for customized GPIO control API defines, in case customer add a new platform.
 *               we expect customer to implement these APIs. These APIs are used by high
 *               level applications.
 * $license$
 *
 */

#ifndef __HAL_LED_H
#define __HAL_LED_H

#include <stdint.h>

//###########  LED Control Function Declarations    #############

uint16_t ledStart(void);
uint16_t led2ndFinger(void);
uint16_t ledTouch(uint8_t LedColor);
uint16_t ledLastTouch(void);
uint16_t ledStop(void);
uint16_t ledOff(void);
uint16_t ledBlink(void);
uint16_t ledOnOff(uint8_t LedColor);

#endif /* __HAL_LED_H */

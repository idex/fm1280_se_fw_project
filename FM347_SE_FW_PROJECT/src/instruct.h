#ifndef _INSTRUCT_H
#define _INSTRUCT_H

#include "types.h"
extern u16 sw;

//----------------------------------------------------------------------------

#define CLA_ISO7816            (0x00)

//----------------------------------------------------------------------------

#define INS_GET_INFO           (0x4F)

//----------------------------------------------------------------------------
#define set_error( sw1, sw2 ) sw = (sw1 << 8 | sw2) /*sw[0]=sw1;sw[1]=sw2; */

#define PARSE_ERR( x, y ) { \
        set_error( x, y ); \
        return; \
}

#define PARSE_ERR_1( x, y ) { \
        set_error( x, y ); \
        return 1; \
}



#define PARSE_OK return;


#endif//_INSTRUCT_H



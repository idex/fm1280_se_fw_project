#ifndef __MAIN_H__
#define __MAIN_H__


#include "FM_libproto.h"
#include "tools.h"
#include "hw.h"


#define sleeve_enrollment_support 1


#define MEM_SIZE 2576


#define debug_flash_start 0x00b0b200
#define debug_flash_end 0x00b0ee00
#define debug_flash_size 0x3c00
#define idex_used_flash_start 0x00b01000
#define idex_used_flash_end 0x00b11000
#define idex_template_flash_star_addr 0x00b01000

#define idex_template_flash_size 0xa000
#define idex_used_flash_size 0x10000
#define scp03_key_flash_addr 0x00b10000
#define scp03_key_flash_size 0x400


#define TA0_preset	0x00032000 //31ms
#define TA0_lessthan_4ms 0x0002bc00//28ms

#define SYSTEM_LOW_FREQ  (BIT9|BIT1)//|BIT0|BIT8)//0303 is the lowest 4Mhz
#define SYSTEM_DEFAULT_FREQ BIT12|BIT4//22Mhz
#define SYSTEM_HIGH_FREQ BIT13|BIT5|BIT11|BIT3|BIT9|BIT1|BIT8|BIT0
//#define SYSTEM_HIGH_FREQ BIT13|BIT5|BIT11|BIT3//|BIT9|BIT1|BIT8|BIT0


enum 
{
    IO_UNKNOWN,
    IO_CT,
    IO_CL,
    IO_SPI,
    IO_I2C,
    IO_UART,
    IO_SLEEVE_ENROLLMENT,
    
    IO_SHUTDOWN = 0xFD,
    IO_CL_DESELECT = 0xFE,
    IO_IDLE = 0xFF,
};


typedef struct 
{
    u8 curr_io;
    u8 state;          
	u8 auth_pass;
    u8 is_first_apdu;
	u8 rand[8];
    u8 prev_ins;
    //u8 need_antitear;
} cos_runtime_t;

#define int_longjmp(env, val) do {\
    interrupt_exit_before_longjump ();\
    longjmp ((env), (val));\
} while(0);



extern cos_runtime_t card_rte;

#define IO_IS_CL() (IO_CL == card_rte.curr_io)
#define IO_IS_CT() (IO_CT == card_rte.curr_io)

#define EXPECT_BODY()   do { \
    if (IO_IS_CT()) \
    { \
        if (! apdu_expect_body(&g_ct_ctx, apdu)) \
            return SW_COMMAND_BODY_EXPECTED; \
    } \
} while (0);



extern cl_ctx_t g_cl_ctx;
extern ct_ctx_t g_ct_ctx;

extern int_jmp_buf jmp_init;


#endif//__MAIN_H__


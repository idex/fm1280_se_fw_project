#include "nvm.h"
//#include "comm.h"
#include <string.h>

#define NVM_BIG_SECTOR_ACTIVE_FLAG BIT16

//u8 NVM_PAGE_BUF[NVM_PAGE_SIZE];

extern u32 nvm_type_check(u32 nvm_op_dest_addr);
extern u32 get_dataee_size(void);
extern u32 nvm_sector_erase(u8 nvm_type,u32 nvm_op_dest_addr) ;
extern u32 nvm_sector_program(u8 nvm_type,u32 nvm_op_sr_buf_addr, u32 nvm_op_dest_addr, u32 nvm_op_byte_length) ;
extern u32 nvm_wordcell_0_program(u8 nvm_type,u32 nvm_op_dest_addr,u32 nvm_op_byte_length) ;
extern u32 nvm_wordcell_check(u8 nvm_type, u32 nvm_op_dest_addr, u32 nvm_op_byte_length, u32* first_allone_word_addr_in_sector) ;



u8 nvm_check_value( u32 value, u8 retry )
{
    if ( !retry )
    {
        return (0);//PARSE_ERR_0( 0x65, 0x81 );
    }
    if ( value & 0xFF )
    {
        return (1);   /*error, retry! */
    }
    return (0);
}


void nvm_erase( u8 *addr )
{
    u32 rlt;
    u8	retries = FLASH_OPER_RETRIES;
    rlt = nvm_type_check( (u32) addr );
    do
    {
        rlt = nvm_sector_erase( (rlt & NVM_BIG_SECTOR_ACTIVE_FLAG) >> 16, (u32) addr );
    }
    while ( nvm_check_value( rlt, retries-- ) );
}


void nvm_write( u8 *dst, u8 *src, u16 len )
{
	u32 volatile state;
    u32 rlt;
    u8	retries = FLASH_OPER_RETRIES;
    rlt = nvm_type_check( (u32) dst );
    do
    {
        state = nvm_sector_program( (rlt & NVM_BIG_SECTOR_ACTIVE_FLAG) >> 16, (u32) src, (u32) dst, len );
		switch (state & 0x0f)
		{
		case 0:
			return;
		case 2:
			while(reg_clk_status1 & CL_VDD_LV_ALARM_FLAG);
			continue;
			
		case 5:
			if (0 != memcmp((void *)src, (void *)dst, len ))
				continue;
			else
				return;
		default:
			continue;
		}
    }
    while ( nvm_check_value( state, retries-- ) );
}

void Close_MMU_NMI(void)
{
	NVIC_ICER = DATANVM_CHECK_ERR_INT_BIT;
}

void Open_MMU_NMI(void)
{
	reg_mmu_err_flag &= ~(DATA_NVM_CORRECT_ERR_FLAG);
	NVIC_ICPR = BIT19;
	NVIC_ISER = DATANVM_CHECK_ERR_INT_BIT;
}
/*
void nvm_program_test( u8 *dst, u8 *src, u16 len )
{
    u16 i;
    u8	*start;
    u16 offset;
    start	= NVM_PAGE_HEADER_ADDR( dst );
    offset	= NVM_PAGE_OFFSET( dst );
    
    Close_MMU_NMI();
    for ( i = 0; i < NVM_PAGE_SIZE; i++ )
    {
        NVM_PAGE_BUF[i] = *start++;
    }
    for ( i = 0; i < len; i++ )
    {
        if ( i + offset >= NVM_PAGE_SIZE )
        {
            break;
        }
        NVM_PAGE_BUF[offset + i] = *src++;
    }

    Open_MMU_NMI();
        
    start	= NVM_PAGE_HEADER_ADDR( dst );
    nvm_erase( start );
    src = NVM_PAGE_BUF;
    nvm_write( start, src, NVM_PAGE_SIZE );
    //Open_MMU_NMI();
}
*/


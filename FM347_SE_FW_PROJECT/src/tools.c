#include "tools.h"
#include "options.h"

/* EABI unaligned read/write functions
 * @see unaligned-funcs.c __aeabi_uread4
 */
struct __attribute__((packed)) u32_s { u32 data; };
struct __attribute__((packed)) u16_s { u16 data; };

u32 uread4 (void *ptr)
{
	/*u32 tmp;
	u8 *start = (u8 *)ptr;
	tmp = start[0]<<24 | start[1]<<16 | start[2]<<8 | start[3];
	return tmp;
    */
    return ((struct u32_s *) ptr)->data;
}

u32 uwrite4 (void *ptr, u32 data)
{
	/*u8 *dst = (u8 *)ptr;
	dst[0] = (data >> 24);
	dst[1] = (data >> 16) & 0xFF;
	dst[2] = (data >> 8) & 0xFF;
	dst[3] = (data & 0xFF);
    */
    ((struct  u32_s *) ptr)->data = data;
    return data;
}

u16 uread2 (void *ptr)
{
    return ((struct u16_s *) ptr)->data;
}

u16 uwrite2 (void *ptr, u16 data)
{
    ((struct u16_s *) ptr)->data = data;
    return data;
}

///////////////////////////////////////////////////////////////////////////////

#if _configLIBPROTO_IN_FIRMWARE_

void *mem_set (void *dstpp, u8 c, u32 len) 
{  
    u8 *dstp = (u8 *)dstpp;
    
    while(len--)
    {
        *dstp++ = c;
    } 
    
    return dstpp;
}  

void *mem_cpy (void *dstpp, const void *srcpp, u32 len)
{
    u8 *dst = dstpp;
    const u8 *src = srcpp;
    
    while(len--)
    {
        *dst++ = *src++;
    } 
    
    return dstpp;
}

#endif





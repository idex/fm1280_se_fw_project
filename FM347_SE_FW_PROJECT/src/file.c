
#include <string.h>
#include "main.h"               /*主程序头文件 */
#include "file.h"
#include "instruct.h"
//#include "comm.h"
//#include "tool.h"

#include "FM_DriverLib.h"



u32 eeprom_size;

u8 cl_tr_buf[288];

u8  logic_value( u8 value )
{
    return ( (value + (u16) 0xff) >> 8);
}


u8  Cmp_dat_n( u8 *dst, u8 *src, u16 len )          /*比较 */
{
    if ( len )
    {
        return (logic_value( memcmp( dst, src, len ) ) );
    }
    return (0);
}


u8  readByte( u8 *adr )
{
    return (*adr);
}


void Copy_dat( HU8 *dst, HU8 *src, u16 len )    /*复制 */
{
    u16 i;
    HU8 *tmp	= dst;
    HU8 *s		= src;
    /*增加重叠处理 */
    if ( dst < src )
    {
        while ( len-- )
        {
            *tmp++ = *s++;
        }
    }
    else
    {
        for ( i = len; i > 0; i-- )
        {
            *(tmp + i - 1) = *(s + i - 1);
        }
    }
}


void Set_dat_i( iu8 *dst, u16 len, u8 value )   /*设置 */
{
    memset( dst, value, len );
}


u8 chk_sum( hu8 *buf, u8 len )                  /*校验和 */
{
    u8 s = 0;
    while ( len )
    {
        len--;
        s ^= buf[0];
        buf++;
    }
    return (s);
}


u8 crc8( hu8 *buf, u8 len )         /*CRC8 */
{
    u8	crc;
    u32 val = 0;
    crc_cal( 0, (u32) buf, len, &val );
    crc = (u8) (val & 0x000000FF);
    return (crc);
}


void Xor_dat_n( u8 *dst, u8 *src, u8 len )          /*异或 */

{
    while ( len )
    {
        len--;
        dst[len] ^= src[len];
    }
}


void Xor_dat( u8 *dst, hu8 *src, u8 len )        /*异或 */
{
    while ( len )
    {
        len--;
        dst[len] ^= src[len];
    }
}


/*转换函数:两字节数据变成一个U16 */
u16 to_u16( u8 *start )
{
    u16 tmp;
    tmp = start[0];
    tmp = tmp << 8 | start[1];
    return (tmp);
}


/*转换函数:四字节数据变成一个U32 */
u32 to_u32( u8 *start )
{
    u32 tmp;
    tmp = start[0] << 24 | start[1] << 16 | start[2] << 8 | start[3];
    return (tmp);
}


/*U16变成两字节 */
void set_u8_from_u16( u8 *dst, u16 src )
{
    dst[0]	= src >> 8;
    dst[1]	= src & 0xFF;
}


/*U32变成四字节 */
void set_u8_from_u32( u8 *dst, u32 src )
{
    dst[0]	= (src >> 24);
    dst[1]	= (src >> 16) & 0xFF;
    dst[2]	= (src >> 8) & 0xFF;
    dst[3]	= (src & 0xFF);
}


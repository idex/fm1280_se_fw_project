#include "main.h"
#include "hw.h"
#include "FM_libproto.h"
#include "options.h"
#include "io.h"
#include "idxSeBioAPI.h"
#include "serialinterface.h"

cos_runtime_t card_rte;
int_jmp_buf jmp_init;
u16 sw;
extern BaseSerialInterface idxCom;

#ifdef sleeve_enrollment_support
extern vu8	sleeve_enable_flag;
extern vu32 Idex_free_timer_counter;
extern vu8 process_in_waiting;
extern vu8 blinkCtrl;
extern void fm1280_sleeve_enrollment(void);
extern void idex_sleeve_timer_start(void);
extern void idex_sleeve_timer_stop(void);

#endif
extern void idxHalTimerDelayMsec(uint16_t msec);
extern void scp03_related_data_initial(void);
extern void gpio_initial(void);
extern void idxcom_initial(void);
extern void Init_SystemConfig(void);
extern void idx_system_data_initial(void);
extern void program_tracker(u8 *data_buff, u8 data_len);
void card_rte_init (void)
{
    u8 st = 0;
    
    hw_init ();
    
    card_rte.prev_ins = 0x00;
    card_rte.is_first_apdu = 1;
    
    hw_config_get (st);
}


int main (void)
{  
    u8 jmp;

idx_system_data_initial();
scp03_related_data_initial();
idxcom_initial();
Init_SystemConfig();
idxCom.reset(); 
gpio_initial();


#ifdef sleeve_enrollment_support
  sleeve_enable_flag=1;
  Idex_free_timer_counter=0;
  if(reg_clk_status1&CL_CLK_ON_FLAG)
	  {
	  sleeve_enable_flag=0;
	  idex_sleeve_timer_stop();
	  }
  else
	  {
	  idex_sleeve_timer_start();
	  }

#endif

card_rte_init ();
hw_cos_flag = 0;
jmp = int_setjmp (jmp_init);



hw_startup (jmp);
card_rte.curr_io = IO_UNKNOWN;

 while (1)
    {           
        if (g_ct_ctx.activated)
        {
            card_rte.curr_io = IO_CT;
            ct_process (&g_ct_ctx);
            ct_respond (&g_ct_ctx);
        }

        if (g_cl_ctx.activated)
		{ 
            card_rte.curr_io = IO_CL;
            cl_process (&g_cl_ctx);
            cl_respond (&g_cl_ctx);
        }
        card_rte.curr_io = IO_IDLE;
	SLEEP();
    }  
}





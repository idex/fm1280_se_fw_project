#ifndef _TOOLS_H
#define _TOOLS_H

#include "types.h"



#ifndef BIT0
    #define BIT0            (((u32)1) << 0x00)
    #define BIT1            (((u32)1) << 0x01)
    #define BIT2            (((u32)1) << 0x02)
    #define BIT3            (((u32)1) << 0x03)
    #define BIT4            (((u32)1) << 0x04)
    #define BIT5            (((u32)1) << 0x05)
    #define BIT6            (((u32)1) << 0x06)
    #define BIT7            (((u32)1) << 0x07)
    #define BIT8            (((u32)1) << 0x08)
    #define BIT9            (((u32)1) << 0x09)
    #define BIT10           (((u32)1) << 0x0A)
    #define BIT11           (((u32)1) << 0x0B)
    #define BIT12           (((u32)1) << 0x0C)
    #define BIT13           (((u32)1) << 0x0D)
    #define BIT14           (((u32)1) << 0x0E)
    #define BIT15           (((u32)1) << 0x0F)
    #define BIT16           (((u32)1) << 0x10)
    #define BIT17           (((u32)1) << 0x11)
    #define BIT18           (((u32)1) << 0x12)
    #define BIT19           (((u32)1) << 0x13)
    #define BIT20           (((u32)1) << 0x14)
    #define BIT21           (((u32)1) << 0x15)
    #define BIT22           (((u32)1) << 0x16)
    #define BIT23           (((u32)1) << 0x17)
    #define BIT24           (((u32)1) << 0x18)
    #define BIT25           (((u32)1) << 0x19)
    #define BIT26           (((u32)1) << 0x1A)
    #define BIT27           (((u32)1) << 0x1B)
    #define BIT28           (((u32)1) << 0x1C)
    #define BIT29           (((u32)1) << 0x1D)
    #define BIT30           (((u32)1) << 0x1E)
    #define BIT31           (((u32)1) << 0x1F)
#endif

#define REG32(addr)     (*(( volatile unsigned long *)(addr)))
#define REG16(addr)     (*(( volatile unsigned short*)(addr)))
#define REG8(addr)      (*(( volatile unsigned char *)(addr)))


#define MAKEWORD(hi, lo)  ((u16)((((u16)((u8)(hi))) << 8) | ((u8)(lo))))


#define LOWORD(l)        ((u16)(l))
#define HIWORD(l)        ((u16)(((u32)(l) >> 16) & 0xFFFF))
#define LOBYTE(w)        ((u8)(w))
#define HIBYTE(w)        ((u8)(((u16)(w) >> 8) & 0xFF))

                              

#ifndef MAX 
#define MAX(x, y) (((x) > (y)) ? (x) : (y)) 
#endif 
#ifndef MIN 
#define MIN(x, y) (((x) < (y)) ? (x) : (y)) 
#endif 



u32 uread4 (void *ptr);
u32 uwrite4 (void *ptr, u32 data);
u16 uread2 (void *ptr);
u16 uwrite2 (void *ptr, u16 data); 


int   mem_cmp(const void *s1, const void *s2, u32 n);
void *mem_set (void *dstpp, u8 c, u32 len);
void *mem_cpy (void *dstpp, const void *srcpp, u32 len);
                              

#endif//_TOOLS_H

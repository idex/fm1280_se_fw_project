#include "io.h"
#include "hw.h"

ct_ctx_t g_ct_ctx;
cl_ctx_t g_cl_ctx;

/*-----------------------------------------------------------------------------*/

void io_protocol_init (io_config_block_t *io)
{
    /* init ct context */
    g_ct_ctx.ct_cfg = &io->ct_cfg;
    g_ct_ctx.ct_ops = &ct_ops;
    g_ct_ctx.apdu_dispatch = (ct_dispatch_t)&apdu_dispatch;

    g_ct_ctx.tx_buf_size = MAX_CT_RX_BUF_SIZE;//MAX_CT_TX_BUF_SIZE;
    g_ct_ctx.tx_buf_p = g_ct_tx_buf;
    g_ct_ctx.rx_buf_p = g_ct_rx_buf;
    g_ct_ctx.rx_len_p = &g_ct_rx_count;

    ct_init (&g_ct_ctx);

    /* init cl context */
    g_cl_ctx.cl_cfg = &io->cl_cfg;
    g_cl_ctx.cl_ops = &cl_ops;
    g_cl_ctx.apdu_dispatch = (cl_dispatch_t)&apdu_dispatch;

    g_cl_ctx.tx_buf_size = MAX_CLA_RX_BUF_SIZE;//MAX_CLA_TX_BUF_SIZE
    g_cl_ctx.tx_buf_p = g_cl_tx_buf;
    g_cl_ctx.rx_buf_p = g_cl_rx_buf;
    g_cl_ctx.rx_len_p = &g_cl_rx_count;

    cl_init (&g_cl_ctx);

}

void io_config_init (io_config_block_t *io, const u8 *atr, const u8 *ats)
{
    mem_set ((u8 *)io, 0, IO_CFG_BLOCK_SIZE);

    uwrite4 (io->is_valid, IO_CFG_VALID);

    ////////////////////////////////////////////////////////////////////////
    // init ct config

    io->ct_en = IO_DEFAULT;

    io->ct_cfg.ct_atr_len = 9;//12;
    mem_cpy (io->ct_cfg.ct_atr,
             //     TS  T0  TA1 TB1 TC1
             (u8 *)"\x3B\x74\x18\x00\x00\x90\xAA\xBB\x00",
             //(u8 *)"\x3B\x68\x00\x00\x11\x22\x33\x44\x55\x66\x77\x88",
             io->ct_cfg.ct_atr_len);

    io->ct_cfg.ct_atr_baud = ISO7816_BAUD_9600;
    io->ct_cfg.ct_tx_egt_num = _configCT_TX_EGT_NUM_;
    io->ct_cfg.ct_tx_etu_cnt = _configCT_TX_ETU_CNT_;

    uwrite4 (io->ct_cfg.ct_t0_init_cfg, _configCT_T0_INIT_CFG_);
    io->ct_cfg.ct_t0_async = _configCT_T0_ASYNC_;

    ////////////////////////////////////////////////////////////////////////
    // init cl config

    io->cl_en = IO_DEFAULT;

    io->cl_init_cfg = 0;


    io->cl_cfg.cl_ats_len = 9;

    mem_cpy (io->cl_cfg.cl_ats,
             //     TL  T0  TA1 TB1 TC1
             (u8 *)"\x09\x78\x80\x70\x02\x90\xAA\xBB\x00",
             io->cl_cfg.cl_ats_len);

    //"\x10\x78\x80\xB0\x02\x20\x90\x02\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF", 0x10
    io->cl_cfg.cl_ats_tb1 = 0xC0;
    io->cl_cfg.cl_tcl_pcb_chk = _configCL_TCL_CHK_SET_;

    /* S-PARAMETERS tags value*/
    io->cl_cfg.tag80 = _configS_PARAMETERS_TAG80_;
    io->cl_cfg.tag81 = _configS_PARAMETERS_TAG81_;
    io->cl_cfg.tag82 = _configS_PARAMETERS_TAG82_;
}

#ifndef _IO_H
#define _IO_H

#include "options.h"
#include "fm_proto_config.h"
#include "FM_libproto.h"



/////////////////////////////////////////////////////////////////////
/* IO CONFIG BLOCK */

#define IO_CFG_VALID             (0xA569965A)

enum _io_en_e {
    /* IO_DEFAULT:
     *    the corresponding IO is enable, and all configures are valid.
     *    but the last byte of ATR or ATS will be replaced by status
     */
    IO_DEFAULT = 0,
    /* IO_ENABLE:
     *    the corresponding IO is enable, and all configures are valid.
     */
    IO_ENABLE  = 0xA5,
    /* IO_DISABLE:
     *    the corresponding IO is disable.
     */
    IO_DISABLE = 0x5A,
};

typedef struct _ee_io_config_block_s
{
    u8 is_valid[4];
    u8 cl_en;   //see enum _io_en_e
    u8 ct_en;   //see enum _io_en_e

    u8 rfu;

    cfg_ct_t ct_cfg;   //43B

    cfg_cl_t cl_cfg;   //36B
    u8 cl_init_cfg;
    u8 cl_uid[10];
    u8 cl_sak[3];
    u8 cl_atqa[2];

/* Total: 102B */
} io_config_block_t;

#define IO_CFG_BLOCK_SIZE           (sizeof(io_config_block_t))

//u8   io_config_set (void);
void io_config_init (io_config_block_t *io, const u8 *atr, const u8 *ats);

/////////////////////////////////////////////////////////////////////

extern ct_ctx_t g_ct_ctx;
extern cl_ctx_t g_cl_ctx;

void io_protocol_init (io_config_block_t *io);

#endif//_IO_H

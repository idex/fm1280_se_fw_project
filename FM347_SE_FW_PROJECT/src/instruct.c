#include "instruct.h"
#include "main.h"
#include "hw.h"
#include "FM_libproto.h"
#include "file.h"
#include "idxSeEmkDefines.h"
#include "serialinterface.h"


typedef struct  _IDX_info
{ 
u8 *apdu; 
u8 *dat;
}IDX_info;


vu8 idex_wtx_flag;

extern BaseSerialInterface idxCom;
extern hostif_State SerialState;
extern u8 *apdu_handler( void *arg );
extern u16 idxHalFlashWrite(uint8_t *pAddr,     const uint8_t *pData,      uint16_t bytesToWrite);
extern void ussleep(u32 count);
u16 apdu_get_challenge (u8 *buf, u8 len)
{
    u8 ret;
    
    ret = RNG_GetRandNum (buf, len);
    return (RESULT_OK == ret) ? SW_SUCCESS : SW_CONDITION_NOT_SATISFIED;
}

u16 get_info (apdu_t *apdu)
{
    u16 i = 0;
 
    JNE(apdu->p1, 0x00, SW_INCORRECT_P1P2); //Os: 
    JNE(apdu->p2, 0x00, SW_INCORRECT_P1P2); //Os: 

    i += hw_get_info (&apdu->resp[i]);

    Le_ASSERT (IO_IS_CT(), i);
    
	apdu->resplen = i;
    apdu->p3 = i;
    apdu->le = apdu->p3;
 
    return SW_SUCCESS;
}

u16 iso7816_dispatch (apdu_t *apdu)
{
	u16 ret = SW_INS_NOT_SUPPORTED; //0x6D00
	IDX_info info;
	u8 header_size;
	u8 data_size;
	u32 test_data;
	u32 test_data2;
	u8 reg_buffer[6];

    switch (apdu->ins)
    {
        case INS_ENVELOPE:
            if (apdu->p1 || apdu->p2)
                return SW_INCORRECT_P1P2;  //0x6A86

            EXPECT_BODY();
            
            if (IO_IS_CL())
                return SW_INS_NOT_SUPPORTED;

            return apdu_envelope (apdu, (INS_ENVELOPE != card_rte.prev_ins));
            
        case INS_GET_RESPONSE:          
            return apdu_get_response (apdu);
            
        case INS_GET_CHALLENGE:
            ret = apdu_get_challenge (apdu->resp, apdu->p3);
            SW_ASSERT(ret);

            apdu->resplen = apdu->p3;
            apdu->le = apdu->resplen;
            break;
       	case 0x00:
		return_boot();
		apdu->le = 0;
		break;
	case 0x01:
		if((0!=apdu->p3)&& (card_rte.curr_io == IO_CT))EXPECT_BODY();
		test_data=0;
		test_data += (((uint32_t)(apdu->body[0])<<24)&0xff000000);
		test_data += (((uint32_t)(apdu->body[1])<<16)&0x00ff0000);
		test_data += (((uint32_t)(apdu->body[2])<<8)&0x0000ff00);
		test_data += (((uint32_t)(apdu->body[3])<<0)&0x000000ff);
		test_data2= *(uint32_t *)test_data;
		reg_buffer[0]=(uint8_t)((test_data2&0xff000000)>>24);
		reg_buffer[1]=(uint8_t)((test_data2&0x00ff0000)>>16);
		reg_buffer[2]=(uint8_t)((test_data2&0x0000ff00)>>8);
		reg_buffer[3]=(uint8_t)((test_data2&0x000000ff)>>0);

		Copy_dat(apdu->resp,reg_buffer,4);
		apdu->resplen=6;

		if(card_rte.curr_io == IO_CT)
		{
			if(apdu->p3 < apdu->resplen)
			{
				return (0x6100|apdu->resplen);
			}
		}
		apdu->le=6;
		break;
	case 0x54:

		if((0!=apdu->p3)&& (card_rte.curr_io == IO_CT))EXPECT_BODY();
		idex_wtx_flag=1;


		info.apdu=(u8 *)&apdu->cla;
		info.dat=apdu->body;

		idxCom.reset(); 
		apdu_handler(&info);
		header_size=(u8)((idxCom.state->ReplyHeader->wSize &0xff00)>>8);
		Copy_dat(apdu->resp,(HU8 *)idxCom.state->ReplyHeader,HDR_SIZE);
		if(header_size>HDR_SIZE)
			{
			data_size=header_size-HDR_SIZE;
			Copy_dat(apdu->resp+HDR_SIZE,idxCom.state->ReplyData,data_size);
			}
		apdu->resplen=header_size;

		if(card_rte.curr_io == IO_CT)
		{
			if(apdu->p3 < apdu->resplen)
			{
				return (0x6100|apdu->resplen);
			}
		}
		apdu->le=header_size;
		idex_wtx_flag=0;
		break;
        case INS_GET_INFO:
            return get_info (apdu);

        default:
        
            return ret; 
  
    }
    
    return SW_SUCCESS;
}
 
u16 apdu_dispatch (apdu_t *apdu)
{
    u16 ret;
    u8 tmp;
    
    if (card_rte.is_first_apdu)
    {
        card_rte.is_first_apdu = false;
        
        RNG_StartUpTest ();
    }
    		
    if (IO_IS_CL())
    {
        if (apdu->apdulen < ISO7816_TPDU_COMMAND_HEADER_LEN)
            return SW_WRONG_LENGTH;
        
        JNE (apdu->bodylen, apdu->lc, SW_WRONG_LENGTH)
    }
    
    if (INS_GET_RESPONSE == apdu->ins)
    {
        //8301 send: 40 C0 00 00 00
        apdu->cla = CLA_ISO7816;
    }

    while (1)
    {
        ret = SW_INS_NOT_SUPPORTED;
        
        switch (apdu->cla)
        {
            case CLA_ISO7816:
                tmp = apdu->ins;
            
                ret = iso7816_dispatch (apdu);
  
                if (INS_ENVELOPE == tmp)
                {
                    if (SW_SUCCESS == ret) 
                    {
                        continue;
                    }
                    else if (SW_COMMAND_BODY_EXPECTED == ret)
                    {
                        ret = SW_SUCCESS;
                    }
                }
                
                break;
            
            default:
                break;
        }

        card_rte.prev_ins = apdu->ins;
        
        return ret;
        
    } //while(1)
}

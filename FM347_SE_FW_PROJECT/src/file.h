#ifndef _FILE_INCLUDED_
#define _FILE_INCLUDED_

#include "types.h"
#include "main.h"
#define HU8 u8
#define iu8 u8
#define hu8 u8


//#define cl_ram_buf	cl_tr_buf
//#define RAM_TMP_BUF ( (u8 *) (cl_ram_buf + 5) )         /*防止硬件WTX冲数据 */

//#define CL_BUF_SIZE CL_RAM_BUF_SIZE
//extern u8 cl_tr_buf[];

extern void Copy_dat( HU8 *dst, HU8 *src, u16 len );    /*复制 */


#define Copy_dat_i2		Copy_dat
#define Copy_dat_n_i	Copy_dat
#define Copy_dat_n_x2	Copy_dat
#define Copy_dat_i_n	Copy_dat
#define Copy_dat_n		Copy_dat
#define Copy_dat_n_f	Copy_dat
#define Copy_dat_i		Copy_dat
#define Copy_dat_i_x	Copy_dat
#define Copy_dat_n_c	Copy_dat
#define Copy_dat_i_c	Copy_dat

#define Write_ee_n	nvm_copy
#define Write_ee_i	nvm_copy
#define Fill_ee		nvm_fill


extern u8 logic_value( u8 value );


extern u8 Cmp_dat_n( u8 *dst, u8 *src, u16 len );       /*比较 */


#define Cmp_dat		Cmp_dat_n
#define Cmp_dat_i	Cmp_dat_n
#define Cmp_dat_i_x Cmp_dat_n
#define Cmp_dat_i_n Cmp_dat_n


extern void Set_dat_i( iu8 *dst, u16 len, u8 value );   /*设置 */


#define Set_dat_n	Set_dat_i
#define Set_dat_x	Set_dat_i

extern u8 chk_sum( hu8 *buf, u8 len );                  /*校验和 */


#define     chk_sum_n	chk_sum
#define     chk_sum_i	chk_sum
#define     chk_sum_x	chk_sum

#define eeprom_bad()				longjmp( main_loop, -1 )
#define back_to_main_no_writeee()	longjmp( main_loop, 1 )
#define back_to_main()				longjmp( main_loop, 2 )
extern u8 crc8( hu8 *buf, u8 len );


extern void set_u8_from_u16( u8 *dst, u16 src );


extern void set_u8_from_u32( u8 *dst, u32 src );


extern u16 to_u16( u8 *start );


extern u32 to_u32( u8 *start );


#endif

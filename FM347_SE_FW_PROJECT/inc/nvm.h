#ifndef _NVM_H
#define _NVM_H

#include "types.h"
#include "instruct.h"
#include "FM_DriverLib.h"
#include "hw.h"


#define NVM_PAGE_SIZE	(256)
#define MAX_NVM_SIZE	(512 * 1024)
#define MAX_DATA_SIZE	(160 * 1024)
#define NVM_PAGE_HEADER_ADDR( addr )	( (u8 *) ( ( (u32) (addr) ) & (0xffffff00) ) )
#define NVM_PAGE_OFFSET( addr )			( (u16) ( ( (u32) (addr) ) & (0x000000ff) ) )

#define FLASH_OPER_RETRIES 3

extern void nvm_program_test( u8 *dst, u8 *src, u16 len );
void Close_MMU_NMI(void);
void Open_MMU_NMI(void);



#endif/*_NVM_H */

#ifndef _FM_PROTO_CONFIG_H_
#define _FM_PROTO_CONFIG_H_

#include "fm_proto_types.h"

///////////////////////////////////////////////////////////////////////////////

#define ATR_MAX_SIZE  33

typedef struct _ee_ct_cfg_s
{
    u8 ct_atr_len;              //ATR Length
    u8 ct_atr[ATR_MAX_SIZE];    //ATR：3B/3F T0 TA1 TB1...
    u8 ct_atr_baud;             //CT ATR init baudrate
    
    u8 rfu1;
    
    /* GT默认为12etu，为防止有些卡机高速接收有问题，ct_tx_egt_num 可以额外增加
     * etu。但是，为了满足反向间隔时间，最后一个byte不增加。
     */
    u8 ct_tx_egt_num;   
    
    /* 额外增加的反向间隔时间，比如：emvco要求反向间隔时间为16etu。*/
    u8 ct_tx_etu_cnt;
    
    /* T0初始化时，硬件参数配置 */
    u8 ct_t0_init_cfg[4];
    u8 ct_t0_async;  
 
} cfg_ct_t;

///////////////////////////////////////////////////////////////////////////////

#define ATS_MAX_SIZE  33

enum tcl_pcb_chk {
    TCL_CHK_SBLOCK_BIT1_0   = 0x01, //<o2.0> Check b1=0 of PCB in S-Block (EMVco ignore)
    TCL_CHK_RBLOCK_BIT2_1   = 0x02, //<o2.1> Check b2=1 of PCB in R-Block
    TCL_CHK_IBLOCK_BIT6_0   = 0x04, //<o2.2> Check b6=0 of PCB in I-Block 
    TCL_CHK_DESELECT_BIT2_1 = 0x80, //<o2.7> Check Deselect=0xC2. If NOT, 0xC0 is supported.
};

typedef struct _ee_cl_cfg_s
{
    u8 cl_ats_len;              //ATS Length
    u8 cl_ats[ATS_MAX_SIZE];    //ATR：TL T0 TA1 TB1... 
    u8 cl_ats_tb1;
    
    u8 cl_tcl_pcb_chk;              //@see tcl_pcb_chk
    
    /* S-PARAMETERS tags value*/
    u8 tag80;
    u8 tag81;
    u8 tag82;
    
} cfg_cl_t;

///////////////////////////////////////////////////////////////////////////////

// <<< Use Configuration Wizard in Context Menu >>> 

//<<< end of configuration section >>>


#endif//_FM_PROTO_CONFIG_H_

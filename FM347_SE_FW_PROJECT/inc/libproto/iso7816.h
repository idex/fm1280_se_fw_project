#ifndef _ISO7816_H
#define _ISO7816_H

/* various maximum values */
#define ISO7816_MAX_ATR_SIZE			    33
#define ISO7816_MAX_NULLS                   250
#define ISO7816_TPDU_COMMAND_HEADER_LEN     5

///////////////////////////////////////////////////////////////////

#define ISO7816_SHORT_C_APDU_HEADER_LEN     4 /* CLA INS P1 P2 */
#define ISO7816_SHORT_C_APDU_MAX_DATA_LEN   255
#define ISO7816_SHORT_C_APDU_MAX_OVERHEAD   2 /* Lc Le */

#define ISO7816_SHORT_R_APDU_TRAILER_LEN    2 /* SW */
#define ISO7816_SHORT_R_APDU_MAX_DATA_LEN   256

/* CLA INS P1 P2 Lc [255 bytes of data] Le */
#define ISO7816_SHORT_C_APDU_MAX_LEN ( ISO7816_SHORT_C_APDU_HEADER_LEN + ISO7816_SHORT_C_APDU_MAX_DATA_LEN + ISO7816_SHORT_C_APDU_MAX_OVERHEAD )
/* CLA INS P1 P2 */
#define ISO7816_SHORT_C_APDU_MIN_LEN        4

/* [256 bytes of data] SW */
#define ISO7816_SHORT_R_APDU_MAX_LEN ( ISO7816_SHORT_R_APDU_MAX_DATA_LEN + ISO7816_SHORT_R_APDU_TRAILER_LEN )


///////////////////////////////////////////////////////////////////

#define ISO7816_EXT_C_APDU_HEADER_LEN       4 /* CLA INS P1 P2 */
#define ISO7816_EXT_C_APDU_MAX_DATA_LEN     65535
#define ISO7816_EXT_C_APDU_MAX_OVERHEAD     6 /* Lc(C5C6C7) Le(Cn-1Cn) */

#define ISO7816_EXT_R_APDU_MAX_DATA_LEN     65536
#define ISO7816_EXT_R_APDU_TRAILER_LEN      2 /* SW */

/* CLA INS P1 P2 C5C6C7 [65535 bytes of data] Cn-1Cn */
#define ISO7816_EXT_C_APDU_MAX_LEN ( ISO7816_EXT_C_APDU_HEADER_LEN + ISO7816_EXT_C_APDU_MAX_DATA_LEN + ISO7816_EXT_C_APDU_MAX_OVERHEAD )
/* CLA INS P1 P2 C5C6C7 */
#define ISO7816_EXT_C_APDU_MIN_LEN          7

/* [65536 bytes of data] SW */
#define ISO7816_EXT_R_APDU_MAX_LEN ( ISO7816_EXT_R_APDU_MAX_DATA_LEN + ISO7816_EXT_R_APDU_TRAILER_LEN )



/* 7816 baudrate */
#define	ISO7816_BAUD_9600		0x11
#define	ISO7816_BAUD_19200		0x12
#define	ISO7816_BAUD_38400		0x13
#define	ISO7816_BAUD_76800		0x14
#define	ISO7816_BAUD_57600		0x38
#define	ISO7816_BAUD_115200		0x18
#define	ISO7816_BAUD_116000		0x95
#define	ISO7816_BAUD_07000		0x91
#define	ISO7816_BAUD_14000		0x92
#define	ISO7816_BAUD_28000		0x93
#define	ISO7816_BAUD_56000		0x94
#define	ISO7816_BAUD_232000		0x96


#define ISO7816_PPS_PPSS	    0xFF
#define ISO7816_PPS_PPS1_MASK   0x10
#define ISO7816_PPS_PPS2_MASK   0x20
#define ISO7816_PPS_PPS3_MASK   0x40


/* 7816 Status Words */
enum iso7816_errors {
    SW_CORRUPTED_DATA            = 0x6281,  //"Part of returned data may be corrupted"
	SW_FILE_END_REACHED			 = 0x6282,	//"End of file/record reached before reading Le bytes"
    SW_SELECTED_FILE_INVALID     = 0x6283,  //"Selected file invalidated"
    SW_FCI_NOT_FORMATTED         = 0x6283,  //"FCI not formatted according to ISO 7816-4"

	SW_VERIFCATION_FAILED		 = 0x6300,	//"Warning: no information given, non-volatile memory has changed"
	SW_FILE_FILLED				 = 0x6381,	//"Warning: file filled up by last write"
	SW_COUNTER					 = 0x63C0,  //

	SW_MEMORY_FAILURE   		 = 0x6581,  //"Memory failure"

	SW_WRONG_LENGTH				 = 0x6700,  //"CLA INS P1 P2 supported, but P3 incorrect"

    SW_FUNCTION_NOT_SUPPORT      = 0x6800,  //"Functions in CLA not supported"
    SW_LOGICAL_CHANNEL_NOT_SUPPORT = 0x6881, //"Logical channel not supported"

    //0x6900, SW_NOT_ALLOWED,		"Command not allowed"
	SW_INCOMPATIBLE_FILE		 = 0x6981,  //"Command incompatible with file structure"
	SW_SECURITY_STATUS_NOT_SATISFIED = 0x6982,  //"Security status not satisfied"
	SW_AUTH_METHOD_BLOCKED		 = 0x6983,  //"Authentication method blocked"
	SW_REF_DATA_NOT_USABLE		 = 0x6984,  //"Referenced data not usable"
	SW_CONDITION_NOT_SATISFIED	 = 0x6985,  //"Conditions of use not satisfied"
	SW_NOT_ALLOWED				 = 0x6986,  //"Command not allowed (no current EF)"
    SW_SECURITY_DATA_INCORRECT   = 0x6988, 
    
    //0x6A00, SW_INCORRECT_PARAMETERS,"Wrong parameter(s) P1-P2"
	SW_INCORERECT_PARAM_IN_DATA	 = 0x6A80,  //"Incorrect parameters in the data field"
	SW_NO_CARD_SUPPORTED		 = 0x6A81,	//"Function not supported"
	SW_FILE_NOT_FOUND			 = 0x6A82,  //"File not found"
	SW_RECORD_NOT_FOUND			 = 0x6A83,  //"Record not found"
	SW_NOT_ENOUGH_MEMORY		 = 0x6A84,  //"Not enough memory space in the file"
    SW_LC_INCONSISTED_WITH_TLV   = 0x6A85,  //"Lc inconsistent with TLV structure"
	SW_INCORRECT_P1P2			 = 0x6A86,  //"Incorrect parameters P1-P2"
	SW_LC_INCONSISTEND_WITH_P1P2 = 0x6A87,	//"Lc inconsistent with P1-P2"
	SW_DATA_OBJECT_NOT_FOUND	 = 0x6A88,  //"Referenced data not found"
	SW_FILE_ALREADY_EXISTS		 = 0x6A89,  //"File already exists
    SW_DF_ALREADY_EXISTS         = 0x6A8A,  //"DF name already exists"

	SW_INCORRECT_PARAMETERS		 = 0x6B00,  //"Wrong parameter(s) P1-P2"
    SW_INS_NOT_SUPPORTED		 = 0x6D00,  //"INStruction not programmed or invalid"
	SW_CLA_NOT_SUPPORTED		 = 0x6E00,  //"CLAss not supported"
	SW_CARD_CMD_FAILED			 = 0x6F00,  //"Command not supported and no precise diagnosis given"

    SW_SUCCESS                   = 0x9000,  //"Command normally completed"
};
      
/* 7816 instructions */
enum iso7816_instructs {
    INS_ERASE_BIN      = 0x0E,
    INS_VERIFY         = 0x20,
    INS_MANAGE_CHANNEL = 0x70,
    INS_EXTERNAL_AUTENTICATE = 0x82,
    INS_GET_CHALLENGE  = 0x84,
    INS_INTERNAL_AUTENTICATE = 0x88,
    INS_SELECT_FILE    = 0xA4,
    INS_READ_BIN       = 0xB0,
    INS_READ_REC       = 0xB2,
    INS_GET_RESPONSE   = 0xC0,
    INS_ENVELOPE       = 0xC2,
    INS_GET_DATA       = 0xCA,
    INS_WRITE_BIN      = 0xD0,
    INS_WRITE_REC      = 0xD2,
    INS_UPDATE_BIN     = 0xD6,
    INS_PUT_DATA       = 0xDA,
    INS_UPDATE_REC     = 0xDC,
    INS_APPEND_REC     = 0xE2,

};


#endif//#define _ISO7816_H


#ifndef _FM_PROTO_CT_H
#define _FM_PROTO_CT_H

#include "fm_proto_types.h"
#include "fm_proto_t0.h"
#include "fm_proto_t1.h"
#include "fm_proto_config.h"

typedef u16 (*ct_dispatch_t) (void *apdu);   

enum CT_Type
{
    CT_PROTOCOL_T0 = 0,   /* Protocol type T=0 */
    CT_PROTOCOL_T1 = 1,   /* Protocol type T=1 */
    CT_PROTOCOL_T2 = 2,   /* Protocol type T=2 */
    CT_PROTOCOL_T3 = 3,   /* Protocol type T=3 */
    CT_PROTOCOL_T14 = 14  /* Protocol type T=14 */
};

typedef struct _ct_ctx
{
    u8 type;           /** which protocol is used, see CT_Type */
    
    u8 activated;      /** the IFD has activated the card */
    u8 exp_pps;        /** expect pps command */
    u8 t1_support;     /** t=1 protocol is supported */  
    u8 wtx_trigger;    /** trigger wtx immediately */

    u8 fidi;           /** include fi and di */      
       
    t0_ctx_t t0_ctx;
    t1_ctx_t t1_ctx;
    
    u8 c_apdu_head[5];
    u16 apdulen;
    
    u16 resplen;       /** the length of the r-apdu */
    u8 *resp;          /** pointer to the r-apdu buf */
    
    /* the following members should be initialized by user */
    cfg_ct_t *ct_cfg;
    
    u8  *rx_buf_p;
    u32 *rx_len_p;
    
    u8  *tx_buf_p;
    u16 tx_buf_size;
    
    const io_ops_t *ct_ops;
    ct_dispatch_t apdu_dispatch;

} ct_ctx_t;


//////////////////////////////////////////////////////////////////////
// T1


//////////////////////////////////////////////////////////////////////



#endif//_FM_PROTO_CT_H

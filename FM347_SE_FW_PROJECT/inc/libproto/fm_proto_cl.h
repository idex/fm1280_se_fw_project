#ifndef _FM_PROTO_CL_H
#define _FM_PROTO_CL_H

#include "fm_proto_types.h"
#include "fm_proto_tcl.h"
#include "fm_proto_config.h"

typedef u16 (*cl_dispatch_t) (void *apdu); 

typedef struct _cl_ctx_s
{
    volatile u8 activated;     /** activated by RATS */
    volatile u8 wtx_trigger;   /** trigger wtx immediately */
        
    tcl_ctx_t tcl_ctx;
    
    u16 resplen;       /** the length of the r-apdu */
    u8 *resp;          /** pointer to the r-apdu buf */
    
    /* the following members should be initialized by user */
    cfg_cl_t *cl_cfg;

    u8  *tx_buf_p;
    u16 tx_buf_size;
    
    u8  *rx_buf_p;
    u32 *rx_len_p;
    
    const io_ops_t *cl_ops;
    cl_dispatch_t apdu_dispatch;
    
} cl_ctx_t;

#endif//_FM_PROTO_CL_H

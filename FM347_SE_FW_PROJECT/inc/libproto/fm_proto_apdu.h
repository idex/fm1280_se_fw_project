#ifndef _FM_PROTO_APDU_H
#define _FM_PROTO_APDU_H

#include "fm_proto_types.h"
#include "fm_proto_t0.h"
#include "fm_proto_tcl.h"
#include "fm_proto_ct.h"

typedef struct _apdu_s
{
    u16 apdulen;            /* ISO7816_TPDU_COMMAND_HEADER_LEN + bodylen */
    u16 apdu_max_size;      /* max size of apdu buffer */
    
    u8 cla, ins, p1, p2, p3;/* CLA, INS, P1 and P2 bytes */
    u16 lc, le;             /* Lc and Le bytes */
    u8 *body;               /* C-APDU data */
    u16 bodylen;            /* length of data in C-APDU */
    
    u8 *resp;               /* R-APDU data buffer */
    u16 resplen;            /* length of data returned in R-APDU, 
                             * if T0, used to GET_RESPONSE */
    
    u16 sw;
} apdu_t;

enum {
    SW_COMMAND_BODY_EXPECTED = 0x0000,
    SW_USER_DEFINED_ERROR    = 0x9E00,
};

//----------------------------------------------------------------------------

#define JE(x, y, sw)    do { \
                            if ((x) == (y)) \
                                return (sw); \
                        } while (0);
#define JNE(x, y, sw)   do { \
                            if ((x) != (y)) \
                                return (sw); \
                        } while (0);
#define JNE2(x, v1, v2, sw) \
                        do { \
                            if (((x) != (v1)) && ((x) != (v2))) \
                                return (sw);\
                        } while (0);
#define JL(x, y, sw)    do { \
                            if ((x) < (y)) \
                                return (sw); \
                        } while (0);
#define JG(x, y, sw)    do { \
                            if ((x) > (y)) \
                                return (sw); \
                        } while (0);
#define JR(x, v1, v2, sw) \
                        do { \
                            if (((v1) <= (x)) && ((x) <= (v2))) \
                                return (sw);\
                        } while (0);

#define SW_ASSERT(a)    do { \
                            if (SW_SUCCESS != (a)) \
                                return (a); \
                        } while (0);
#define Le_ASSERT(io_is_ct, la) \
                        do { \
                            if (apdu->p3 != (la)) \
                            { \
                                if ((! io_is_ct) && (! apdu->p3)) \
                                    break; \
                                return MAKEWORD(0x6C, (la)); \
                            } \
                        } while (0);
                        
#define RETURN_61XX(a)  do { \
                           if ((a) >= ISO7816_SHORT_R_APDU_MAX_DATA_LEN) \
                                return MAKEWORD(0x61, 0); \
                           else \
                                return MAKEWORD(0x61, (u8)(a)); \
                        } while (0);
/*
#define EXPECT_BODY()   do { \
                            if (IO_IS_CT()) \
                            { \
                                if (!apdu_expect_body(&g_ct_ctx, apdu)) \
                                    return SW_COMMAND_BODY_EXPECTED; \
                            } \
                        } while (0);
*/


/* apdu_dispatch() function must be implemented by user code */                        
u16 apdu_dispatch (apdu_t *apdu); 

#endif//_FM_PROTO_APDU_H

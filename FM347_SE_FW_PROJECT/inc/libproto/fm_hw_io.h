#ifndef _FM_HW_IO_H
#define _FM_HW_IO_H

#include "fm_proto_types.h"


//NOTE: 
//  为了节省代码，这里的enum和实际的cla_transceive_mode/ct_transceive_mode一致
enum {
    CMD_NONE = 0,
    CMD_RECEIVE = 1,
    CMD_SEND = 2,
    CMD_TRANSCEIVE = 3,  
    CMD_DESELECT = 6,
    
    CMD_WTX_START,
    CMD_WTX_STOP,
    
    CMD_DIRECT_PROCESS = 0x80,
};

#define CMD_SW_OFFSET          (16)
#define CMD_SW(cmd)            (u16)(((cmd) >> CMD_SW_OFFSET) & 0x0000FFFF)
#define CMD_CLEAR_SW(cmd)      ((cmd) &= 0x0000FFFF)
#define CMD_APPEND_SW(sw)      ((sw) << CMD_SW_OFFSET)

#define CMD_INS(cmd)           ((cmd) & 0x0000FFFF)

enum _io_ctrl_e
{
    IO_CTRL_OPEN,
    IO_CTRL_CLOSE,
    IO_CTRL_RESET,
};

typedef struct _io_ops_s
{  
    void (*ioctrl) (u8 param);
    u8   (*transceive) (u32 cmd, const u8 *tx_buf, u16 tx_len, \
                       u8 *rx_buf, u32 *rx_len);
    void (*wtx) (u8 cmd, u8 param1, u8 param2);
    void (*setbaud) (u8 dsi_fi, u8 dri_di);
} io_ops_t;

#endif//_FM_HW_IO_H

#ifndef _FM_PROTO_TYPES_H_
#define _FM_PROTO_TYPES_H_

#define code const

#ifndef u8
typedef unsigned char   u8;
#endif
#ifndef vu8
typedef u8 volatile     vu8;
#endif
#ifndef u16
typedef unsigned short  u16;
#endif
#ifndef vu16
typedef u16 volatile    vu16;
#endif
#ifndef u32
typedef unsigned long   u32;
#endif
#ifndef vu32
typedef u32 volatile    vu32;
#endif
#ifndef s8
typedef signed char     s8;
#endif
#ifndef vs8
typedef s8 volatile     vs8;
#endif
#ifndef s16
typedef signed short    s16;
#endif
#ifndef vs16
typedef s16 volatile    vs16;
#endif
#ifndef s32
typedef signed long     s32;
#endif
#ifndef vs32
typedef s32 volatile    vs32;
#endif

#define true    1
#define false   0
    
#define TRUE    0xBA
#define FALSE   0x45

#define POINTER_SIZE   4

#ifndef NULL
#define NULL            ((void *)0)
#endif

#endif//_FM_PROTO_TYPES_H_

#ifndef _FM_PROTO_T1_H
#define _FM_PROTO_T1_H

#include "fm_proto_types.h"
#include "fm_hw_io.h"
#include "fm_proto_config.h"

#define MIN_T1_BLOCK_LEN    3 /* NAD+PCB+LEN*/ //+EDC(2 CRC) 

enum t1_nad_bits {
    T1_NAD_SAD      = 0x07,  /* b1-3 */
    T1_NAD_DAD      = 0x70,  /* b5-7 */
};

#define T1_PCB_NONE    (0xFF)

#define T1_NAD_OFFSET  (0)
#define T1_PCB_OFFSET  (1)
#define T1_LEN_OFFSET  (2)
#define T1_INF_OFFSET  (3)

enum t1_pcb_bits {  
    T1_PCB_X_BLOCK_MASK  = 0xC0,
    
    T1_PCB_I_BLOCK  = 0x00,
    T1_PCB_I_BLOCK_CHAIN = 0x01,
    T1_PCB_R_BLOCK  = 0x80,
    T1_PCB_S_BLOCK  = 0xC0,
};

enum t1_i_block {
    T1_I_NS         = 0x40,  //send-sequence number
    T1_I_M_BIT      = 0x20,  //more-data bit
};

enum t1_r_block {
    T1_R_NS         = 0x10,  //send-sequence number
    
    T1_R_ACK        = 0x00,  //0-N(R)-0000, an error-free acknowledgement 
    T1_R_ERR_CHK    = 0x01,  //0-N(R)-0001, a redundancy code error or a character parity error
    T1_R_ERR_OTHER  = 0x02,  //0-N(R)-0010, other errors
    // Any other values is reserved for future use.
};

enum t1_s_block {
    T1_S_REQ_MASK      = 0x3F, //
    T1_S_REP_BIT       = 0x20,
    
    T1_S_RESYNCH_REQ   = 0x00,
    T1_S_RESYNCH_RES   = 0x20,
    T1_S_IFS_REQ       = 0x01,
    T1_S_IFS_RES       = 0x21,
    T1_S_ABORT_REQ     = 0x02,
    T1_S_ABORT_RES     = 0x22,
    T1_S_WTX_REQ       = 0x03,
    T1_S_WTX_RES       = 0x23,
};

#define T1_IFSC_DEFAULT  (32)
#define T1_IFSD_DEFAULT  (32)

#define T1_IFSC_MAX      (0xFE)

#define T1_CWI_DEFUALT   (13)
#define T1_BWI_DEFUALT   (4)

#define T1_FI_DEFAULT    (1)

typedef struct _t1_ctx
{   
    volatile u8 tx_ready;
    volatile u8 rx_ready;
    volatile u8 chaining; 
    volatile u8 wtxing;
             u8 exp_nad;
             u8 resent;
                
    volatile u8 s_request;  /* S(... request) is sent, waiting S(... response) */
    u8 s_type;  /* type of the sent s-block */
    
    //u8 snd_pcb; /* last sent PCB */
    u8 rcv_pcb; /* last received PCB */
    u8 nad;     /* node address, include SAD and DAD */
    u8 sns;     /* PCB.b7: send-sequence numbers of I-blocks for pcd */
    u8 dns;     /* PCB.b7: send-sequence numbers of I-blocks for picc */  
    
    u16 ifsd;   /* max. size of info field that recieved by pcd */
    u16 ifsc;   /* TA1, max. size of info field that sent by picc */
    u8 epilogue_len;
    
    u8 cwi;     /* TB1[4-1], same direction, */
    u8 bwi;     /* TB1[8-5], opposite direction, default value is BWI=4, */
    u8 wtxm;
    
    u8 *apdu;   /* store the complete c-apdu or r-apdu*/
    u16 apdu_len;
    u16 apdu_max_size;
    
    u8  *rx_buf_p;
    u32 *rx_len_p;
    
    u8 *tx_buf_p;
    u16 tx_len;
    u16 tx_off;
    
    u32 io_cmd;
    const io_ops_t *t1_ops;

} t1_ctx_t;

#if _configCT_T1_SURPPORTED_

u8   t1_tx_start (t1_ctx_t *t1_ctx);
void t1_send (t1_ctx_t *t1_ctx, u8 mode, u16 offset, u16 count);                  
void t1_snd_sblock (t1_ctx_t *t1_ctx);
u8   t1_rcv_xblock (t1_ctx_t *t1_ctx, u8 pcb);

void t1_reset (t1_ctx_t *t1_ctx);
void t1_init (t1_ctx_t *t1_ctx);

#else

#define t1_tx_start(a)
#define t1_send(a,b,c,d) 
#define t1_snd_sblock(a)
#define t1_rcv_xblock(a,b)
#define t1_init(a)

#endif//_configCT_T1_SURPPORTED_

#endif//_FM_PROTO_T1_H

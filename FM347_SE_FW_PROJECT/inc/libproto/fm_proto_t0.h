#ifndef _FM_PROTO_T0_H
#define _FM_PROTO_T0_H

#include "fm_proto_types.h"
#include "fm_hw_io.h"

#define T0_FI_DEFAULT               (1)
#define T0_WI_DEFAULT               (10)
#define T0_R_APDU_DATA_OFFSET       (1/*SW1(INS)*/ + 1/*SW2*/)
#define T0_R_APDU_BUF_BASE(ctx)     (&((ctx).tx_buf_p[T0_R_APDU_DATA_OFFSET]))

typedef struct _t0_ctx
{   
    u8 exp_hdr;   /** expect the header of five data */
    volatile u8 rx_ready;
    volatile u8 tx_ready;
    
    u16 exp_rxlen;
    
    u8 wi;  /** wi used for wtx */
    u8 fi;  /** fi used for wtx */
    
    u8  *rx_buf_p;
    u32 *rx_len_p;
    
    u8 *tx_buf_p;
    u16 tx_len;
    
    u32 io_cmd;

    const io_ops_t *t0_ops;

} t0_ctx_t;


#endif//_FM_PROTO_T0_H

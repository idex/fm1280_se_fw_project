#ifndef _FM_PROTO_TCL_H
#define _FM_PROTO_TCL_H

#include "fm_proto_types.h"
#include "fm_hw_io.h"

enum tcl_transport_rate {
    TCL_RATE_106    = 0x00,
    TCL_RATE_212    = 0x01,
    TCL_RATE_424    = 0x02,
    TCL_RATE_848    = 0x03,
};

enum tcl_flags {
    TCL_F_NAD_SUPPORTED = 0x01,
    TCL_F_CID_SUPPORTED = 0x02,

    TCL_F_NAD_USED   = 0x10,
    TCL_F_CID_USED   = 0x20,
    TCL_F_CHAIN_USED = 0x40,   
};

enum tcl_cmd {
    TCL_CMD_IBLOCK    = 0x00,
    
    TCL_CMD_RATS      = 0xE0,
    TCL_CMD_PPS       = 0xD0,

#define TCL_CMD_MASK    0xF2   //poll mode     
    TCL_CMD_ACK       = 0xA2,
    TCL_CMD_NAK       = 0xB2,
    TCL_CMD_WTX       = 0xF2,
    TCL_CMD_DESELECT  = 0xC2,
    TCL_CMD_PARAMETERS= 0xF0,
    
    TCL_CMD_UNKNOWN   = 0xFF,
};

enum tcl_ats_bits {
    //T0
    TCL_ATS_T0_TA            = 0x10,
    TCL_ATS_T0_TB            = 0x20,
    TCL_ATS_T0_TC            = 0x40,
    TCL_ATS_T0_FSCI          = 0x0F,
    
    //TC1
    TCL_ATS_TC_CID_SUPPORTED = 0x02,
    TCL_ATS_TC_NAD_SUPPORTED = 0x01,
};

#define TCL_FWI_ACTIVATION      4
#define TCL_FWI_DEACTIVATION    4
#define TCL_FWI_MAX             14
#define ISO_14443_4_FWT_MAX     4949   //ms               

enum tcl_pcb_bits {
    TCL_PCB_BLOCKNO       = 0x01,
    TCL_PCB_RFU_1         = 0x02,
    TCL_PCB_CHAIN         = 0x10,
    TCL_PCB_CID_FOLLOWING = 0x08,
    TCL_PCB_NAD_FOLLOWING = 0x04,
    
    TCL_PCB_X_BLOCK       = 0xC0,
    TCL_PCB_I_BLOCK       = 0x00,       
    TCL_PCB_R_BLOCK       = 0x80,
    TCL_PCB_S_BLOCK       = 0xC0,
};

enum tcl_pps_bits {
    TCL_PPS_PPS1          = 0x10,
    TCL_PPS_PPS1_DRI      = 0x03,  //从PCD到PICC的除数整数(D)
    TCL_PPS_PPS1_DSI      = 0x0C,  //从PICC到PCD的除数整数(D)
};


#define TCL_FSCI_DEFAULT    (0x08)  //{ FSDI = 8 (256 bytes) }
#define TCL_FWI_DEFAULT     (0x07)
#define TCL_WTXM_DEFAULT    (0x01)

#define TCL_MIN_BLOCK_LEN   (3) //PCB + CRC_1 + CRC_2

#define TCL_BAUD_DSI_MASK   (4)
#define TCL_BAUD_DRI_MASK   (4)
#define TCL_BAUD_DSI_OFFSET (4)

typedef struct _tcl_ctx_s {
	 /* activated when received RATS,
	  * deactivated when received deselect
	  */
	volatile u8 activated;

    volatile u8 tx_ready;
    volatile u8 rx_ready;
    volatile u8 wtxing;      /* wtx is processing */
    u8 exp_pps;  /* expect pps after rats */
    //u8 exp_rats; /* expect first command rats */
    
    u16 fsd;    /* max. frame size for reader */
    u16 fsc;    /* max. frame size for icc */

    u8 fwi;     /* frame waiting time integer (in usec) */
    u8 wtxm;
    
    u8 baud;    /* baudrate */

    u8 pcb;     /* last received PCB */
    u8 cid;     /* Card ID */
    //u8 nad;   /* Node Address */
    u8 bn;      /* Block Number */  

    u8 *apdu;   /* store the complete c-apdu or r-apdu*/
    u16 apdu_len;
    u16 apdu_max_size;
    
    u8 flags;   /* flags: see enum tcl_flags */
    
    u8 *rx_buf_p;
    u32 *rx_len_p;
    
    u8 *tx_buf_p;
    u16 tx_len;
    u16 tx_off;
    
    u32 io_cmd;
    const io_ops_t *tcl_ops;
    
} tcl_ctx_t;


#endif//_FM_PROTO_TCL_H

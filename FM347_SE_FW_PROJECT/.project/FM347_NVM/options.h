#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#include "types.h"


///////////////////////////////////////////////////////////////////////////////

// <<< Use Configuration Wizard in Context Menu >>>

/*
// <o0.0> Use LibProto existed in firmware
*/
#define _configLIBPROTO_IN_FIRMWARE_   1

/*
// <o0> Max APDU Size   <1024-5120>
*/
#define _configMAX_APDU_SIZE_       2100


/*
//	<h> CL Configs
//    <e0.0> CL Init Info
//       <o1.6..7> UID len        <0=> default (4 Bytes)  <1=> 7 Bytes
//                                <2=> 10 Bytes  <3=> 4 Bytes
//       <o1.4> Change CL info in RAM
//       <o1.3> Change CL info in NVR, DON'T check it when EMVCo
//       <o1.2> Enable Initialize UID
//       <o1.1> Enable Initialize SAK
//       <o1.0> Enable Initialize ATQA
//    </e>
//    <h> Check Conditions in TCL blocks
//       <o2.7> Check Deselect=0xC2. If NOT, 0xC0 is supported.
//       <o2.2> Check b6=0 of PCB in I-Block
//       <o2.1> Check b2=1 of PCB in R-Block
//       <o2.0> Check b1=0 of PCB in S-Block (EMVCo ignore)
//    </h>
//    <h> S-PARAMETERS tags value
//       <o3> Tag80: Supported bit rates from PCD to PICC (1st byte)  <0-127>
//       <o4> Tag81: Supported bit rates from PICC to PCD (1st byte)  <0-127>
//       <o5> Tag82: Supported framing options PICC to PCD <0-3>
//    </h>
//  </h>
*/

#define _configCL_ENABLE_INIT_CFG_      0
#define _configCL_INIT_CFG_             0x97
#define _configCL_TCL_CHK_SET_          128
#define _configS_PARAMETERS_TAG80_      0x0F
#define _configS_PARAMETERS_TAG81_      0x7F
#define _configS_PARAMETERS_TAG82_      0

/*
//	<h> CT Configs
//    <h> T0 Init Info
//       <o0.24..25> Parity       <0=> Even  <1=> Odd
//                                <2=> Always 1  <3=> RFU
//       <o0.22> Enable Repetition When Rx Error Signal
//       <o0.21> Enable Repetition When Tx Error Signal
//       <o0.20> LSB or MSB first <0=> LSB first  <1=> MSB first
//       <o0.16..18> Max. Number of Repetition When Error Signal <0-7>
//       <o0.12> Guard Timer ETU  <0=> 2 etus     <1=> 1 etu
//    </h>
//    <o1.0..3> Extra ETU Count - Receive To Transmit <0-15>
//    <o2.0..3> Character Timing - Same Direction <0-15>
//	  <o3.0> Asynchronous Receive Body When T=0
//  </h>
*/

#define _configCT_T0_INIT_CFG_        0x00650000
#define _configCT_TX_ETU_CNT_         5
#define _configCT_TX_EGT_NUM_         1
#define _configCT_T0_ASYNC_           0



//<<< end of configuration section >>>


#endif//_OPTIONS_H_

